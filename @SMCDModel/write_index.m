function write_index(this, entity, dir_export, perm)
    % Write the index of the specified entity to textfile.
    %
    % Parameters:
    %   dir_export : string : path to export directory.
    %   perm : integer : permission level.
    
    objs = this.get_objs(entity);
    
    % Write text.
    file_text = cellfun(@(x) sprintf('%s %s %d', x.musc, x.seg, x.fidx), objs, 'UniformOutput', false);
    file_text = strjoin(file_text, '\n');
    
    % Write file.
    file_path = fullfile(dir_export, sprintf('%s_index.txt', entity));
    if perm || ~ exist(file_path, 'file')
        fileID = fopen(file_path, 'w');
        fprintf(fileID, file_text);
        fclose(fileID);
    end
    
end
