function obj = get_obj(this, entity, varargin)
    % Return the specified entity instance.
    %
    % Usage:
    %   obj = smcd.get_obj(entity, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   obj = smcd.get_obj(entity, 'gidx', gidx)
    %
    % Parameters:
    %   entity : string :
    %       Identifier of neural entity.
    %
    %   musc : string :
    %       Name of muscle.
    %
    %   seg : string :
    %       Name of segment.
    %
    %   fidx : double :
    %       Family index of entity instance.
    %
    %   gidx : double :
    %       Global index of entity instance.
    %
    % Return:
    %   obj: an entity instance.
    %
    % Description: see method get_objs.
    objs = this.get_objs(entity, varargin{:});
    obj = objs{1};
end
