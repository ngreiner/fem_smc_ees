function outbool = is_interp(this, entity, AS_idx, varargin)
    % Return a logical indicating whether the specified entity is
    % well-interpolated.
    %
    % Usage:
    %   outbool = smcd.is_interp(entity, AS_idx)
    %   outbool = smcd.is_interp(entity, AS_idx, 'musc', musc)
    %   outbool = smcd.is_interp(entity, AS_idx, 'seg', seg)
    %   outbool = smcd.is_interp(entity, AS_idx, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   outbool = smcd.is_interp(entity, AS_idx, 'gidx', gidx)
    %
    % Parameters:
    %   entity : string :
    %       Identifier of neural entity.
    %   
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   musc : str / N*1 cell :
    %       Name(s) of muscle(s).
    %
    %   seg : str / N*1 cell :
    %       Name(s) of segment(s).
    %
    %   fidx : N*1 double :
    %       Family index(es) of entity instance(s).
    %
    %   gidx : N*1 double :
    %       Global index(es) of entity instance(s).
    %
    % Return:
    %   outbool: logical.
    %
    % Description:
    %   See homonymous method of specified entity class.
    objs = this.get_objs(entity, varargin{:});
    outbool = all(cellfun(@(x) x.is_interp(AS_idx), objs));
end
