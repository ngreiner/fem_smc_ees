function IaOD3 = generate_IaOD3(this, varargin)
    % Generate and return a IaFiber object.
    %
    % Usage:
    %   IaOD3 = generate_IaOD3(this, 'gidx', gidx)
    %   IaOD3 = generate_IaOD3(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   IaOD3 = generate_IaOD3(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of IaOD3.
    %
    %   musc : string :
    %       Muscle of IaOD3.
    %
    %   seg : string :
    %       Segment of IaOD3.
    %       
    %   fidx : integer :
    %       Family index of IaOD3.
    %
    % Return: 
    %   IaOD3 : IaFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('IaOD3', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('IaOD3', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('IaOD3', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_IaOD3 (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for Ia.
    rng(Utils.gen_seed(this.name, 'IaOD3', gidx));
    
    % Generate IaFiber.
    IaOD3 = IaFiberOD3();
    IaOD3.musc = musc;
    IaOD3.seg = seg;
    IaOD3.fidx = fidx;
    IaOD3.gidx = gidx;
    IaOD3.diam = lognrnd(this.morpho.IaOD3.mu, this.morpho.IaOD3.sig);
    IaOD3.model = this.morpho.IaOD3.model;
    IaOD3.assign_MNs(this);
    IaOD3.set_DRbranch(this);
    IaOD3.set_Abranch(this);
    IaOD3.set_Dbranch(this);
    IaOD3.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_IaOD3. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
