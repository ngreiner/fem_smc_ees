function DCcB = generate_DCcB(this, varargin)
    % Generate and return a DCcBFiber object.
    %
    % Usage:
    %   DCcB = generate_DCcB(this, 'gidx', gidx)
    %   DCcB = generate_DCcB(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   DCcB = generate_DCcB(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of DCcB.
    %
    %   musc : string :
    %       Muscle of DCcB.
    %
    %   seg : string :
    %       Segment of DCcB.
    %       
    %   fidx : integer :
    %       Family index of DCcB.
    %
    % Return: 
    %   DCcB : DCcBFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('DCcB', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('DCcB', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('DCcB', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_DCcB (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for DCcB.
    rng(Utils.gen_seed(this.name, 'DCcB', gidx));
    
    % Generate DCcBFiber.
    DCcB = DCcBFiber();
    DCcB.musc = musc;
    DCcB.seg = seg;
    DCcB.fidx = fidx;
    DCcB.gidx = gidx;
    DCcB.diam = lognrnd(this.morpho.DCcB.mu, this.morpho.DCcB.sig);
    DCcB.model = this.morpho.DCcB.model;
    DCcB.set_DCbranch(this);
    DCcB.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_DCcB. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
