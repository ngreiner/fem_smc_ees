function save(this, file_path)
	% Saves the SMCDModel object in a .mat file at file_path.
    %
    % Parameters:
    %   file_path : string : path to .mat file.
    %
    % Description:
    %   If file_path is not given, it is automatically derived from the
    %   SMCDModel attributes. In this case, the directory of the file is
    %   created first if it does not exist yet.
    %   In the converse case, the directory of the file should already
    %   exist.

    % Convert SMCDModel to struct to enable saving to .mat file.
    struc = this.to_struct();

    % If file_path is provided, save and return.
    if nargin == 2
        save(file_path, '-struct', 'struc');
        return
    end

    % Otherwise, derive file_path from SMCDModel attributes and create
    % enclosing directories if necessary.
    file_path = fullfile(this.dirs.SMCD, this.name, 'smcd.mat');
    if ~ exist(fullfile(this.dirs.SMCD, this.name), 'dir')
        if ~ exist(this.dirs.SMCD, 'dir')
            mkdir(this.dirs.SMCD)
        end
        mkdir(this.dirs.SMCD, this.name);
    end
    save(file_path, '-struct', 'struc');
    
end
