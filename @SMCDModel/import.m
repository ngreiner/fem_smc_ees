function import(this, dir_import, varargin)
	% Import a previously exported sensorimotor circuit dataset
	% and rebuild the attributes of the SMCDModel.
    %
    % Parameters:
    %   dir_import : string :
    %       Path to import directory.
    %
    %   varargin : fields can be :
    %       'Ia'/'Ias' : indicates to import IaFibers.
    %       'II'/'IIs' : indicates to import IIFibers.
    %       'DCcB'/'DCcBs' : indicates to import DCcBFibers.
    %       'CST'/'CSTs' : indicates to import CSTFibers.
    %       'ST'/'STs' : indicates to import STFibers.
    %       'V' : indicates if the electric potentials shall be loaded
    %       'morpho' : indicates if morphology shall be loaded
    %       'geom' : indicates if geometry shall be loaded
    %       'all' : equivalent of 'V' and 'morpho' and 'geom'
    %
    % Description:
    %   This method assumes that the SMCDModel has already been
    %   instantiated with a configuration file corresponding to the
    %   exported sensorimotor circuit dataset.
    %   In particular, the represented muscles and segments, and the
    %   distributions of the neural entities and their model should be
    %   appropriately defined in the attribute morpho of the SMCDModel.
    
    % Parse inputs.
    mode = [];
    import_Ias = false;
    import_IIs = false;
    import_CSTs = false;
    import_DCcBs = false;
    import_STs = false;
    for i = 1 : size(varargin, 2)
        switch varargin{i}
            case 'V'
                mode{end + 1} = 'V';
            case 'morpho'
                mode{end + 1} = 'morpho';
            case 'geom'
                mode{end + 1} = 'geom';
            case 'connec'
                mode{end + 1} = 'connec';
            case 'all'
                mode{end + 1} = 'all';
            case {'Ia', 'Ias'}
                import_Ias = true;
            case {'II', 'IIs'}
                import_IIs = true;
            case {'DCcB', 'DCcBs'}
                import_DCcBs = true;
            case {'CST', 'CSTs'}
                import_CSTs = true;
            case {'ST', 'STs'}
                import_STs = true;
        end
    end
    if isempty(mode)
        mode = {'all'};
    end
    
    % Import sensorimotor circuit dataset.
    
    % Loop over muscles.
    for i = 1 : this.compo.nmuscs
        musc = this.compo.muscles{i};
        musc_folder = fullfile(dir_import, musc);
        
        % Loop over segments.
        for j = 1 : this.compo.nsegs
            seg = this.compo.segments{j};
            seg_folder = fullfile(musc_folder, seg);
            
            % Import Ia fibers.
            if import_Ias
                idxs = this.extract_idxs(seg_folder, 'Ia');
                for k = 1 : numel(idxs)
                    folder = fullfile(seg_folder, sprintf('IaFiber%d', k));
                    % Initialize IaFiber.
                    Iaijk = IaFiber();
                    % Set identity.
                    Iaijk.musc = musc;
                    Iaijk.seg = seg;
                    Iaijk.fidx = idxs(k);
                    Iaijk.gidx = this.info_2_gidx('Ia', musc, seg, idxs(k));
                    Iaijk.model = this.morpho.Ia.model;
                    % Read morphology.
                    if any(strcmp('morpho', mode)) || any(strcmp('all', mode))
                        Iaijk.read_morpho(folder);
                    end
                    % Read geometry.
                    if any(strcmp('geom', mode)) || any(strcmp('all', mode))
                        Iaijk.read_geom(folder);
                    end
                    % Read potentials.
                    if any(strcmp('V', mode)) || any(strcmp('all', mode))
                        Iaijk.read_V(folder, this.get_nAS());
                    end
                    % Store Ia fiber in the table.
                    this.entities.Ias{Iaijk.gidx, 1} = Iaijk;
                end
            end
            
            % Import II fibers.
            if import_IIs
                idxs = this.extract_idxs(seg_folder, 'II');
                for k = 1 : numel(idxs)
                    folder = fullfile(seg_folder, sprintf('IIFiber%d', k));
                    % Initialize IIFiber.
                    IIijk = IIFiber();
                    % Set identity.
                    IIijk.musc = musc;
                    IIijk.seg = seg;
                    IIijk.fidx = idxs(k);
                    IIijk.gidx = this.info_2_gidx('II', musc, seg, idxs(k));
                    IIijk.model = this.morpho.II.model;
                    % Read morphology.
                    if any(strcmp('morpho', mode)) || any(strcmp('all', mode))
                        IIijk.read_morpho(folder);
                    end
                    % Read geometry.
                    if any(strcmp('geom', mode)) || any(strcmp('all', mode))
                        IIijk.read_geom(folder);
                    end
                    % Read potentials.
                    if any(strcmp('V', mode)) || any(strcmp('all', mode))
                        IIijk.read_V(folder, this.get_nAS());
                    end
                    % Store II fiber in the table.
                    this.entities.IIs{IIijk.gidx, 1} = IIijk;
                end
            end
            
            % Import CST fibers.
            if import_CSTs
                idxs = this.extract_idxs(seg_folder, 'CST');
                for k = 1 : numel(idxs)
                    folder = fullfile(seg_folder, sprintf('CSTFiber%d', k));
                    % Initialize CSTFiber.
                    CSTijk = CSTFiber();
                    % Set identity.
                    CSTijk.musc = musc;
                    CSTijk.seg = seg;
                    CSTijk.fidx = idxs(k);
                    CSTijk.gidx = this.info_2_gidx('CST', musc, seg, idxs(k));
                    CSTijk.model = this.morpho.CST.model;
                    % Read morphology.
                    if any(strcmp('morpho', mode)) || any(strcmp('all', mode))
                        CSTijk.read_morpho(folder);
                    end
                    % Read geometry.
                    if any(strcmp('geom', mode)) || any(strcmp('all', mode))
                        CSTijk.read_geom(folder);
                    end
                    % Read potentials.
                    if any(strcmp('V', mode)) || any(strcmp('all', mode))
                        CSTijk.read_V(folder, this.get_nAS());
                    end
                    % Store CST fiber in the table.
                    this.entities.CSTs{CSTijk.gidx, 1} = CSTijk;
                end
            end
            
            % Import DCcB fibers.
            if import_DCcBs
                idxs = this.extract_idxs(seg_folder, 'DCcB');
                for k = 1 : numel(idxs)
                    folder = fullfile(seg_folder, sprintf('DCcBFiber%d', k));
                    % Initialize DCcBFiber.
                    DCcBijk = DCcBFiber();
                    % Set identity.
                    DCcBijk.musc = musc;
                    DCcBijk.seg = seg;
                    DCcBijk.fidx = idxs(k);
                    DCcBijk.gidx = this.info_2_gidx('DCcB', musc, seg, idxs(k));
                    DCcBijk.model = this.morpho.DCcB.model;
                    % Read morphology.
                    if any(strcmp('morpho', mode)) || any(strcmp('all', mode))
                        DCcBijk.read_morpho(folder);
                    end
                    % Read geometry.
                    if any(strcmp('geom', mode)) || any(strcmp('all', mode))
                        DCcBijk.read_geom(folder);
                    end
                    % Read potentials.
                    if any(strcmp('V', mode)) || any(strcmp('all', mode))
                        DCcBijk.read_V(fullfile(folder, 'potentials'), this.get_nAS());
                    end
                    % Store DCcB fiber in the table.
                    this.entities.DCcBs{DCcBijk.gidx, 1} = DCcBijk;
                end
            end
            
            % Import ST fibers.
            if import_STs
                idxs = this.extract_idxs(seg_folder, 'ST');
                for k = 1 : numel(idxs)
                    folder = fullfile(seg_folder, sprintf('STFiber%d', k));
                    % Initialize CSTFiber.
                    STijk = STFiber();
                    % Set identity.
                    STijk.musc = musc;
                    STijk.seg = seg;
                    STijk.fidx = idxs(k);
                    STijk.gidx = this.info_2_gidx('ST', musc, seg, idxs(k));
                    STijk.model = this.morpho.ST.model;
                    % Read morphology.
                    if any(strcmp('morpho', mode)) || any(strcmp('all', mode))
                        STijk.read_morpho(folder);
                    end
                    % Read geometry.
                    if any(strcmp('geom', mode)) || any(strcmp('all', mode))
                        STijk.read_geom(folder);
                    end
                    % Read potentials.
                    if any(strcmp('V', mode)) || any(strcmp('all', mode))
                        STijk.read_V(folder, this.get_nAS());
                    end
                    % Store ST fiber in the table.
                    this.entities.STs{STijk.gidx, 1} = STijk;
                end
            end
            
        end
        
    end
    
end
