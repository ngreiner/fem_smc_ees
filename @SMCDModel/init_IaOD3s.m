function init_IaOD3s(this)
    % Initialize the attribute IaOD3s.
    %
    % Usage:
    %   smcd.init_IaOD3s()
    %
    % Description:
    %   Generates IaFiberOD3 objects according to the SMCDModel composition
    %   and assign them to the attribute entities.IaOD3s.
    %   See method generate_IaOD3. 
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_IaOD3s   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;
    
    % Initialize IaOD3s.
    this.entities.IaOD3s = cell(this.compo.nIaOD3s, 1);
    
    % Set IaOD3s.
    for i = 1 : this.compo.nIaOD3s
        this.entities.IaOD3s{i} = this.generate_IaOD3('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_IaOD3s. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end