function [musc, seg, fidx] = gidx_2_info(this, entity, gidx)
    % Return family info (musc, seg, fidx) of the entity instance #gidx.
    %
    % Usage:
    %   [musc, seg, fidx] = smcd.gidx_2_info(entity, gidx)
    %
    % Parameters: 
    %   entity : string : entity identifier ('Ia', 'II', 'MN', ...).
    %   gidx : integer : global index of entity instance.
    %
    % Return:
    %   musc : string : muscle of entity instance #gidx.
    %   seg : string : segment of entity instance #gidx.
    %   fidx : integer : family index of entity instance #gidx.
    
    % Recover appropriate entity distribution.
    distrib = this.get_distrib(entity);
    
    % Initialize counter.
    cnt = 0;
    
    % Determine muscle index.
    i = 1;
    while cnt + sum(distrib(i, :)) < gidx
        cnt = cnt + sum(distrib(i, :));
        i = i + 1;
    end
    
    % Determine segment index.
    j = 1;
    while cnt + distrib(i, j) < gidx
        cnt = cnt + distrib(i, j);
        j = j + 1;
    end
    
    % Determine family index.
    k = gidx - cnt;
    
    % Return.
    musc = this.compo.muscles{i};
    seg = this.compo.segments{j};
    fidx = k;
    
end
