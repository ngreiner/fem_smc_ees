function init_DCcAs(this)
    % Initialize the DCcAFibers of the SMCD.
    %
    % Description: see method generateDCcAs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_DCcAs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize DCcAs array.
    this.entities.DCcAs = cell(this.compo.nDCcAs, 1);
    
    % Set DCcBs.
    for i = 1 : this.compo.nDCcAs
        this.entities.DCcAs{i} = this.generate_DCcAs('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_DCcAs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end