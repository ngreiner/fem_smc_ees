function set_morpho(this)
    % Set the lognormal distribution parameters for the fiber diameters.
    %
    % Description:
    %   This method that the SMCDModel has been configured with a
    %   configuration file. Such a file should specify the mean and
    %   standard deviations of the fiber diameter distributions for various
    %   class of fibers (Ia, II, DCcB, CST, ...).
    %   These distributions are assumed to be lognormal distributions
    %   (https://en.wikipedia.org/wiki/Log-normal_distribution).
    %   This method computes and stores the parameters mu and sig of these
    %   lognormal distributions.
    
    % Ia-fibers.
    try
        m = this.morpho.Ia.mean_diam;
        s = this.morpho.Ia.sd_diam;
        this.morpho.Ia.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.Ia.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.Ia.mean_diam_cols;
        s = this.morpho.Ia.sd_diam_cols;
        this.morpho.Ia.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.Ia.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for Ia-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % II-fibers.
    try
        m = this.morpho.II.mean_diam;
        s = this.morpho.II.sd_diam;
        this.morpho.II.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.II.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.II.mean_diam_cols;
        s = this.morpho.II.sd_diam_cols;
        this.morpho.II.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.II.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for II-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % DCcB-fibers.
    try
        m = this.morpho.DCcB.mean_diam;
        s = this.morpho.DCcB.sd_diam;
        this.morpho.DCcB.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.DCcB.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.DCcB.mean_diam_cols;
        s = this.morpho.DCcB.sd_diam_cols;
        this.morpho.DCcB.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.DCcB.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for DCcB-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % DRcA-fibers.
    try
        m = this.morpho.DRcA.mean_diam;
        s = this.morpho.DRcA.sd_diam;
        this.morpho.DRcA.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.DRcA.sig = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for DRcA-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % CST-fibers.
    try
        m = this.morpho.CST.mean_diam;
        s = this.morpho.CST.sd_diam;
        this.morpho.CST.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.CST.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.CST.mean_diam_cols;
        s = this.morpho.CST.sd_diam_cols;
        this.morpho.CST.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.CST.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for CST-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % ST-fibers.
    try
        m = this.morpho.ST.mean_diam;
        s = this.morpho.ST.sd_diam;
        this.morpho.ST.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.ST.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.ST.mean_diam_cols;
        s = this.morpho.ST.sd_diam_cols;
        this.morpho.ST.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.ST.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for ST-fibers', ...
                 'diameters.\n\n']) 
    end
    
    % IaOD3-fibers.
    try
        m = this.morpho.IaOD3.mean_diam;
        s = this.morpho.IaOD3.sd_diam;
        this.morpho.IaOD3.mu = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.IaOD3.sig = sqrt(log(1 + s^2 / m^2));

        m = this.morpho.IaOD3.mean_diam_cols;
        s = this.morpho.IaOD3.sd_diam_cols;
        this.morpho.IaOD3.mu_cols = log(m^2 / sqrt(s^2 + m^2));
        this.morpho.IaOD3.sig_cols = sqrt(log(1 + s^2 / m^2));
    catch
        fprintf(['Couldn''t set lognormal parameters for Ia-fibers', ...
                 'diameters.\n\n']) 
    end
    
end
