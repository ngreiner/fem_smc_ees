function write_connec(this, entity, dir_export, perm)
    % Write the connectivity table of the specified entity to textfile.
    %
    % Parameters:
    %   dir_export : string : path to export directory.
    %   perm : integer : writing permission level.
    
    objs = this.get_objs(entity);
    
    % Write text.
    file_text = '';
    for i = 1 : size(objs, 1)
        for j = 1 : size(objs{i}.MNfamilies, 1)
            for k = 1 : size(objs{i}.MNfamilies{j}, 1)
                idx_MN = objs{i}.MNfamilies{j}(k);
                file_text = [file_text sprintf('%d %d %d\n', i, idx_MN, j)];
            end
        end
    end
    
    % Write file.
    file_path = fullfile(dir_export, sprintf('%s_connec.txt', entity));
    if perm || ~ exist(file_path, 'file')
        fileID = fopen(file_path, 'w');
        fprintf(fileID, file_text);
        fclose(fileID);
    end
    
end
