function load(this, file_path)
	% Loads the SMCDModel from the .mat file at file_path.
    %
    % Parameters:
    %   file_path : string : path to .mat file.
    %
    % Description:
    %   If file_path is not given, it is automatically derived from the
    %   SMCDModel attributes.

    if nargin == 1
        file_path = fullfile(this.dirs.SMCD, this.name, 'smcd.mat');
    end

    struc = load(file_path);
    this.from_struct(struc);
    
end
