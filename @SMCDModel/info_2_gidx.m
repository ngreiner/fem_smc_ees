function gidx = info_2_gidx(this, entity, musc, seg, fidx)
    % Return the global index of the entity instance described by
    % (musc, seg, fidx).
    %
    % Usage:
    %   gidx = smcd.info_2_gidx(entity, musc, seg, fidx)
    %
    % Parameters: 
    %   entity : string :
    %       Entity identifier ('Ia', 'II', 'MN', ...).
    %
    %   musc : string :
    %       Muscle of entity instance.
    %
    %   seg : string :
    %       Segment of entity instance.
    %
    %   fidx : integer :
    %       Family index of entity instance.
    %
    % Return:
    %   gidx : integer : global index of entity instance.
    
    % Recover appropriate entity distribution.
    distrib = this.get_distrib(entity);
    
    % Retrieve global index of entity instance.
    i = find(strcmp(musc, this.compo.muscles));
    j = find(strcmp(seg, this.compo.segments));
    gidx = sum(sum(distrib(1 : i - 1, :))) ...
         + sum(distrib(i, 1 : j - 1)) ...
         + fidx;
     
end