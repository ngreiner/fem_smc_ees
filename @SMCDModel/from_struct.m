function from_struct(this, struc)
	% Set the SMCDModel from the input struct.
    %
    % Usage:
    %   smcd.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with same fields as the SMCDModel class.
    this.name = struc.name;
    this.fem = FEMModel('topdir', this.dirs.top);
    try
        this.fem.from_struct(struc.fem);
    catch ME
        if ~ strcmp(ME.identifier, 'MATLAB:nonExistentField')
            throw(ME)
        end
    end
    this.compo = struc.compo;
    this.connec = struc.connec;
    this.morpho = struc.morpho;
    this.params = struc.params;
    entities = fieldnames(struc.entities);
    for i = 1 : size(entities, 1)
        switch entities{i}
            case 'MNs'
                this.entities.MNs = cellfun(@(x) Motoneuron(), struc.entities.MNs, 'UniformOutput', false);
            case 'Ias'
                this.entities.Ias = cellfun(@(x) IaFiber(), struc.entities.Ias, 'UniformOutput', false);
            case 'IIs'
                this.entities.IIs = cellfun(@(x) IIFiber(), struc.entities.IIs, 'UniformOutput', false);
            case 'DRcAs'
                this.entities.DRcAs = cellfun(@(x) DRcAFiber(), struc.entities.DRcAs, 'UniformOutput', false);
            case 'DRcBs'
                this.entities.DRcBs = cellfun(@(x) DRcBFiber(), struc.entities.DRcBs, 'UniformOutput', false);
            case 'DCcAs'
                this.entities.DCcAs = cellfun(@(x) DCcAFiber(), struc.entities.DCcAs, 'UniformOutput', false);
            case 'DCcBs'
                this.entities.DCcBs = cellfun(@(x) DCcBFiber(), struc.entities.DCcBs, 'UniformOutput', false);
            case 'CSTs'
                this.entities.CSTs = cellfun(@(x) CSTFiber(), struc.entities.CSTs, 'UniformOutput', false);
            case 'STs'
                this.entities.STs = cellfun(@(x) STFiber(), struc.entities.STs, 'UniformOutput', false);
            case 'IaOD3s'
                this.entities.IaOD3s = cellfun(@(x) IaFiberOD3(), struc.entities.IaOD3s, 'UniformOutput', false);
        end
        cellfun(@(x, y) x.from_struct(y), this.entities.(entities{i}), struc.entities.(entities{i}), 'UniformOutput', false);
    end
end
