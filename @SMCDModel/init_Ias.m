function init_Ias(this)
    % Initialize the attribute Ias.
    %
    % Usage:
    %   smcd.init_Ias()
    %
    % Description:
    %   Generates IaFiber objects according to the SMCDModel composition
    %   and assign them to the attribute Ias. See method generate_Ia. 
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_Ias   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;
    
    % Initialize Ias.
    this.entities.Ias = cell(this.compo.nIas, 1);
    
    % Set Ias.
    for i = 1 : this.compo.nIas
        this.entities.Ias{i} = this.generate_Ia('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_Ias. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end