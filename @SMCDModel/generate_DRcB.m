function DRcB = generate_DRcB(this, varargin)
    % Generate and return a DRcBFiber object.
    %
    % Usage:
    %   DRcB = generate_DRcB(this, 'gidx', gidx)
    %   DRcB = generate_DRcB(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   DRcB = generate_DRcB(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of DRcB.
    %
    %   musc : string :
    %       Muscle of DRcB.
    %
    %   seg : string :
    %       Segment of DRcB.
    %       
    %   fidx : integer :
    %       Family index of DRcB.
    %
    % Return: 
    %   DRcB : DRcBFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('DRcB', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('DRcB', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('DRcB', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_DRcB (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for DRcB.
    rng(Utils.gen_seed(this.name, 'DRcB', gidx));
    
    % Generate DRcBFiber.
    DRcB = DRcBFiber();
    DRcB.musc = musc;
    DRcB.seg = seg;
    DRcB.fidx = fidx;
    DRcB.gidx = gidx;
    DRcB.diam = lognrnd(this.morpho.DRcB.mu, this.morpho.DRcB.sig);
    DRcB.model = this.morpho.DRcB.model;
    DRcB.set_DRbranch(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_DRcB. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
