function parse_config_file(this, file_path)
	% Parse the configuration file at file_path.
    %
    % Usage:
    %   smc.parse_config_file(file_path)
    %
    % Parameters:
    %   file_path : string : path to configuration file.
    
    % Open file.
   if exist(file_path, 'file')
        fileID = fopen(file_path);
    else
        error('%s: file not found.', file_path);
    end
    
    % Read file lines.
    C = textscan(fileID, '%s', 'Delimiter', '\n');
    C = C{1};
    
    % Close file.
    fclose(fileID);
    
    % Parse config.
    
    while ~ isempty(C)
        
        if isempty(C{1}) || strcmp(C{1}(1), '#')
            C(1) = [];
            continue
        end
        
        words = strsplit(C{1});
        
        switch words{1}
            
            case 'name'
                this.name = words{3};
            
            case 'nmuscs'
                this.compo.nmuscs = str2double(words{3});
                
            case 'muscles'
                this.compo.muscles = words(3 : end)';
                
            case 'nsegs'
                this.compo.nsegs = str2double(words{3});
                
            case 'segments'
                this.compo.segments = words(3 : end)';
                
            case 'MN_distrib'
                this.compo.MN_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.MN_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.MN_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nMNs = sum(sum(this.compo.MN_distrib));
                
            case 'MN_with_axon'
                this.morpho.MN.with_axon = strcmp(words{3}, 'true');
                
            case 'MN_with_dtree'
                this.morpho.MN.with_dtree = strcmp(words{3}, 'true');
                if this.morpho.MN.with_dtree
                    swc_files = Utils.scan_files(fullfile(this.dirs.resources, 'swc_files'));
                    swc_files = swc_files(cellfun(@(x) strcmp(x(end - 3 : end), '.swc'), swc_files));
                    this.morpho.MN.swc_files = cellfun(@(x) fullfile(this.dirs.resources, 'swc_files', x), swc_files, 'UniformOutput', false);
                end
                
            case 'MN_min_diam'
                this.morpho.MN.min_diam = str2double(words{3});
                
            case 'MN_max_diam'
                this.morpho.MN.max_diam = str2double(words{3});
                
            case 'MA_model'
                this.morpho.MA.model = words{3};
                
            case 'MA_min_diam'
                this.morpho.MA.min_diam = str2double(words{3});
                
            case 'MA_max_diam'
                this.morpho.MA.max_diam = str2double(words{3});
                
            case 'Ia_distrib'
                this.compo.Ia_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.Ia_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.Ia_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nIas = sum(sum(this.compo.Ia_distrib));
                
            case 'Ia_synergies'
                if strcmp(words{3}, '-')
                    this.connec.Ia.synergies = cellfun(@(x) {}, cell(this.compo.nmuscs, 1), 'UniformOutput', false);
                end
                
            case 'Ia_p_homo'
                this.connec.Ia.p_homo = str2double(words{3});
                
            case 'Ia_p_hete'
                this.connec.Ia.p_hete = str2double(words{3});
                
            case 'Ia_model'
                this.morpho.Ia.model = words{3};
                
            case 'Ia_mean_diam'
                this.morpho.Ia.mean_diam = str2double(words{3});
                
            case 'Ia_sd_diam'
                this.morpho.Ia.sd_diam = str2double(words{3});
                
            case 'Ia_mean_diam_cols'
                this.morpho.Ia.mean_diam_cols = str2double(words{3});
                
            case 'Ia_sd_diam_cols'
                this.morpho.Ia.sd_diam_cols = str2double(words{3});
                
            case 'II_distrib'
                this.compo.II_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.II_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.II_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nIIs = sum(sum(this.compo.II_distrib));
                
            case 'II_synergies'
                if strcmp(words{3}, '-')
                    this.connec.II.synergies = cellfun(@(x) {}, cell(this.compo.nmuscs, 1), 'UniformOutput', false);
                end
                
            case 'II_p_homo'
                this.connec.II.p_homo = str2double(words{3});
                
            case 'II_p_hete'
                this.connec.II.p_hete = str2double(words{3});
                
            case 'II_model'
                this.morpho.II.model = words{3};
                
            case 'II_mean_diam'
                this.morpho.II.mean_diam = str2double(words{3});
                
            case 'II_sd_diam'
                this.morpho.II.sd_diam = str2double(words{3});
                
            case 'II_mean_diam_cols'
                this.morpho.II.mean_diam_cols = str2double(words{3});
                
            case 'II_sd_diam_cols'
                this.morpho.II.sd_diam_cols = str2double(words{3});
                
            case 'DRcA_distrib'
                this.compo.DRcA_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.DRcA_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.DRcA_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nDRcAs = sum(sum(this.compo.DRcA_distrib));
                
            case 'DRcA_model'
                this.morpho.DRcA.model = words{3};
                
            case 'DRcA_mean_diam'
                this.morpho.DRcA.mean_diam = str2double(words{3});
                
            case 'DRcA_sd_diam'
                this.morpho.DRcA.sd_diam = str2double(words{3});
                
            case 'DRcB_distrib'
                this.compo.DRcB_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.DRcB_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.DRcB_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nDRcBs = sum(sum(this.compo.DRcB_distrib));
                
            case 'DRcB_model'
                this.morpho.DRcB.model = words{3};
                
            case 'DRcB_mean_diam'
                this.morpho.DRcB.mean_diam = str2double(words{3});
                
            case 'DRcB_sd_diam'
                this.morpho.DRcB.sd_diam = str2double(words{3});
                
            case 'DCcB_distrib'
                this.compo.DCcB_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.DCcB_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.DCcB_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nDCcBs = sum(sum(this.compo.DCcB_distrib));
                
            case 'DCcB_model'
                this.morpho.DCcB.model = words{3};
                
            case 'DCcB_mean_diam'
                this.morpho.DCcB.mean_diam = str2double(words{3});
                
            case 'DCcB_sd_diam'
                this.morpho.DCcB.sd_diam = str2double(words{3});
                
            case 'DCcB_mean_diam_cols'
                this.morpho.DCcB.mean_diam_cols = str2double(words{3});
                
            case 'DCcB_sd_diam_cols'
                this.morpho.DCcB.sd_diam_cols = str2double(words{3});
                
            case 'DCcB_comp'
                this.morpho.DCcB.comp = words{3};
                
            case 'CST_distrib'
                this.compo.CST_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.CST_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.CST_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nCSTs = sum(sum(this.compo.CST_distrib));
                
            case 'CST_synergies'
                if strcmp(words{3}, '-')
                    this.connec.CST.synergies = cellfun(@(x) {}, cell(this.compo.nmuscs, 1), 'UniformOutput', false);
                end
                
            case 'CST_p_homo'
                this.connec.CST.p_homo = str2double(words{3});
                
            case 'CST_p_hete'
                this.connec.CST.p_hete = str2double(words{3});
                
            case 'CST_model'
                this.morpho.CST.model = words{3};
                
            case 'CST_mean_diam'
                this.morpho.CST.mean_diam = str2double(words{3});
                
            case 'CST_sd_diam'
                this.morpho.CST.sd_diam = str2double(words{3});
                
            case 'CST_mean_diam_cols'
                this.morpho.CST.mean_diam_cols = str2double(words{3});
                
            case 'CST_sd_diam_cols'
                this.morpho.CST.sd_diam_cols = str2double(words{3});
                
            case 'ST_distrib'
                this.compo.ST_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.ST_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.ST_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nSTs = sum(sum(this.compo.ST_distrib));
                
            case 'ST_model'
                this.morpho.ST.model = words{3};
                
            case 'ST_mean_diam'
                this.morpho.ST.mean_diam = str2double(words{3});
                
            case 'ST_sd_diam'
                this.morpho.ST.sd_diam = str2double(words{3});
                
            case 'ST_mean_diam_cols'
                this.morpho.ST.mean_diam_cols = str2double(words{3});
                
            case 'ST_sd_diam_cols'
                this.morpho.ST.sd_diam_cols = str2double(words{3});
                
            case 'IaOD3_distrib'
                this.compo.IaOD3_distrib = zeros(this.compo.nmuscs, this.compo.nsegs);
                this.compo.IaOD3_distrib(1, :) = cellfun(@str2double, words(3 : end));
                for j = 2 : this.compo.nmuscs
                    C(1) = [];
                    words = strsplit(C{1});
                    this.compo.IaOD3_distrib(j, :) = cellfun(@str2double, words);
                end
                this.compo.nIaOD3s = sum(sum(this.compo.IaOD3_distrib));
                
            case 'IaOD3_synergies'
                if strcmp(words{3}, '-')
                    this.connec.IaOD3.synergies = cellfun(@(x) {}, cell(this.compo.nmuscs, 1), 'UniformOutput', false);
                end
                
            case 'IaOD3_p_homo'
                this.connec.IaOD3.p_homo = str2double(words{3});
                
            case 'IaOD3_p_hete'
                this.connec.IaOD3.p_hete = str2double(words{3});
                
            case 'IaOD3_model'
                this.morpho.IaOD3.model = words{3};
                
            case 'IaOD3_mean_diam'
                this.morpho.IaOD3.mean_diam = str2double(words{3});
                
            case 'IaOD3_sd_diam'
                this.morpho.IaOD3.sd_diam = str2double(words{3});
                
            case 'IaOD3_mean_diam_cols'
                this.morpho.IaOD3.mean_diam_cols = str2double(words{3});
                
            case 'IaOD3_sd_diam_cols'
                this.morpho.IaOD3.sd_diam_cols = str2double(words{3});
                
            case 'fem_config_file_name'
                if ~ strcmp(words{3}, '-')
                    this.set_fem(words{3});
                end
                
            case 'params_interp_type'
                this.params.interp_type = words{3};
                
            case 'params_pdens'
                this.params.pdens = str2double(words{3});
                
            case 'params_verbose'
                this.params.verbose = str2double(words{3});

        end
        
        C(1) = [];
        
    end
    
    this.set_morpho();
        
end
