function objs = get_objs(this, entity, varargin)
    % Return the cell-array of selected entity instances.
    %
    % Usage:
    %   objs = smcd.get_objs(entity)
    %   objs = smcd.get_objs(entity, 'musc', musc)
    %   objs = smcd.get_objs(entity, 'seg', seg)
    %   objs = smcd.get_objs(entity, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   objs = smcd.get_objs(entity, 'gidx', gidx)
    %
    % Parameters:
    %   entity : string :
    %       Identifier of neural entity.
    %
    %   musc : string / N*1 cell :
    %       Name(s) of muscle(s).
    %
    %   seg : string / N*1 cell :
    %       Name(s) of segment(s).
    %
    %   fidx : N*1 double :
    %       Family index(es) of entity instance(s).
    %
    %   gidx : N*1 double :
    %       Global index(es) of entity instance(s).
    %
    % Return:
    %   objs : K*1 cell : each cell is an entity instance.
    
    % First select all the instances of the specified entity.
    switch entity
        
        case {'Ia', 'IaFiber'}
            objs = this.entities.Ias;
            
        case {'II', 'IIFiber'}
            objs = this.entities.IIs;
            
        case {'MN', 'MA', 'Motoneuron', 'MotorAxon'}
            objs = this.entities.MNs;
            
        case {'DCcB', 'DCcBFiber'}
            objs = this.entities.DCcBs;
            
        case {'DRcA', 'DRcAFiber'}
            objs = this.entities.DRcAs;
            
        case {'CST', 'CSTFiber'}
            objs = this.entities.CSTs;
            
        case {'ST', 'STFiber'}
            objs = this.entities.STs;
            
        case {'IaOD3', 'IaFiberOD3'}
            objs = this.entities.IaOD3s;
            
    end
    
    
    % Second, select only the specified instances by means of their global
    % indexes.
        
    % If 'gidx' is given, the global indexes are readily available.
    if any(strcmp('gidx', varargin))
        idx = find(strcmp('gidx', varargin));    
        gidx = varargin{idx + 1};

     % If 'musc', 'seg' or 'fidx' is given, we derive the gidx from them.
    elseif any(strcmp('musc', varargin)) || any(strcmp('seg', varargin)) || any(strcmp('fidx', varargin))
        
        % Muscles.
        idx = find(strcmp('musc', varargin));
        if ~ isempty(idx)
            musc = varargin{idx + 1};
            if ischar(musc)
                musc = {musc};
            end
        else
            musc = this.compo.muscles;
        end
        
        % Segments.
        idx = find(strcmp('seg', varargin));
        if ~ isempty(idx)
            seg = varargin{idx + 1};
            if ischar(seg)
                seg = {seg};
            end
        else
            seg = this.compo.segments;
        end
        
        % fidx.
        idx = find(strcmp('fidx', varargin));
        if ~ isempty(idx)
            fidx = varargin{idx + 1};
            prescribed_fidx = true;
        else
            prescribed_fidx = false;
        end
        
        % Derive gidx.
        gidx = [];
        for i = 1 : size(musc, 1)
            for j = 1 : size(seg, 1)
                if ~ prescribed_fidx
                    fidx = (1 : this.get_family_count(entity, musc{i}, seg{j}))';
                end
                for k = 1 : size(fidx, 1)
                    gidx(end + 1, 1) = this.info_2_gidx(entity, musc{i}, seg{j}, fidx(k));
                end
            end
        end
        
    % If no specification given, return all entity instances.
    else
        return
        
    end
            
    objs = objs(gidx);
    
end
