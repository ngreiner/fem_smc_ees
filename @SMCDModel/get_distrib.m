function distrib = get_distrib(this, entity)
    % Return the distribution of the specified entity.
    %
    % Usage:
    %   distrib = smcd.get_distrib(entity)
    %
    % Parameters:
    %   entity : string : identifier of neural entity.
    %
    % Return:
    %   distrib : nmuscs * nsegs double.
    
    switch entity
        
        case {'Ia', 'IaFiber'}
            distrib = this.compo.Ia_distrib;
            
        case {'II', 'IIFiber'}
            distrib = this.compo.II_distrib;
            
        case {'MN', 'MA', 'Motoneuron', 'MotorAxon'}
            distrib = this.compo.MN_distrib;
            
        case {'DCcB', 'DCcBFiber'}
            distrib = this.compo.DCcB_distrib;
            
        case {'DRcA', 'DRcAFiber'}
            distrib = this.compo.DRcA_distrib;
            
        case {'CST', 'CSTFiber'}
            distrib = this.compo.CST_distrib;
            
        case {'ST', 'STFiber'}
            distrib = this.compo.ST_distrib;
            
        case {'IaOD3', 'IaFiberOD3'}
            distrib = this.compo.IaOD3_distrib;
            
    end
    
end
