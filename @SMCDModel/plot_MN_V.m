function plot_MN_V(this, MN, AS_idx, varargin)
	% Plot the electric potential along the branches of the specified MN.
    %
    % Parameters:
    %   MN : Motoneuron object.
    %
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   varargin : fields can be :
    %       'with_axon', indicating that the MN axon should be plotted.
    
    % Parse inputs.
    with_axon = any(strcmp('with_axon', varargin));

    % Unpack attributes in convenient variables.
    sc = this.fem.sc;
    CS = sc.CS;
    nCS = sc.morpho.nCS;
    rts = sc.roots;
    nrts = size(rts,1);
    soma = MN.soma;
    dsecs = MN.dtree.dsecs;
    axon = MN.axon;

    % Set axis dimensions.
    xmin = -sc.morpho.fat_max_trans_width;
    xmax = +sc.morpho.fat_max_trans_width;
    ymin = -sc.morpho.fat_max_sag_width;
    ymax = +sc.morpho.fat_max_sag_width;
    zmin = -0.05*sc.morpho.seg_length;
    zmax = +1.05*sc.morpho.seg_length;
    axis([xmin xmax ymin ymax zmin zmax])
    hold on
    
    % Plot spinal cord cross-sections.
    for i = 1 : nCS
        plot3(CS{i}.GM(:, 1), CS{i}.GM(:, 2), CS{i}.GM(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        plot3(CS{i}.WM(:, 1), CS{i}.WM(:, 2), CS{i}.WM(:, 3), 'LineWidth', 1.2, 'Color', 'black');
    end
    
    % Plot roots' cross-sections.
    for i = 1 : nrts
        DCSi = rts{i}.LD.full_branches{1}.CS;
        VCSi = rts{i}.LV.full_branches{1}.CS;
        for k = 1 : size(DCSi, 1)
            plot3(DCSi{k}(:, 1), DCSi{k}(:, 2), DCSi{k}(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        end
        for k = 1 : size(VCSi, 1)
            plot3(VCSi{k}(:, 1), VCSi{k}(:, 2), VCSi{k}(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        end
    end

    % Derive linewidths of the different branches.
    axon_lw = 1.2 ;
    dsecs_lw = cellfun(@(x) axon_lw * x.ds / axon.diam, dsecs, 'UniformOutput', false);

    % Derive intensities of green for line color.
    soma_V = soma.V{AS_idx};
    dsecs_V = cellfun(@(x) x.V{AS_idx}, dsecs);
    if with_axon
        axon_V = axon.get_path_V(AS_idx);
        maxV = max([soma_V; axon_V; dsecs_V]);
        minV = min([soma_V; axon_V; dsecs_V]);
        axon_greens = (maxV - axon_V) / (maxV - minV);
    else
        maxV = max([soma_V; dsecs_V]);
        minV = min([soma_V; dsecs_V]);
    end
    soma_green = (maxV - soma_V) / (maxV - minV);
    dsecs_greens = (maxV - dsecs_V) / (maxV - minV);
    
    % Plot MN with potential-derived colormap.
    
    % Soma
    scatter3(soma.coords(1), soma.coords(2), soma.coords(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', [1.0, soma_green, 0.0])
    
    % Axon
    if with_axon
        ps = axon.path;
        g0 = axon_greens(1);
        for j = 2 : size(ps, 1)
            g1 = axon_greens(j);
            g = (g0 + g1) / 2;
            plot3(ps(j - 1 : j, 1), ps(j - 1 : j, 2), ps(j - 1 : j, 3), 'LineWidth', axon_lw, 'Color', [1.0, g, 0.0])
            g0 = g1;
        end
    end

    % DendTree.
    for i = 1 : size(dsecs, 1)
        ps = dsecs{i}.coords;
        lws = dsecs_lw{i};
        lw0 = lws(1);
        for j = 2 : size(ps, 1)
            lw1 = lws(j);
            lw = (lw0 + lw1) / 2;
            plot3(ps(j - 1 : j, 1), ps(j - 1 : j, 2), ps(j - 1 : j, 3), 'LineWidth', lw, 'Color', [1.0, dsecs_greens(i), 0.0])
            lw0 = lw1;
        end
    end
    
end
