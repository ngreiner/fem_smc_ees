function MN = generate_MN(this, varargin)
    % Generate and return a Motoneuron object.
    %
    % Usage:
    %   MN = generate_MN(this, 'gidx', gidx)
    %   MN = generate_MN(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   MN = generate_MN(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of MN.
    %
    %   musc : string :
    %       Muscle of MN.
    %
    %   seg : string :
    %       Segment of MN.
    %       
    %   fidx : integer :
    %       Family index of MN.
    %
    % Return: 
    %   MN : Motoneuron object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('MN', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('MN', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('Ia', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_MN (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for MN.
    rng(Utils.gen_seed(this.name, 'MN', gidx));
    
    % Unpack attributes in convenient variables.
    M = this.morpho.MN.max_diam;
    m = this.morpho.MN.min_diam;
    
    % Generate MN's coordinates.
    coords = this.fem.sc.gen_VQ_points(seg, 1);
    
    % Flip x-coordinates if muscle is right-sided.
    if strcmp(musc(end - 1 : end), '_r')
        coords = Geom.flip_x_coords(coords);
    end
    
    % Generate the MN.
    MN = Motoneuron();
    MN.musc = musc;
    MN.seg = seg;
    MN.fidx = fidx;
    MN.gidx = gidx;
    MN.diam = m + (M - m) * rand(1);
    MN.set_soma(this, coords);
    if this.morpho.MN.with_axon
        MN.set_axon(this);
    end
    if this.morpho.MN.with_dtree
        MN.set_dtree(this);
    end
    
    fprintf(Utils.padn(sprintf('    END PROC: generate_MN. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
