function nAS = get_nAS(this)
    % Return the number of active sites of the SMCDModel fem.
    %
    % Usage:
    %   nAS = smcd.get_nAS()
    nAS = this.fem.elec.AS.count;
end
