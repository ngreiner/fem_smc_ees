function idxs = extract_idxs(folder_path, entity)
    % Return the indexes of exported entity instances at folder_path.
    %
    % Parameters:
    %   folder_path : string : path to folder to be scanned.
    %   entity : string : identifier of entity type.
    %
    % Return:
    %   idxs : N*1 double : indexes of the exported entity instances.
    
    switch entity
        
        case {'Ia', 'Ias', 'IaFiber'}
            basename = 'IaFiber';
            n = 8;
            
        case {'II', 'IIs', 'IIFiber'}
            basename = 'IIFiber';
            n = 8;
            
        case {'CST', 'CSTs', 'CSTFiber'}
            basename = 'CSTFiber';
            n = 9;
            
        case {'DCcB', 'DCcBs', 'DCcBFiber'}
            basename = 'DCcBFiber';
            n = 10;
            
        case {'MN', 'MNs'}
            basename = 'MN';
            n = 3;
            
        case {'ST', 'STs', 'STFiber'}
            basename = 'STFiber';
            n = 9;
            
    end            
    
    % Scan folder.
    subfolders = Utils.scan_subfolders(folder_path);
    
    % Extract appropriate subfolders.
    idx = cellfun(@(x) any(regexp(x, basename)), subfolders);
    folders = subfolders(idx);
    
    % Extract indexes.
    idxs = cellfun(@(x) str2double(x(n : end)), folders);
    
end
