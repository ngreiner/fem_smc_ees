function interp_V(this, entity, AS_idx, varargin)
    % Interpolate the electric potential for the specified entity.
    %
    % Usage:
    %   smcd.interp_V(entity)
    %   smcd.interp_V(entity, AS_idx)
    %   smcd.interp_V(entity, AS_idx, 'perm', perm)
    %   smcd.interp_V(entity, AS_idx, 'musc', musc)
    %   smcd.interp_V(entity, AS_idx, 'seg', seg)
    %   smcd.interp_V(entity, AS_idx, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   smcd.interp_V(entity, AS_idx, 'gidx', gidx)
    %
    % Parameters:
    %   entity : string :
    %       Identifier of neural entity ('Ia', 'II', 'MN', ...).
    %
    %   AS_idx : N*1 integer :
    %       Index(es) of active site(s).
    %
    %   perm : integer/string :
    %       Permission level (0, 1, 2 or 'i').
    %
    %   musc : str/cell :
    %       Name(s) of muscle(s).
    %
    %   seg : str/cell :
    %       Name(s) of segment(s).
    %
    %   fidx : K*1 integer :
    %       Family index(es) of entity instances.
    %
    %   gidx : M*1 integer :
    %       Global index(es) of entity instances.
    %
    % Description: 
    %   The potentials are obtained by interpolation in the Comsol model
    %   attached to the attribute fem, using the specified active sites.
    %   If AS_idx is not specified, the interpolation is performed for all
    %   the active sites available in the fem.
    
    % Parse inputs.
    if any(strcmp('perm', varargin))
        idx = find(strcmp('perm', varargin));
        perm = varargin{idx + 1};
    else
        perm = 1;
    end
    
    if (nargin <= 2)
        AS_idx = (1 : this.params.nAS)';
    end
    
    % Retrieve entity instances whose potentials should be interpolated.
    objs = this.get_objs(entity, varargin{:});
    
    for i = 1 : size(AS_idx, 1)
        % Load Comsol Model corresponding to active site AS_idx(i).
        this.fem.load_CM(AS_idx(i));
        this.fem.set_AS_area();
        % Loop over all objs.
        for j = 1 : size(objs, 1)
            objs{j}.interp_V(this, perm);
        end
    end
    
end
