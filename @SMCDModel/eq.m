function outbool = eq(obj1, obj2)
    % Check whether 2 IIFiber objects are equal.
    
    outbool = false;
    
    if ~ strcmp(obj1.name, obj2.name)
        fprintf('Attributes `name` are different.\n')
        return
    end
    
    if ~ isequal(obj1.dirs, obj2.dirs)
        fprintf('Attributes `dirs` are different.\n')
        return
    end
    
    if ~ isequal(obj1.fem, obj2.fem)
        fprintf('Attributes `fem` are different.\n')
        return
    end
    
    if ~ isequal(obj1.compo, obj2.compo)
        fprintf('Attributes `compo` are different.\n')
        return
    end
    
    if ~ isequal(obj1.connec, obj2.connec)
        fprintf('Attributes `connec` are different.\n')
        return
    end
    
    if ~ isequal(obj1.morpho, obj2.morpho)
        fprintf('Attributes `morpho` are different.\n')
        return
    end
    
    if ~ isequal(obj1.params, obj2.params)
        fprintf('Attributes `params` are different.\n')
        return
    end
    
    entities1 = fieldnames(obj1.entities);
    entities2 = fieldnames(obj2.entities);
    if ~ isequal(sort(entities1), sort(entities2))
        fprintf('The two objects do not have the same entities.\n')
        return
    end
    
    for i = 1 : size(entities1, 1)
        if ~ all(cellfun(@(x, y) isequal(x, y), obj1.entities.(entities1{i}), obj2.entities.(entities1{i})))
            fprintf('Attributes `entities.%s` are different.\n', entities1{i})
            return
        end
    end
    
    outbool = true;
    
end
