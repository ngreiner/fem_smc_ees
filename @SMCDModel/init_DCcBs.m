function init_DCcBs(this)
    % Initialize the DCcBFibers of the SMCD.
    %
    % Description: see method generateDCcBs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_DCcBs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize DCcBs array.
    this.entities.DCcBs = cell(this.compo.nDCcBs, 1);
    
    % Set DCcBs.
    for i = 1 : this.compo.nDCcBs
        this.entities.DCcBs{i} = this.generate_DCcB('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_DCcBs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end
