function init_DRcAs(this)
    % Initialize the DRcAFibers of the SMCD.
    %
    % Description: see method generateDRcAs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_DRcAs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize DCcAs array.
    this.entities.DRcAs = cell(this.compo.nDRcAs, 1);
    
    % Set DCcBs.
    for i = 1 : this.compo.nDRcAs
        this.entities.DRcAs{i} = this.generate_DRcA('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_DRcAs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end