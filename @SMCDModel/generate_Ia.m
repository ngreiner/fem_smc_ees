function Ia = generate_Ia(this, varargin)
    % Generate and return a IaFiber object.
    %
    % Usage:
    %   Ia = generate_Ia(this, 'gidx', gidx)
    %   Ia = generate_Ia(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   Ia = generate_Ia(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of Ia.
    %
    %   musc : string :
    %       Muscle of Ia.
    %
    %   seg : string :
    %       Segment of Ia.
    %       
    %   fidx : integer :
    %       Family index of Ia.
    %
    % Return: 
    %   Ia : IaFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('Ia', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('Ia', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('Ia', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_Ia (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for Ia.
    rng(Utils.gen_seed(this.name, 'Ia', gidx));
    
    % Generate IaFiber.
    Ia = IaFiber();
    Ia.musc = musc;
    Ia.seg = seg;
    Ia.fidx = fidx;
    Ia.gidx = gidx;
    Ia.diam = lognrnd(this.morpho.Ia.mu, this.morpho.Ia.sig);
    Ia.model = this.morpho.Ia.model;
    Ia.assign_MNs(this);
    Ia.set_DRbranch(this);
    Ia.set_Abranch(this);
    Ia.set_Dbranch(this);
    Ia.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_Ia. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
