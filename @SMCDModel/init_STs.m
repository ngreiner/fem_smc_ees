function init_STs(this)
    % Initialize the STs of the SMCDModel.
    %
    % Description: see method generate_STs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_STs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize STs array.
    this.entities.STs = cell(this.compo.nSTs, 1);
    
    % Set CSTs.
    for i = 1 : this.compo.nSTs
        this.entities.STs{i} = this.generate_ST('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_STs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end
