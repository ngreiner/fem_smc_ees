function init_MNs(this)
    % Initialize the attribute MNs.
    %
    % Description: see method generate_MN.

    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_MNs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize MNs.
    this.entities.MNs = cell(this.compo.nMNs, 1);
    
    % Set MNs.
    for i = 1 : this.compo.nMNs
        this.entities.MNs{i} = this.generate_MN('gidx', i);
    end

    fprintf(Utils.padn(sprintf('  END PROC: init_MNs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    
end
