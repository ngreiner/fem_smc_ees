function nobjs = get_family_count(this, entity, musc, seg)
    % Return the number of entity instances in the (musc, seg) family.
    %
    % Usage:
    %   nobjs = smcd.get_family_count(entity, musc, seg)
    %
    % Parameters: 
    %   entity : string : entity identifier ('Ia', 'II', 'MN', ...).
    %   musc : string : muscle name.
    %   seg : string : segment name.
    %
    % Return:
    %   nobjs : integer :
    %       Number of entity instances in the (musc, seg) family.
    
    % Recover appropriate entity distribution.
    distrib = this.get_distrib(entity);

    % Retrieve number of entity instances in the (musc, seg) family.
    i = strcmp(musc, this.compo.muscles);
    j = strcmp(seg, this.compo.segments);
    nobjs = distrib(i, j);
    
end
