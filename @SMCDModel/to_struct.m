function struc = to_struct(this)
	% Returns a matlab struct equivalent to the SMCDModel.
    %
    % Return:
    %   struc : struct : with same fields as the SMCDModel.
    struc.name = this.name;
    try
        struc.fem = this.fem.to_struct();
    catch ME
        if ~ strcmp(ME.identifier, 'MATLAB:structRefFromNonStruct')
            throw(ME)
        end
    end
    struc.compo = this.compo;
    struc.connec = this.connec;
    struc.morpho = this.morpho;
    struc.params = this.params;
    entities = fieldnames(this.entities);
    for i = 1 : size(entities, 1)
        struc.entities.(entities{i}) = cellfun(@(x) x.to_struct(), this.entities.(entities{i}), 'UniformOutput', false);
    end
end
