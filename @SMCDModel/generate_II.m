function II = generate_II(this, varargin)
    % Generate and return a IIFiber object.
    %
    % Usage:
    %   II = generate_II(this, 'gidx', gidx)
    %   II = generate_II(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   II = generate_II(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of II.
    %
    %   musc : string :
    %       Muscle of II.
    %
    %   seg : string :
    %       Segment of II.
    %       
    %   fidx : integer :
    %       Family index of II.
    %
    % Return: 
    %   II : IIFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('II', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('II', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('II', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_II (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for II.
    rng(Utils.gen_seed(this.name, 'II', gidx));
    
    % Generate IIFiber.
    II = IIFiber();
    II.musc = musc;
    II.seg = seg;
    II.fidx = fidx;
    II.gidx = gidx;
    II.diam = lognrnd(this.morpho.II.mu, this.morpho.II.sig);
    II.model = this.morpho.II.model;
    II.assign_MNs(this);
    II.set_DRbranch(this);
    II.set_Abranch(this);
    II.set_Dbranch(this);
    II.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_II. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
