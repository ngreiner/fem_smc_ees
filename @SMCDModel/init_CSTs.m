function init_CSTs(this)
    % Initialize the CSTs of the SMCDModel.
    %
    % Description: see method generate_CSTs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_CSTs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize CSTs array.
    this.entities.CSTs = cell(this.compo.nCSTs, 1);
    
    % Set CSTs.
    for i = 1 : this.compo.nCSTs
        this.entities.CSTs{i} = this.generate_CST('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_CSTs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end
