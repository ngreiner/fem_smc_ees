function DRcA = generate_DRcA(this, varargin)
    % Generate and return a DRcAFiber object.
    %
    % Usage:
    %   DRcA = generate_DRcA(this, 'gidx', gidx)
    %   DRcA = generate_DRcA(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   DRcA = generate_DRcA(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of DRcA.
    %
    %   musc : string :
    %       Muscle of DRcA.
    %
    %   seg : string :
    %       Segment of DRcA.
    %       
    %   fidx : integer :
    %       Family index of DRcA.
    %
    % Return: 
    %   DRcA : DRcBFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('DRcA', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('DRcA', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('DRcA', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_DRcA (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for DRcB.
    rng(Utils.gen_seed(this.name, 'DRcA', gidx));
    
    % Generate DRcBFiber.
    DRcA = DRcAFiber();
    DRcA.musc = musc;
    DRcA.seg = seg;
    DRcA.fidx = fidx;
    DRcA.gidx = gidx;
    DRcA.diam = lognrnd(this.morpho.DRcA.mu, this.morpho.DRcA.sig);
    DRcA.model = this.morpho.DRcA.model;
    DRcA.set_DRbranch(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_DRcA. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
