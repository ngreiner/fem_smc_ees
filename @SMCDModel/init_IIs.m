function init_IIs(this)
    % Initializes the attribute IIs.
    %
    % Description: see method generate_IIs.
    
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))
    fprintf(Utils.padn('   START PROC: init_IIs   ', 'both', '*', 70))
    fprintf(Utils.padnn('', 'left', '*', 70))
    t0 = cputime;

    % Initialize IIs.
    this.entities.IIs = cell(this.compo.nIIs, 1);
    
    % Set IIs.
    for i = 1 : this.compo.nIIs
        this.entities.IIs{i} = this.generate_II('gidx', i);
    end
    
    fprintf(Utils.padn(sprintf('  END PROC: init_IIs. (Time spent: %d s.)  ', cputime - t0), 'both', '*', 70))
    fprintf(Utils.padn('', 'left', '*', 70))

end