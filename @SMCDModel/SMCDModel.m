classdef SMCDModel < handle

    % Properties defined here:
    %
    %   name : string :
    %       Name of SensoriMotor Circuit Dataset.
    %
    %   dirs : struct :
    %       Each field is a path to a directory. These are 'top' and
    %       'smcds'.
    %
    %   fem : FEMModel object :
    %       Used to generate the geometry of the neural entities of the
    %       SMCD and to interpolate the electric potential along their
    %       trajectories.
    %
    %   compo : struct :
    %       Describing the composition of the SMCD. Fields are ...
    %
    %   connec : struct :
    %       Describing the connectivity of the SMCD. Fields are ...
    %
    %   morpho : struct :
    %       Holding morphologic parameters used to generate the neural
    %       entities of the SMCD.
    %
    %   params : struct :
    %       Holding various parameters necessary to build the SMCD.
    %
    %   entities : struct :
    %       Holding cell-arrays of neural entities. Fields can be:
    %           - MNs
    %           - Ias
    %           - IIs
    %           - DRcAs
    %           - DRcBs
    %           - DCcAs
    %           - DCcBs
    %           - CSTs
    %           - STs
    %           - IaOD3s

	properties
        
        name        % string
		dirs        % struct
		fem   		% FEMModel object
        compo       % struct
        connec      % struct
		morpho 		% struct
        params      % struct
        entities    % struct
        
    end
    

	methods
        
		function this = SMCDModel(varargin)
            % Class constructor method.
            %
            % Usage:
            %   smcd = SMCDModel()
            %   smcd = SMCDModel('topdir', topdir)
            %   smcd = SMCDModel('name', name)
            %   smcd = SMCDModel('fem_input', fem_input)
            %   smcd = SMCDModel('config_file_name', config_file_name)
            %   smcd = SMCDModel('verbose', verbose)
            %
            % Parameters:
            %   topdir : string :
            %       Path to top-level directory.
            %
            %   name : string :
            %       Name of the SMCDModel.
            %
            %   fem_input : string/FEMModel object :
            %       See method 'setfem'.
            %
            %   config_file_name : string :
            %       Name of configuration file.
            %
            %   verbose : int :
            %       Specifies the verbosity of the SMCDModel methods
            %       (0: minimal, 1: maximal).
            
            idx = find(strcmp('topdir', varargin));
            if ~isempty(idx)
                this.set_dirs(varargin{idx + 1});
            else
                this.set_dirs(pwd);
            end
            
            idx = find(strcmp('name', varargin));
            if ~isempty(idx)
                this.name = varargin{idx + 1};
            end
            
            idx = find(strcmp('fem_input', varargin));
            if ~isempty(idx)
                this.set_fem(varargin{idx + 1});
            end
            
            idx = find(strcmp('config_file_name', varargin));
            if ~ isempty(idx)
                this.parse_config_file(fullfile(this.dirs.config, varargin{idx + 1}));
            end
            
            idx = find(strcmp('verbose', varargin));
            if ~ isempty(idx)
                this.params.verbose = varargin{idx + 1};
            end
            
        end
        
    end
    

	methods
        clear_V(this, varargin)
        export(this, dir_export, varargin)
        from_struct(this, struc)
        CST = generate_CST(this, varargin)
        DCcB = generate_DCcB(this, varargin)
        DRcA = generate_DRcA(this, varargin)
        DRcB = generate_DRcB(this, varargin)
        Ia = generate_Ia(this, varargin)
        IaOD3 = generate_IaOD3(this, varargin)
        II = generate_II(this, varargin)
        MN = generate_MN(this, varargin)
        ST = generate_ST(this, varargin)
        distrib = get_distrib(this, entity)
        nobjs = get_family_count(this, entity, musc, seg)
        nAS = get_nAS(this)
        obj = get_obj(this, entity, varargin)
        objs = get_objs(this, entity, varargin)
        [musc, seg, fidx] = gidx_2_info(this, entity, gidx)
        import(this, varargin)
        gidx = info_2_gidx(this, entity, musc, seg, idx)
        init_CSTs(this)
        init_DCcAs(this)
        init_DCcBs(this)
        init_DRcAs(this)
        init_Ias(this)
        init_IaOD3s(this)
        init_IIs(this)
        init_MNs(this)
        init_STs(this)
        interp_V(this, entity, AS_idx, varargin)
        is_interp(this, entity, AS_idx, varargin)
        load(this, file_path)
        parse_config_file(this, file_path)
        plot(this, varargin)
        plot_Ia_V(this, Ia, AS_idx, varargin)
        plot_MN_V(this, MN, AS_idx, varargin)
        save(this, file_path)
		set_dirs(this, topdir)
        set_fem(this, fem_input)
        set_morpho(this)
        struc = to_struct(this)
        write_compo(this, dir_export, perm)
        write_connec(this, entity, dir_export, perm)
        write_index(this, entity, dir_export, perm)
    end
    
    methods (Static)
        idxs = extract_idxs(folder_path, entity)
    end
    
end
