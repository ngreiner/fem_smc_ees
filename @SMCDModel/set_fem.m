function set_fem(this, fem_input)
	% Set the attribute fem.
    %
    % Usage:
    %   smcd.set_fem(fem_input)
    %
    % Parameters:
    %   fem_input : string/FEMModel object :
    %       Either a FEMmodel object, either the path to a configuration
    %       file for a FEMModel object.
    %
    % Description:
    %   If fem_input is not provided, the fem attribute is left empty.
    %   If not, it specifies a FEMModel object (either directly or via a
    %   configuration file). 
    
    if nargin == 1
        return

    elseif isempty(fem_input)
        return

    elseif ischar(fem_input)
        this.fem = FEMModel('topdir', this.dirs.top, 'config_file_name', fem_input);

    else
        this.fem = fem_input;

    end
    
end
