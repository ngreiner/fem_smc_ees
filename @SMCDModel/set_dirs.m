function set_dirs(this, topdir)
    % Sets the attribute dirs.
    %
    % Parameters:
    %   topdir : string : path to top-level directory.
    %
    % Description:
    %   dirs is a struct whose fields are paths to directories necessary
    %   to the construction of the SMCDModel object. These are 'top',
    %   'resources', 'SMCD' and 'config'.
    this.dirs.top = topdir;
    this.dirs.SMCD = fullfile(this.dirs.top, 'sensorimotor_circuit_datasets');
    this.dirs.resources = fullfile(this.dirs.top, 'resources');
    this.dirs.config = fullfile(this.dirs.resources, 'SMCD_config_files');
end
