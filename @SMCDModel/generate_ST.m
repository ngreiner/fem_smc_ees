function ST = generate_ST(this, varargin)
    % Generate and return a STFiber object.
    %
    % Usage:
    %   ST = generate_ST(this, 'gidx', gidx)
    %   ST = generate_ST(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   ST = generate_ST(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of ST.
    %
    %   musc : string :
    %       Muscle of ST.
    %
    %   seg : string :
    %       Segment of ST.
    %       
    %   fidx : integer :
    %       Family index of ST.
    %
    % Return: 
    %   ST : STFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('ST', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('ST', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('ST', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_ST (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for CST.
    rng(Utils.gen_seed(this.name, 'ST', gidx));
    
    % Generate STFiber.
    ST = STFiber();
    ST.musc = musc;
    ST.seg = seg;
    ST.fidx = fidx;
    ST.gidx = gidx;
    ST.diam = lognrnd(this.morpho.ST.mu, this.morpho.ST.sig);
    ST.model = this.morpho.ST.model;
    ST.set_STbranch(this);
    ST.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_ST. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
