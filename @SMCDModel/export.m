function export(this, dir_export, varargin)
	% Export the Sensorimotor Circuit Dataset into post-processable
	% textfiles. 
    %
    % Parameters:
    %   dir_export : string : path to export directory.
    %
    %   varargin : fields can be:
    %       'Ias'   : indicating to export Ia-fibers
    %       'IIs'   : indicating to export II-fibers
    %       'DCcBs' : indicating to export DCcB-fibers
    %       'DRcAs' : indicating to export DRcA-fibers
    %       'CSTs'  : indicating to export CST-fibers
    %       'MNs'   : indicating to export Motoneurons
    %       'STs'   : indicating to export ST-fibers
    %       None provided <=> All provided
    %
    % Description: 
    %   The Sensorimotor Circuit Dataset textfiles are organized as
    %   follows:
    %
    %      dir_export/ -- composition.txt
    %                  |_ entityX_connec.txt
    %                  |_ entityX_index.txt
    %                  |_ entityY_connec.txt
    %                  |_ entityY_index.txt
    %                  |_ Muscle1/ -- Segment1/ -- entityX1/ -- ...
    %                  |           |            |
    %                  |           |            |_ entityX2/ -- ...
    %                  |           |            |
    %                  |           |            |_ ...
    %                  |           |            |
    %                  |           |            |_ entityY1/ -- ...
    %                  |           |            |
    %                  |           |            |_ entityY2/ -- ...
    %                  |           |            |
    %                  |           |            |_ ...
    %                  |           |
    %                  |           |_ Segment2/ -- ...
    %                  |           |
    %                  |           |_ ...
    %                  |
    %                  |_ Muscle2/ -- ...
    %                  |
    %                  |_ ...
    %
    %   In the above architecture, dir_export is the export directory, and
    %   is either user-specified or derived from the SMCDModel attributes.
    %   If this directory already exists, the user is prompted to overwrite
    %   or abort the export procedure.
    
    % Parse inputs.
    if nargin == 1
        dir_export = fullfile(this.dirs.SMCD, this.name);
    end
    
    Ias = any(strcmp(varargin, 'Ias'));
    IIs = any(strcmp(varargin, 'IIs'));
    CSTs = any(strcmp(varargin, 'CSTs'));
    DCcBs = any(strcmp(varargin, 'DCcBs'));
    DRcAs = any(strcmp(varargin, 'DRcAs'));
    MNs = any(strcmp(varargin, 'MNs'));
    STs = any(strcmp(varargin, 'STs'));
    IaOD3s = any(strcmp(varargin, 'IaOD3'));
    if ~ (Ias || IIs || CSTs || DCcBs || DRcAs || MNs || STs || IaOD3s)
        Ias = true;
        IIs = true;
        CSTs = true;
        DCcBs = true;
        DRcAs = true;
        MNs = true;
        STs = true;
        IaOD3s = true;
    end
    
    % Check current existence of export directory.
    if exist(dir_export, 'dir')
        msg = sprintf('%s directory already exists.\n', dir_export);
        msg = strcat(msg, 'What do you want to do?\n');
        msg = strcat(msg, '1. erase and rebuild\n');
        msg = strcat(msg, '2. abort\n');
        msg = strcat(msg, '3. overwrite\n');
        msg = strcat(msg, '4. augment\n');
        answer = input(msg, 's');
        if strcmp(answer, '1')
            fprintf('Erasing and rebuilding ...\n')
            rmdir(dir_export, 's');
            mkdir(dir_export);
            perm = 1;
        elseif strcmp(answer, '2')
            fprintf('Aborted export.\n')
            return
        elseif strcmp(answer, '3')
            fprintf('Overwriting ...\n')
            perm = 1;
        else
            fprintf('Augmenting ...\n')
            perm = 0;
        end
    else
        fprintf('Exporting Sensorimotor Circuit Dataset %s to %s ...\n', this.name, dir_export)
        mkdir(dir_export)
        perm = 1;
    end
    
    % Export SMCD composition.
    this.write_compo(dir_export, perm);
    
    % Export MNs.
    if isfield(this.entities, 'MNs') && ~ isempty(this.entities.MNs) && MNs
        this.write_index('MN', dir_export, perm);
        for i = 1 : this.compo.nMNs
            this.entities.MNs{i}.export(dir_export, perm);
        end
    end
    
    % Export Ias.
    if isfield(this.entities, 'Ias') && ~ isempty(this.entities.Ias) && Ias
        this.write_index('Ia', dir_export, perm);
        this.write_connec('Ia', dir_export, perm);
        for i = 1 : this.compo.nIas
            this.entities.Ias{i}.export(dir_export, perm);
        end
    end
    
    % Export IIs.
    if isfield(this.entities, 'IIs') && ~ isempty(this.entities.IIs) && IIs
        this.write_index('II', dir_export, perm);
        this.write_connec('II', dir_export, perm);
        for i = 1 : this.compo.nIIs
            this.entities.IIs{i}.export(dir_export, perm);
        end
    end
    
    % Export DRcAs.
    if isfield(this.entities, 'DRcAs') && ~ isempty(this.entities.DRcAs) && DRcAs
        this.write_index('DRcA', dir_export, perm);
        for i = 1 : this.compo.nDRcAs
            this.entities.DRcAs{i}.export(dir_export, perm);
        end
    end
    
    % Export DRcBs.
    if isfield(this.entities, 'DRcBs') && ~ isempty(this.entities.DRcBs) && DRcAs
        this.write_index('DRcB', dir_export, perm);
        for i = 1 : this.compo.nDRcBs
            this.entities.DRcBs{i}.export(dir_export, perm);
        end
    end
    
    % Export DCcAs.
    if isfield(this.entities, 'DCcAs') && ~ isempty(this.entities.DCcAs) && DCcBs
        this.write_index('DCcA', dir_export, perm);
        for i = 1 : this.compo.nDCcAs
            this.entities.DCcAs{i}.export(dir_export, perm);
        end
    end
    
    % Export DCcBs.
    if isfield(this.entities, 'DCcBs') && ~ isempty(this.entities.DCcBs) && DCcBs
        this.write_index('DCcB', dir_export, perm);
        for i = 1 : this.compo.nDCcBs
            this.entities.DCcBs{i}.export(dir_export, perm);
        end
    end
    
    % Export CSTs.
    if isfield(this.entities, 'CSTs') && ~ isempty(this.entities.CSTs) && CSTs
        this.write_index('CST', dir_export, perm);
        this.write_connec('CST', dir_export, perm);
        for i = 1 : this.compo.nCSTs
            this.entities.CSTs{i}.export(dir_export, perm);
        end
    end
    
    % Export STs.
    if isfield(this.entities, 'STs') && ~ isempty(this.entities.STs) && STs
        this.write_index('ST', dir_export, perm);
        for i = 1 : this.compo.nSTs
            this.entities.STs{i}.export(dir_export, perm);
        end
    end
    
    % Export IaOD3s.
    if isfield(this.entities, 'IaOD3s') && ~ isempty(this.entities.IaOD3s) && IaOD3s
        this.write_index('IaOD3', dir_export, perm);
        this.write_connec('IaOD3', dir_export, perm);
        for i = 1 : this.compo.nIaOD3s
            this.entities.IaOD3s{i}.export(dir_export, perm);
        end
    end
    
end
