function plot(this, varargin)
	% Plots the specified elements of the SMCDModel.
    %
    % Parameters:
    %
    %   varargin : fields can be :
    %       'Ias', indicating that Ias should be plotted.
    %       'IIs', indicating that IIs should be plotted.
    %       'DCcBs', indicating that DCcBs should be plotted.
    %       'DRcAs', indicating that DRcAs should be plotted.
    %       'CSTs', indicating that CSTs should be plotted.
    %       'STs', indicating that STs should be plotted.
    %       'IaOD3s', indicating that STs should be plotted.
    %       'MNs', indicating that MNs should be plotted.
    %       'Ia_nodes', indicating that nodes of Ranvier of Ias
    %       should be plotted.
    %       'II_nodes', indicating that nodes of Ranvier of IIs
    %       should be plotted.
    %       'DCcB_nodes', indicating that nodes of Ranvier of
    %       DCcBs should be plotted.
    %       'DRcA_nodes', indicating that nodes of Ranvier of
    %       DRcAs should be plotted.
    %       'CST_nodes', indicating that nodes of Ranvier of CSTs
    %       should be plotted.
    %       'ST_nodes', indicating that nodes of Ranvier of STs
    %       should be plotted.
    %       'IaOD3_nodes', indicating that nodes of Ranvier of STs
    %       should be plotted.
    %       'MN_axons', indicating that the motor axons of MNs
    %       should be plotted.
    %       'MN_nodes', indicating that nodes of Ranvier of MNs
    %       should be plotted.
    %       'inisegs', indicating that initial segments should be
    %       plotted.
    %       'dtrees', indicating that denditric trees should be
    %       plotted.
    %       'MN_nodes', indicating that nodes of motor axons
    %       should be plotted.
    %       'Ia_muscs', followed by cell array of strings, the
    %       muscles whose Ias should be plotted.
    %       'II_muscs', followed by cell array of strings, the
    %       muscles whose IIs should be plotted.
    %       'DCcB_muscs', followed by cell array of strings, the
    %       muscles whose DCcBs should be plotted.
    %       'DRcA_muscs', followed by cell array of strings, the
    %       muscles whose DRcAs should be plotted.
    %       'CST_muscs', followed by cell array of strings, the
    %       muscles whose CSTs should be plotted.
    %       'ST_muscs', followed by cell array of strings, the
    %       muscles whose STs should be plotted.
    %       'IaOD3_muscs', followed by cell array of strings, the
    %       muscles whose STs should be plotted.
    %       'MN_muscs', followed by cell array of strings, the
    %       muscles whose MNs should be plotted.
    %       'Ia_segs', followed by cell array of strings, the
    %       segments whose Ias should be plotted.
    %       'II_segs', followed by cell array of strings, the
    %       segments whose IIs should be plotted.
    %       'DCcB_segs', followed by cell array of strings, the
    %       segments whose DCcBs should be plotted.
    %       'DRcA_segs', followed by cell array of strings, the
    %       segments whose DRcAs should be plotted.
    %       'CST_segs', followed by cell array of strings, the
    %       segments whose CSTs should be plotted.
    %       'ST_segs', followed by cell array of strings, the
    %       segments whose STs should be plotted.
    %       'IaOD3_segs', followed by cell array of strings, the
    %       segments whose STs should be plotted.
    %       'MN_segs', followed by cell array of strings, the
    %       segments whose MNs should be plotted.
    %       'Ia_frac', followed by integer, inverse of the
    %       fraction of Ias to be plotted.
    %       'II_frac', followed by integer, inverse of the
    %       fraction of IIs to be plotted.
    %       'DCcB_frac', followed by integer, inverse of the
    %       fraction of DCcBs to be plotted.
    %       'DRcA_frac', followed by integer, inverse of the
    %       fraction of DRcAs to be plotted.
    %       'CST_frac', followed by integer, inverse of the
    %       fraction of CSTs to be plotted.
    %       'ST_frac', followed by integer, inverse of the
    %       fraction of STs to be plotted.
    %       'IaOD3_frac', followed by integer, inverse of the
    %       fraction of STs to be plotted.
    %       'MN_frac', followed by integer, inverse of the
    %       fraction of MNs to be plotted.
    %       'Ia_fidx', followed by integer, family index of Ia to
    %       be plotted.
    %       'II_fidx', followed by integer, family index of II to
    %       be plotted.
    %       'DCcB_fidx', followed by integer, family index of DCcB
    %       to be plotted.
    %       'DRcA_fidx', followed by integer, family index of DRcA
    %       to be plotted.
    %       'CST_fidx', followed by integer, family index of CST
    %       to be plotted.
    %       'ST_fidx', followed by integer, family index of ST
    %       to be plotted.
    %       'IaOD3_fidx', followed by integer, family index of ST
    %       to be plotted.
    %       'MN_fidx', followed by integer, family index of MN to
    %       be plotted.
    %       'Ia_gidx', followed by integer, global index of Ia to
    %       be plotted.
    %       'II_gidx', followed by integer, global index of II to
    %       be plotted.
    %       'DCcB_gidx', followed by integer, global index of DCcB
    %       to be plotted.
    %       'DRcA_gidx', followed by integer, global index of DRcA
    %       to be plotted.
    %       'CST_gidx', followed by integer, global index of CST
    %       to be plotted.
    %       'ST_gidx', followed by integer, global index of ST
    %       to be plotted.
    %       'IaOD3_gidx', followed by integer, global index of ST
    %       to be plotted.
    %       'MN_gidx', followed by integer, global index of MN to be
    %       plotted.
    
    % Parse inputs.
    with_CS = 1;
    with_Ias = 0;
    with_IIs = 0;
    with_DCcBs = 0;
    with_DRcAs = 0;
    with_CSTs = 0;
    with_STs = 0;
    with_IaOD3s = 0;
    with_MNs = 0;
    Ia_nodes = 0;
    II_nodes = 0;
    DCcB_nodes = 0;
    DRcA_nodes = 0;
    CST_nodes = 0;
    ST_nodes = 0;
    IaOD3_nodes = 0;
    MN_axons = 0;
    MN_nodes = 0;
    inisegs = 0;
    dtrees = 0;
    Ia_muscs = this.compo.muscles;
    II_muscs = this.compo.muscles;
    DCcB_muscs = this.compo.muscles;
    DRcA_muscs = this.compo.muscles;
    CST_muscs = this.compo.muscles;
    ST_muscs = this.compo.muscles;
    IaOD3_muscs = this.compo.muscles;
    MN_muscs = this.compo.muscles;
    Ia_segs = this.compo.segments;
    II_segs = this.compo.segments;
    DCcB_segs = this.compo.segments;
    DRcA_segs = this.compo.segments;
    CST_segs = this.compo.segments;
    ST_segs = this.compo.segments;
    IaOD3_segs = this.compo.segments;
    MN_segs = this.compo.segments;
    Ia_frac = 1;
    II_frac = 1;
    DCcB_frac = 1;
    DRcA_frac = 1;
    CST_frac = 1;
    ST_frac = 1;
    IaOD3_frac = 1;
    MN_frac = 1;
    Ia_fidx = [];
    II_fidx = [];
    DCcB_fidx = [];
    DRcA_fidx = [];
    CST_fidx = [];
    ST_fidx = [];
    IaOD3_fidx = [];
    MN_fidx = [];
    Ia_gidx = [];
    II_gidx = [];
    DCcB_gidx = [];
    DRcA_gidx = [];
    CST_gidx = [];
    ST_gidx = [];
    IaOD3_gidx = [];
    MN_gidx = [];
    Ia_istart = 1;
    II_istart = 1;
    DCcB_istart = 1;
    DRcA_istart = 1;
    CST_istart = 1;
    ST_istart = 1;
    IaOD3_istart = 1;
    MN_istart = 1;
    for i = 1 : size(varargin, 2)
        try
            switch varargin{i}
                case 'withoutCS'
                    with_CS = 0;
                case 'Ias'
                    with_Ias = 1;
                case 'IIs'
                    with_IIs = 1;
                case 'DCcBs'
                    with_DCcBs = 1;
                case 'DRcAs'
                    with_DRcAs = 1;
                case 'CSTs'
                    with_CSTs = 1;
                case 'STs'
                    with_STs = 1;
                case 'IaOD3s'
                    with_IaOD3s = 1;
                case 'MNs'
                    with_MNs = 1;
                    MN_axons = 1;
                    dtrees = 1;
                case 'Ia_nodes'
                    Ia_nodes = 1;
                case 'II_nodes'
                    II_nodes = 1;
                case 'DCcB_nodes'
                    DCcB_nodes = 1;
                case 'DRcA_nodes'
                    DRcA_nodes = 1;
                case 'CST_nodes'
                    CST_nodes = 1;
                case 'ST_nodes'
                    ST_nodes = 1;
                case 'IaOD3_nodes'
                    IaOD3_nodes = 1;
                case 'no_MN_axons'
                    MN_axons = 0;
                case 'MN_nodes'
                    MN_nodes = 1;
                case 'inisegs'
                    inisegs = 1;
                case 'no_dtrees'
                    dtrees = 0;
                case 'Ia_muscs'
                    Ia_muscs = varargin{i + 1};
                case 'II_muscs'
                    II_muscs = varargin{i + 1};
                case 'DCcB_muscs'
                    DCcB_muscs = varargin{i + 1};
                case 'DRcA_muscs'
                    DRcA_muscs = varargin{i + 1};
                case 'CST_muscs'
                    CST_muscs = varargin{i + 1};
                case 'ST_muscs'
                    ST_muscs = varargin{i + 1};
                case 'IaOD3_muscs'
                    IaOD3_muscs = varargin{i + 1};
                case 'MN_muscs'
                    MN_muscs = varargin{i + 1};
                case 'Ia_segs'
                    Ia_segs = varargin{i + 1};
                case 'II_segs'
                    II_segs = varargin{i + 1};
                case 'DCcB_segs'
                    DCcB_segs = varargin{i + 1};
                case 'DRcA_segs'
                    DRcA_segs = varargin{i + 1};
                case 'CST_segs'
                    CST_segs = varargin{i + 1};
                case 'ST_segs'
                    ST_segs = varargin{i + 1};
                case 'IaOD3_segs'
                    IaOD3_segs = varargin{i + 1};
                case 'MN_segs'
                    MN_segs = varargin{i + 1};
                case 'Ia_frac'
                    Ia_frac = varargin{i + 1};
                case 'II_frac'
                    II_frac = varargin{i + 1};
                case 'DCcB_frac'
                    DCcB_frac = varargin{i + 1};
                case 'DRcA_frac'
                    DRcA_frac = varargin{i + 1};
                case 'CST_frac'
                    CST_frac = varargin{i + 1};
                case 'ST_frac'
                    ST_frac = varargin{i + 1};
                case 'IaOD3_frac'
                    IaOD3_frac = varargin{i + 1};
                case 'MN_frac'
                    MN_frac = varargin{i + 1};
                case 'Ia_fidx'
                    Ia_fidx = varargin{i + 1};
                case 'II_fidx'
                    II_fidx = varargin{i + 1};
                case 'DCcB_fidx'
                    DCcB_fidx = varargin{i + 1};
                case 'DRcA_fidx'
                    DRcA_fidx = varargin{i + 1};
                case 'CST_fidx'
                    CST_fidx = varargin{i + 1};
                case 'ST_fidx'
                    ST_fidx = varargin{i + 1};
                case 'IaOD3_fidx'
                    IaOD3_fidx = varargin{i + 1};
                case 'MN_fidx'
                    MN_fidx = varargin{i + 1};
                case 'Ia_gidx'
                    Ia_gidx = varargin{i + 1};
                case 'II_gidx'
                    II_gidx = varargin{i + 1};
                case 'DCcB_gidx'
                    DCcB_gidx = varargin{i + 1};
                case 'DRcA_gidx'
                    DRcA_gidx = varargin{i + 1};
                case 'CST_gidx'
                    CST_gidx = varargin{i + 1};
                case 'ST_gidx'
                    ST_gidx = varargin{i + 1};
                case 'IaOD3_gidx'
                    IaOD3_gidx = varargin{i + 1};
                case 'MN_gidx'
                    MN_gidx = varargin{i + 1};
            end
        catch
        end
    end
    if ischar(Ia_muscs)
        Ia_muscs = {Ia_muscs};
    end
    if ischar(II_muscs)
        II_muscs = {II_muscs};
    end
    if ischar(DCcB_muscs)
        DCcB_muscs = {DCcB_muscs};
    end
    if ischar(DRcA_muscs)
        DRcA_muscs = {DRcA_muscs};
    end
    if ischar(CST_muscs)
        CST_muscs = {CST_muscs};
    end
    if ischar(ST_muscs)
        ST_muscs = {ST_muscs};
    end
    if ischar(IaOD3_muscs)
        IaOD3_muscs = {IaOD3_muscs};
    end
    if ischar(MN_muscs)
        MN_muscs = {MN_muscs};
    end
    if ischar(Ia_segs)
        Ia_segs = {Ia_segs};
    end
    if ischar(II_segs)
        II_segs = {II_segs};
    end
    if ischar(DCcB_segs)
        DCcB_segs = {DCcB_segs};
    end
    if ischar(DRcA_segs)
        DRcA_segs = {DRcA_segs};
    end
    if ischar(CST_segs)
        CST_segs = {CST_segs};
    end
    if ischar(ST_segs)
        ST_segs = {ST_segs};
    end
    if ischar(IaOD3_segs)
        IaOD3_segs = {IaOD3_segs};
    end
    if ischar(MN_segs)
        MN_segs = {MN_segs};
    end
    if ~isempty(Ia_fidx)
        Ia_istart = Ia_fidx;
        Ia_frac = Inf;
    elseif ~isempty(Ia_gidx)
        [musc, seg, fidx] = this.gidx_2_info('Ia', Ia_gidx);
        Ia_muscs = {musc};
        Ia_segs = {seg};
        Ia_istart = fidx;
        Ia_frac = Inf;
    end
    if ~isempty(II_fidx)
        II_istart = II_fidx;
        II_frac = Inf;
    elseif ~isempty(II_gidx)
        [musc, seg, fidx] = this.gidx_2_info('II', II_gidx);
        II_muscs = {musc};
        II_segs = {seg};
        II_istart = fidx;
        II_frac = Inf;
    end
    if ~isempty(DCcB_fidx)
        DCcB_istart = DCcB_fidx;
        DCcB_frac = Inf;
    elseif ~isempty(DCcB_gidx)
        [musc, seg, fidx] = this.gidx_2_info('DCcB', DCcB_gidx);
        DCcB_muscs = {musc};
        DCcB_segs = {seg};
        DCcB_istart = fidx;
        DCcB_frac = Inf;
    end
    if ~isempty(DRcA_fidx)
        DRcA_istart = DRcA_fidx;
        DRcA_frac = Inf;
    elseif ~isempty(DRcA_gidx)
        [musc, seg, fidx] = this.gidx_2_info('DRcA', DRcA_gidx);
        DRcA_muscs = {musc};
        DRcA_segs = {seg};
        DRcA_istart = fidx;
        DRcA_frac = Inf;
    end
    if ~isempty(CST_fidx)
        CST_istart = CST_fidx;
        CST_frac = Inf;
    elseif ~isempty(CST_gidx)
        [musc, seg, fidx] = this.gidx_2_info('CST', CST_gidx);
        CST_muscs = {musc};
        CST_segs = {seg};
        CST_istart = fidx;
        CST_frac = Inf;
    end
    if ~isempty(ST_fidx)
        ST_istart = ST_fidx;
        ST_frac = Inf;
    elseif ~isempty(ST_gidx)
        [musc, seg, fidx] = this.gidx_2_info('ST', ST_gidx);
        ST_muscs = {musc};
        ST_segs = {seg};
        ST_istart = fidx;
        ST_frac = Inf;
    end
    if ~isempty(IaOD3_fidx)
        IaOD3_istart = IaOD3_fidx;
        IaOD3_frac = Inf;
    elseif ~isempty(IaOD3_gidx)
        [musc, seg, fidx] = this.gidx_2_info('IaOD3', IaOD3_gidx);
        IaOD3_muscs = {musc};
        IaOD3_segs = {seg};
        IaOD3_istart = fidx;
        IaOD3_frac = Inf;
    end
    if ~isempty(MN_fidx)
        MN_istart = MN_fidx;
        MN_frac = Inf;
    elseif ~isempty(MN_gidx)
        [musc, seg, fidx] = this.gidx_2_info('MN', MN_gidx);
        MN_muscs = {musc};
        MN_segs = {seg};
        MN_istart = fidx;
        MN_frac = Inf;
    end

    % Unpack attributes in convenient variables.
    sc = this.fem.sc;
    CS = sc.CS;
    nCS = sc.morpho.nCS;
    rts = sc.roots;
    nrts = size(rts,1);
    Ias = this.entities.Ias(~cellfun(@isempty, this.entities.Ias));
    IIs = this.entities.IIs(~cellfun(@isempty, this.entities.IIs));
    DCcBs = this.entities.DCcBs(~cellfun(@isempty, this.entities.DCcBs));
    DRcAs = this.entities.DRcAs(~cellfun(@isempty, this.entities.DRcAs));
    CSTs = this.entities.CSTs(~cellfun(@isempty, this.entities.CSTs));
    STs = this.entities.STs(~cellfun(@isempty, this.entities.STs));
    try
        IaOD3s = this.entities.IaOD3s(~cellfun(@isempty, this.entities.IaOD3s));
    catch
    end
    MNs = this.entities.MNs(~cellfun(@isempty, this.entities.MNs));
    nmuscs_Ia = size(Ia_muscs, 1);
    nmuscs_II = size(II_muscs, 1);
    nmuscs_DCcB = size(DCcB_muscs, 1);
    nmuscs_DRcA = size(DRcA_muscs, 1);
    nmuscs_CST = size(CST_muscs, 1);
    nmuscs_ST = size(ST_muscs, 1);
    nmuscs_IaOD3 = size(IaOD3_muscs, 1);
    nmuscs_MN = size(MN_muscs, 1);
    nsegs_Ia = size(Ia_segs, 1);
    nsegs_II = size(II_segs, 1);
    nsegs_DCcB = size(DCcB_segs, 1);
    nsegs_DRcA = size(DRcA_segs, 1);
    nsegs_CST = size(CST_segs, 1);
    nsegs_ST = size(ST_segs, 1);
    nsegs_IaOD3 = size(IaOD3_segs, 1);
    nsegs_MN = size(MN_segs, 1);
    
    % Get colors for the different MN pools.
    muscles = union(Ia_muscs, II_muscs);
    muscles = union(muscles, DCcB_muscs);
    muscles = union(muscles, DRcA_muscs);
    muscles = union(muscles, CST_muscs);
    muscles = union(muscles, ST_muscs);
    muscles = union(muscles, IaOD3_muscs);
    muscles = union(muscles, MN_muscs);
    nmuscs = size(muscles, 1);
    colors = Utils.get_colors(nmuscs);
    idx = find(strcmp(varargin, 'color'));
    if idx
        colors = varargin(idx + 1);
    end
    hold on

    if with_CS
        % Set axis dimensions.
        xmin = - sc.morpho.CSF_max_trans_width;
        xmax = + sc.morpho.CSF_max_trans_width;
        ymin = - sc.morpho.CSF_max_sag_width;
        ymax = + sc.morpho.CSF_max_sag_width;
        zmin = - 0.05 * sc.morpho.seg_length;
        zmax = + 1.05 * sc.morpho.seg_length;
        axis([xmin xmax ymin ymax zmin zmax])

        % Plot spinal cord cross-sections.
        for i = 1 : nCS
%             plot3(CS{i}.GM(:, 1), CS{i}.GM(:, 2), CS{i}.GM(:, 3), 'LineWidth', 1, 'Color', 'black');
            plot3(CS{i}.WM(:, 1), CS{i}.WM(:, 2), CS{i}.WM(:, 3), 'LineWidth', 1, 'Color', 'black');
        end

        % Plot roots' cross-sections.
%         for i = 1 : nrts
%             DCSi = rts{i}.LD.full_branches{1}.CS;
%             VCSi = rts{i}.LV.full_branches{1}.CS;
%             for k = 1 : 3 : size(DCSi, 1)
%                 plot3(DCSi{k}(:, 1), DCSi{k}(:, 2), DCSi{k}(:, 3), 'LineWidth', 1, 'Color', 'black');
%             end
%             for k = 1 : 3 : size(VCSi, 1)
%                 plot3(VCSi{k}(:, 1), VCSi{k}(:, 2), VCSi{k}(:, 3), 'LineWidth', 1, 'Color', 'black');
%             end
%         end
    end
    
    % Plot Ias.
    if with_Ias
        for i = 1 : nmuscs_Ia
            for j = 1 : nsegs_Ia
                Iasij = Ias(cellfun(@(x) strcmp(Ia_muscs{i}, x.musc) & strcmp(Ia_segs{j}, x.seg), Ias));
                DRbranchesij = cellfun(@(x) x.DRbranch, Iasij, 'UniformOutput', false);
                Abranchesij = cellfun(@(x) x.Abranch, Iasij, 'UniformOutput', false);
                Dbranchesij = cellfun(@(x) x.Dbranch, Iasij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, Iasij, 'UniformOutput', false);
                for k = Ia_istart : Ia_frac : size(Iasij, 1)
                    plot3(DRbranchesij{k}.path(1 : end, 1), DRbranchesij{k}.path(1 : end, 2), DRbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(Ia_muscs{i}, muscles)})
                    plot3(Abranchesij{k}.path(1 : end, 1), Abranchesij{k}.path(1 : end, 2), Abranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(Ia_muscs{i}, muscles)})
                    plot3(Dbranchesij{k}.path(1 : end ,1), Dbranchesij{k}.path(1 : end, 2), Dbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(Ia_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(Ia_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot IIs.
    if with_IIs
        for i = 1 : nmuscs_II
            for j = 1 : nsegs_II
                IIsij = IIs(cellfun(@(x) strcmp(II_muscs{i}, x.musc) & strcmp(II_segs{j}, x.seg), IIs));
                DRbranchesij = cellfun(@(x) x.DRbranch, IIsij, 'UniformOutput', false);
                Abranchesij = cellfun(@(x) x.Abranch, IIsij, 'UniformOutput', false);
                Dbranchesij = cellfun(@(x) x.Dbranch, IIsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, IIsij, 'UniformOutput', false);
                for k = II_istart : II_frac : size(IIsij, 1)
                    plot3(DRbranchesij{k}.path(1 : end, 1), DRbranchesij{k}.path(1 : end, 2), DRbranchesij{k}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(II_muscs{i}, muscles)})
                    plot3(Abranchesij{k}.path(1 : end, 1), Abranchesij{k}.path(1 : end, 2), Abranchesij{k}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(II_muscs{i}, muscles)})
                    plot3(Dbranchesij{k}.path(1 : end, 1), Dbranchesij{k}.path(1 : end, 2), Dbranchesij{k}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(II_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(II_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot DCcBs.
    if with_DCcBs
        for i = 1 : nmuscs_DCcB
            for j = 1 : nsegs_DCcB
                DCcBsij = DCcBs(cellfun(@(x) strcmp(DCcB_muscs{i}, x.musc) & strcmp(DCcB_segs{j}, x.seg), DCcBs));
                DCbranchesij = cellfun(@(x) x.DCbranch, DCcBsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, DCcBsij, 'UniformOutput', false);
                for k = DCcB_istart : DCcB_frac : size(DCcBsij, 1)
                    plot3(DCbranchesij{k}.path(1 : end, 1), DCbranchesij{k}.path(1 : end, 2), DCbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(DCcB_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(DCcB_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot DRcAs.
    if with_DRcAs
        for i = 1 : nmuscs_DRcA
            for j = 1 : nsegs_DRcA
                DRcAsij = DRcAs(cellfun(@(x) strcmp(DRcA_muscs{i}, x.musc) & strcmp(DRcA_segs{j}, x.seg), DRcAs));
                DRbranchesij = cellfun(@(x) x.DRbranch, DRcAsij, 'UniformOutput', false);
                for k = DRcA_istart : DRcA_frac : size(DRcAsij, 1)
                    plot3(DRbranchesij{k}.path(1 : end, 1), DRbranchesij{k}.path(1 : end, 2), DRbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(DRcA_muscs{i}, muscles)})
                end
            end
        end
    end
    
    % Plot CSTs.
    if with_CSTs
        for i = 1 : nmuscs_CST
            for j = 1 : nsegs_CST
                CSTsij = CSTs(cellfun(@(x) strcmp(CST_muscs{i}, x.musc) & strcmp(CST_segs{j}, x.seg), CSTs));
                CSTbranchesij = cellfun(@(x) x.CSTbranch, CSTsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, CSTsij, 'UniformOutput', false);
                for k = CST_istart : CST_frac : size(CSTsij, 1)
                    plot3(CSTbranchesij{k}.path(1 : end, 1), CSTbranchesij{k}.path(1 : end, 2), CSTbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(CST_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(CST_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot STs.
    if with_STs
        for i = 1 : nmuscs_ST
            for j = 1 : nsegs_ST
                STsij = STs(cellfun(@(x) strcmp(ST_muscs{i}, x.musc) & strcmp(ST_segs{j}, x.seg), STs));
                STbranchesij = cellfun(@(x) x.STbranch, STsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, STsij, 'UniformOutput', false);
                for k = ST_istart : ST_frac : size(STsij, 1)
                    plot3(STbranchesij{k}.path(1 : end, 1), STbranchesij{k}.path(1 : end, 2), STbranchesij{k}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(ST_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(ST_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot IaOD3s.
    if with_IaOD3s
        for i = 1 : nmuscs_IaOD3
            for j = 1 : nsegs_IaOD3
                IaOD3sij = IaOD3s(cellfun(@(x) strcmp(IaOD3_muscs{i}, x.musc) & strcmp(IaOD3_segs{j}, x.seg), IaOD3s));
                DRbranchesij = cellfun(@(x) x.DRbranch, IaOD3sij, 'UniformOutput', false);
                Abranchesij = cellfun(@(x) x.Abranch, IaOD3sij, 'UniformOutput', false);
                Dbranchesij = cellfun(@(x) x.Dbranch, IaOD3sij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, IaOD3sij, 'UniformOutput', false);
                for k = IaOD3_istart : IaOD3_frac : size(IaOD3sij, 1)
                    plot3(DRbranchesij{k}.path(1 : end, 1), DRbranchesij{k}.path(1 : end, 2), DRbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    plot3(Abranchesij{k}.path(1 : end, 1), Abranchesij{k}.path(1 : end, 2), Abranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    plot3(Dbranchesij{k}.path(1 : end ,1), Dbranchesij{k}.path(1 : end, 2), Dbranchesij{k}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        plot3(colsij{k}{n}.path(1 : end, 1), colsij{k}{n}.path(1 : end, 2), colsij{k}{n}.path(1 : end, 3), 'LineWidth', 2, 'Color', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot MNs.
    if with_MNs
        for i = 1 : nmuscs_MN
            for j = 1 : nsegs_MN
                MNsij = MNs(cellfun(@(x) strcmp(MN_muscs{i}, x.musc) & strcmp(MN_segs{j}, x.seg), MNs));
                for k = MN_istart : MN_frac : size(MNsij,1)
                    scatter3(MNsij{k}.soma.coords(1), MNsij{k}.soma.coords(2), MNsij{k}.soma.coords(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(MN_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display Ia nodes of Ranvier.
    if Ia_nodes
        for i = 1 : nmuscs_Ia
            for j = 1 : nsegs_Ia
                Iasij = Ias(cellfun(@(x) strcmp(Ia_muscs{i}, x.musc) & strcmp(Ia_segs{j}, x.seg), Ias));
                DRbranchesij = cellfun(@(x) x.DRbranch, Iasij, 'UniformOutput', false);
                Abranchesij = cellfun(@(x) x.Abranch, Iasij, 'UniformOutput', false);
                Dbranchesij = cellfun(@(x) x.Dbranch, Iasij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, Iasij, 'UniformOutput', false);
                for k = Ia_istart : Ia_frac : size(Iasij, 1)
                    scatter3(DRbranchesij{k}.Ncoords(1 : end, 1), DRbranchesij{k}.Ncoords(1 : end, 2), DRbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(Ia_muscs{i}, muscles)})
                    scatter3(Abranchesij{k}.Ncoords(1 : end, 1), Abranchesij{k}.Ncoords(1 : end, 2), Abranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(Ia_muscs{i}, muscles)})
                    scatter3(Dbranchesij{k}.Ncoords(1 : end, 1), Dbranchesij{k}.Ncoords(1 : end, 2), Dbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(Ia_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        scatter3(colsij{k}{n}.Ncoords(1 : end, 1), colsij{k}{n}.Ncoords(1 : end, 2), colsij{k}{n}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(Ia_muscs{i}, muscles)})
                    end
                end
            end
        end
    end

    % Display II nodes of Ranvier.
    if II_nodes
        for i = 1 : nmuscs_II
            for j = 1 : nsegs_II
                IIsij = IIs(cellfun(@(x) strcmp(II_muscs{i}, x.musc) & strcmp(II_segs{j}, x.seg), IIs));
                DRbranchesij = cellfun(@(x) x.DRbranch, IIsij, 'UniformOutput', false);
                for k = II_istart : II_frac : size(IIsij, 1)
                    scatter3(DRbranchesij{k}.Ncoords(1 : end, 1), DRbranchesij{k}.Ncoords(1 : end, 2), DRbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(II_muscs{i}, muscles)})
                    scatter3(DRbranchesij{k}.Icoords(1 : end, 1), DRbranchesij{k}.Icoords(1 : end, 2), DRbranchesij{k}.Icoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(II_muscs{i}, muscles)})
                    scatter3(DRbranchesij{k}.P2coords(1 : end, 1), DRbranchesij{k}.P2coords(1 : end, 2), DRbranchesij{k}.P2coords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(II_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display DCcB nodes of Ranvier.
    if DCcB_nodes
        for i = 1 : nmuscs_DCcB
            for j = 1 : nsegs_DCcB
                DCcBsij = DCcBs(cellfun(@(x) strcmp(DCcB_muscs{i}, x.musc) & strcmp(DCcB_segs{j}, x.seg), DCcBs));
                DCbranchesij = cellfun(@(x) x.DCbranch, DCcBsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, DCcBsij, 'UniformOutput', false);
                for k = DCcB_istart : DCcB_frac : size(DCcBsij, 1)
                    scatter3(DCbranchesij{k}.Ncoords(1 : end, 1), DCbranchesij{k}.Ncoords(1 : end, 2), DCbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DCcB_muscs{i}, muscles)})
                    scatter3(DCbranchesij{k}.Icoords(1 : end, 1), DCbranchesij{k}.Icoords(1 : end, 2), DCbranchesij{k}.Icoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DCcB_muscs{i}, muscles)})
                    scatter3(DCbranchesij{k}.P2coords(1 : end, 1), DCbranchesij{k}.P2coords(1 : end, 2), DCbranchesij{k}.P2coords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DCcB_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        scatter3(colsij{k}{n}.Ncoords(1 : end, 1), colsij{k}{n}.Ncoords(1 : end, 2), colsij{k}{n}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DCcB_muscs{i}, muscles)})
                    end
                end
            end
        end
    end

    % Display DRcA nodes of Ranvier.
    if DRcA_nodes
        for i = 1 : nmuscs_DRcA
            for j = 1 : nsegs_DRcA
                DRcAsij = DRcAs(cellfun(@(x) strcmp(DRcA_muscs{i}, x.musc) & strcmp(DRcA_segs{j}, x.seg), DRcAs));
                DRbranchesij = cellfun(@(x) x.DRbranch, DRcAsij, 'UniformOutput', false);
                for k = DRcA_istart : DRcA_frac : size(DRcAsij, 1)
                    scatter3(DRbranchesij{k}.Ncoords(1 : end, 1), DRbranchesij{k}.Ncoords(1 : end, 2), DRbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DRcA_muscs{i}, muscles)})
                    scatter3(DRbranchesij{k}.Icoords(1 : end, 1), DRbranchesij{k}.Icoords(1 : end, 2), DRbranchesij{k}.Icoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DRcA_muscs{i}, muscles)})
                    scatter3(DRbranchesij{k}.P2coords(1 : end, 1), DRbranchesij{k}.P2coords(1 : end, 2), DRbranchesij{k}.P2coords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(DRcA_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display CST nodes of Ranvier.
    if CST_nodes
        for i = 1 : nmuscs_CST
            for j = 1 : nsegs_CST
                CSTsij = CSTs(cellfun(@(x) strcmp(CST_muscs{i}, x.musc) & strcmp(CST_segs{j}, x.seg), CSTs));
                CSTbranchesij = cellfun(@(x) x.CSTbranch, CSTsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, CSTsij, 'UniformOutput', false);
                for k = CST_istart : CST_frac : size(CSTsij, 1)
                    scatter3(CSTbranchesij{k}.Ncoords(1 : end, 1), CSTbranchesij{k}.Ncoords(1 : end, 2), CSTbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(CST_muscs{i}, muscles)})
                    scatter3(CSTbranchesij{k}.Icoords(1 : end, 1), CSTbranchesij{k}.Icoords(1 : end, 2), CSTbranchesij{k}.Icoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(CST_muscs{i}, muscles)})
                    scatter3(CSTbranchesij{k}.P2coords(1 : end, 1), CSTbranchesij{k}.P2coords(1 : end, 2), CSTbranchesij{k}.P2coords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(CST_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        scatter3(colsij{k}{n}.Ncoords(1 : end, 1), colsij{k}{n}.Ncoords(1 : end, 2), colsij{k}{n}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(CST_muscs{i}, muscles)})
                    end
                end
            end
        end
    end

    % Display ST nodes of Ranvier.
    if ST_nodes
        for i = 1 : nmuscs_ST
            for j = 1 : nsegs_ST
                STsij = STs(cellfun(@(x) strcmp(ST_muscs{i}, x.musc) & strcmp(ST_segs{j}, x.seg), STs));
                STbranchesij = cellfun(@(x) x.STbranch, STsij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, STsij, 'UniformOutput', false);
                for k = ST_istart : ST_frac : size(STsij, 1)
                    scatter3(STbranchesij{k}.Ncoords(1 : end, 1), STbranchesij{k}.Ncoords(1 : end, 2), STbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(ST_muscs{i}, muscles)})
                    scatter3(STbranchesij{k}.Icoords(1 : end, 1), STbranchesij{k}.Icoords(1 : end, 2), STbranchesij{k}.Icoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(ST_muscs{i}, muscles)})
                    scatter3(STbranchesij{k}.P2coords(1 : end, 1), STbranchesij{k}.P2coords(1 : end, 2), STbranchesij{k}.P2coords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(ST_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        scatter3(colsij{k}{n}.Ncoords(1 : end, 1), colsij{k}{n}.Ncoords(1 : end, 2), colsij{k}{n}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(ST_muscs{i}, muscles)})
                    end
                end
            end
        end
    end

    % Display IaOD3 nodes of Ranvier.
    if IaOD3_nodes
        for i = 1 : nmuscs_IaOD3
            for j = 1 : nsegs_IaOD3
                IaOD3sij = IaOD3s(cellfun(@(x) strcmp(IaOD3_muscs{i}, x.musc) & strcmp(IaOD3_segs{j}, x.seg), IaOD3s));
                DRbranchesij = cellfun(@(x) x.DRbranch, IaOD3sij, 'UniformOutput', false);
                Abranchesij = cellfun(@(x) x.Abranch, IaOD3sij, 'UniformOutput', false);
                Dbranchesij = cellfun(@(x) x.Dbranch, IaOD3sij, 'UniformOutput', false);
                colsij = cellfun(@(x) x.cols, IaOD3sij, 'UniformOutput', false);
                for k = IaOD3_istart : IaOD3_frac : size(IaOD3sij, 1)
                    scatter3(DRbranchesij{k}.Ncoords(1 : end, 1), DRbranchesij{k}.Ncoords(1 : end, 2), DRbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    scatter3(Abranchesij{k}.Ncoords(1 : end, 1), Abranchesij{k}.Ncoords(1 : end, 2), Abranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    scatter3(Dbranchesij{k}.Ncoords(1 : end, 1), Dbranchesij{k}.Ncoords(1 : end, 2), Dbranchesij{k}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    for n = 1 : size(colsij{k}, 1)
                        scatter3(colsij{k}{n}.Ncoords(1 : end, 1), colsij{k}{n}.Ncoords(1 : end, 2), colsij{k}{n}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(IaOD3_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
    % Plot MNs axons.
    if MN_axons
        for i = 1 : nmuscs_MN
            for j = 1 : nsegs_MN
                MNsij = MNs(cellfun(@(x) strcmp(MN_muscs{i}, x.musc) & strcmp(MN_segs{j}, x.seg), MNs));
                axonsij = cellfun(@(x) x.axon, MNsij, 'UniformOutput', false);
                for k = MN_istart : MN_frac : size(MNsij,1)
                    plot3(axonsij{k}.path(1 : end, 1), axonsij{k}.path(1 : end, 2), axonsij{k}.path(1 : end, 3), 'LineWidth', 1.2, 'Color', colors{strcmp(MN_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display Motor axon nodes of Ranvier.
    if MN_nodes
        for i = 1 : nmuscs_MN
            for j = 1 : nsegs_MN
                MNsij = MNs(cellfun(@(x) strcmp(MN_muscs{i}, x.musc) & strcmp(MN_segs{j}, x.seg), MNs));
                axonsij = cellfun(@(x) x.axon, MNsij, 'UniformOutput', false);
                for k = MN_istart : MN_frac : size(MNsij, 1)
                    scatter3(axonsij{k}.NCNScoords(:, 1), axonsij{k}.NCNScoords(:, 2), axonsij{k}.NCNScoords(:, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(MN_muscs{i}, muscles)})
                    scatter3(axonsij{k}.NPNScoords(:, 1), axonsij{k}.NPNScoords(:, 2), axonsij{k}.NPNScoords(:, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(MN_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display initial segments.
    if inisegs
        for i = 1 : nmuscs_MN
            for j = 1 : nsegs_MN
                MNsij = MNs(cellfun(@(x) strcmp(MN_muscs{i}, x.musc) & strcmp(MN_segs{j}, x.seg), MNs));
                axonsij = cellfun(@(x) x.axon, MNsij, 'UniformOutput', false);
                for k = MN_istart : MN_frac : size(MNsij, 1)
                    scatter3(axonsij{k}.IScoords(:, 1), axonsij{k}.IScoords(:, 2), axonsij{k}.IScoords(:, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', colors{strcmp(MN_muscs{i}, muscles)})
                end
            end
        end
    end

    % Display dendritic trees.
    if dtrees
        for i = 1 : nmuscs_MN
            for j = 1 : nsegs_MN
                MNsij = MNs(cellfun(@(x) strcmp(MN_muscs{i}, x.musc) & strcmp(MN_segs{j}, x.seg), MNs));
                dtreesij = cellfun(@(x) x.dtree, MNsij, 'UniformOutput', false);
                for k = MN_istart : MN_frac : size(MNsij, 1)
                    for l = 1 : size(dtreesij{k}.dsecs, 1)
                        plot3(dtreesij{k}.dsecs{l}.coords(:, 1), dtreesij{k}.dsecs{l}.coords(:, 2), dtreesij{k}.dsecs{l}.coords(:, 3), 'LineWidth', 1, 'Color', colors{strcmp(MN_muscs{i}, muscles)})
                    end
                end
            end
        end
    end
    
end
