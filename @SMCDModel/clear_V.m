function clear_V(this, entity, varargin)
	% Clear the potential arrays of selected entities.
    %
    % Usage:
    %   smcd.clear_V(entity)
    %   smcd.clear_V(entity, 'musc', musc)
    %   smcd.clear_V(entity, 'seg', seg)
    %   smcd.clear_V(entity, 'musc', musc, 'seg', seg)
    %   smcd.clear_V(entity, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   smcd.clear_V(entity, 'gidx', gidx)
    %   smcd.clear_V(entity, 'AS_idx', AS_idx)
    %
    % Parameters:
    %   entity : string / N*1 cell :
    %       Name(s) of entity(ies).
    %
    %   musc : string / M*1 cell :
    %       Name(s) of muscle(s).
    %
    %   seg : string / S*1 cell :
    %       Name(s) of segment(s).
    %
    %   fidx : F*1 integer :
    %       Family index(es) of entity in (musc, seg) family.
    %
    %   gidx : G*1 integer :
    %       Global index(es) of entity.
    %
    %   AS_idx : K*1 integer :
    %       Index(es) of active site(s).
    %
    % Description: 
    %   Clear the potential arrays of selected entities.
    %   If none of 'musc', 'seg', 'fidx' and 'gidx' are specified, clear
    %   the potentials of all the instances of the specified entities.
    %   If 'musc' alone is specified, clear only the instances belonging to
    %   those specified muscles.
    %   If 'seg' alone is specified, clear only the instances belonging to
    %   those specified segments.
    %   If 'musc' and 'seg' alone are specified, clear only the instances
    %   belonging to those specified (musc, segment) pairs.
    %   If 'fidx' is specified, only those instances indexed by fidx in the
    %   (musc, seg) families are cleared.
    %   If 'gidx' is specified, none of 'musc', 'seg', and 'fidx' should,
    %   and only those instances globally indexed by gidx are cleared.
    %   If 'AS_idx' is not specified, all the potential arrays of the
    %   selected entities should be cleared entirely.
    %   If 'AS_idx' is specified, only those indexes specified by AS_idx
    %   should be cleared.
    
    % Recursive call if entity is cell-array.
    if iscell(entity)
        for i = 1 : size(entity, 1)
            this.clear_V(entity{i}, varargin{:});
        end
        return
    end
    
    % Parse inputs.
    idx = find(strcmp('AS_idx', varargin));
    if ~ isempty(idx)
        AS_idx = varargin{idx + 1};
    else
        AS_idx = linspace(1, this.get_nAS(), this.get_nAS())';
    end
    
    % Recover neural objects.
    objs = this.get_objs(entity, varargin{:});
    
    % Clear potentials. How to proceed depends upon the entity.
    switch entity
        
        case {'Ia', 'II', 'DCcB', 'CST', 'ST', 'IaOD3'}
            for i = 1 : size(objs, 1)
                branches = objs{i}.get_branches();
                for j = 1 : size(branches, 1)
                    branches{j}.VN(AS_idx) = cell(size(AS_idx));
                    branches{j}.VP1(AS_idx) = cell(size(AS_idx));
                    branches{j}.VP2(AS_idx) = cell(size(AS_idx));
                    branches{j}.VI(AS_idx) = cell(size(AS_idx));
                end
            end

        case 'MN'
            for i = 1 : size(objs, 1)
                objs{i}.soma.V(AS_idx) = cell(size(AS_idx));
                if objs{i}.with_axon()
                    objs{i}.axon.VIS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VNCNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VP1CNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VP2CNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VICNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VNPNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VP1PNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VP2PNS(AS_idx) = cell(size(AS_idx));
                    objs{i}.axon.VIPNS(AS_idx) = cell(size(AS_idx));
                end
                if objs{i}.with_dtree()
                    for j = 1 : size(objs{i}.dtree.dsecs, 1)
                        objs{i}.dtree.dsecs{j}.V(AS_idx) = cell(size(AS_idx));
                    end
                end
            end

    end
    
end
