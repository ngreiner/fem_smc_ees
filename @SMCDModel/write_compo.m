function write_compo(this, dir_export, perm)
    % Writes the composition of the SMCDModel to textfile.
    %
    % Parameters:
    %   dir_export : string : path to export directory.
    %   perm : integer : permission level.
    
    % Build format_spec to write the entity distributions.
    format_spec = [strjoin(repmat({'%d'}, 1, this.compo.nsegs), ' ') '\n'];
    
    % Write text.
    file_text = '';
    file_text = [file_text sprintf('Muscles\n')];
    file_text = [file_text sprintf('%s\n', strjoin(this.compo.muscles, ' '))];
    file_text = [file_text sprintf('Segments\n')];
    file_text = [file_text sprintf('%s\n', strjoin(this.compo.segments, ' '))];
    if isfield(this.compo, 'MN_distrib')
        file_text = [file_text sprintf('MN_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.MN_distrib')];
    end
    if isfield(this.compo, 'Ia_distrib')
        file_text = [file_text sprintf('Ia_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.Ia_distrib')];
    end
    if isfield(this.compo, 'II_distrib')
        file_text = [file_text sprintf('II_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.II_distrib')];
    end
    if isfield(this.compo, 'DRcA_distrib')
        file_text = [file_text sprintf('DRcA_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.DRcA_distrib')];
    end
    if isfield(this.compo, 'DRcB_distrib')
        file_text = [file_text sprintf('DRcB_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.DRcB_distrib')];
    end
    if isfield(this.compo, 'DCcA_distrib')
        file_text = [file_text sprintf('DCcA_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.DCcA_distrib')];
    end
    if isfield(this.compo, 'DCcB_distrib')
        file_text = [file_text sprintf('DCcB_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.DCcB_distrib')];
    end
    if isfield(this.compo, 'CST_distrib')
        file_text = [file_text sprintf('CST_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.CST_distrib')];
    end
    if isfield(this.compo, 'ST_distrib')
        file_text = [file_text sprintf('ST_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.ST_distrib')];
    end
    if isfield(this.compo, 'IaOD3_distrib')
        file_text = [file_text sprintf('IaOD3_distrib\n')];
        file_text = [file_text sprintf(format_spec, this.compo.IaOD3_distrib')];
    end
    
    % Write file.
    file_path = fullfile(dir_export, 'composition.txt');
    if perm || ~exist(file_path, 'file')
        fileID = fopen(file_path, 'w');
        fprintf(fileID, file_text);
        fclose(fileID);
    end
    
end
