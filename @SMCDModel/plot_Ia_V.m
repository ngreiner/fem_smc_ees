function plot_Ia_V(this, Ia, AS_idx, varargin)
    % Plot the electric potential along the branches of the specified Ia.
    %
    % Parameters:
    %   Ia : IaFiber object.
    %
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   varargin : fields can be :
    %       'nodes', indicating that nodes of Ranvier should be plotted.
    
    % Parse inputs.
    nodes = any(strcmp('nodes', varargin));

    % Unpack attributes in convenient variables.
    sc = this.fem.sc;
    CS = sc.CS;
    nCS = sc.morpho.nCS;
    rts = sc.roots;
    nrts = size(rts,1);
    branches = Ia.get_branches();

    % Set axis dimensions.
    xmin = -sc.morpho.fat_max_trans_width;
    xmax = +sc.morpho.fat_max_trans_width;
    ymin = -sc.morpho.fat_max_sag_width;
    ymax = +sc.morpho.fat_max_sag_width;
    zmin = -0.05*sc.morpho.seg_length;
    zmax = +1.05*sc.morpho.seg_length;
    axis([xmin xmax ymin ymax zmin zmax])
    hold on
    
    % Plot spinal cord cross-sections.
    for i = 1:nCS
        plot3(CS{i}.GM(:, 1), CS{i}.GM(:, 2), CS{i}.GM(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        plot3(CS{i}.WM(:, 1), CS{i}.WM(:, 2), CS{i}.WM(:, 3), 'LineWidth', 1.2, 'Color', 'black');
    end
    
    % Plot roots' cross-sections.
    for i = 1 : nrts
        DCSi = rts{i}.LD.full_branches{1}.CS;
        VCSi = rts{i}.LV.full_branches{1}.CS;
        for k = 1 : size(DCSi, 1)
            plot3(DCSi{k}(:, 1), DCSi{k}(:, 2), DCSi{k}(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        end
        for k = 1 : size(VCSi,1)
            plot3(VCSi{k}(:, 1), VCSi{k}(:, 2), VCSi{k}(:, 3), 'LineWidth', 1.2, 'Color', 'black');
        end
    end

    % Derive linewidths of the different branches.
    maxD = max(cellfun(@(x) x.diam, branches));
    lw0 = 1.2 ;
    lw = cellfun(@(x) lw0 * x.diam / maxD, branches);

    % Derive intensities of green for line color.
    branchesV = cellfun(@(x) x.get_path_V(AS_idx), branches, 'UniformOutput', false);
    maxV = max(cellfun(@max, branchesV));
    minV = min(cellfun(@min, branchesV));
    green_ints = cellfun(@(x) (maxV - x) / (maxV - minV), branchesV, 'UniformOutput', false);
    
    % Plot Ia with potential-derived colormap.
    for i = 1 : size(branches, 1)
        ps = branches{i}.path;
        greens = green_ints{i};
        g0 = greens(1);
        for j = 2 : size(ps, 1)
            g1 = greens(j);
            g = (g0 + g1) / 2;
            plot3(ps(j - 1 : j, 1), ps(j - 1 : j, 2), ps(j - 1 : j, 3), 'LineWidth', lw(i), 'Color', [1.0, g, 0.0])
        end
    end

    % Display nodes of Ranvier.
    if nodes
        for i = 1 : size(branches, 1)
            scatter3(branches{i}.Ncoords(1 : end, 1), branches{i}.Ncoords(1 : end, 2), branches{i}.Ncoords(1 : end, 3), 6, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'black')
        end
    end
    
end
