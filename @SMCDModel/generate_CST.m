function CST = generate_CST(this, varargin)
    % Generate and return a CSTFiber object.
    %
    % Usage:
    %   CST = generate_CST(this, 'gidx', gidx)
    %   CST = generate_CST(this, 'musc', musc, 'seg', seg, 'fidx', fidx)
    %   CST = generate_CST(this, 'info', musc, seg, fidx)
    %
    % Parameters:
    %   gidx : integer :
    %       Global index of CST.
    %
    %   musc : string :
    %       Muscle of CST.
    %
    %   seg : string :
    %       Segment of CST.
    %       
    %   fidx : integer :
    %       Family index of CST.
    %
    % Return: 
    %   CST : CSTFiber object.
    
    % Parse inputs.
    while ~ isempty(varargin)
        switch varargin{1}
            case 'gidx'
                gidx = varargin{2};
                [musc, seg, fidx] = this.gidx_2_info('CST', gidx);
                varargin(1) = [];
            case 'musc'
                musc = varargin{2};
                seg = varargin{4};
                fidx = varargin{6};
                gidx = this.info_2_gidx('CST', musc, seg, fidx);
                varargin(1 : 5) = [];
            case 'info'
                musc = varargin{2};
                seg = varargin{3};
                fidx = varargin{4};
                gidx = this.info_2_gidx('CST', musc, seg, fidx);
                varargin(1 : 3) = [];
        end
        varargin(1) = [];
    end
    
    % Console message.
    msg = sprintf('  START PROC: generate_CST (gidx=%d)  ', gidx);
    fprintf(Utils.padn(msg, 'both', '*', 70))
    t0 = cputime;
    
    % Seed random number generator for CST.
    rng(Utils.gen_seed(this.name, 'CST', gidx));
    
    % Generate CSTFiber.
    CST = CSTFiber();
    CST.musc = musc;
    CST.seg = seg;
    CST.fidx = fidx;
    CST.gidx = gidx;
    CST.diam = lognrnd(this.morpho.CST.mu, this.morpho.CST.sig);
    CST.model = this.morpho.CST.model;
    CST.assign_MNs(this);
    CST.set_CSTbranch(this);
    CST.set_cols(this);

    fprintf(Utils.padn(sprintf('    END PROC: generate_CST. (Time spent: %f s.)    ', cputime - t0), 'both', '*', 70))
    
end
