function struc = to_struct(this)
	% Return a matlab struct equivalent to the MotorAxon.
    %
    % Usage:
    %   struc = axon.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the MotorAxon.
    struc.label = this.label;
    struc.model = this.model;
    struc.diam = this.diam;
    struc.path = this.path;
    struc.nIS = this.nIS;
    struc.nNCNS = this.nNCNS;
    struc.nP1CNS = this.nP1CNS;
    struc.nP2CNS = this.nP2CNS;
    struc.nICNS = this.nICNS;
    struc.nNPNS = this.nNPNS;
    struc.nP1PNS = this.nP1PNS;
    struc.nP2PNS = this.nP2PNS;
    struc.nIPNS = this.nIPNS;
    struc.IScoords = this.IScoords;
    struc.NCNScoords = this.NCNScoords;
    struc.P1CNScoords = this.P1CNScoords;
    struc.P2CNScoords = this.P2CNScoords;
    struc.ICNScoords = this.ICNScoords;
    struc.NPNScoords = this.NPNScoords;
    struc.P1PNScoords = this.P1PNScoords;
    struc.P2PNScoords = this.P2PNScoords;
    struc.IPNScoords = this.IPNScoords;
    struc.VIS = this.VIS;
    struc.VNCNS = this.VNCNS;
    struc.VP1CNS = this.VP1CNS;
    struc.VP2CNS = this.VP2CNS;
    struc.VICNS = this.VICNS;
    struc.VNPNS = this.VNPNS;
    struc.VP1PNS = this.VP1PNS;
    struc.VP2PNS = this.VP2PNS;
    struc.VIPNS = this.VIPNS;
end
