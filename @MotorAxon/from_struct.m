function from_struct(this, struc)
	% Set the MotorAxon object from the input matlab struct.
    %
    % Usage:
    %   axon.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with same fields as the MotorAxon.
    this.label = struc.label;
    this.model = struc.model;
    this.diam = struc.diam;
    this.path = struc.path;
    this.nIS = struc.nIS;
    this.nNCNS = struc.nNCNS;
    this.nP1CNS = struc.nP1CNS;
    this.nP2CNS = struc.nP2CNS;
    this.nICNS = struc.nICNS;
    this.nNPNS = struc.nNPNS;
    this.nP1PNS = struc.nP1PNS;
    this.nP2PNS = struc.nP2PNS;
    this.nIPNS = struc.nIPNS;
    this.IScoords = struc.IScoords;
    this.NCNScoords = struc.NCNScoords;
    this.P1CNScoords = struc.P1CNScoords;
    this.P2CNScoords = struc.P2CNScoords;
    this.ICNScoords = struc.ICNScoords;
    this.NPNScoords = struc.NPNScoords;
    this.P1PNScoords = struc.P1PNScoords;
    this.P2PNScoords = struc.P2PNScoords;
    this.IPNScoords = struc.IPNScoords;
    this.VIS = struc.VIS;
    this.VNCNS = struc.VNCNS;
    this.VP1CNS = struc.VP1CNS;
    this.VP2CNS = struc.VP2CNS;
    this.VICNS = struc.VICNS;
    this.VNPNS = struc.VNPNS;
    this.VP1PNS = struc.VP1PNS;
    this.VP2PNS = struc.VP2PNS;
    this.VIPNS = struc.VIPNS;
end
