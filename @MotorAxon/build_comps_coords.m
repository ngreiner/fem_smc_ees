function build_comps_coords(this)
    % Build the coordinates of the compartments of the MotorAxon.
    %
    % Usage:
    %   ma.build_comps_coords()
    %
    % Description:
    %   The MotorAxon is composed of three contiguous pieces: the initial
    %   segment, the CNS portion, and the PNS portion.
    %
    %   The initial segment is made of nIS compartments:
    %       IS - IS - ... - IS
    %
    %   The CNS portion is made of the concatenation of nNCNS replicas of
    %   the same sequence:
    %       P1 - P2 - ICNS - ICNS - ICNS - ICNS - ICNS - ICNS - P2 - P1 - N
    %
    %   The PNS portion is made of the concatenation of nNPNS replicas of
    %   the same sequence:
    %       P1 - P2 - IPNS - IPNS - IPNS - IPNS - IPNS - IPNS - P2 - P1 - N
    %   
    %   The positions of the compartments are determined by following the
    %   path of the MotorAxon and distributing the compartments along it
    %   on the basis of their lengths.
    %
    %   This method thus sets the attributes nIS, IScoords, NCNScoords,
    %   P1CNScoords, P2CNScoords, ICNScoords, nNPNS, NPNScoords, nP1PNS,
    %   P1PNScoords, nP2PNS, P2PNScoords, nIPNS, and IPNScoords.
    %   If necessary (when the path of the MotorAxon is too short), nNCNS
    %   and consequently nP1CNS, nP2CNS and nICNS are adjusted.
    
    % Unpack branch path.
    path = this.path;

    % Get compartments lengths.
    [ISlength, Nlength, P1length, P2length, ICNSlength, IPNSlength] = this.get_comps_lengths();
    deltaXCNS = Nlength + 2 * P1length + 2 * P2length + 6 * ICNSlength;
    deltaXPNS = Nlength + 2 * P1length + 2 * P2length + 6 * IPNSlength;

    % Compute length of each path segment, cumulated length and total
    % length. 
    seg_lengths = zeros(size(path, 1) - 1, 1);
    cum_lengths = zeros(size(path, 1) - 1, 1);
    tot_length = 0;
    for i = 1 : size(seg_lengths, 1)
        seg_lengths(i) = norm(path(i + 1, :) - path(i, :));
        tot_length = tot_length + seg_lengths(i);
        cum_lengths(i) = tot_length;
    end

    % Compute distance of each compartment from path origin.
    
    % Initial segment.
    nIS = 3;
    ISdists = [ISlength * 0.5 ; ISlength * 1.5 ; ISlength * 2.5];
    rem_length = tot_length - nIS * ISlength;
    
    % CNS portion.
    nNCNS = 4;
    while rem_length < nNCNS * deltaXCNS
        nNCNS = nNCNS - 1;
    end
    nP1CNS = 2 * nNCNS;
    nP2CNS = 2 * nNCNS;
    nICNS = 6 * nNCNS;
    NCNSdists = zeros(nNCNS, 1);
    P1CNSdists = zeros(nP1CNS, 1);
    P2CNSdists = zeros(nP2CNS, 1);
    ICNSdists = zeros(nICNS, 1);
    for i = 1 : nNCNS
        if i == 1
            P1CNSdists(1) = ISdists(end) + ISlength / 2 + P1length / 2;
        else
            P1CNSdists(2 * i - 1) = NCNSdists(i - 1) + Nlength / 2 + P1length / 2;
        end
        P2CNSdists(2 * i - 1) = P1CNSdists(2 * i - 1) + P1length / 2 + P2length / 2;
        ICNSdists(6 * i - 5) = P2CNSdists(2 * i - 1) + P2length / 2 + ICNSlength / 2;
        ICNSdists(6 * i - 4) = ICNSdists(6 * i - 5) + ICNSlength;
        ICNSdists(6 * i - 3) = ICNSdists(6 * i - 4) + ICNSlength;
        ICNSdists(6 * i - 2) = ICNSdists(6 * i - 3) + ICNSlength;
        ICNSdists(6 * i - 1) = ICNSdists(6 * i - 2) + ICNSlength;
        ICNSdists(6 * i) = ICNSdists(6 * i - 1) + ICNSlength;
        P2CNSdists(2 * i) = ICNSdists(6 * i) + ICNSlength / 2 + P2length / 2;
        P1CNSdists(2 * i) = P2CNSdists(2 * i) + P2length / 2 + P1length / 2;
        NCNSdists(i) = P1CNSdists(2 * i) + P1length / 2 + Nlength / 2;
    end
    rem_length = rem_length - nNCNS * deltaXCNS;
    
    % PNS portions.
    nNPNS = floor(rem_length / deltaXPNS);
    nP1PNS = 2 * nNPNS;
    nP2PNS = 2 * nNPNS;
    nIPNS = 6 * nNPNS;
    NPNSdists = zeros(nNPNS, 1);
    P1PNSdists = zeros(nP1PNS, 1);
    P2PNSdists = zeros(nP2PNS, 1);
    IPNSdists = zeros(nIPNS, 1);
    for i = 1 : nNPNS
        if i == 1
            P1PNSdists(1) = NCNSdists(end) + Nlength / 2 + P1length / 2;
        else
            P1PNSdists(2 * i - 1) = NPNSdists(i - 1) + Nlength / 2 + P1length / 2;
        end
        P2PNSdists(2 * i - 1) = P1PNSdists(2 * i - 1) + P1length / 2 + P2length / 2;
        IPNSdists(6 * i - 5) = P2PNSdists(2 * i - 1) + P2length / 2 + IPNSlength / 2;
        IPNSdists(6 * i - 4) = IPNSdists(6 * i - 5) + IPNSlength;
        IPNSdists(6 * i - 3) = IPNSdists(6 * i - 4) + IPNSlength;
        IPNSdists(6 * i - 2) = IPNSdists(6 * i - 3) + IPNSlength;
        IPNSdists(6 * i - 1) = IPNSdists(6 * i - 2) + IPNSlength;
        IPNSdists(6 * i) = IPNSdists(6 * i - 1) + IPNSlength;
        P2PNSdists(2 * i) = IPNSdists(6 * i) + IPNSlength / 2 + P2length / 2;
        P1PNSdists(2 * i) = P2PNSdists(2 * i) + P2length / 2 + P1length / 2;
        NPNSdists(i) = P1PNSdists(2 * i) + P1length / 2 + Nlength / 2;
    end
    
    % Compute the compartments coordinates.
    
    % We need 1 variable to track the segment path we are roaming, and 1
    % variable to store the unitary vector of this segment (uvect, defined
    % in the first loop below).
    cseg = 1;
    
    % Initial segment.
    IScoords = zeros(nIS, 3);
    for i = 1 : nIS
        while ISdists(i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - ISdists(i));
        IScoords(i, :) = path(cseg, :) + uvect * rdist;
    end
    
    % CNS portion.
    NCNScoords = zeros(nNCNS, 3);
    P1CNScoords = zeros(nP1CNS, 3);
    P2CNScoords = zeros(nP2CNS, 3);
    ICNScoords = zeros(nICNS, 3);    
    for i = 1 : nNCNS
        % P1CNS(2 * i - 1)
        while P1CNSdists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1CNSdists(2 * i - 1));
        P1CNScoords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        % P2CNS(2 * i - 1)
        while P2CNSdists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2CNSdists(2 * i - 1));
        P2CNScoords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        % ICNS(6 * i - 5 : 6 * i)
        for k = 1 : 6
            while ICNSdists(6 * (i - 1) + k) >= cum_lengths(cseg)
                cseg = cseg + 1;
            end
            uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
            rdist = seg_lengths(cseg) - (cum_lengths(cseg) - ICNSdists(6 * (i - 1) + k));
            ICNScoords(6 * (i - 1) + k, :) = path(cseg, :) + uvect * rdist;
        end
        % P2CNS(2 * i)
        while P2CNSdists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2CNSdists(2 * i));
        P2CNScoords(2 * i, :) = path(cseg, :) + uvect * rdist;
        % P1CNS(2 * i)
        while P1CNSdists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1CNSdists(2 * i));
        P1CNScoords(2 * i, :) = path(cseg, :) + uvect * rdist;
        % NCNS(i)
        while NCNSdists(i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - NCNSdists(i));
        NCNScoords(i, :) = path(cseg, :) + uvect * rdist;
    end
    
    % CNS portion.
    NPNScoords = zeros(nNPNS, 3);
    P1PNScoords = zeros(nP1PNS, 3);
    P2PNScoords = zeros(nP2PNS, 3);
    IPNScoords = zeros(nIPNS, 3);    
    for i = 1 : nNPNS
        % P1PNS(2 * i - 1)
        while P1PNSdists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1PNSdists(2 * i - 1));
        P1PNScoords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        % P2PNS(2 * i - 1)
        while P2PNSdists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2PNSdists(2 * i - 1));
        P2PNScoords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        % IPNS(6 * i - 5 : 6 * i)
        for k = 1 : 6
            while IPNSdists(6 * (i - 1) + k) >= cum_lengths(cseg)
                cseg = cseg + 1;
            end
            uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
            rdist = seg_lengths(cseg) - (cum_lengths(cseg) - IPNSdists(6 * (i - 1) + k));
            IPNScoords(6 * (i - 1) + k, :) = path(cseg, :) + uvect * rdist;
        end
        % P2PNS(2 * i)
        while P2PNSdists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2PNSdists(2 * i));
        P2PNScoords(2 * i, :) = path(cseg, :) + uvect * rdist;
        % P1PNS(2 * i)
        while P1PNSdists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1PNSdists(2 * i));
        P1PNScoords(2 * i, :) = path(cseg, :) + uvect * rdist;
        % NPNS(i)
        while NPNSdists(i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - NPNSdists(i));
        NPNScoords(i, :) = path(cseg, :) + uvect * rdist;
    end
    
    % Assign values to attributes.
    this.nIS = nIS;
    this.IScoords = IScoords;
    this.nNCNS = nNCNS;
    this.NCNScoords = NCNScoords;
    this.nP1CNS = nP1CNS;
    this.P1CNScoords = P1CNScoords;
    this.nP2CNS = nP2CNS;
    this.P2CNScoords = P2CNScoords;
    this.nICNS = nICNS;
    this.ICNScoords = ICNScoords;
    this.nNPNS = nNPNS;
    this.NPNScoords = NPNScoords;
    this.nP1PNS = nP1PNS;
    this.P1PNScoords = P1PNScoords;
    this.nP2PNS = nP2PNS;
    this.P2PNScoords = P2PNScoords;
    this.nIPNS = nIPNS;
    this.IPNScoords = IPNScoords;

end
