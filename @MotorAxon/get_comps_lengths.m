function [ISlength, Nlength, P1length, P2length, ICNSlength, IPNSlength] = get_comps_lengths(this)
    % Return the lengths of the inisegs, nodes, paranodes1, paranodes2, CNS
    % internodes and PNS internodes of the MotorAxon.
    %
    % Usage:
    %   [ISlength, Nlength, P1length, P2length, ICNSlength, ...
    %                               IPNSlength] = ma.get_comps_lengths()
    %
    % Return:
    %   ISlength : double : length of MotorAxon's inisegs.
    %   Nlength : double : length of MotorAxon's nodes.
    %   P1length : double : length of MotorAxon's paranodes1.
    %   P2length : double : length of MotorAxon's paranodes2.
    %   ICNSlength : double : length of MotorAxon's CNS internodes.
    %   IPNSlength : double : length of MotorAxon's PNS internodes.
    %
    % Description:
    %   The lengths of the nodes, paranodes1, paranodes2 and internodes
    %   are obtained almost similarly for the MotorAxon and for the Branch
    %   classes (MRG model, see Branch class). The only difference is that
    %   the lengths of the CNS internodes are three times less that of the
    %   PNS internodes.
    %   Besides, the initial segments lengths obey to a linear relationship
    %   with the MotorAxon diameter (McIntyre and Grill, 2002).
    
    % The initial segments lengths obey to a linear relationship with the
    % MotorAxon diameter.
    ISlength = 10 / 11.5 * this.diam;
    
    % McIntyre's database for ModelC fiber models.
    diams = [0; 5.7; 7.3; 8.7; 10; 11.5; 12.8; 14; 15; 16] * 1e-3;
    P2lengths = [0; 35; 38; 40; 46; 50; 54; 56; 58; 60] * 1e-3;
    deltaXs = [0; 500; 750; 1000; 1150; 1250; 1350; 1400; 1450; 1500] * 1e-3;

    % Piecewise linear interpolants for P2length and deltaX.
    PP2length = mkpp(diams, [diff(P2lengths) ./ diff(diams), P2lengths(1 : end - 1)]);
    PdeltaX = mkpp(diams, [diff(deltaXs) ./ diff(diams), deltaXs(1 : end - 1)]);
    
    % Interpolate compartments lengths according to fiber model.
    Nlength = 1e-3;
    deltaX = ppval(PdeltaX, this.diam);
    if regexp(this.model, 'ModelC|ModelD')
        P1length = 3e-3;
        P2length = ppval(PP2length, this.diam);
        IPNSlength = (deltaX - 2 * P1length - 2 * P2length - Nlength) / 6;
    elseif strcmp(this.fiberModel, 'ModelB')
        P1length = deltaX / 10;
        P2length =  deltaX / 10;
        IPNSlength =  deltaX / 10;
    else
        error('Unknown model.')
    end
    ICNSlength = IPNSlength / 3;
    
end
