function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if abs(obj1.diam - obj2.diam) >= 1e-6
        fprintf('Attributes `diam` are different.\n')
        return
    
    elseif xor(isempty(obj1.path), isempty(obj2.path)) || max(max(abs(obj1.path - obj2.path))) >= 1e-6;
        fprintf('Attributes `path` are different.\n')
        return
    
    elseif ~ (obj1.nIS == obj2.nIS)
        fprintf('Attributes `nIS` are different.\n')
        return
    
    elseif ~ (isempty(obj1.IScoords) && isempty(obj2.IScoords)) && ~ (max(max(abs(obj1.IScoords - obj2.IScoords))) < 1e-6)
        fprintf('Attributes `IScoords` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) ~(isempty(x) && isempty(y)) && ~(max(abs(x - y)) < 1e-6), obj1.VIS, obj2.VIS))
        fprintf('Attributes `VIS` are different.\n')
        return

    elseif ~ (obj1.nNCNS == obj2.nNCNS)
        fprintf('Attributes `nNCNS` are different.\n')
        return

    elseif ~ (obj1.nP1CNS == obj2.nP1CNS)
        fprintf('Attributes `nP1CNS` are different.\n')
        return

    elseif ~ (obj1.nP2CNS == obj2.nP2CNS)
        fprintf('Attributes `nP2CNS` are different.\n')
        return

    elseif ~ (obj1.nICNS == obj2.nICNS)
        fprintf('Attributes `nICNS` are different.\n')
        return
    
    elseif xor(isempty(obj1.NCNScoords), isempty(obj2.NCNScoords)) || ~ isempty(obj1.NCNScoords) && max(max(abs(obj1.NCNScoords - obj2.NCNScoords))) >= 1e-6;
        fprintf('Attributes `NCNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.P1CNScoords), isempty(obj2.P1CNScoords)) || ~ isempty(obj1.P1CNScoords) && max(max(abs(obj1.P1CNScoords - obj2.P1CNScoords))) >= 1e-6;
        fprintf('Attributes `P1CNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.P2CNScoords), isempty(obj2.P2CNScoords)) || ~ isempty(obj1.P2CNScoords) && max(max(abs(obj1.P2CNScoords - obj2.P2CNScoords))) >= 1e-6;
        fprintf('Attributes `P2CNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.ICNScoords), isempty(obj2.ICNScoords)) || ~ isempty(obj1.ICNScoords) && max(max(abs(obj1.ICNScoords - obj2.ICNScoords))) >= 1e-6;
        fprintf('Attributes `ICNScoords` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VNCNS, obj2.VNCNS));
        fprintf('Attributes `VNCNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP1CNS, obj2.VP1CNS));
        fprintf('Attributes `VP1CNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP2CNS, obj2.VP2CNS));
        fprintf('Attributes `VP2CNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VICNS, obj2.VICNS));
        fprintf('Attributes `VICNS` are different.\n')
        return

    elseif ~ (obj1.nNPNS == obj2.nNPNS)
        fprintf('Attributes `nNPNS` are different.\n')
        return

    elseif ~ (obj1.nP1PNS == obj2.nP1PNS)
        fprintf('Attributes `nP1PNS` are different.\n')
        return

    elseif ~ (obj1.nP2PNS == obj2.nP2PNS)
        fprintf('Attributes `nP2PNS` are different.\n')
        return

    elseif ~ (obj1.nIPNS == obj2.nIPNS)
        fprintf('Attributes `nIPNS` are different.\n')
        return
    
    elseif xor(isempty(obj1.NPNScoords), isempty(obj2.NPNScoords)) || ~ isempty(obj1.NPNScoords) && max(max(abs(obj1.NPNScoords - obj2.NPNScoords))) >= 1e-6;
        fprintf('Attributes `NPNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.P1PNScoords), isempty(obj2.P1PNScoords)) || ~ isempty(obj1.P1PNScoords) && max(max(abs(obj1.P1PNScoords - obj2.P1PNScoords))) >= 1e-6;
        fprintf('Attributes `P1PNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.P2PNScoords), isempty(obj2.P2PNScoords)) || ~ isempty(obj1.P2PNScoords) && max(max(abs(obj1.P2PNScoords - obj2.P2PNScoords))) >= 1e-6;
        fprintf('Attributes `P2PNScoords` are different.\n')
        return
    
    elseif xor(isempty(obj1.IPNScoords), isempty(obj2.IPNScoords)) || ~ isempty(obj1.IPNScoords) && max(max(abs(obj1.IPNScoords - obj2.IPNScoords))) >= 1e-6;
        fprintf('Attributes `IPNScoords` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VNPNS, obj2.VNPNS));
        fprintf('Attributes `VNPNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP1PNS, obj2.VP1PNS));
        fprintf('Attributes `VP1PNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP2PNS, obj2.VP2PNS));
        fprintf('Attributes `VP2PNS` are different.\n')
        return
    
    elseif any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VIPNS, obj2.VIPNS));
        fprintf('Attributes `VIPNS` are different.\n')
        return
        
    end
    
    outbool = true;
    
end