function interp_V(this, smcd, perm)
    % Interpolate the MotorAxon compartments' electric potentials.
    %
    % Usage:
    %   branch.interp_V(smcd)
    %   branch.interp_V(smcd, perm)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Potentials are interpolated in the Comsol Model attached to the
    %   input SMCDModel thanks to the coordinates of the compartments of
    %   the MotorAxon.  
    %   Depending upon the value of smcd.params.interp_type, the potentials
    %   at the compartments are either all directly obtained by
    %   interpolation in the Comsol Model, either are obtained from Comsol
    %   for some of them and the others by linear interpolation from the
    %   first ones.
    %
    %   This method sets the fields VIS{AS_idx}, VNCNS{AS_idx},
    %   VP1CNS{AS_idx}, VP2CNS{AS_idx}, VICNS{AS_idx},  VNPNS{AS_idx},
    %   VP1PNS{AS_idx}, VP2PNS{AS_idx} and VIPNS{AS_idx} of the
    %   MotorAxon (where AS_idx = smcd.fem.AS.idx). 
    
    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    if smcd.fem.params.physics.with_curvi_coords
        dset = 'dset2';
    else
        dset = 'dset1';
    end
    
    % Unpack attributes in convenient variables.
    idx = smcd.fem.AS.idx;
    CM = smcd.fem.CM;
    IScoords = this.IScoords * 1e-3;
    NCNScoords = this.NCNScoords * 1e-3;
    P1CNScoords = this.P1CNScoords * 1e-3;
    P2CNScoords = this.P2CNScoords * 1e-3;
    ICNScoords = this.ICNScoords * 1e-3;
    NPNScoords = this.NPNScoords * 1e-3;
    P1PNScoords = this.P1PNScoords * 1e-3;
    P2PNScoords = this.P2PNScoords * 1e-3;
    IPNScoords = this.IPNScoords * 1e-3;
    [ISlength, Nlength, P1length, P2length, ICNSlength, IPNSlength] = this.get_comps_lengths();
    
    % If potentials are already interpolated, interpolate anew only with
    % permission. 
    if this.is_interp(idx)
        fprintf('%s already interpolated for AS_idx #%d.\n', class(this), idx)
        if (perm == 0) || (perm == 1)
            fprintf('Writing permission = %d. Exiting method without interpolating.\n', perm)
            return
        elseif strcmp(perm, 'i') && ~ Utils.prompt_confirm('Do you want to overwrite?')
            fprintf('Exiting method without interpolating.\n')
            return
        end
        fprintf('Overwriting fibers''potentials.\n')
    end
    
    % Initial segment potentials.
    VIS = zeros(this.nIS, 1);
    for k = 1 : this.nIS
        VIS(k) = mphinterp(CM, 'V', 'coord', IScoords(k, :)', 'dataset', dset);
    end
    
    % CNS portion potentials.
    switch smcd.params.interp_type

        case 'full'
            % NCNS potentials.
            VNCNS = zeros(this.nNCNS, 1);
            for k = 1 : this.nNCNS
                VNCNS(k) = mphinterp(CM, 'V', 'coord', NCNScoords(k, :)', 'dataset', dset);
            end
            % P1CNS potentials.
            VP1CNS = zeros(this.nP1CNS, 1);
            for k = 1 : this.nP1CNS
                VP1CNS(k) = mphinterp(CM, 'V', 'coord', P1CNScoords(k, :)', 'dataset', dset);
            end
            % P2CNS potentials.
            VP2CNS = zeros(this.nP2CNS, 1);
            for k = 1 : this.nP2CNS
                VP2CNS(k) = mphinterp(CM, 'V', 'coord', P2CNScoords(k, :)', 'dataset', dset);
            end
            % ICNS potentials.
            VICNS = zeros(this.nICNS, 1);
            for k = 1 : this.nICNS
                VICNS(k) = mphinterp(CM, 'V', 'coord', ICNScoords(k, :)', 'dataset', dset);
            end

        case 'mixed'
            % NCNS potentials. Interpolate in Comsol Model.
            VNCNS = zeros(this.nNCNS, 1);
            for k = 1 : this.nNCNS
                VNCNS(k) = mphinterp(CM, 'V', 'coord', NCNScoords(k, :)', 'dataset', dset);
            end
            % P2CNS potentials. Interpolate in Comsol Model.
            VP2CNS = zeros(this.nP2CNS, 1);
            for k = 1 : this.nP2CNS
                VP2CNS(k) = mphinterp(CM, 'V', 'coord', P2CNScoords(k, :)', 'dataset', dset);
            end        
            % P1CNS potentials.
            VP1CNS = zeros(this.nP1CNS, 1);
            % Interpolate from neighboring iniseg and paranode2 for the
            % first one.
            w1 = (P1length / 2 + P2length / 2) / (ISlength / 2 + P1length + P2length / 2);
            w2 = (P1length / 2 + ISlength / 2) / (ISlength / 2 + P1length + P2length / 2);
            VP1CNS(1) = w1 * VIS(end) + w2 * VP2CNS(1);
            % Interpolate from neighboring node and paranode2 for the
            % subsequent ones.
            w1 = (P1length / 2 + P2length / 2) / (Nlength / 2 + P1length + P2length / 2);
            w2 = (P1length / 2 + Nlength / 2) / (Nlength / 2 + P1length + P2length / 2);
            VP1CNS(2) = w1 * VNCNS(1) + w2 * VP2CNS(2);
            for k = 2 : this.nNCNS
                VP1CNS(2 * k - 1) = w1 * VNCNS(k - 1) + w2 * VP2CNS(2 * k - 1);
                VP1CNS(2 * k) = w1 * VNCNS(k) + w2 * VP2CNS(2 * k);
            end
            % ICNS potentials. 
            VICNS = zeros(this.nICNS, 1);            
            % Interpolate in Comsol Model for the 2nd and 5th internodes of
            % each internodal section.
            for k = 1 : this.nNCNS
                VICNS(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', ICNScoords(6 * (k - 1) + 2, :)', 'dataset', dset);
                VICNS(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', ICNScoords(6 * (k - 1) + 5, :)', 'dataset', dset);
            end
            % Interpolate from neighboring compartments for 1st, 3rd, 4th
            % and 6th.            
            w1 = (ICNSlength / 2 + P2length / 2) / (P2length / 2 + 3 * ICNSlength / 2);
            w2 = ICNSlength / (P2length / 2 + 3 * ICNSlength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nNCNS
                VICNS(6 * (k - 1) + 1) = w2 * VP2CNS(2 * k - 1) + w1 * VICNS(6 * (k - 1) + 2);
                VICNS(6 * (k - 1) + 3) = w4 * VICNS(6 * (k - 1) + 2) + w3 * VICNS(6 * (k - 1) + 5);
                VICNS(6 * (k - 1) + 4) = w3 * VICNS(6 * (k - 1) + 2) + w4 * VICNS(6 * (k - 1) + 5);
                VICNS(6 * (k - 1) + 6) = w2 * VP2CNS(2 * k) + w1 * VICNS(6 * (k - 1) + 5);
            end

        case 'poor'
            % NCNS potentials. Interpolate in Comsol Model.
            VNCNS = zeros(this.nNCNS, 1);
            for k = 1 : this.nNCNS
                VNCNS(k) = mphinterp(CM, 'V', 'coord', NCNScoords(k, :)', 'dataset', dset);
            end
            % ICNS potentials.
            VICNS = zeros(this.nICNS, 1);            
            % Interpolate in Comsol Model for the 2nd and 5th internodes of
            % each internodal section.
            for k = 1 : this.nNCNS
                VICNS(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', Icoords(6 * (k - 1) + 2, 1)', 'dataset', dset);
                VICNS(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', Icoords(6 * (k - 1) + 5, 1)', 'dataset', dset);
            end
            % Interpolate from 2nd internode and last iniseg for 1st
            % internode.
            w1 = ICNSlength / (ISlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            w2 = (ISlength / 2 + P1length + P2length + ICNSlength / 2) / (ISlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            VICNS(1) = w1 * VIS(end) + w2 * VICNS(2);            
            % Interpolate from neighboring internodes and nodes for
            % subsequent internodes.
            w1 = (Nlength / 2 + P1length + P2length + ICNSlength / 2) / (Nlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            w2 = ICNSlength / (Nlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nNCNS
                if k ~= 1
                    VICNS(6 * (k - 1) + 1) = w1 * VICNS(6 * (k - 1) + 2) + w2 * VNCNS(k);
                end
                VICNS(6 * (k - 1) + 3) = w4 * VICNS(6 * (k - 1) + 2) + w3 * VICNS(6 * (k - 1) + 5);
                VICNS(6 * (k - 1) + 4) = w3 * VICNS(6 * (k - 1) + 2) + w4 * VICNS(6 * (k - 1) + 5);
                VICNS(6 * (k - 1) + 6) = w2 * VNCNS(k + 1) + w1 * VICNS(6 * (k - 1) + 5);
            end
            % P1CNS potentials. 
            VP1CNS = zeros(this.nP1CNS, 1);
            % Interpolate from 2nd internode and last iniseg for 1st one.
            w1 = (P1length / 2 + P2length + 3 * ICNSlength / 2) / (ISlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            w2 = (ISlength / 2 + P1length / 2) / (ISlength / 2 + P1length + P2length + 3 * ICNSlength / 2);
            VP1CNS(1) = w1 * VIS(end) + w2 * VICNS(2);
            % Interpolate from neighboring nodes and internodes for
            % subsequent ones.
            w1 = (Nlength / 2 + P1length / 2) / (Nlength / 2 + P1length + P2length + ICNSlength / 2);
            w2 = (ICNSlength / 2 + P2length + P1length / 2) / (Nlength / 2 + P1length + P2length + ICNSlength / 2);
            for k = 1 : this.nNCNS
                if k ~= 1
                    VP1CNS(2 * k - 1) = w2 * VNCNS(k) + w1 * VICNS(6 * (k - 1) + 1);
                end
                VP1CNS(2 * k) = w2 * VNCNS(k + 1) + w1 * VICNS(6 * (k - 1) + 6);
            end
            % P2CNS potentials. Interpolate from neighboring paranodes1 and
            % internodes.
            VP2CNS = zeros(this.nP2CNS, 1);
            w1 = (P1length / 2 + P2length / 2) / (P1length / 2 + P2length + ICNSlength / 2);
            w2 = (ICNSlength / 2 + P2length / 2) / (P1length / 2 + P2length + ICNSlength / 2);
            for k = 1 : this.nNCNS
                VP2CNS(2 * k - 1) = w2 * VP1CNS(2 * k - 1) + w1 * VICNS(6 * (k - 1) + 1);
                VP2CNS(2 * k) = w2 * VP1CNS(2 * k) + w1 * VICNS(6 * (k - 1) + 6);
            end

    end

    % PNS portion potentials.
    switch smcd.params.interp_type

        case 'full'
            % NPNS potentials.
            VNPNS = zeros(this.nNPNS, 1);
            for k = 1 : this.nNPNS
                VNPNS(k) = mphinterp(CM, 'V', 'coord', NPNScoords(k, :)', 'dataset', dset);
            end
            % P1PNS potentials.
            VP1PNS = zeros(this.nP1PNS, 1);
            for k = 1 : this.nP1PNS
                VP1PNS(k) = mphinterp(CM, 'V', 'coord', P1PNScoords(k, :)', 'dataset', dset);
            end
            % P2PNS potentials.
            VP2PNS = zeros(this.nP2PNS, 1);
            for k = 1 : this.nP2PNS
                VP2PNS(k) = mphinterp(CM, 'V', 'coord', P2PNScoords(k, :)', 'dataset', dset);
            end
            % IPNS potentials.
            VIPNS = zeros(this.nIPNS, 1);
            for k = 1 : this.nIPNS
                VIPNS(k) = mphinterp(CM, 'V', 'coord', IPNScoords(k, :)', 'dataset', dset);
            end

        case 'mixed'
            % NPNS potentials. Interpolate in Comsol Model.
            VNPNS = zeros(this.nNPNS, 1);
            for k = 1 : this.nNPNS
                VNPNS(k) = mphinterp(CM, 'V', 'coord', NPNScoords(k, :)', 'dataset', dset);
            end
            % P2PNS potentials. Interpolate in Comsol Model.
            VP2PNS = zeros(this.nP2PNS, 1);
            for k = 1 : this.nP2PNS
                VP2PNS(k) = mphinterp(CM, 'V', 'coord', P2PNScoords(k, :)', 'dataset', dset);
            end        
            % P1PNS potentials.
            VP1PNS = zeros(this.nP1PNS, 1);
            % Interpolate from neighboring paranode2 and last NCNS for the
            % first one.
            w1 = (P1length / 2 + P2length / 2) / (Nlength / 2 + P1length + P2length / 2);
            w2 = (P1length / 2 + Nlength / 2) / (Nlength / 2 + P1length + P2length / 2);
            VP1PNS(1) = w1 * VNCNS(end) + w2 * VP2PNS(1);
            % Interpolate from neighboring node and paranode2 for the
            % subsequent ones.
            w1 = (P1length / 2 + P2length / 2) / (Nlength / 2 + P1length + P2length / 2);
            w2 = (P1length / 2 + Nlength / 2) / (Nlength / 2 + P1length + P2length / 2);
            VP1PNS(2) = w1 * VNPNS(1) + w2 * VP2PNS(2);
            for k = 2 : this.nNPNS
                VP1PNS(2 * k - 1) = w1 * VNPNS(k - 1) + w2 * VP2PNS(2 * k - 1);
                VP1PNS(2 * k) = w1 * VNPNS(k) + w2 * VP2PNS(2 * k);
            end
            % IPNS potentials. 
            VIPNS = zeros(this.nIPNS, 1);            
            % Interpolate in Comsol Model for the 2nd and 5th internodes of
            % each internodal section.
            for k = 1 : this.nNPNS
                VIPNS(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', IPNScoords(6 * (k - 1) + 2, :)', 'dataset', dset);
                VIPNS(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', IPNScoords(6 * (k - 1) + 5, :)', 'dataset', dset);
            end
            % Interpolate from neighboring compartments for 1st, 3rd, 4th
            % and 6th.            
            w1 = (IPNSlength / 2 + P2length / 2) / (P2length / 2 + 3 * IPNSlength / 2);
            w2 = IPNSlength / (P2length / 2 + 3 * IPNSlength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nNPNS
                VIPNS(6 * (k - 1) + 1) = w2 * VP2PNS(2 * k - 1) + w1 * VIPNS(6 * (k - 1) + 2);
                VIPNS(6 * (k - 1) + 3) = w4 * VIPNS(6 * (k - 1) + 2) + w3 * VIPNS(6 * (k - 1) + 5);
                VIPNS(6 * (k - 1) + 4) = w3 * VIPNS(6 * (k - 1) + 2) + w4 * VIPNS(6 * (k - 1) + 5);
                VIPNS(6 * (k - 1) + 6) = w2 * VP2PNS(2 * k) + w1 * VIPNS(6 * (k - 1) + 5);
            end

        case 'poor'
            % NPNS potentials. Interpolate in Comsol Model.
            VNPNS = zeros(this.nNPNS, 1);
            for k = 1 : this.nNPNS
                VNPNS(k) = mphinterp(CM, 'V', 'coord', NPNScoords(k, :)', 'dataset', dset);
            end
            % IPNS potentials.
            VIPNS = zeros(this.nIPNS, 1);            
            % Interpolate in Comsol Model for the 2nd and 5th internodes of
            % each internodal section.
            for k = 1 : this.nNPNS
                VIPNS(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', Icoords(6 * (k - 1) + 2, 1)', 'dataset', dset);
                VIPNS(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', Icoords(6 * (k - 1) + 5, 1)', 'dataset', dset);
            end
            % Interpolate from 2nd internode and last NCNS for 1st
            % internode.
            w1 = IPNSlength / (Nlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            w2 = (Nlength / 2 + P1length + P2length + IPNSlength / 2) / (Nlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            VIPNS(1) = w1 * VNCNS(end) + w2 * VIPNS(2);            
            % Interpolate from neighboring internodes and nodes for
            % subsequent internodes.
            w1 = (Nlength / 2 + P1length + P2length + IPNSlength / 2) / (Nlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            w2 = IPNSlength / (Nlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nNPNS
                if k ~= 1
                    VIPNS(6 * (k - 1) + 1) = w1 * VIPNS(6 * (k - 1) + 2) + w2 * VNPNS(k);
                end
                VIPNS(6 * (k - 1) + 3) = w4 * VIPNS(6 * (k - 1) + 2) + w3 * VIPNS(6 * (k - 1) + 5);
                VIPNS(6 * (k - 1) + 4) = w3 * VIPNS(6 * (k - 1) + 2) + w4 * VIPNS(6 * (k - 1) + 5);
                VIPNS(6 * (k - 1) + 6) = w2 * VNPNS(k + 1) + w1 * VIPNS(6 * (k - 1) + 5);
            end
            % P1PNS potentials. 
            VP1PNS = zeros(this.nP1PNS, 1);
            % Interpolate from 2nd internode and last iniseg for 1st one.
            w1 = (P1length / 2 + P2length + 3 * IPNSlength / 2) / (ISlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            w2 = (ISlength / 2 + P1length / 2) / (ISlength / 2 + P1length + P2length + 3 * IPNSlength / 2);
            VP1PNS(1) = w1 * VIS(end) + w2 * VIPNS(2);
            % Interpolate from neighboring nodes and internodes for
            % subsequent ones.
            w1 = (Nlength / 2 + P1length / 2) / (Nlength / 2 + P1length + P2length + IPNSlength / 2);
            w2 = (IPNSlength / 2 + P2length + P1length / 2) / (Nlength / 2 + P1length + P2length + IPNSlength / 2);
            for k = 1 : this.nNPNS
                if k ~= 1
                    VP1PNS(2 * k - 1) = w2 * VNPNS(k) + w1 * VIPNS(6 * (k - 1) + 1);
                end
                VP1PNS(2 * k) = w2 * VNPNS(k + 1) + w1 * VIPNS(6 * (k - 1) + 6);
            end
            % P2PNS potentials. Interpolate from neighboring paranodes1 and
            % internodes.
            VP2PNS = zeros(this.nP2PNS, 1);
            w1 = (P1length / 2 + P2length / 2) / (P1length / 2 + P2length + IPNSlength / 2);
            w2 = (IPNSlength / 2 + P2length / 2) / (P1length / 2 + P2length + IPNSlength / 2);
            for k = 1 : this.nNPNS
                VP2PNS(2 * k - 1) = w2 * VP1PNS(2 * k - 1) + w1 * VIPNS(6 * (k - 1) + 1);
                VP2PNS(2 * k) = w2 * VP1PNS(2 * k) + w1 * VIPNS(6 * (k - 1) + 6);
            end

    end
    
    % Rectify potentials so that they are expressed in mV and for a
    % stimulation amplitude of 1uA and store in dedicated arrays.
    VIS = VIS / smcd.fem.AS.area * 1e-3;
    VNCNS = VNCNS / smcd.fem.AS.area * 1e-3;
    VP1CNS = VP1CNS / smcd.fem.AS.area * 1e-3;
    VP2CNS = VP2CNS / smcd.fem.AS.area * 1e-3;
    VICNS = VICNS / smcd.fem.AS.area * 1e-3;
    VNPNS = VNPNS / smcd.fem.AS.area * 1e-3;
    VP1PNS = VP1PNS / smcd.fem.AS.area * 1e-3;
    VP2PNS = VP2PNS / smcd.fem.AS.area * 1e-3;
    VIPNS = VIPNS / smcd.fem.AS.area * 1e-3;
    
    % Assign to Branch attributes.
    if ~ isempty(VNCNS)
        this.VIS{idx} = VIS;
        this.VNCNS{idx} = VNCNS;
        this.VP1CNS{idx} = VP1CNS;
        this.VP2CNS{idx} = VP2CNS;
        this.VICNS{idx} = VICNS;
        this.VNPNS{idx} = VNPNS;
        this.VP1PNS{idx} = VP1PNS;
        this.VP2PNS{idx} = VP2PNS;
        this.VIPNS{idx} = VIPNS;
    end

end
