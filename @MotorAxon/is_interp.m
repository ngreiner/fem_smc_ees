function outbool = is_interp(this, AS_idx, varargin)
    % Return a logical indicating whether the MotorAxon is already well
    % interpolated for the input AS_idx.
    %
    % Usage:
    %   outbool = ma.is_interp(AS_idx)
    %   outbool = ma.is_interp(AS_idx, 'IS')
    %   outbool = ma.is_interp(AS_idx, 'NCNS')
    %   outbool = ma.is_interp(AS_idx, 'P1CNS')
    %   outbool = ma.is_interp(AS_idx, 'P2CNS')
    %   outbool = ma.is_interp(AS_idx, 'ICNS')
    %   outbool = ma.is_interp(AS_idx, 'NPNS')
    %   outbool = ma.is_interp(AS_idx, 'P1PNS')
    %   outbool = ma.is_interp(AS_idx, 'P2PNS')
    %   outbool = ma.is_interp(AS_idx, 'IPNS')
    %
    % Parameters:
    %   AS_idx : double.
    %
    % Return:
    %   outbool : logical.
    %
    % Description:
    %   If the MotorAxon lacks the potential array for one of its IS, NCNS,
    %   P1CNS, P2CNS, ICNS, NPNS, P1PNS, P2PNS or IPNS, the MotorAxon is
    %   not well interpolated. 
    %
    %   If one of 'IS', 'NCNS', 'P1CNS', 'P2CNS', 'ICNS', 'NPNS', 'P1PNS',
    %   'P2PNS' or 'IPNS',  is given in varargin, the test should be
    %   performed solely on the IS, NCNS, P1CNS, P2CNS, ICNS, NPNS, P1PNS,
    %   P2PNS or IPNS respectively.
    %
    %   Otherwise it is the conjunction of all of them.
    
    if strcmp('IS', varargin)
        outbool = ~isempty(this.VIS{AS_idx});
        return
    elseif strcmp('NCNS', varargin)
        outbool = ~isempty(this.VNCNS{AS_idx});
        return
    elseif strcmp('P1CNS', varargin)
        outbool = ~isempty(this.VP1CNS{AS_idx});
        return
    elseif strcmp('P2CNS', varargin)
        outbool = ~isempty(this.VP2CNS{AS_idx});
        return
    elseif strcmp('ICNS', varargin)
        outbool = ~isempty(this.VICNS{AS_idx});
        return
    elseif strcmp('NPNS', varargin)
        outbool = ~isempty(this.VNPNS{AS_idx});
        return
    elseif strcmp('P1PNS', varargin)
        outbool = ~isempty(this.VP1PNS{AS_idx});
        return
    elseif strcmp('P2PNS', varargin)
        outbool = ~isempty(this.VP2PNS{AS_idx});
        return
    elseif strcmp('IPNS', varargin)
        outbool = ~isempty(this.VIPNS{AS_idx});
        return
    end
    
    outbool = this.is_interp(AS_idx, 'IS') && ...
              this.is_interp(AS_idx, 'NCNS') && ...
              this.is_interp(AS_idx, 'P1CNS') && ...
              this.is_interp(AS_idx, 'P2CNS') && ...
              this.is_interp(AS_idx, 'ICNS') && ...
              this.is_interp(AS_idx, 'NPNS') && ...
              this.is_interp(AS_idx, 'P1PNS') && ...
              this.is_interp(AS_idx, 'P2PNS') && ...
              this.is_interp(AS_idx, 'IPNS');
          
end
