function [longi_abs, longi_V] = get_longi_V(this, AS_idx, varargin)
    % Interpolates the electric potential at the points of the Branch path.
    %
    % Parameters:
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   varargin : fields can be :
    %       'pstep': followed by integer : step to roam the potentials
    %       vectors.
    %
    % Description:
    %   The potentials at the points of the path are obtained by linear
    %   interpolation from the values at the nodes/paranodes/internodes.

    pstep = 1;
    idx = find(strcmp('pstep', varargin));
    if idx
        pstep = varargin{idx + 1};
    end
    
    % Unpack attributes in convenient variables.
    npath = size(this.path, 1);
    nIS = this.nIS;
    nNCNS = this.nNCNS;
    nNPNS = this.nNPNS;
    VIS = this.VIS{AS_idx};
    VNCNS = this.VNCNS{AS_idx};
    VP1CNS = this.VP1CNS{AS_idx};
    VP2CNS = this.VP2CNS{AS_idx};
    VICNS = this.VICNS{AS_idx};
    VNPNS = this.VNPNS{AS_idx};
    VP1PNS = this.VP1PNS{AS_idx};
    VP2PNS = this.VP2PNS{AS_idx};
    VIPNS = this.VIPNS{AS_idx};
    [ISlength, Nlength, P1length, P2length, ICNSlength, IPNSlength] = this.get_comps_lengths();
    deltaXCNS = Nlength + 2 * P1length + 2 * P2length + 6 * ICNSlength;
    deltaXPNS = Nlength + 2 * P1length + 2 * P2length + 6 * IPNSlength;
    
    % Form longitudinal abscissa and potential vectors for initial segment.
    xs = ISlength * linspace(0, nIS - 1, nIS)' + ISlength / 2;
    Vs = VIS;
    
    % Append longitudinal abscissa and potentials of the CNS portion.
    for i = 1 : nNCNS
        Vs = [Vs; VP1CNS(2 * (i - 1) + 1); ...
                  VP2CNS(2 * (i - 1) + 1); ...
                  VICNS(6 * (i - 1) + 1); ...
                  VICNS(6 * (i - 1) + 2); ...
                  VICNS(6 * (i - 1) + 3); ...
                  VICNS(6 * (i - 1) + 4); ...
                  VICNS(6 * (i - 1) + 5); ...
                  VICNS(6 * (i - 1) + 6); ...
                  VP2CNS(2 * (i - 1) + 2); ...
                  VP1CNS(2 * (i - 1) + 2); ...
                  VNCNS(i)];

        xs = [xs; nIS * ISlength + (i - 1) * deltaXCNS + P1length / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 3 * ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 5 * ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 7 * ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 9 * ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 11 * ICNSlength / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 6 * ICNSlength + P2length / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 6 * ICNSlength + P2length + P1length / 2; ...
                  nIS * ISlength + (i - 1) * deltaXCNS + P1length + P2length + 6 * ICNSlength + P2length + P1length + Nlength / 2];
    end
        
    % Append longitudinal abscissa and potentials of the PNS portion.
    for i = 1 : nNPNS
        Vs = [Vs; VP1PNS(2 * (i - 1) + 1); ...
                  VP2PNS(2 * (i - 1) + 1); ...
                  VIPNS(6 * (i - 1) + 1); ...
                  VIPNS(6 * (i - 1) + 2); ...
                  VIPNS(6 * (i - 1) + 3); ...
                  VIPNS(6 * (i - 1) + 4); ...
                  VIPNS(6 * (i - 1) + 5); ...
                  VIPNS(6 * (i - 1) + 6); ...
                  VP2PNS(2 * (i - 1) + 2); ...
                  VP1PNS(2 * (i - 1) + 2); ...
                  VNPNS(i)];

        xs = [xs; nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 3 * IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 5 * IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 7 * IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 9 * IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 11 * IPNSlength / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 6 * IPNSlength + P2length / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 6 * IPNSlength + P2length + P1length / 2; ...
                  nIS * ISlength + nNCNS * deltaXCNS + (i - 1) * deltaXPNS + P1length + P2length + 6 * IPNSlength + P2length + P1length + Nlength / 2];
    end
    
    % Interpolate potentials of points of Branch's path.
    np = floor(npath / pstep);
    tmp = Geom.interparc(np, xs, Vs, 'linear');
    longi_abs = tmp(:, 1);
    longi_V = tmp(:, 2);
    
end
