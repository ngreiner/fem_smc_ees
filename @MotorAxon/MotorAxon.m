classdef MotorAxon < handle

    % Properties defined here:
    %
    %   label : string :
    %       Label of MotorAxon (e.g. 'Axon', ...).
    %
    %   model : string :
    %       Indicator of MotorAxon model.
    %
    %   diam : double :
    %       MotorAxon diameter (in um).
    %
    %   path : N*3 double :
    %       MotorAxon endpoints' coordinates (in mm).
    %
    %   nIS : integer :
    %       Number of initial segments of the MotorAxon.
    %
    %   nNCNS : integer :
    %       Number of nodes in the CNS of the MotorAxon.
    %
    %   nP1CNS : integer :
    %       Number of paranodes1 in the CNS of the MotorAxon.
    %
    %   nP2CNS : integer :
    %       Number of paranodes2 in the CNS of the MotorAxon.
    %
    %   nICNS : integer :
    %       Number of internodes in the CNS of the MotorAxon.
    %
    %   nNPNS : integer :
    %       Number of nodes in the PNS of the MotorAxon.
    %
    %   nP1PNS : integer :
    %       Number of paranodes1 in the PNS of the MotorAxon.
    %
    %   nP2PNS : integer :
    %       Number of paranodes2 in the PNS of the MotorAxon.
    %
    %   nIPNS : integer :
    %       Number of internodes in the PNS of the MotorAxon.
    %
    %   NCNScoords : nN*3 double :
    %       CNS nodes coordinates (in mm).
    %
    %   P1CNScoords : nP1*3 double :
    %       CNS paranodes1 coordinates (in mm).
    %
    %   P2CNScoords : nP2*3 double :
    %       CNS paranodes2 coordinates (in mm).
    %
    %   ICNScoords : nI*3 double :
    %       CNS internodes coordinates (in mm).
    %
    %   NPNScoords : nN*3 double :
    %       PNS nodes coordinates (in mm).
    %
    %   P1PNScoords : nP1*3 double :
    %       PNS paranodes1 coordinates (in mm).
    %
    %   P2PNScoords : nP2*3 double :
    %       PNS paranodes2 coordinates (in mm).
    %
    %   IPNScoords : nI*3 double :
    %       PNS internodes coordinates (in mm).
    %
    %   VNCNS : K*1 cell :
    %       CNS nodes potentials for each of K active sites (in mV).
    %
    %   VP1CNS : K*1 cell :
    %       CNS paranodes1 potentials for each of K active sites (in mV).
    %
    %   VP2CNS : K*1 cell :
    %       CNS paranodes2 potentials for each of K active sites (in mV).
    %
    %   VICNS : K*1 cell :
    %       CNS internodes potentials for each of K active sites (in mV).
    %
    %   VNPNS : K*1 cell :
    %       PNS nodes potentials for each of K active sites (in mV).
    %
    %   VP1PNS : K*1 cell :
    %       PNS paranodes1 potentials for each of K active sites (in mV).
    %
    %   VP2PNS : K*1 cell :
    %       PNS paranodes2 potentials for each of K active sites (in mV).
    %
    %   VIPNS : K*1 cell :
    %       PNS internodes potentials for each of K active sites (in mV).
    
    properties
        label           % string
        model           % string
        diam            % double
        path            % N*3 double
        nIS             % integer
        nNCNS           % integer
        nP1CNS          % integer
        nP2CNS          % integer
        nICNS           % integer
        nNPNS           % integer
        nP1PNS          % integer
        nP2PNS          % integer
        nIPNS           % integer
        IScoords        % nIS*3 double
        NCNScoords      % nNCNS*3 double
        P1CNScoords     % nP1CNS*3 double
        P2CNScoords     % nP2CNS*3 double
        ICNScoords      % nICNS*3 double
        NPNScoords      % nNPNS*3 double
        P1PNScoords     % nP1PNS*3 double
        P2PNScoords     % nP2PNS*3 double
        IPNScoords      % nIPNS*3 double
        VIS             % K*1 cell
        VNCNS           % K*1 cell
        VP1CNS          % K*1 cell
        VP2CNS          % K*1 cell
        VICNS           % K*1 cell
        VNPNS           % K*1 cell
        VP1PNS          % K*1 cell
        VP2PNS          % K*1 cell
        VIPNS           % K*1 cell
    end
    
    methods
        function this = MotorAxon()
        end
    end
    
    methods
        build_comps_coords(this)
        from_struct(this, struc)
        [ISlength, Nlength, P1length, P2length, ICNSlength, IPNSlength] = get_comps_lengths(this)
        [longi_abs, longi_V] = get_longi_V(this, AS_idx, varargin)
        path_V = get_path_V(this, AS_idx, varargin)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx, varargin)
        plot_V(this, AS_idx, varargin)
        struc = to_struct(this)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
