classdef STFiber < Fiber

    % Properties defined here:
    %
    %   STbranch : Branch object :
    %       Cortico-spinal tract branch of the CSTFiber.
    %
    %   cols : ncols*1 cell :
    %       Collaterals (Branch objects) of the CSTFiber.
    
    properties
        STbranch    % Branch object
        cols        % N*1 cell
    end
    
    methods
        function this = STFiber()
        end
    end
    
    methods
        struc = to_struct(this)
        [P, loc] = find_node(this, z)
        ncols = gen_ncols(this, varargin)
        branches = get_branches(this)
        read_morpho(this, folder)
        set_cols(this, smcd)
        set_STbranch(this, smcd)
        from_struct(this, struc)
        write_morpho(this, dir_export, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end