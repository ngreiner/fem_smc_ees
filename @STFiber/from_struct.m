function from_struct(this, struc)
	% Set the STFiber from the input struct.
    %
    % Usage:
    %   stf.from_struct(struc)
    %
    % Parameters:
    %   struc : struct :
    %       Fields are the same as the STFiber class properties.
    from_struct@Fiber(this, struc);
    this.STbranch = Branch();
    this.cols = cellfun(@(x) Branch(), struc.cols, 'UniformOutput', false);
    this.STbranch.from_struct(struc.STbranch);
    cellfun(@(x, y) x.from_struct(y), this.cols, struc.cols, 'UniformOutput', false);
end
