function struc = to_struct(this)
	% Return a matlab struct equivalent to the STFiber.
    %
    % Usage:
    %   struc = stf.to_struct()
    %
    % Return:
    %   strc : struct : with same fields as the STFiber.
    struc = to_struct@Fiber(this);
    struc.STbranch = this.STbranch.to_struct();
    struc.cols = cellfun(@(x) x.to_struct(), this.cols, 'UniformOutput', false);
end
