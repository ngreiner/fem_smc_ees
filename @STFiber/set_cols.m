function set_cols(this, smcd)
    % Set the collaterals 'cols' of the STFiber.
    %
    % Usage: 
    %   st.set_cols(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The collaterals of the ST fiber are generated as follows.
    %   First the number of them is generated from a statistical model
    %   based on the data obtained by (Brown and Fyffe, 1978), originally
    %   describing the morphology of cat hindlimb Ia fibers.
    %   Then the nodes of origin of the collaterals are chosen at random
    %   among the nodes of the ST branch.
    %   The collaterals run from one of these nodes towards a point in the
    %   ventro-lateral quarter of the ventral horn of the GM.
    %   An additional point in the middle region of the GM serves for
    %   interpolation of a cubic spline materializing the pathway of the
    %   collaterals.
    %   The coordinates of the collaterals' compartments are finally
    %   distributed along these trajectories.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting Collaterals  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Parameters for the collaterals' diameters.
    mu = smcd.morpho.ST.mu_cols;
    sig = smcd.morpho.ST.sig_cols;
    
    % Generate theoretical number of collaterals.
    ncols = this.gen_ncols();
    
    % Rectify number of collaterals according to the number of nodes
    % available on the STbranch of the STFiber.
    ncols = min(ncols, this.STbranch.nN);
    
    % Choose attachment nodes for the collaterals.
    idxs = randperm(this.STbranch.nN);
    idxs = sort(idxs(1 : ncols), 'descend');
    
    % Generate interpolation points for the collaterals' pathways.    
    P1s = zeros(ncols, 3);
    P2s = zeros(ncols, 3);
    P3s = zeros(ncols, 3);
    locs = cell(ncols, 1);
    i = 1;
    while i <= ncols
        P1 = this.STbranch.Ncoords(idxs(i), :);
        dz = 0.1 + rand(1) * 0.2;
        zP3 = P1(3) + dz;
        P2 = smcd.fem.sc.gen_MR_point(zP3);
        P3 = smcd.fem.sc.gen_VQ_points(smcd.fem.sc.get_CS_above(zP3).name, 1, zP3);
        P2 = 0.25 * P2 + 0.75 * (0.5 * P1 + 0.5 * P3);
        loc = sprintf('ST%d', idxs(i));
        P3s(i, :) = P3;
        P2s(i, :) = P2;
        P1s(i, :) = P1;
        locs{i, 1} = loc;
        i = i + 1;
    end

    % Generate cols.
    cols = cell(ncols, 1);
    for i = 1 : ncols
        
        if smcd.params.verbose
            fprintf(Utils.padn(sprintf('  Collateral  #%d  ', i), 'both', ':', 40))
        end
        
        % Initialize branch.
        cols{i} = Branch();
        cols{i}.label = sprintf('col%d', i);
        cols{i}.model = this.model;
        
        % Generate random diameter.
        cols{i}.diam = lognrnd(mu, sig);
        
        % Generate path.
        inter_points = [P1s(i, :); P2s(i, :); P3s(i, :)];
        d = norm(P2s(i, :) - P1s(i, :)) + norm(P3s(i, :) - P2s(i, :));
        nP = round(smcd.params.pdens * d);
        cols{i}.path = Geom.interparc(nP, inter_points(:, 1), inter_points(:, 2), inter_points(:, 3), 'verbose', smcd.params.verbose);
        
        % Derive and build branch's compartments' coordinates.
        cols{i}.build_comps_coords(0);
        
        % Initialize cell-arrays for branch's compartments potentials.
        cols{i}.VN = cell(smcd.get_nAS(), 1);
        cols{i}.VP1 = cell(smcd.get_nAS(), 1);
        cols{i}.VP2 = cell(smcd.get_nAS(), 1);
        cols{i}.VI = cell(smcd.get_nAS(), 1);
        
        % Store attachment point.
        cols{i}.p_attach = locs{i};
        
    end
    
    % Return.
    this.cols = cols;
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting collaterals. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end
    
end
