function set_STbranch(this, smcd)
    % Set the spino-cerebellar branch of the STFiber.
    %
    % Usage:
    %   cst.set_STbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The spino-cerebellar tract branch of the ST fiber runs in the
    %   spino-cerebellar tract.
    %   The contours materializing the spino-cerebellar tract are contained
    %   in the cross-sections of the spinal cord model attached to the
    %   input smcd.
    %   The trajectory of the ST branch is simply a cubic spline running
    %   through these contours.
    %   The coordinates of the ST branch's compartments are finally
    %   distributed along this trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting ST branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.STbranch = Branch();
    this.STbranch.label = 'STbranch';
    this.STbranch.model = this.model;
    
    % The diameter of the STbranch is the diameter of the STFiber.
    this.STbranch.diam = this.diam;
    
    % Recover spino-cerebellar tract CS, and build path of STbranch.
    CS = cellfun(@(x) x.ST.contour, flipud(smcd.fem.sc.CS), 'UniformOutput', false);
    this.STbranch.path = Geom.build_unconstrained_path(CS, smcd.params.pdens, 'refine_interp');
    
    % Derive branch's compartments' coordinates.
    this.STbranch.build_comps_coords(1);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.STbranch.VN = cell(smcd.get_nAS(), 1);
    this.STbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.STbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.STbranch.VI = cell(smcd.get_nAS(), 1);
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting ST branch. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
