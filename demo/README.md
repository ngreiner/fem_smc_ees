DEMO
====

This folder provides the resources and the scripts necessary to :

- prepare the coordinates textfiles and 3d volumes composing a volume conductor model of the cervical spinal cord of monkeys 
- build a volume conductor model and compute the electric potential distribution elicited by multiple electrode active sites
- build sensorimotor circuits populating the volume conductor and interpolate the electric potential distribution along the trajectories of the generated neural entities

To do so, the scripts in the _scripts_ folder should be executed in the following order:

1. _build_GM_WM_database_from_tracings.m_
2. _build_GM_WM_CSF_dura_fat_dataset.m_
3. _build_3d_coordinates.m_
4. _build_3d_volumes.py_ (from within FreeCAD, see comments inside the script)
5. _build_run_FEM_model.m_ (requires Comsol server to be running and Matlab connected to the server)
6. _build_smcd_1.m_ (requires Comsol server to be running and Matlab connected to the server)
7. _build_smcd_2.m_ (requires Comsol server to be running and Matlab connected to the server)
8. _build_smcd_3.m_ (requires Comsol server to be running and Matlab connected to the server)
9. _build_smcd_4.m_ (requires Comsol server to be running and Matlab connected to the server)
