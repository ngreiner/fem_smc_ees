CROSS-SECTION DATABASE
======================
---

The folder _tracings_ contains 2-dimensional digital tracings of the contours of the grey matter and white matter of the spinal cord.  
These were obtained from scanned pictures of cross-sections of the monkey spinal cord at segmental levels C2 to T6.  
The pictures were taken from :

> Atlas of the Spinal Cord. Sengul, Watson, Tanaka, Paxinos. 2013. Elsevier.

We used the plugin **NeuronJ** of the free open-source software **ImageJ** (<https://imagej.net/NeuronJ>) to realize the digital tracings.

---

Each folder _tracings/XX/_ is dedicated to the spinal cross-section XX and contains the files _Tracing.Out.txt_, _Tracing.In.txt_, _Tracing.Center.txt_ and _Tracing.ScaleBar.txt_.  
Each _Tracing.YY.txt_ is a two-column text file (separator = single whitespace) where each row gives the coordinates (in pixels) of one point of the tracing.  
_Tracing.Out_ is the tracing of the contour of the white matter (WM) in the corresponding cross-section.  
_Tracing.In_ is the tracing of the contour of the grey matter (GM).  
_Tracing.Center_ is the tracing of the common center of the GM and WM. It consists in two points which should be averaged.
Finally, _Tracing.ScaleBar_ is the tracing of the scale bar of the cross-section picture. It consists in two points, distant from 1mm.
