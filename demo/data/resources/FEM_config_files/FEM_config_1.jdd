#######################################################################
#######################################################################
################                                        ###############
################          Dataset for FEMModel          ###############
################                                        ###############
#######################################################################
#######################################################################
##
## Dataset name: FEM_config_1
##
## Created on: 26 March 2019
#
## Author: Nathan GREINER, PhD candidate
## Institution: EPFL BMI SV UPCOURTINE
## email: nathan.greiner@epfl.ch
##
#######################################################################
#######################################################################


#######################################################################
################                GENERAL                ################
#######################################################################
name = FEM_config_1
AS_idx = 1


#######################################################################
################           SPINAL CORD MODEL           ################
#######################################################################
sc_config_file_name = MC_C5_T1.jdd
sc_scale = false
sc_scale_factors = 
sc_CAD_file_format = step


#######################################################################
################                 ROOTS                 ################
#######################################################################
roots_repr = left_right
roots_comsol_file_name = -
roots_CAD_file_format = step
roots_CAD_file_dorsal_suffix = _dorsal_root
roots_CAD_file_ventral_suffix = _ventral_root
roots_CAD_file_left_suffix = _left_root
roots_CAD_file_right_suffix = _right_root
roots_mirror = true
roots_make_diff = false


#######################################################################
################         VERTEBRAL COLUMN              ################
#######################################################################
bone_repr = single_comsol_CAD_file
bone_comsol_CAD_file_name = spine
bone_CAD_file_format = comsol
bone_nvertebrae = -
bone_vertebrae = -
bone_radius = -


#######################################################################
################       MATERIAL CONDUCTIVITIES         ################
#######################################################################
cond_file_name = conductivities.txt


#######################################################################
################                MESH                   ################
#######################################################################
mesh_fineness_type = custom
mesh_fineness = extra_fine
mesh_type_exts = freetet
mesh_WM_with_roots_hmax = 1e-3
mesh_WM_with_roots_hmin = 1e-5
mesh_WM_with_roots_hgrad = 1.35
mesh_WM_with_roots_hcurve = 0.6
mesh_WM_with_roots_hnarrow = 0.5
mesh_elec_hmax = 2.5e-5
mesh_elec_hmin = 1e-6
mesh_elec_hgrad = 1.3
mesh_elec_hcurve = 0.2
mesh_elec_hnarrow = 1
mesh_remaining_hmax = 5e-3
mesh_remaining_hmin = 1e-5
mesh_remaining_hgrad = 1.5
mesh_remaining_hcurve = 0.6
mesh_remaining_hnarrow = 0.5


#######################################################################
################          ELECTRODE ARRAY              ################
#######################################################################
elec_config_file_name = elec_5as_z2.txt
elec_CAD_file_format = step


#######################################################################
################            SALINE LAYER               ################
#######################################################################
saline_repr = single_filled_cylinder
saline_size = small


#######################################################################
################               PHYSICS                 ################
#######################################################################
physics_with_curvi_coords = true
physics_stim_type = current
physics_border_condition = von_neumann
physics_with_ground_point = true
