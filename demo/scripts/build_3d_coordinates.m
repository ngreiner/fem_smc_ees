%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_3d_coordinates.m
%
% Created on : 15 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the script
%   'build_GM_WM_CSF_dura_fat_dataset.m'.
%
%   It builds the final 3d coordinates textfiles describing the GM, WM,
%   CSF, fat, dura, spinal roots, and electrode paddle composing the volume
%   conductor model.
%
%   These coordinates need be exported to textfiles to allow a third party
%   application (FreeCAD) to build the volumes representing the components
%   mentioned above (in the form of .step files).
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEMModel top directory and configuration file.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');
config_file_name = 'FEM_config_1.jdd';

% Build FEMModel and export 3d coordinates.
fem = FEMModel('topdir', topdir, 'config_file_name', config_file_name);
fem.sc.export_coords();
fem.elec.export_paddle_coords();
