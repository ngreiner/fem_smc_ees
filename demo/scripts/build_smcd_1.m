%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_smcd_1.m
%
% Created on : 16 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the script
%   'build_run_FEM_model.m'.
%
%   It builds a sensorimotor circuit populating the volume conductor model
%   and interpolates the electric potential along the trajectories of the
%   generated neural entities for the appropriate electrode active sites.
%
%   Execution of this script may take several hours.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEM dataset directory.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');

% Instantiate sensorimotor circuit model.
config_file_name = 'smcd_C6_MNs_Ias_IIs_DCcBs_CSTs_STs.jdd';
smcd = SMCDModel('topdir', topdir, 'config_file_name', config_file_name);

% Electrode active sites to be used.
AS_idxs = [10; 11];

% Generate neural entities.
smcd.init_MNs();
smcd.init_Ias();
smcd.init_IIs();
smcd.init_DCcBs();
smcd.init_CSTs();
smcd.init_STs();
smcd.save();

% Interpolate electric potentials.
smcd.interp_V('MN', AS_idxs);
smcd.save();
smcd.interp_V('Ia', AS_idxs);
smcd.save();
smcd.interp_V('II', AS_idxs);
smcd.save();
smcd.interp_V('DCcB', AS_idxs);
smcd.save();
smcd.interp_V('CST', AS_idxs);
smcd.save();
smcd.interp_V('ST', AS_idxs);
smcd.save();

% Export sensorimotor circuit dataset.
dir_export = fullfile('..', '..', 'simulation_data', 'Python_NEURON', ...
    'sensorimotor_circuit_datasets', 'datasets', smcd.name);
smcd.export(dir_export);
