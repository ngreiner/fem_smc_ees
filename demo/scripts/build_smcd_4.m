%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_smcd_4.m
%
% Created on : 17 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the script
%   'build_run_FEM_model.m'.
%
%   It builds a sensorimotor circuit populating the volume conductor model
%   and interpolates the electric potential along the trajectories of the
%   generated neural entities for the appropriate electrode active sites.
%
%   Execution of this script may take several hours.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEM dataset directory.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');

% Instantiate sensorimotor circuit model.
config_file_name = 'smcd_MNs_nogeom.jdd';
smcd = SMCDModel('topdir', topdir, 'config_file_name', config_file_name);

% Generate MNs.
smcd.init_MNs();
smcd.save();
