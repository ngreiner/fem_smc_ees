# -*- coding: utf-8 -*


#############################################################################
# Module:  build_3d_volumes.py.
#
# Created on : 2 May 2019
#
# Author : Nathan GREINER, PhD candidate
# Institution : EPFL BMI SV UPCOURTINE
# email : nathan.greiner@epfl.ch
#
#
# Description :
#   This module builds one .step file for each of the GM, WM, CSF, dura, fat;
#   for each spinal root, and for the electrode paddle.
#
#   It should be executed from within the Python console embedded in FreeCAD
#   using the following idiom:
#
#       `exec(open('/path/to/this/module', 'r'))`
#
#   An empty FreeCAD document should be opened prior to running
#   the above command.
#
#   Finally, you should edit the value assigned to the variable
#   'resources_folder' in the function 'main', replacing it by the path to
#   the resources folder in the demo of the package.
#
#############################################################################


from FreeCAD import Base
from FreeCAD import Part
from FreeCAD import Mesh
from FreeCAD import ImportGui
import numpy as np
import os


def main():

    # Specify path to top-level directory.
    resources_folder = '/path/to/resources/folder/'

    # Make roots.
    roots = ['C5', 'C6', 'C7', 'C8', 'T1']
    in_folder = os.path.join(resources_folder, 'CAD', 'coords', 'roots')
    out_folder = os.path.join(resources_folder, 'CAD', 'step')
    for root in roots:
        build_struct('{}_left_dorsal_branch_1'.format(root), in_folder)
        build_struct('{}_left_ventral_branch_1'.format(root), in_folder)
        obj = make_union('{}_left_root'.format(root), ['{}_left_dorsal_branch_1'.format(root), '{}_left_ventral_branch_1'.format(root)])
        export_obj(obj, os.path.join(out_folder, '{}_left_root.step'.format(root)))
        delete_all_objs()
    
    # Make spinal structs.
    struct_names = ['GM', 'WM', 'CSF', 'dura', 'fat']
    in_folder = os.path.join(resources_folder, 'CAD', 'coords', 'structs')
    out_folder = os.path.join(resources_folder, 'CAD', 'step')
    for struct_name in struct_names:
        build_struct(struct_name, in_folder, export=True, out_folder=out_folder, delete=True)
    
    # Make paddle.
    in_folder = os.path.join(resources_folder, 'CAD', 'coords', 'paddle')
    out_folder = os.path.join(resources_folder, 'CAD', 'step')
    build_struct('paddle', in_folder, export=True, out_folder=out_folder, delete=True)

    # The end.
    print('Finished without errors.')


def make_curve(name, coords):
    """Build a BSplineCurve object.

    Arguments:
        name : str : name of the BSplineCurve object.
        coords : N*3 numpy-array : interpolation points.

    Return:
        obj : Part.Feature : the BSplineCurve object.

    """
    xs = []
    for i in range(coords.shape[0]):
        xs.append(Base.Vector(coords[i, 0], coords[i, 1], coords[i, 2]))

    curve = Part.BSplineCurve()
    curve.interpolate(xs)

    obj = App.ActiveDocument.addObject('Part::Feature', name)
    obj.Shape = curve.toShape()

    return obj


def import_curve(file_path):
    """Import coordinates from file_path and build BSplineCurve object.

    Arguments:
        file_path : str : path to coordinates textfile.

    Return:
        obj : Part.Feature : the BSplineCurve object.

    """
    name = os.path.splitext(os.path.basename(file_path))[0]
    coords = np.loadtxt(file_path)
    return make_curve(name, coords)


def make_point(name, coords):
    """Build a Point object.

    Arguments:
        name : str : name of the Point object.
        coords : (3,) numpy-array : point coordinates.

    Return:
        obj : Part.Point : the Point object.

    """

    point = Part.Point()
    point.X = coords[0]
    point.Y = coords[1]
    point.Z = coords[2]

    obj = App.ActiveDocument.addObject('Part::Feature', name)
    obj.Shape = point.toShape()

    return obj


def import_point(file_path):
    """Import coordinates from file_path and build Point object.

    Arguments:
        file_path : str : path to coordinates textfile.

    Return:
        obj : Part.Feature : the Point object.

    """
    name = os.path.splitext(os.path.basename(file_path))[0]
    coords = np.loadtxt(file_path)
    make_point(name, coords)


def make_sweep(name, section_labels, guide_label, solid=True, frenet=False):
    """Make a Sweep object from a series of contours and a guide.

    Arguments:
        section_labels : list of str : labels of contours.
        guide_label : str : label of guide.

    Keyword arguments:
        solid : bool [default: True] : 
            Indicator whether to make solid object.
        frenet : bool [default: False] :
            Indicator whether to use 'Frenet' option to make the Sweep.

    Return:
        obj : Part.Sweep : Sweep object.

    """
    obj = App.ActiveDocument.addObject('Part::Sweep', name)
    obj.Sections = [App.ActiveDocument.getObject(sec) for sec in section_labels]
    obj.Solid = solid
    obj.Frenet = frenet
    obj.Spine = App.ActiveDocument.getObject(guide_label)
    App.ActiveDocument.recompute()
    return obj


def make_loft(name, section_labels, solid=True, ruled=True, closed=False):
    """Make a Loft object from a series of contours.

    Arguments:
        section_labels : list of str : labels of contours.

    Keyword arguments:
        solid : bool [default: True] : 
            Indicator whether to make solid object.
        ruled : bool [default: True] :
            Indicator whether to use 'Ruled' option to make the Loft.
        closed : bool [default: False] :
            Indicator whether to close the Loft object.

    Return:
        obj : Part.Loft : Loft object.

    """
    obj = App.ActiveDocument.addObject('Part::Loft', name)
    obj.Sections = [App.ActiveDocument.getObject(sec) for sec in section_labels]
    obj.Solid = solid
    obj.Ruled = ruled
    obj.Closed = closed
    App.ActiveDocument.recompute()
    return obj


def make_union(name, obj_labels):
    """Unify multiple objects.

    Arguments:
        name : str : label of objects union.
        obj_labels : list of str : labels of objects.

    Return:
        obj : Part.MultiFuse : MultiFuse object.

    """
    obj = App.ActiveDocument.addObject('Part::MultiFuse', name)
    obj.Shapes = [App.ActiveDocument.getObject(obj_label) for obj_label in obj_labels]
    App.ActiveDocument.recompute()
    return obj


def build_struct(struct_name, in_folder, export=False, out_folder=None, ruled=True, with_extreme_points=False, delete=False):
    """Build a structure from cross-section folder.

    Arguments:
        struct_name : str : label of structure to build.
        in_folder : str : path to folder with structure cross-section files.

    Keyword arguments:
        export : bool [default: False] : 
            Indicator whether to export the resulting object.
        out_folder : str :
            Path to folder where to export .step file.
        ruled : bool [default: True] :
            Indicator whether to use 'Ruled' option to make the Loft objects.
        with_extreme_points : bool [default: False] :
            Indicator whether to use center-points of extreme cross-sections
            the Loft object.
        delete : bool [default: False] :
            Indicator to delete objects from Document before returning from
            function.

    """

    # Find automatically the number and names of cross-section files composing the structure.
    CS_files = [file_path for file_path in os.listdir(in_folder) if struct_name + '_CS_' in file_path]
    nCS = len(CS_files)

    CS_names = list()
    for idx in range(1, nCS + 1):
        file_path = os.path.join(in_folder, '{}_CS_{:d}.txt'.format(struct_name, idx))
        import_curve(file_path)
        CS_names.append('{}_CS_{:d}'.format(struct_name, idx))

    # Build extreme points if applicable.
    if with_extreme_points:

        # Center of 1st cross-section.
        name1 = 'P1_{}'.format(struct_name)
        file_path = os.path.join(in_folder, '{}_CS_1.txt'.format(struct_name))
        coords = np.loadtxt(file_path)
        coords = np.mean(coords, axis=0)
        make_point(name1, coords)

        # Center of last cross-section.
        name2 = 'P2_{}'.format(struct_name)
        file_path = os.path.join(in_folder, '{}_CS_{:d}.txt'.format(struct_name, nCS))
        coords = np.loadtxt(file_path)
        coords = np.mean(coords, axis=0)
        make_point(name2, coords)

        # Edit list of object names for further loft.
        CS_names = ['P1_{}'.format(struct_name)] + CS_names + ['P2_{}'.format(struct_name)]

    # Make solid object by lofting the previous curves/points.
    obj = make_loft(struct_name, CS_names, ruled=ruled)

    # Export object to .step file if applicable.
    if export:
        export_file_path = os.path.join(out_folder, '{}.step'.format(struct_name))
        ImportGui.export([obj], export_file_path)

    # Delete object from FreeCAD workspace if applicable.
    if delete:
        App.ActiveDocument.removeObject(struct_name)
        for CS_name in CS_names:
            App.ActiveDocument.removeObject(CS_name)


def export_obj(obj, file_path):
    """Export object to .step file.

    Arguments:
        obj : Part.Feature : object to export.
        file_path : str : path to .step file.

    """
    ImportGui.export([obj], file_path)


def delete_all_objs():
    """Delete all objects in ActiveDocument."""
    for obj in App.ActiveDocument.Objects:
        App.ActiveDocument.removeObject(obj.Name)


def stl_2_step(stl_file_path, step_file_path):
    """Converts .stl mesh to .step solid volume.

    Arguments:
        stl_file_path : str : path to .stl file.
        step_file_path : str : path of output .step file.

    Description:
        This function imports the mesh from the specified .stl file,
        converts it to a solid volume, cleans the solid volume, and export
        it to the specified path.

    """
    Mesh.insert(stl_file_path)
    shape = Part.Shape()
    mesh_name = os.path.splitext(os.path.basename(stl_file_path))[0]
    shape.makeShapeFromMesh(App.ActiveDocument.getObject(mesh_name).Mesh.Topology, 0.100000)
    App.ActiveDocument.addObject('Part::Feature', 'x')
    App.ActiveDocument.getObject('x').Shape = shape
    App.ActiveDocument.getObject('x').purgeTouched()
    App.ActiveDocument.addObject('Part::Feature', 'y')
    App.ActiveDocument.getObject('y').Shape = App.ActiveDocument.x.Shape.removeSplitter()
    shape = App.ActiveDocument.y.Shape
    shape = Part.Solid(shape)
    obj = App.ActiveDocument.addObject('Part::Feature', 'z')
    obj.Shape = shape
    ImportGui.export([obj], step_file_path)
    App.ActiveDocument.removeObject('z')
    App.ActiveDocument.removeObject('y')
    App.ActiveDocument.removeObject('x')
    App.ActiveDocument.removeObject(mesh_name)


if __name__ == '__main__':

    main()
