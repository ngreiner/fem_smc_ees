%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_run_FEM_model.m
%
% Created on : 16 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the module
%   'build_3d_volumes.py'.
%
%   It builds the volume conductor model, builds the mesh, generates the
%   curvilinear coordinates, and compute the electric potential
%   distributions for all the necessary electrode active sites.
%
%   The process of building the geometry is highly error-prone. Slight
%   modifications in the coordinates of the imported volumes may cause
%   failure or success of this process.
%   If the process fails on your system, try to modify some parameters
%   involved in the computation of these coordinates (for instance
%   morphological parameters regarding the CSF or the fat => textfiles in
%   the folder '<root>/simulation_data/Matlab_Comsol/resources/morpho/').
%   You will need to run the scripts building the coordinates and resulting
%   volumes again before rerunning the present script.
%
%   The process of meshing is also highly error-prone. In case it fails on
%   your system, try to modify some parameters in the FEM configuration
%   file at '<root>/simulation_data/Matlab_Comsol/resources/...
%   FEM_config_files/FEM_config_1.jdd'.
%
%   Execution of this script may take several hours.
%   However, the model is being saved at intermediary steps (after building
%   the geometry, after building the mesh, after building the curvilinear
%   coordinates, and after each computation of the electric potential for
%   the different active sites).
%   It is possible to load a previously built model and avoid repeating
%   some of the computational steps by assigning the appropriate values
%   to the different switches in the corpus of the script.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEM dataset directory and configuration file for the
% FEMModel.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');
config_file_name = 'FEM_config_1.jdd';

% Instantiate FEM model.
fem = FEMModel('topdir', topdir, 'config_file_name', config_file_name);

% Choose electrode active sites to be used.
AS_idxs = [1; 2; 4; 5; 7; 8; 10; 11; 13; 14];

% Set switches.
skip_geom = true;
skip_mesh = true;
skip_curv = true;

% Build volume conductor (geometry).
if ~ skip_geom
    fem.build_CM();
    fem.save_CM();
else
    fem.load_CM(1);
end

% Build mesh.
if ~ skip_mesh
    fem.run_mesh();
    fem.save_CM();
end

% Build curvilinear coordinates.
if ~ skip_curv
    fem.run_CC();
    fem.save_CM();
end

% Loop over the active sites to compute the electric potential
% distributions.
for i = 1 : size(AS_idxs, 1)
    fem.update_AS(AS_idxs(i));
    fem.run_EC();
    fem.save_CM();
end
