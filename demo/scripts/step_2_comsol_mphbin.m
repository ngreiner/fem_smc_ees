%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :   step_2_comsol_mphbin.m
%
% Created on : 3 May 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
%
% Description:
%   This converts a collection of .step files into .mphbin files.
%	It requires that a Comsol server be running, and that Matlab be
%	connected to that server.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify input folder, output folder, and names of .step files to convert.
in_folder = fullfile('..', 'data', 'resources', 'CAD', 'step');
out_folder = fullfile('..', 'data', 'resources', 'CAD', 'comsol');
file_names = {'GM'; 'WM'; 'CSF'; 'dura'; 'fat'; 'paddle'; ...
'C5_left_root'; 'C6_left_root'; 'C7_left_root'; 'C8_left_root'; 'T1_left_root'};

% Create Comsol model, component, and geom node.
import com.comsol.model.*
import com.comsol.model.util.*
model = ModelUtil.create('Model');
model.modelNode.create('comp1');
model.geom.create('geom1', 3);

% Import and export to .mphbin format each .step file.
for i = 1 : size(file_names, 1)
	model.geom('geom1').create('imp1', 'Import');
	model.geom('geom1').feature('imp1').set('filename', fullfile(in_folder, sprintf('%s.step', file_names{i})));
	model.geom('geom1').feature('imp1').importData;
    model.geom('geom1').create('ccom1', 'ConvertToCOMSOL');
    model.geom('geom1').feature('ccom1').selection('input').set({'imp1'});
    model.geom('geom1').run('ccom1');
	model.geom('geom1').export(fullfile(out_folder, sprintf('%s.mphbin', file_names{i})));
	model.geom('geom1').feature().remove('ccom1');
	model.geom('geom1').feature().remove('imp1');
end
