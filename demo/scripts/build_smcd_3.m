%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script :  build_smcd_3.m
%
% Created on : 17 October 2019
%
% Author : Nathan GREINER, PhD candidate
% Institution : EPFL BMI SV UPCOURTINE
% email : nathan.greiner@epfl.ch
%
% Description:
%   This script should be executed after the script
%   'build_run_FEM_model.m'.
%
%   It builds a sensorimotor circuit populating the volume conductor model
%   and interpolates the electric potential along the trajectories of the
%   generated neural entities for the appropriate electrode active sites.
%
%   Execution of this script may take several hours.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Specify path to FEM dataset directory.
topdir = fullfile('..', '..', 'simulation_data', 'Matlab_Comsol');


% Instantiate sensorimotor circuit model.
config_file_name = 'smcd_Ias_MNs_8muscs_5segs.jdd';
smcd = SMCDModel('topdir', topdir, 'config_file_name', config_file_name);

% Electrode active sites to be used.
AS_idxs = [1; 4; 7; 10];

% Generate neural entities.
smcd.init_MNs();
smcd.init_Ias();
smcd.save();

% Interpolate electric potentials.
smcd.interp_V('Ia', AS_idxs);
smcd.save();
