function set_mean_pdens(this)
    % Set the atrribute mean_pdens.
    %
    % Usage:
    %   cs.set_mean_pdens()
    %
    % Description:
    %   Assumes that the attributes GM_pdens and WM_pdens are already
    %   properly defined.
    this.mean_pdens = (this.GM_pdens + this.WM_pdens) / 2;
end
