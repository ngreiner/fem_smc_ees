function set_GM_pdens(this)
    % Set the atrribute GM_pdens.
    %
    % Usage:
    %   cs.set_GM_pdens()
    %
    % Description:
    %   Assumes that the attribute GM is already properly defined.
    L = sum(sqrt(sum(diff(this.GM(:, 1 : 2)).^2, 2)), 1);
    np = size(this.GM(:, 1), 1);
    this.GM_pdens = np / L;
end
