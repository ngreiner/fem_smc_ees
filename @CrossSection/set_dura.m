function set_dura(this)
    % Set the atrribute dura.
    %
    % Usage:
    %   cs.set_dura()
    %
    % Description:
    %   Assumes that the attributes dir and z are already properly defined.
    fileID = fopen(fullfile(this.dir, 'dura.txt'));
    dura = textscan(fileID, '%f %f %f');
    fclose(fileID);
    this.dura = [dura{1}, dura{2}, this.z * ones(size(dura{1}))];
end
