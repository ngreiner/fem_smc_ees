function set_ST(this)
    % Set the attribute ST.
    %
    % Usage:
    %   cs.set_ST()
    %
    % Description:
    %   The spino-cerebellar tract  (ST)is embedded in the WM. It is a
    %   layer of constant thickness bordering the WM contour from the
    %   lateral LT segment until the DHline's intersection with the WM
    %   contour. Here we focus only on the left ST.
    %
    %   The attribute ST is a structure with 2 fields: 'contour' and
    %   'line'. 
    %   'contour' is the entire contour describing the ST.
    %   'line' is the medial curved path delimiting the ST.
    %
    %   This method assumes that the attributes LT and DH are already
    %   properly defined.
    %
    %   This method assumes that the WM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.

    % Recover DHline and lateral LT segment.
    l_seg_LT = this.LT.l_seg;
    DH_line = this.DH.line;
    
    % Extract lower half of lateral LT segment (the one connected to the WM
    % cross-sectioncal contour).
    l_seg_LT = l_seg_LT(floor(end / 2) : end, :);

    % Take only dorso-left quadrant of WM contour for convenience.
    WM_quad = this.WM(this.WM(:, 1)<=0 & this.WM(:, 2)<=0, :);
    WM_quad(end, :) = [];

    % Upper segment of ST contour. It is a translation of the lateral LT
    % segment.
    % Its endpoint on the WM is obtained as the one having its
    % y-coordinate equal to the DH_line y-coordinate minus the absolute
    % delta-y of the lateral LT segment.
    l_seg_dy = l_seg_LT(1, 2) - l_seg_LT(end, 2);
    [~, D] = knnsearch(DH_line(end, 2) - l_seg_dy, WM_quad(:, 2), 'k', 1);
    [~, seg_up_p1_WM_idx] = min(D);
    seg_up_p1 = WM_quad(seg_up_p1_WM_idx, :);
    l_seg_vec = l_seg_LT(1, :) - l_seg_LT(end, :);
    seg_up_p2 = seg_up_p1 + l_seg_vec;
    % Form segment.
    d = norm(seg_up_p1 - seg_up_p2);
    x = linspace(seg_up_p1(1), seg_up_p2(1), floor(d * this.mean_pdens))';
    y = linspace(seg_up_p1(2), seg_up_p2(2), floor(d * this.mean_pdens))';
    seg_up = [x, y, this.z * ones(size(x))];
    
    % Lateral portion of ST contour. It is the portion of the WM between
    % the lateral LT segment point and the previously defined seg_up_p1.
    seg_left_p_WM_idx = find(ismember(WM_quad, l_seg_LT(end, :), 'rows'), 1);
    seg_left = WM_quad(seg_left_p_WM_idx : seg_up_p1_WM_idx, :);
    
    % Bottom segment.
    seg_down = l_seg_LT(1 : end, :);
    
    % Medial portion of ST contour. It is the translated of the lateral
    % portion by the vector connecting the endpoints of the lateral LT
    % segment.
    seg_right = flipud(seg_left + repmat(l_seg_vec, size(seg_left, 1), 1));
    
    % Form ST contour. To have a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the
    % endpoint of the lateral LT segment (which is on the WM as well).
    this.ST.contour = [seg_left(1 : end - 1, :); seg_up; seg_right(2 : end - 1, :);  seg_down];
    
    % Store seg_right for further purposes.
    this.ST.line = seg_right;
 
end
