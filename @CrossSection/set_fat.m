function set_fat(this)
    % Set the atrribute fat.
    %
    % Usage:
    %   cs.set_fat()
    %
    % Description:
    %   Assumes that the attributes dir and z are already properly defined.
    fileID = fopen(fullfile(this.dir, 'fat.txt'));
    fat = textscan(fileID, '%f %f %f');
    fclose(fileID);
    this.fat = [fat{1}, fat{2}, this.z * ones(size(fat{1}))];
end
