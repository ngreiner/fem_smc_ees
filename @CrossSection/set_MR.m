function set_MR(this)
    % Set the attribute MR.
    %
    % Usage: 
    %   cs.set_MR()
    %
    % Description:
    %   The MR denotes the middle region of the middle grey matter.
    %
    %   The attribute MR is a structure with only 1 field: 'contour'.
    %   'contour' is the entire contour describing the MR.
    %
    %   This method assumes that the attribute MGM is already properly
    %   defined.
    
    % Recover MGM contour.
    MGM = this.MGM.contour;
    MGM(end, :) = [];
    np = size(MGM, 1);
    
    % Recover total area of MGM. Divide it by 2, thus defining the target
    % area for the MR.
    targ_area = polyarea(this.MGM.contour(:, 1), this.MGM.contour(:, 2)) / 2;
    
    % Recover upper segment of MGM (see method 'setMGM') and recover its
    % middle point.
    seg_up = this.MGM.line;
    seg_up_mid_p = seg_up(ceil(size(seg_up, 1) / 2), :);
    
    % Recover dorso-medial portion of GM contour of MGM (see method
    % 'setMGM') and recover its middle point.
    dm_portion_GM = this.MGM.dm_portion_GM;
    dm_portion_GM_mid_p = dm_portion_GM(ceil(size(dm_portion_GM, 1) / 2), :);
    
    % Initialize variables necessary to the definition of the MR.
    uidx0 = find(ismember(MGM, seg_up_mid_p, 'rows'), 1);
    uidx1 = uidx0 + 1;
    uidx2 = uidx0 - 1;
    uP1 = MGM(uidx1, :);
    uP2 = MGM(uidx2, :);
    seg_up_MR = [uP2; seg_up_mid_p; uP1];
    didx0 = find(ismember(MGM, dm_portion_GM_mid_p, 'rows'), 1);
    didx1 = mod(didx0, np) + 1;
    didx2 = mod(didx0 - 2, np) + 1;
    dP1 = MGM(didx1, :);
    dP2 = MGM(didx2, :);
    seg_down_MR = [dP2; dm_portion_GM_mid_p; dP1];
    
%     plot(this.GM(:,1), this.GM(:,2))
%     hold on
%     plot(usMGM(:,1), usMGM(:,2))
%     scatter(uP0(1), uP0(2))
%     plot(lsMGM(:,1), lsMGM(:,2))
%     scatter(lP0(1), lP0(2))
    
    % Find initial area of the MR as defined by the trapezoid formed on
    % the two previous segments seg_up_MR and seg_down_MR.
    % Add points to the MR as long as the area is lower than the target
    % area. We need a counter to keep track of which point to add.
    cur_area = polyarea([seg_up_MR(:, 1); seg_down_MR(:, 1)], [seg_up_MR(:, 2); seg_down_MR(:, 2)]);
    cnt = 0;
    while (cur_area < targ_area)
        if mod(cnt, 4) == 0
            uidx1 = uidx1 + 1;
            uP1 = MGM(uidx1, :);
            seg_up_MR = [seg_up_MR; uP1];
        elseif mod(cnt, 4) == 1
            didx2 = mod(didx2 - 2, np) + 1;
            dP2 = MGM(didx2, :);
            seg_down_MR = [dP2; seg_down_MR];
        elseif mod(cnt, 4) == 2
            didx1 = mod(didx1, np) + 1;
            dP1 = MGM(didx1, :);
            seg_down_MR = [seg_down_MR; dP1];
        elseif mod(cnt, 4) == 3
            uidx2 = uidx2 - 1;
            uP2 = MGM(uidx2, :);
            seg_up_MR = [uP2; seg_up_MR];
        end
        cur_area = polyarea([seg_up_MR(:, 1); seg_down_MR(:, 1)], [seg_up_MR(:, 2); seg_down_MR(:, 2)]);
        cnt = cnt + 1;
    end
    
    % Form left vertical segment of the MR.
    d = norm(uP1 - dP2);
    x = linspace(uP1(1), dP2(1), floor(d * this.GM_pdens))';
    y = linspace(uP1(2), dP2(2), floor(d * this.GM_pdens))';
    seg_left_MR = [x, y, this.z * ones(size(x, 1), 1)];
    
    % Form right vertical segment of the MR.
    d = norm(dP1 - uP2);
    x = linspace(dP1(1), uP2(1), floor(d * this.GM_pdens))';
    y = linspace(dP1(2), uP2(2), floor(d * this.GM_pdens))';
    seg_right_MR = [x, y, this.z * ones(size(x, 1), 1)];
    
    % Form MR contour. To get a closed-contour, we need to repeat one
    % and only one point in the point sequence. We choose to repeat uP2.
    this.MR.contour = [seg_up_MR; seg_left_MR(2 : end - 1, :); seg_down_MR; seg_right_MR(2 : end, :)];
    
end
