classdef CrossSection < handle
    
    properties
        name        % string        : cross-section name.
        dir         % string        : path to folder of coordinates textfiles.
        z           % double        : z-coordinate of plane of cross-section.
        GM          % N*3 double    : GM contour coordinates.
        WM          % N*3 double    : WM contour coordinates.
        GM_pdens    % double        : GM contour point density.
        WM_pdens    % double        : WM contour point density.
        mean_pdens  % double        : mean point density on GM and WM contours.
        CSF         % N*3 double    : CSF contour coordinates.
        fat         % N*3 double    : fat contour coordinates.
        dura        % N*3 double    : dura contour coordinates.
        DH          % struct        : left dorsal horn of GM.
        LT          % struct        : left Lissaeur tract of spinal cord.
        MGM         % struct        : left middle GM region.
        MR          % struct        : middle region of the MGM.
        VH          % struct        : left ventral horn of GM.
        VQ          % struct        : ventro-lateral quarter of the VH.
        ST          % struct        : left spino-cerebellar tract of spinal cord.
        CT          % struct        : left cortico-spinal tract of spinal cord.
        DC          % struct        : left dorsal-columns of spinal cord.
        OD          % 6*1 cell      : left outer dorsal-columns of spinal cord.
    end
    
    methods
        
        function this = CrossSection(name, dir, z)
            % Class constructor method.
            %
            % Usage:
            %   CS = CrossSection()
            %   CS = CrossSection(name, dir, z)
            %
            % Parameters:
            %   name    : str       : name of the CrossSection.
            %   dir     : str       : path to directory of geometric textfiles.
            %   z       : double    : z-coordinate of the CrossSection.
            %
            % Description:
            %   The directory `dir` should contain 5 textfiles made of 2 or
            %   3 columns of real values representing coordinates expressed
            %   in mm.
            %   These should be 'GM.txt', 'WM.txt', 'CSF.txt', 'dura.txt'
            %   and 'fat.txt'.
            %   In case no arguments are provided, the method does nothing
            %   but create the object.
            %   If all 3 arguments are provided, the CrossSection is
            %   completely configured, thanks to the calls to
            %   set_contours() and set_inner_comps().
            
            if nargin == 0
                return
            end

            this.name = name;
            this.dir = dir;
            this.z = z;
            this.set_contours();
            this.set_inner_comps();
            
        end
        
    end
    
    methods
        export_coords(this, folder_path, varargin)
        from_struct(this, struc)
        plot(this, varargin)
        scale(this, scale_factors)
        set_contours(this)
        set_CSF(this)
        set_CT(this)
        set_DC(this)
        set_DH(this)
        set_dura(this)
        set_fat(this)
        set_GM(this)
        set_GM_pdens(this)
        set_inner_comps(this)
        set_LT(this)
        set_mean_pdens(this)
        set_MGM(this)
        set_MR(this)
        set_OD(this)
        set_ST(this)  
        set_VH(this)
        set_VQ(this)
        set_WM(this)
        set_WM_pdens(this)
        struc = to_struct(this)
    end
    
end
