function set_CSF(this)
    % Set the attribute CSF.
    %
    % Usage:
    %   cs.setCSF()
    %
    % Description:
    %   Assumes that the attributes dir and z are already properly defined.
    fileID = fopen(fullfile(this.dir, 'CSF.txt'));
    CSF = textscan(fileID, '%f %f %f');
    fclose(fileID);
    this.CSF = [CSF{1}, CSF{2}, this.z * ones(size(CSF{1}))];
end
