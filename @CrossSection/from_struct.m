function from_struct(this, struc)
    % Set the CrossSection object using the fields of the input struct.
    %
    % Usage:
    %   cs.from_struct(struc)
    %
    % Parameters:
    %   struc   : struct    : with same fields as the CrossSection object.
    this.name = struc.name;
    this.z = struc.z;
    this.GM = struc.GM;
    this.WM = struc.WM;
    this.GM_pdens = struc.GM_pdens;
    this.WM_pdens = struc.WM_pdens;
    this.mean_pdens = struc.mean_pdens;
    this.CSF = struc.CSF;
    this.fat = struc.fat;
    this.dura = struc.dura;
    this.DH = struc.DH;
    this.LT = struc.LT;
    this.MGM = struc.MGM;
    this.MR = struc.MR;
    this.VH = struc.VH;
    this.VQ = struc.VQ;
    this.ST = struc.ST;
    this.CT = struc.CT;
    this.DC = struc.DC;
    this.OD = struc.OD;
end
