function export_coords(this, folder_path, varargin)
    % Write the coordinates of the specified compartments to textfiles.
    %
    % Usage:
    %   cs.export_coords(folder_path)
    %   cs.export_coords(folder_path, 'comps', comps)
    %   cs.export_coords(folder_path, 'dim', dim)
    %
    % Parameters:
    %   folder_path : string :
    %       Path to folder where to export the textfiles.
    %
    %   comps [optional] : N*1 cell :
    %       Any of 'WM', 'GM', 'CSF', 'dura', 'fat', 'DH', 'VH', 'VQ',
    %       'MGM', 'MR', 'LT', 'CT', 'ST', 'DC' or 'OD' indicating to
    %       export the corresponding compartment.
    %
    %   dim : integer :
    %       2 or 3, indicating to export the (x,y)-coordinates or the
    %       (x,y,z)-coordinates.
    
    % Restrict list of compartments whose coords should be exported if
    % specified in varargin.
    idx = find(strcmp(varargin, 'comps'));
    if ~ isempty(idx)
        comps = varargin{idx + 1};
    else
        comps = {'WM'; 'GM'; 'CSF'; 'dura'; 'fat'; 'DH'; 'VH'; 'VQ';
                 'MGM'; 'MR'; 'LT'; 'CT'; 'ST'; 'DC'; 'OD'};
    end
    
    % Find out how much columns should be exported (xy-coordinates or
    % xyz-coordinates).
    idx = find(strcmp(varargin, 'dim'));
    if ~ isempty(idx)
        dim = varargin{idx + 1};
    else
        dim = 3;
    end
    
    % Set format specifier accordingly.
    if dim == 2
        format_spec = '%f %f\n';
    else
        format_spec = '%f %f %f\n';
    end
    
    % Make export directory if needed.
    if ~ exist(folder_path, 'dir')
        mkdir(folder_path);
    end
    
    % Export compartments coordinates.
    for i = 1 : size(comps, 1)
        if strcmp('OD', comps{i})
            for j = 1 : 6
                contour = this.OD{j}(:, 1 : dim)';
                fileID = fopen(fullfile(folder_path, sprintf('OD%d.txt', j)), 'w');
                fprintf(fileID, format_spec, contour);
                fclose(fileID);
            end
        else
            try
                contour = this.(comps{i}).contour(:, 1 : dim)';
            catch
                contour = this.(comps{i})(:, 1 : dim)';
            end
            fileID = fopen(fullfile(folder_path, sprintf('%s.txt', comps{i})), 'w');
            fprintf(fileID, format_spec, contour);
            fclose(fileID);
        end
    end
    
end
