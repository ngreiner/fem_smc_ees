function set_CT(this)
    % Set the attribute CT.
    %
    % Usage:
    %   cs.setCT()
    %
    % Description:
    %   The cortico-spinal tract (CT) is a region of the WM juxtaposed to
    %   and above of the dorsal horn of the GM. Its contour is made of a
    %   portion of the GM contour, the upper curved path of the ST, and a
    %   horizontal segment delimiting it on the top.
    %
    %   The attribute CT is a structure with only 1 field: 'contour',
    %   describing the entire contour of the CT.
    %
    %   This method assumes that the attributes LT, DH and ST are already
    %   properly defined.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.

    % Recover LTl, DHline and SCline.
    l_seg_LT = this.LT.l_seg;
    DH_line = this.DH.line;
    ST_line = this.ST.line;

    % Take only dorso-left quadrant of GM contour for convenience.
    GM_quad = this.GM(this.GM(:, 1)<=0 & this.GM(:, 2)<=0, :);
    GM_quad(end, :) = [];
    
    % Medial GM point of CT contour.
    m_GM_p_idx = find(ismember(GM_quad, DH_line(end, :), 'rows'), 1);
    m_GM_p = DH_line(end, :);
    
    % Lateral GM point of CT contour.
    l_GM_p_idx = find(ismember(GM_quad, l_seg_LT(1, :), 'rows'), 1);
    
    % Lateral ST point of CT contour.
    l_ST_p = ST_line(1, :);

    % Upper segment of CT contour.
    d = norm(l_ST_p - m_GM_p);
    x = linspace(m_GM_p(1), l_ST_p(1), floor(d * this.GM_pdens))';
    y = linspace(m_GM_p(2), l_ST_p(2), floor(d * this.GM_pdens))';
    seg_up = [x, y, this.z * ones(size(x))];

    % Form CT contour. To get a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the
    % medial GM point.
    seg_GM = GM_quad(l_GM_p_idx : m_GM_p_idx, :);
    seg_LT = flipud(l_seg_LT(2 : floor(end / 2), :));
    this.CT.contour = [seg_up; ST_line(2 : end - 1, :); seg_LT; seg_GM];

end
