function set_MGM(this)
    % Set the attribute MGM.
    %
    % Usage:
    %   cs.set_MGM()
    %
    % Description:
    %   MGM = 'Middle area of the Grey Matter'.
    %   It is the region of the grey matter between the ventral horn and
    %   the dorsal horn. Here we consider only the left MGM.
    %   It is delimited by a lateral portion of GM contour on the left, by
    %   a dorso-medial portion of GM contour on the lower-right, by two
    %   horizontal segments on the top and on the bottom (separating it
    %   from the ventral and dorsal horns) and by a vertical segment on the
    %   right (superposed onto the y-axis).
    %
    %   The attribute MGM is a structure with 3 fields: 'contour', 'line',
    %   and 'dm_portion_GM'.
    %   'contour' is the entire contour describing the MGM.
    %   'line' is the upper horizontal segment separating the MGM from the
    %   ventral horn.
    %   'dm_portion_GM' is the dorso-medial portion of GM contour included
    %   in the MGM contour.
    %
    %   This method assumes that the attribute DH is already properly
    %   defined.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    
    % Recover DHline.
    DH_line = this.DH.line;

    % Take only left half of GM for convenience.
    GM_half = this.GM(this.GM(:, 1)<=0, :);
    GM_half(end, :) = [];

    % Form medial segment of MGM: it connects the first and last points
    % of GMhalf (both lying on the y-axis, x=0).
    d = abs(GM_half(1, 2) - GM_half(end, 2));
    x = linspace(GM_half(1, 1), GM_half(end, 1), floor(d * this.GM_pdens))';
    y = linspace(GM_half(1, 2), GM_half(end, 2), floor(d * this.GM_pdens))';
    m_seg = [x, y, this.z * ones(size(x))];

    % Form upper segment of MGM (which separates the MGM from the VH): it 
    % connects the last point of GM_half (x=0 and y>0) and the point of the
    % lateral GM contour with y-coordinate closest to the one of the
    % previous point.
    % The lateral GM can be isolated by selecting all the points between
    % the 1st point of the contour and the point fulfilling the condition
    % y=ymax.
    [~, ymax_idx] = max(GM_half(:, 2));
    [~, D] = knnsearch(GM_half(end, 2), GM_half(1 : ymax_idx, 2), 'k', 1);
    [~, l_p_up_idx] = min(D);
    d = norm(GM_half(l_p_up_idx, :) - GM_half(end, :));
    x = linspace(GM_half(end, 1), GM_half(l_p_up_idx, 1), floor(d * this.GM_pdens))';
    y = linspace(GM_half(end, 2), GM_half(l_p_up_idx, 2), floor(d * this.GM_pdens))';
    this.MGM.line = [x, y, this.z * ones(size(x))];

    % Find indexes of the intersection points of GM and DHline.
    m_p_idx = find(ismember(GM_half, DH_line(1, :), 'rows'), 1);
    l_p_down_idx = find(ismember(GM_half, DH_line(end, :), 'rows'), 1);

    % Form lateral portion of GM that belongs to MGM.
    l_portion_GM = GM_half(l_p_up_idx - 1 : -1 : l_p_down_idx + 1, :);
    
    % Form dorso-medial portion of GM that belongs to MGM.
    if (m_p_idx == 1)
        dm_portion_GM = GM_half(1, :);
    else
        dm_portion_GM = GM_half(m_p_idx - 1: -1 : 1, :);
    end
    this.MGM.dm_portion_GM = dm_portion_GM;
    
    % Form MGM contour. To have a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the 1st 
    % point of the medial MGM segment (which belongs to the GM as well).
    this.MGM.contour =  [m_seg; this.MGM.line; l_portion_GM; flipud(DH_line); dm_portion_GM];
    
end
