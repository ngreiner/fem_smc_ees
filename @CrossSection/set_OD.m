function set_OD(this)
    % Set the attribute OD.
    %
    % Usage:
    %   cs.set_OD()
    %
    % Description:
    %   The outer dorsal-columns consist in 6 adjacent regular compartments
    %   spanning the outermost layer of the dorsal-columns. They are
    %   organized in two sublayers.
    %   The outermost, medial-most compartment is number #1. While staying
    %   in the outermost layer and moving laterally, we glide over
    %   compartments #2 and #3 sequentially.
    %   Again from medial to lateral within the second outermost layer, we
    %   glide over compartments #4, #5 and #6.
    %
    %   The attribute OD is a 6*1 cell-array, where each cell is a N*3
    %   double-array of geometric coordinates. Each cell describe one
    %   compartment of the OD.
    %
    %   This method assumes that the attribute LT is already properly
    %   defined.
    %
    %   This method assumes that the WM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    
    % Recover medial LT segment.
    m_seg = this.LT.m_seg;

    % Take only dorso-left quadrant of WM contour for convenience.
    WM_quad = this.WM(this.WM(:, 1)<=0 & this.WM(:, 2)<=0, :);
    WM_quad(end, :) = [];

    % Find index of lateral WM point of DC.
    l_WM_p = m_seg(1, :);
    l_WM_p_idx = find(ismember(WM_quad, l_WM_p, 'rows'), 1);
    
    % Define WM path, and scaled-down versions of this path.
    WM1 = WM_quad(1 : l_WM_p_idx, :);
    d1 = mean(sqrt(sum(WM1.^2, 2)));
    d2 = d1 * 0.95;
    d3 = d1 * 0.9;
    WM2 = [WM1(:, 1 : 2) * (d2 / d1) WM1(:, 3)];
    WM3 = [WM1(:, 1 : 2) * (d3 / d1) WM1(:, 3)];
    
    % Split in thirds the two strips delimited by WM1, WM2 and WM3. For
    % that, define splitting segments.
    x = linspace(WM1(1, 1), WM2(1, 1), floor((d1 - d2) * this.WM_pdens))';
    y = linspace(WM1(1, 2), WM2(1, 2), floor((d1 - d2) * this.WM_pdens))';
    S1 = [x, y, this.z * ones(size(x))];
    x = linspace(WM1(floor(end / 3), 1), WM2(floor(end / 3), 1), floor((d1 - d2) * this.WM_pdens))';
    y = linspace(WM1(floor(end / 3), 2), WM2(floor(end / 3), 2), floor((d1 - d2) * this.WM_pdens))';
    S2 = [x, y, this.z * ones(size(x))];
    x = linspace(WM1(floor(end * 2 / 3), 1), WM2(floor(end * 2 / 3), 1), floor((d1 - d2) * this.WM_pdens))';
    y = linspace(WM1(floor(end * 2 / 3), 2), WM2(floor(end * 2 / 3), 2), floor((d1 - d2) * this.WM_pdens))';
    S3 = [x, y, this.z * ones(size(x))];
    x = linspace(WM1(end, 1), WM2(end, 1), floor((d1 - d2) * this.WM_pdens))';
    y = linspace(WM1(end, 2), WM2(end, 2), floor((d1 - d2) * this.WM_pdens))';
    S4 = [x, y, this.z * ones(size(x))];
    x = linspace(WM2(1, 1), WM3(1, 1), floor((d2 - d3) * this.WM_pdens))';
    y = linspace(WM2(1, 2), WM3(1, 2), floor((d2 - d3) * this.WM_pdens))';
    S5 = [x, y, this.z * ones(size(x))];
    x = linspace(WM2(floor(end / 3), 1), WM3(floor(end / 3), 1), floor((d2 - d3) * this.WM_pdens))';
    y = linspace(WM2(floor(end / 3), 2), WM3(floor(end / 3), 2), floor((d2 - d3) * this.WM_pdens))';
    S6 = [x y this.z*ones(size(x))];
    x = linspace(WM2(floor(end * 2 / 3), 1), WM3(floor(end * 2 / 3), 1), floor((d2 - d3) * this.WM_pdens))';
    y = linspace(WM2(floor(end * 2 / 3), 2), WM3(floor(end * 2 / 3), 2), floor((d2 - d3) * this.WM_pdens))';
    S7 = [x, y, this.z * ones(size(x))];
    x = linspace(WM2(end, 1), WM3(end, 1), floor((d2 - d3) * this.WM_pdens))';
    y = linspace(WM2(end, 2), WM3(end, 2), floor((d2 - d3) * this.WM_pdens))';
    S8 = [x, y, this.z * ones(size(x))];
    
    % Form outer dorsal columns.
    OD1 = [WM1(1 : floor(end / 3), :); S2(2 : end - 1, :); WM2(floor(end / 3) : -1 : 1, :); S1(end - 1 : -1 : 1, :)];
    OD2 = [WM1(floor(end / 3) : floor(end * 2 / 3), :); S3(2 : end - 1, :); WM2(floor(end * 2 / 3) : -1 : floor(end / 3), :); S2(end - 1: -1 : 1, :)];
    OD3 = [WM1(floor(end * 2 / 3) : end, :); S4(2 : end - 1, :); WM2(end : -1 : floor(end * 2 / 3), :); S3(end - 1 : -1 : 1, :)];
    OD4 = [WM2(1 : floor(end / 3), :); S6(2 : end - 1, :); WM3(floor(end / 3): -1 : 1, :); S5(end - 1: -1 : 1, :)];
    OD5 = [WM2(floor(end / 3) : floor(end * 2 / 3), :); S7(2 : end - 1, :); WM3(floor(end * 2 / 3) : -1 :floor(end / 3), :); S6(end - 1: -1 : 1, :)];
    OD6 = [WM2(floor(end * 2 / 3) : end, :); S8(2 : end - 1, :); WM3(end : -1 : floor(end * 2 / 3),:); S7(end - 1 : -1 : 1, :)];
    
    % Set attribute.
    this.OD = cell(6, 1);
    this.OD{1} = OD1;
    this.OD{2} = OD2;
    this.OD{3} = OD3;
    this.OD{4} = OD4;
    this.OD{5} = OD5;
    this.OD{6} = OD6;
    
end
