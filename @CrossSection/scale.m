function scale(this, scale_factors)
    % Scale the geometric attributes of the CrossSection.
    %
    % Usage:
    %   cs.scale(scale_factors)
    %
    % Parameters:
    %   scale_factors : 1*3 double : scaling factors.
    
    scale_mat = diag(scale_factors);
    
    this.z = this.z * scale_factors(3);
    this.GM = this.GM * scale_mat;
    this.WM = this.WM * scale_mat;
    this.set_GM_pdens();
    this.set_WM_pdens();
    this.set_mean_pdens();
    this.CSF = this.CSF * scale_mat;
    this.fat = this.fat * scale_mat;
    this.dura = this.dura * scale_mat;
    this.DH.line = this.DH.line * scale_mat;
    this.DH.contour = this.DH.contour * scale_mat;
    this.LT.m_seg = this.LT.m_seg * scale_mat;
    this.LT.l_seg = this.LT.l_seg * scale_mat;
    this.LT.contour = this.LT.contour * scale_mat;
    this.MGM.line = this.MGM.line * scale_mat;
    this.MGM.dm_portion_GM = this.MGM.dm_portion_GM * scale_mat;
    this.MGM.contour = this.MGM.contour * scale_mat;
    this.MR.contour = this.MR.contour * scale_mat;
    this.VH.contour = this.VH.contour * scale_mat;
    this.VQ.contour = this.VQ.contour * scale_mat;
    this.ST.line = this.ST.line * scale_mat;
    this.ST.contour = this.ST.contour * scale_mat;
    this.CT.contour = this.CT.contour * scale_mat;
    this.DC.contour = this.DC.contour * scale_mat;
    this.DC.area = polyarea(this.DC.contour(:, 1), this.DC.contour(:, 2));
    this.OD = cellfun(@(x) x * scale_mat, this.OD, 'UniformOutput', false);
    
end
