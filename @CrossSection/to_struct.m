function struc = to_struct(this)
	% Return a struct equivalent to the CrossSection object.
    %
    % Usage:
    %   struc = cs.to_struct()
    %
    % Return:
    %   struc   : struct    : with same fields as the CrossSection object.
    struc.name = this.name;
    struc.z = this.z;
    struc.GM = this.GM;
    struc.WM = this.WM;
    struc.GM_pdens = this.GM_pdens;
    struc.WM_pdens = this.WM_pdens;
    struc.mean_pdens = this.mean_pdens;
    struc.CSF = this.CSF;
    struc.fat = this.fat;
    struc.dura = this.dura;
    struc.DH = this.DH;
    struc.LT = this.LT;
    struc.MGM = this.MGM;
    struc.MR = this.MR;
    struc.VH = this.VH;
    struc.VQ = this.VQ;
    struc.ST = this.ST;
    struc.CT = this.CT;
    struc.DC = this.DC;
    struc.OD = this.OD;
end
