function set_VH(this)
    % Set the attribute VQ.
    %
    % Usage: 
    %   cs.set_VH()
    %
    % Description:
    %   The ventral horn (VH) of the GM is the ventral region of the GM.
    %   Here we focus only on the left VH. We define it as the upper left
    %   region of the GM, separated from the lower region (central and
    %   dorsal) by a horizontal line already obtained during the
    %   computation of the MGM.
    %
    %   The attribute VH is a structure with only 1 field: 'contour'.
    %   'contour' is the entire contour describing the VH.
    %
    %   This method assumes that the attribute MGM is already properly
    %   defined.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.

    % Recover MGMline.
    MGM_line = this.MGM.line;

    % Take only left half of GM contour for convenience.
    GM_half = this.GM(this.GM(:, 1)<=0, :);
    GM_half(end, :) = [];

    % Find index of intersecting point of lateral GM and MGM_line.
    l_p_idx = find(ismember(GM_half, MGM_line(end, :), 'rows'), 1);
    
    % Form VH contour. To get a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the 1st 
    % point of the medial MGM line (which belongs to the GM as well).
    this.VH.contour = [MGM_line; GM_half(l_p_idx + 1 : end, :)];
    
end
