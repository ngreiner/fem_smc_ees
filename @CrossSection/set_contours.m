function set_contours(this)
    % Set the attributes GM, WM, CSF, Fat, Dura, GM_pdens, WM_pdens, and
    % mean_pdens. 
    %
    % Usage:
    %   cs.set_ontours()
    %
    % Description: 
    %   The method calls the respective constructors of the previous
    %   attributes.
    this.set_GM();
    this.set_WM();
    this.set_CSF();
    this.set_fat();
    this.set_dura();
    this.set_GM_pdens();
    this.set_WM_pdens();
    this.set_mean_pdens();
end
