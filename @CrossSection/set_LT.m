function set_LT(this)
    % Set the attribute LT.
    %
    % Usage:
    %   cs.set_LT()
    %
    % Description:
    %   The Lissaeur tract (LT) is the region of the spinal cord where the
    %   dorsal roots are entering. It is the intermediary zone between the
    %   dorsal roots and the dorsal horn of the GM. Here we consider only
    %   the left LT. Its contour is made of a portion of the GM contour, a
    %   portion of the WM contour, and 2 straight segments joining 2 by 2
    %   the tips of the portions of GM and WM.
    %
    %   The attribute LT is a structure with 3 fields: 'contour', 'l_seg'
    %   and 'm_seg'.
    %   'contour' is the entire contour describing the LT.
    %   'l_seg' is the lateral segment joining the GM  and WM portions.
    %   'm_seg' is the medial segment joining the GM and WM portions.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    %
    %   This method assumes that the WM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    
    % Take only dorso-left quadrant of GM and WM contours for convenience.
    GM_quad = this.GM(this.GM(:, 1)<=0 & this.GM(:, 2)<=0, :);
    GM_quad(end, :) = [];
    WM_quad = this.WM(this.WM(:, 1)<=0 & this.WM(:, 2)<=0, :);
    WM_quad(end, :) = [];

    % Lateral LT segment GM point: it is a point in the lower region of the
    % GM contour where the slope passes from negative to positive.
    % First, check for such slope inversion events.
    slopes_GM = diff(GM_quad(:, 2)) ./ diff(GM_quad(:, 1));
    idx = find(diff(sign(slopes_GM)) > 0);
    % Second, divide GM into 3 regions with equal extent along y-axis.
    % The position of slope inversion event we are looking for should be in
    % the lower region.
    ymax = max(GM_quad(:, 2));
    ymin = min(GM_quad(:, 2));
    d = (ymax - ymin) / 3;
    idx2 = [];
    for j = 1 : size(idx, 1)
        if (GM_quad(idx(j), 2) >= ymin) && (GM_quad(idx(j), 2) <= ymin + d)
            idx2 = idx(j);
        end
    end
    % If there is no such event in the lower region, take the first slope
    % inversion event found.
    if isempty(idx2)
        l_GM_p_idx = idx(1);
    else
        l_GM_p_idx = idx2;
    end
    % Define lateral LT segment GM point.
    l_GM_p = GM_quad(l_GM_p_idx, :);
    
    % Lateral LT segment WM point: it is the point of the WM contour
    % closest to the lateral LT segment GM point.
    [~, D] = knnsearch(l_GM_p, WM_quad, 'k', 1);
    [~, l_WM_p_idx] = min(D);
    l_WM_p = WM_quad(l_WM_p_idx, :);

    % Medial LT segment GM point. It is the closest point of the
    % GM to first point of WM (the one with x=0 and y<0).
    [~, D] = knnsearch(WM_quad(1,:), GM_quad, 'k', 1);
    [~, m_GM_p_idx] = min(D);
    % If the index of that point is smaller than a quarter of the
    % number of points in GMquad, use this latter index instead.
    if m_GM_p_idx < floor(l_GM_p_idx / 4)
        m_GM_p = GM_quad(floor(l_GM_p_idx / 4), :);
    else
        m_GM_p = GM_quad(m_GM_p_idx, :);
    end
    
    % Medial LT segment WM point: it is the point of the WM contour closest
    % to the medial LT segment GM point.
    [~, D] = knnsearch(m_GM_p, WM_quad, 'k', 1);
    [~, m_WM_p_idx] = min(D);
    m_WM_p = WM_quad(m_WM_p_idx, :);

    % Form medial LT segment.
    d = norm(m_GM_p - m_WM_p);
    x = linspace(m_WM_p(1), m_GM_p(1), floor(d * this.mean_pdens))';
    y = linspace(m_WM_p(2), m_GM_p(2), floor(d * this.mean_pdens))';
    this.LT.m_seg = [x, y, this.z * ones(size(x, 1), 1)];
    
    % Form lateral LT segment.
    d = norm(l_GM_p - l_WM_p);
    x = linspace(l_GM_p(1), l_WM_p(1), floor(d * this.mean_pdens))';
    y = linspace(l_GM_p(2), l_WM_p(2), floor(d * this.mean_pdens))';
    this.LT.l_seg = [x, y, this.z * ones(size(x, 1), 1)];

    % Form LT contour. To have a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the 1st 
    % point of the medial LT segment (which belongs to the WM as well).
    seg_GM = GM_quad(m_GM_p_idx + 1 : l_GM_p_idx - 1, :);
    seg_WM = WM_quad(l_WM_p_idx - 1 : -1 : m_WM_p_idx, :);
    this.LT.contour = [this.LT.m_seg;  seg_GM;  this.LT.l_seg; seg_WM];

end
