function set_inner_comps(this)
    % Set the attributes DH, LT, MGM, MR, VH, VQ, ST, CT, DC and OD.
    %
    % Usage:
    %   cs.set_inner_comps()
    %
    % Description: 
    %   Calls the respective constructors of the previous attributes.
    this.set_DH();
    this.set_LT();
    this.set_MGM();
    this.set_MR();
    this.set_VH();
    this.set_VQ();
    this.set_ST();
    this.set_CT();
    this.set_DC();
    this.set_OD();
end
