function set_DC(this)
    % Set the attribute DC.
    %
    % Usage:
    %   cs.setDC()
    %
    % Description:
    %   The dorscal columns (DC) compose the medial dorsal region of the
    %   WM. 
    %   Here we focus only on the left DC. It is delimited on the left by
    %   the GM and the LT, and on the right by the y-axis.
    %
    %   The attribute DC is a structure with 2 fields: 'contour' and
    %   'area'. 'contour' is the entire contour describing the DC and
    %   'area' its area.
    %
    %   This method assumes that the attribute LT is already properly
    %   defined.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    %
    %   This method assumes that the WM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    
    % Recover medial LT segment.
    m_seg_LT = this.LT.m_seg;

    % Take only dorso-left quadrant of GM and WM contours for convenience.
    GM_quad = this.GM(this.GM(:, 1)<=0 & this.GM(:, 2)<=0, :);
    GM_quad(end, :) = [];
    WM_quad = this.WM(this.WM(:, 1)<=0 & this.WM(:, 2)<=0, :);
    WM_quad(end, :) = [];
    
    % Medial DC segment: it's a vertical segment connecting the points of
    % the GM and WM with x=0 and y<0.
    d = abs(GM_quad(1, 2) - WM_quad(1, 2));
    x = linspace(GM_quad(1, 1), WM_quad(1, 1), floor(d * this.mean_pdens))';
    y = linspace(GM_quad(1, 2), WM_quad(1, 2), floor(d * this.mean_pdens))';
    m_seg_DC = [x, y, this.z * ones(size(x))];
    
    % Lateral WM point of DC contour.
    l_WM_p = m_seg_LT(1, :);
    l_WM_p_idx = find(ismember(WM_quad, l_WM_p, 'rows'), 1);
    
    % Lateral GM point of DC contour.
    l_GM_p = m_seg_LT(end, :);
    l_GM_p_idx = find(ismember(GM_quad, l_GM_p, 'rows'), 1);
    
    % Form DC contour. To have a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the point
    % of the WM with x=0 and y<0.
    if (l_GM_p_idx <= 2)
        seg_GM = [];
    else
        seg_GM = GM_quad(l_GM_p_idx - 1: -1 : 2, :);
    end
    seg_WM = WM_quad(1 : l_WM_p_idx - 1, :);
    this.DC.contour = [seg_WM; m_seg_LT; seg_GM; m_seg_DC];
    
    % Form area.
    this.DC.area = polyarea(this.DC.contour(:, 1), this.DC.contour(:, 2));
    
end
