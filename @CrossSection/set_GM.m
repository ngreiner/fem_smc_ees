function set_GM(this)
    % Set the atrribute GM.
    %
    % Usage:
    %   cs.set_GM()
    %
    % Description:
    %   Assumes that the attributes dir and z are already properly defined.
    fileID = fopen(fullfile(this.dir, 'GM.txt'));
    GM = textscan(fileID, '%f %f');
    fclose(fileID);
    this.GM = [GM{1}, GM{2}, this.z * ones(size(GM{1}))];
end
