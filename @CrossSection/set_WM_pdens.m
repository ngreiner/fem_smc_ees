function set_WM_pdens(this)
    % Sets the attribute WM_pdens.
    %
    % Usage: 
    %   cs.set_WM_pdens()
    L = sum(sqrt(sum(diff(this.WM(:, 1 : 2)).^2, 2)), 1);
    np = size(this.WM(:, 1), 1);
    this.WM_pdens = np / L;
end
