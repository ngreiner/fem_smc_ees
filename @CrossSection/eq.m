function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == "obj2".
    
    outbool = false;
    
    if ~ strcmp(obj1.name, obj2.name)
        fprintf('Attributes `name` are different.\n')
        return
    end
    
    if ~ strcmp(obj1.dir, obj2.dir)
        fprintf('Attributes `dir` are different.\n')
        return
    end
    
    if ~ (obj1.z == obj2.z)
        fprintf('Attributes `z` are different.\n')
        return
    end
    
    if ~ isequal(obj1.GM, obj2.GM)
        fprintf('Attributes `GM` are different.\n')
        return
    end
    
    if ~ isequal(obj1.WM, obj2.WM)
        fprintf('Attributes `WM` are different.\n')
        return
    end
    
    if ~ isequal(obj1.GM_pdens, obj2.GM_pdens)
        fprintf('Attributes `GM_pdens` are different.\n')
        return
    end
    
    if ~ isequal(obj1.WM_pdens, obj2.WM_pdens)
        fprintf('Attributes `WM_pdens` are different.\n')
        return
    end
    
    if ~ isequal(obj1.mean_pdens, obj2.mean_pdens)
        fprintf('Attributes `mean_pdens` are different.\n')
        return
    end
    
    if ~ isequal(obj1.CSF, obj2.CSF)
        fprintf('Attributes `CSF` are different.\n')
        return
    end
    
    if ~ isequal(obj1.fat, obj2.fat)
        fprintf('Attributes `fat` are different.\n')
        return
    end
    
    if ~ isequal(obj1.dura, obj2.dura)
        fprintf('Attributes `dura` are different.\n')
        return
    end
    
    if ~ isequal(obj1.DH, obj2.DH)
        fprintf('Attributes `DH` are different.\n')
        return
    end
    
    if ~ isequal(obj1.LT, obj2.LT)
        fprintf('Attributes `LT` are different.\n')
        return
    end
    
    if ~ isequal(obj1.MGM, obj2.MGM)
        fprintf('Attributes `MGM` are different.\n')
        return
    end
    
    if ~ isequal(obj1.MR, obj2.MR)
        fprintf('Attributes `MR` are different.\n')
        return
    end
    
    if ~ isequal(obj1.VH, obj2.VH)
        fprintf('Attributes `VH` are different.\n')
        return
    end
    
    if ~ isequal(obj1.VQ, obj2.VQ)
        fprintf('Attributes `VQ` are different.\n')
        return
    end
    
    if ~ isequal(obj1.ST, obj2.ST)
        fprintf('Attributes `ST` are different.\n')
        return
    end
    
    if ~ isequal(obj1.CT, obj2.CT)
        fprintf('Attributes `CT` are different.\n')
        return
    end
    
    if ~ isequal(obj1.DC, obj2.DC)
        fprintf('Attributes `DC` are different.\n')
        return
    end
    
    if ~ isequal(obj1.OD, obj2.OD)
        fprintf('Attributes `OD` are different.\n')
        return
    end
    
    outbool = true;
    
end
