function plot(this, varargin)
    % Plot the cross-section's compartments' contours.
    %
    % Usage:
    %   cs.plotCS()
    %
    % Description:
    %   The compartments include the GM, the WM, the CSF, the fat, the dura
    %   and the inner compartments of the GM: DH, MGM, VH, LT, DC, CT and
    %   ST. 
    %   A new figure is created.
    
    % Parse inputs.
    GM = true;
    WM = true;
    CSF = true;
    fat = true;
    dura = true;
    LT = false;
    DH = false;
    VH = false;
    VQ = false;
    MGM = false;
    MR = false;
    ST = false;
    CT = false;
    DC = false;
    OD1 = false;
    OD2 = false;
    OD3 = false;
    OD4 = false;
    OD5 = false;
    OD6 = false;
    lims = [];
    lw = 1;
    while ~ isempty(varargin)
        switch varargin{1}
            case 'noGM'
                GM = false;
            case 'noWM'
                WM = false;
            case 'noCSF'
                CSF = false;
            case 'nofat'
                fat = false;
            case 'nodura'
                dura = false;
            case 'LT'
                LT = true;
            case 'DH'
                DH = true;
            case 'VH'
                VH = true;
            case 'VQ'
                VQ = true;
            case 'MGM'
                MGM = true;
            case 'MR'
                MR = true;
            case 'ST'
                ST = true;
            case 'CT'
                CT = true;
            case 'DC'
                DC = true;
            case 'OD1'
                OD1 = true;
            case 'OD2'
                OD2 = true;
            case 'OD3'
                OD3 = true;
            case 'OD4'
                OD4 = true;
            case 'OD5'
                OD5 = true;
            case 'OD6'
                OD6 = true;
            case 'all'
                LT = true;
                DH = true;
                VH = true;
                VQ = true;
                MGM = true;
                MR = true;
                ST = true;
                CT = true;
                DC = true;
                OD1 = true;
                OD2 = true;
                OD3 = true;
                OD4 = true;
                OD5 = true;
                OD6 = true;
            case 'OD'
                OD1 = true;
                OD2 = true;
                OD3 = true;
                OD4 = true;
                OD5 = true;
                OD6 = true;
            case 'noOD'
                OD1 = false;
                OD2 = false;
                OD3 = false;
                OD4 = false;
                OD5 = false;
                OD6 = false;
            case {'axis', 'lims'}
                lims = varargin{2};
                varargin(1) = [];
            case 'lw'
                lw = varargin{2};
                varargin(1) = [];
        end
        varargin(1) = [];
                
    end
    
    figure
    title(sprintf('%s', this.name))
    hold on
    if ~ isempty(lims)
        axis(lims)
    end
    
    if GM
        plot(this.GM(:, 1), this.GM(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if WM
        plot(this.WM(:, 1), this.WM(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if CSF
        plot(this.CSF(:, 1), this.CSF(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if fat
        plot(this.fat(:, 1), this.fat(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if dura
        plot(this.dura(:, 1), this.dura(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if LT
        plot(this.LT.contour(:, 1), this.LT.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if DH
        plot(this.DH.contour(:, 1), this.DH.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if VH
        plot(this.VH.contour(:, 1), this.VH.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if VQ
        plot(this.VQ.contour(:, 1), this.VQ.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if MGM
        plot(this.MGM.contour(:, 1), this.MGM.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if MR
        plot(this.MR.contour(:, 1), this.MR.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if ST
        plot(this.ST.contour(:, 1), this.ST.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if CT
        plot(this.CT.contour(:, 1), this.CT.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if DC
        plot(this.DC.contour(:, 1), this.DC.contour(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD1
        plot(this.OD{1}(:, 1), this.OD{1}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD2
        plot(this.OD{2}(:, 1), this.OD{2}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD3
        plot(this.OD{3}(:, 1), this.OD{3}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD4
        plot(this.OD{4}(:, 1), this.OD{4}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD5
        plot(this.OD{5}(:, 1), this.OD{5}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
    if OD6
        plot(this.OD{6}(:, 1), this.OD{6}(:, 2), 'LineWidth', lw, 'Color', 'black')
    end
    
end
