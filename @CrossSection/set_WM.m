function set_WM(this)
    % Set the attribute of the WM.
    %
    % Usage: 
    %   cs.set_WM()
    %
    % Description:
    %   Assumes that the attributes dir and z are already properly defined.
    fileID = fopen(fullfile(this.dir, 'WM.txt'));
    WM = textscan(fileID, '%f %f %f');
    fclose(fileID);
    this.WM = [WM{1}, WM{2}, this.z * ones(size(WM{1}))];
end
