function set_VQ(this)
    % Set the attribute VQ.
    %
    % Usage: 
    %   cs.set_VQ()
    %
    % Description:
    %   The VQ denotes the ventro-lateral quarter of the ventral horn of
    %   the GM.
    %
    %   The attribute VQ is a structure with only 1 field: 'contour'.
    %   'contour' is the entire contour describing the VQ.
    %
    %   This method assumes that the attribute VH is already properly
    %   defined.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.

    % Recover VH.
    VH = this.VH.contour;

    % Get center of mass.
    com = mean(VH);

    % Recover left half of GM contour.
    GM_half = this.GM(this.GM(:, 1)<=0, :);
    GM_half(end, :) = [];

    % Find point of the GM half contour with y-coordinate closest to com's
    % y-coordinate.
    [idx, ~] = knnsearch(GM_half(:, 2), com(:, 2), 'k', 3);
    idx = idx(GM_half(idx, 1) < com(:, 1));
    l_p_idx = idx(1);
    l_p = GM_half(l_p_idx, :);

    % Find point of the GM half contour with x-coordinate closest to com's
    % x-coordinate.
    [idx, ~] = knnsearch(GM_half(:, 1), com(:, 1), 'k', 3);
    idx = idx(GM_half(idx, 2) > com(:, 2));
    t_p_idx = idx(1);
    t_p = GM_half(t_p_idx, :);

    % Form horizontal segment of the VQ.
    d = norm(com - l_p);
    x = linspace(com(1), l_p(1), floor(d * this.GM_pdens))';
    y = linspace(com(2), l_p(2), floor(d * this.GM_pdens))';
    h_seg = [x, y, this.z * ones(size(x, 1), 1)];

    % Form vertical segment of the VQ.
    d = norm(t_p - com);
    x = linspace(t_p(1), com(1), floor(d * this.GM_pdens))';
    y = linspace(t_p(2), com(2), floor(d * this.GM_pdens))';
    v_seg = [x, y, this.z*ones(size(x, 1), 1)];
    
    % Form VQ contour. To get a closed-contour, we need to repeat one
    % and only one point in the point sequence. We choose to repeat C.
    this.VQ.contour = [h_seg; GM_half(l_p_idx + 1 : t_p_idx - 1, :); v_seg];
    
end
