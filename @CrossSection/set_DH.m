function set_DH(this)
    % Set the attribute DH.
    %
    % Usage:
    %   cs.set_DH()
    %
    % Description:
    %   The dorsal horn (DH) is the dorsal region of the GM. Here we focus
    %   only on the left DH. We define it as the lower left region of the
    %   GM, separated from the above region (central and ventral) by a
    %   horizontal line.
    %
    %   The attribute DH is a structure with 2 fields: 'contour' and
    %   'line'. 'contour' is the entire contour describing the DH and
    %   'line' is the horizontal line separating it from the central region
    %   of the GM.
    %
    %   This method assumes that the GM contour is clockwise oriented and
    %   that its first point (which is also the last) has x=0 and y<0.
    
    % Take only dorso-left quadrant of GM contour for convenience.
    GM_quad = this.GM(this.GM(:, 1)<=0 & this.GM(:, 2)<=0, :);
    GM_quad(end, :) = [];

    % Lateral DH point: it is a point in the middle region of the lateral
    % GM contour where the slope passes from positive to negative.
    % The lateral GM contour can be isolated by selecting all the points
    % between the point of the contour fulfilling the condition y=ymin and
    % the last point of the contour.
    [~, ymin_idx] = min(GM_quad(:, 2));
    GM_quad_lat = GM_quad(ymin_idx : end, :);
    % Check for slope inversion events.
    slopes_GM = diff(GM_quad_lat(:, 2)) ./ diff(GM_quad_lat(:, 1));
    idx = find(diff(sign(slopes_GM)) < 0);
    % Divide GM_quad_Lat into 3 regions with equal extent along y-axis.
    % The position of slope inversion event we are looking for should be in
    % the middle region, and the lowermost among them events.
    ymax = max(GM_quad_lat(:, 2));
    ymin = min(GM_quad_lat(:, 2));
    d = (ymax - ymin) / 3;
    idx2 = [];
    for j = 1 : size(idx, 1)
        if  (GM_quad_lat(idx(j), 2) >= ymin + d) && (GM_quad_lat(idx(j), 2) <= ymax - d)
            idx2 = idx(j);
            continue
        end
    end
    % If no such event occurs in the middle region, look for the point of
    % the lower region which is closest to the middle region.
    if isempty(idx2)
        D = min(abs(GM_quad_lat(idx, 2) - (ymin + d)), abs(GM_quad_lat(idx, 2) - (ymax - d)));
        l_p_idx = idx(D == min(D)) + ymin_idx;
    else
        l_p_idx = idx2 + ymin_idx;
    end
    l_p = GM_quad(l_p_idx, :);

    % Medial DH point: it is the point on the medial GM contour having its
    % y-coordinate closest to that of the lateral DH point.
    % The medial GM contour can be isolated by selecting all the points
    % between the 1st point of the contour and the point fulfilling the
    % condition y=ymin.
    [~, ymin_idx] = min(GM_quad(:, 2));
    [~, D] = knnsearch(GM_quad(l_p_idx, 2), GM_quad(1 : ymin_idx, 2), 'k', 1);
    [~, m_p_idx] = min(D);
    m_p = GM_quad(m_p_idx, :);

    % Form DH line.
    d = norm(m_p - l_p);
    x = linspace(m_p(1), l_p(1), floor(d * this.GM_pdens))';
    y = linspace(m_p(2), l_p(2), floor(d * this.GM_pdens))';
    this.DH.line = [x, y, this.z * ones(size(x))];

    % Form DH contour. To have a closed-contour, we need to repeat one and
    % only one point in the point sequence. We choose to repeat the lateral
    % point of the DHline (which belongs to the GM as well).
    seg_GM = GM_quad(l_p_idx : -1 : m_p_idx + 1, :);
    this.DH.contour = [seg_GM; this.DH.line];

end
