classdef DRcBFiber < DRFiber

	% This class is a perfect copy of the DRFiber class.
	% They only differ by their name, which influences the reading
	% and writing of files in the file system.
    
    methods
        function this = DRcBFiber()
        end
    end
    
end