classdef IaFiberOD3 < IaFiber

	% This class is a near-perfect copy of the IaFiber class.
	% Its purpose is to represent Ia fibers whose dorsal column branches
	% run in the OD3 compartment.
	%
	% The only differences lie in the get_RDCCS and get_CDCCS methods.
    
    properties
    end
    
    methods
        function this = IaFiberOD3()
        end
    end
    
    methods
        assign_MNs(this, smcd)
        CS = get_CDCCS(this, smcd)
        CS = get_RDCCS(this, smcd)
        set_cols(this, smcd)
    end
    
end
