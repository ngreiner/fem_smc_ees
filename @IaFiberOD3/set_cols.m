function set_cols(this, smcd)
    % Set the collaterals 'cols' of the IaFiber.
    %
    % Usage:
    %   ia.set_cols(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The collaterals of the IaFiber depart from the ascending and
    %   descending branches, and run ventro-laterally (and slightly
    %   rostrally) towards the motor nuclei in the grey matter of the
    %   spinal cord.
    %   The number of them is generated from a statistical model based
    %   on the experimental evidence of (Brown and Fyffe, 1978).
    %   Each of them innervate a single family of MNs, and the families do
    %   not overlap.
    %   They run from a node on either the ascending or the descending
    %   branch to the center of mass of their target MN family.
    %   Additional points in the middle region of the GM serves for
    %   interpolation of cubic splines materializing their trajectories.
    %   Their diameters are drawn from a log-normal distribution with
    %   parameters specified by the input SMCDModel.
    %   Finally, their compartments' coordinates are distributed along
    %   their trajectories.
    
    if smcd.params.verbose
        fprintf(Utils.padn('  Setting collaterals  ', 'both', '-', 70))
        t0 = cputime;
    end
    
    % Parameters for the collaterals' diameters.
    mu = smcd.morpho.IaOD3.mu_cols;
    sig = smcd.morpho.IaOD3.sig_cols;
    
    % Generate theoretical number of collaterals.
    ncols = this.gen_ncols();
    
    % Split MNs innervated by the Ia into ncols families corresponding to
    % each collateral.
    [MNfamilies, ncols] = this.gen_MN_families(smcd, ncols);
    
    % Generate interpolation points for the collaterals' pathways.
    % In so doing, rectify number of collaterals so that no 2 collaterals
    % originate from the same node of Ranvier. If ever that would be the
    % case, merge the two corresponding MN families such that they be
    % innervated by the same and single collateral.
    P3s = [];
    P2s = [];
    P1s = [];
    locs = {};
    i = 1;
    while i <= ncols
        P3 = mean(cell2mat(cellfun(@(x) x.soma.coords, smcd.entities.MNs(MNfamilies{i}), 'UniformOutput', false)), 1);
        P2 = smcd.fem.sc.gen_MR_point(P3(3));
        if strcmp(this.musc(end - 1 : end), '_r')
            P2(1) = - P2(1);
        end
        [P1, loc] = this.find_node(P2(3));
        if (i > 1) && isequal(P1, P1s(i - 1, :))
            ncols = ncols - 1;
            P3s = [];
            P2s = [];
            P1s = [];
            locs = {};
            i = 1;
            [MNfamilies, ncols] = this.gen_MN_families(smcd, ncols);
        else
            P3s = [P3s; P3];
            P2s = [P2s; P2];
            P1s = [P1s; P1];
            locs{end + 1, 1} = loc;
            i = i + 1;
        end
    end
    this.MNfamilies = MNfamilies;
    
    % Generate Cols.
    cols = cell(ncols, 1);
    for i = 1 : ncols
        
        if smcd.params.verbose
            fprintf(Utils.padn(sprintf('  collateral  #%d  ', i), 'both', ':', 40))
        end
        
        % Initialize branch.
        cols{i} = Branch();
        cols{i}.label = sprintf('col%d', i);
        cols{i}.model = this.model;
        
        % Generate random diameter.
        cols{i}.diam = lognrnd(mu, sig);
        
        % Generate path.
        inter_points = [P1s(i, :); P2s(i, :); P3s(i, :)];
        d = norm(P2s(i, :) - P1s(i, :)) + norm(P3s(i, :) - P2s(i, :));
        np = round(smcd.params.pdens * d);
        cols{i}.path = Geom.interparc(np, inter_points(:, 1), inter_points(:, 2), inter_points(:, 3), 'verbose', smcd.params.verbose);
        
        % Derive and build branch's compartments' coordinates.
        cols{i}.build_comps_coords(0);
        
        % Initialize cell-arrays for branch's compartments potentials.
        cols{i}.VN = cell(smcd.get_nAS(), 1);
        cols{i}.VP1 = cell(smcd.get_nAS(), 1);
        cols{i}.VP2 = cell(smcd.get_nAS(), 1);
        cols{i}.VI = cell(smcd.get_nAS(), 1);
        
        % Store attachment point.
        cols{i}.p_attach = locs{i};
        
    end
    
    % Return.
    this.cols = cols;
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting collaterals. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end
    
end
