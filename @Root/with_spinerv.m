function bool = with_spinerv(this)
    % Return the attribute sc.roots_params.with_spinerv.
    %
    % Usage:
    %   bool = rt.with_spinerv()
    %
    % Return:
    %   bool : logical.
    bool = this.sc.roots_params.with_spinerv;
end
