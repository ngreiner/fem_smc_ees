function set_Lspinerv_Gen(this)
    % Set the attribute Lspinerv.Gen.
    %
    % Usage:
    %   rt.set_Lspinerv_Gen()
    %
    % Description:
    %   Lspinerv.Gen stores the cell-array of generatrices of the left
    %   spinal nerve.
    %   See set_LD_Gen for explanations.
    
    % Unpack attributes in convenient variables.
    nCS = this.Lspinerv.nCS;
    CS = this.Lspinerv.CS;
    
    % Build generatrix.
    Gen = cell(4, 1);
    for i = 1 : 4
        Gen{i} = zeros(nCS, 3);
        for k = 1 : nCS
            Gen{i}(k, :) = CS{k}(1 + floor((i - 1) * size(CS{k}, 1) / 4), :);
        end
    end
    
    % Assign attribute.
    this.Lspinerv.Gen = Gen;
    
end
