function set_LV_p_entry(this)
    % Set the attribute LV.p_entry
    %
    % Usage:
    %   rt.set_LV_p_entry()
    %
    % Description: 
    %   LV.p_entry stores the position of the left ventral root entry
    %   point.
    %   This point is the the first point of the LV guide curve, and as
    %   such, it is to be the center of the first cross-section of the LV
    %   (if no rootlets are included) or of the first cross-section of the
    %   central rootlet of the LV (if rootlets are included).
    %   Its coordinates are obtained as follows:
    %       1. We determine its elevation z_entry based on the elevation of
    %       the lower cross-section CS_down of the segment of insertion of
    %       the root, on the diameter and excentricity of the root, and
    %       on the presence or absence of rootlets. z_entry is such that
    %       the caudal-most point of the first cross-section of the LV
    %       (if no rootlets are included) or of the first cross-section of
    %       the caudal-most rootlet (if rootlets are included) lies at the
    %       elevation of CS_down.
    %       2. We determine the direction alpha_entry of the entry point as
    %       the min between:
    %           a. a weighted average of the angular directions of the
    %           center of masses of the VQs of the cross-sections CS_down
    %           and CS_up surrounding z_entry;
    %           b. a little less than the angular direction of the
    %           intervertebral foramina through which the root exits.
    %       3. We determine the coordinates of the point of a virtual WM
    %       contour lying at the elevation z_entry in the direction
    %       alpha_entry. 
    %       4. We scale down the distance to the z-axis of the
    %       computed point to ensure that it will be strictly within the
    %       convex hull of the WM.
    
    % Unpack root parameters.
    params = this.sc.roots_params;

    % Compute z_entry, z-coordinate of the entry point.
    idx = find(strcmp(this.sc.morpho.CS_names, this.name));
    z_up = this.sc.geom.zCS(idx);
    z_down = this.sc.geom.zCS(idx + 1);
    if ~ this.with_rlets()
        z_entry = z_down + params.diam * params.excREZ / 2;
    else
        z_entry = z_down + floor(params.nrlets / 2) * ...
            (params.rlet_diam * params.rlet_exc + params.rlet_gap) + ...
            params.rlet_diam * params.rlet_exc / 2;
    end
    
    % Compute angular direction of the entry point. It is the min between:
    %   1. a weighted average of the angular directions of the center of
    %   masses of the VQs of the cross-sections surrounding z_entry
    %   2. a little less than the angular direction of the intervertebral
    %   foramina through which the root exits
    [w_up, w_down] = this.sc.get_weights(z_entry);
    VQ_up = this.sc.CS{idx}.VQ.contour;
    VQ_down = this.sc.CS{idx + 1}.VQ.contour;
    COM_up = mean(VQ_up, 1);
    COM_down = mean(VQ_down, 1);
    tmp = w_up * COM_up(1 : 2) + w_down * COM_down(1 : 2);
    alpha1 = atan2(tmp(2), tmp(1));
    alpha2 = 0.95 * atan2(this.LD.p_exit_IF(2), this.LD.p_exit_IF(1));
    alpha_entry = min(alpha1, alpha2);

    % Compute virtual WM point in the direction alpha_entry at z_entry.
    WM_up = this.sc.get_contour('WM', z_up);
    WM_down = this.sc.get_contour('WM', z_down);
    p_up = Geom.get_point_from_angle(WM_up, alpha_entry); 
    p_down = Geom.get_point_from_angle(WM_down, alpha_entry);
    p = w_up * p_up + w_down * p_down;

    % The entry point.
    this.LV.p_entry = [p(1 : 2) * params.eps_p_entry, z_entry];

end
