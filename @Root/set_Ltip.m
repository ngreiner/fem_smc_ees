function set_Ltip(this)
    % Set the attribute Ltip.
    %
    % Usage:
    %   rt.set_Ltip()
    %
    % Description: 
    %   Ltip stores the coordinates of the left spinal nerve tip, i.e. of
    %   the most distal point of the guide curve of the spinal nerve.
    %   It is not defined when the spinal nerve is not included.
    if this.with_spinerv()
        this.Ltip = this.Lspinerv.GC(end, :);
    end
end
