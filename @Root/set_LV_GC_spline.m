function set_LV_GC_spline(this)
    % Build the left ventral guide curve.
    %
    % Usage:
    %   rt.set_LV_GC_spline()
    %
    % Description:
    %   This method is called to construct the LV.GC when the attribute
    %   sc.roots_params.GC_type is 'spline'.
    %
    %   The strategy is similar to that employed for the LD.GC with the
    %   exception of a notable difference when the spinerv is included.
    %
    %   When this is the case, we want the LV.GC to smoothly connect the
    %   spinerv GC. To do so, we add to the collection of the interpolation
    %   points defining the LV.GC the points of the spinerv GC itself, and
    %   we truncate the distribution of points along the spline
    %   interpolating these points at the LV.p_exit_IF.
    %   However, we have found that if the initial collection of
    %   interpolation points (before adding the spinerv GC) ends with the
    %   LV.p_exit_IF, as is the case for the LD.GC, the resulting
    %   connection between the LV.GC and the spinerv GC remains rather
    %   abrupt. Thus, we instead set this last interpolation point to be
    %   the LV.p_exit_CSF, which results in a smoother connection.
    
    % Convenient variables.
    params = this.sc.roots_params;
    nip = params.LV_GC_ninter_points;

    % Recover entry and exit points.
    x1 = this.LV.p_entry;
    if this.with_spinerv()
        x2 = this.LV.p_exit_CSF;
    else
        x2 = this.LV.p_exit_IF;
    end
    
    % Angles of x1 and x2.
    alpha1 = atan2(x1(2), x1(1));
    if x2(1) < 0
        alpha2 = atan(x2(2) / x2(1)) + pi;
    elseif x2(1) > 0
        alpha2 = atan(x2(2) / x2(1));
    else
        if x2(2) > 0
            alpha2 = pi/2;
        else
            alpha2 = -pi/2;
        end
    end
    
    % Regress alpha2-alpha1 in interval ]-pi,pi].
    theta = alpha2 - alpha1;
    while theta > pi
        theta = theta - 2*pi;
    end
    while theta <= -pi
        theta = theta + 2*pi;
    end
    
    % Define z-levels of interpolation points.
    dz = (x2(3) - x1(3)) / (nip - 1);
    zip = zeros(nip - 2, 1);
    for i = 1 : nip - 2
        zip(i) = x1(3) + dz * i;
    end

    % Define angles of interpolation points. 
    dalpha = theta / (nip - 1);
    angles = zeros(nip - 2, 1);
    for i = 1 : nip - 2
        if i == 1
            w = (nip - 1) * (i / (nip - 1))^1.75;
        else
            w = i;
        end
        angles(i) = alpha1 + dalpha * w;
        while angles(i) > pi
            angles(i) = angles(i) - 2*pi;
        end
        while angles(i) <= -pi
            angles(i) = angles(i) + 2*pi;
        end
    end
    
    % Index of point at which a switch in the way to compute the distance
    % is necessary. It is the median point of the collection.
    idx = floor((nip + 1) / 2);

    % Define interpolation points.
    inter_points = zeros(nip, 3);
    inter_points(1, :) = x1;
    inter_points(end, :) = x2;
    for i = 1 : nip - 2
        if i <= idx - 1
            p_WM_above = Geom.get_point_from_angle(this.sc.get_contour('WM', zip(i), 'above'), angles(i));
            p_WM_below = Geom.get_point_from_angle(this.sc.get_contour('WM', zip(i), 'below'), angles(i));
            [w_above, w_below] = this.sc.get_weights(zip(i));
            d_WM_p = w_above * norm(p_WM_above(1 : 2)) + w_below * norm(p_WM_below(1 : 2));
            w = 0;
        else
            w = (i - idx + 1) / (nip - idx);
        end
        d = (1 - w) * (d_WM_p + params.GC_gap) + w * norm(x2(1 : 2));
        inter_points(i + 1, :) = [d * cos(angles(i)), d * sin(angles(i)), zip(i)];
    end
    
    % If spinerv is included, its guide curve has already been created
    % during the call to set_LD_GC, and we want to constrain the LV guide
    % curve to smoothly connect to the spinerv guide curve. This can be
    % done by appending to the previous inter_points the spinerv guide
    % curve itself, interpolating this extended collection of points, and
    % truncating the resulting curve at the first point of the spinerv
    % guide curve.
    if this.with_spinerv()
        inter_points = [inter_points; this.Lspinerv.GC];
        args = {'stop_at_idx', nip + 1, 'spline'};
    else
        args = {'spline'};
    end
    
    % Build spline interpolating the previous interpolation points and
    % distribute points along it. This is all achieved by the function
    % interparc.
    np = floor(params.GC_pdens * Geom.get_arc_length(inter_points));
    GC = Geom.interparc(np, inter_points(:, 1), inter_points(:, 2), inter_points(:, 3), args{:});

    % Assign attributes.
    this.LV.GC = GC;
    
end
