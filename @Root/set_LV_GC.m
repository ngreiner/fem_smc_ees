function set_LV_GC(this)
    % Set the guide curve of the left ventral root (attribute LV.GC).
    %
    % Usage:
    %   rt.set_LV_GC()
    %
    % Description:
    %   The construction of the guide curve depends upon the type of
    %   guide curve, which is controlled by the attribute
    %   sc.roots_params.GC_type.
    
    if strcmp(this.sc.roots_params.GC_type, 'generic')
        this.set_LV_GC_generic();
        
    elseif strcmp(this.sc.roots_params.GC_type, 'spline')
        this.set_LV_GC_spline();
        
    else
        warning('Wrong value for attribute sc.roots_params.GC_type. Using ''spline'' option.')
        this.set_LV_GC_spline();
        
    end

end
