function set_LD_p_entry(this)
    % Set the attribute LD.p_entry
    %
    % Usage:
    %   rt.set_LD_p_entry()
    %
    % Description: 
    %   LD.p_entry stores the position of the left dorsal root entry point.
    %   This point is the the first point of the LD guide curve, and as
    %   such, it is to be the center of the first cross-section of the LD
    %   (if no rootlets are included) or of the first cross-section of the
    %   central rootlet of the LD (if rootlets are included).
    %   Its coordinates are obtained as follows:
    %       1. We determine its elevation z_entry based on the elevation of
    %       the cross-sections CS_up and CS_down of the segment of
    %       insertion of the root, on the diameter and excentricity of the
    %       root, and on the presence or absence of rootlets. z_entry is
    %       such that the caudal-most point of the single cross-section of
    %       the LD (if no rootlets are included) or of the cross-section of
    %       the caudal-most rootlet (if rootlets are included) lies at the
    %       elevation of CS_down.
    %       2. We compute the coordinates of the point of a virtual WM
    %       contour lying at the elevation z_entry in the direction alphaD.
    %       3. We scale down the distance to the z-axis of the
    %       computed point to ensure that it will be strictly within the
    %       convex hull of the WM.
    
    % Unpack root parameters.
    params = this.sc.roots_params;

    % Compute z_entry, z-coordinate of the entry point.
    idx = find(strcmp(this.sc.morpho.CS_names, this.name));
    z_up = this.sc.geom.zCS(idx);
    z_down = this.sc.geom.zCS(idx + 1);
    if ~ this.with_rlets()
        z_entry = z_down + params.diam * params.excREZ / 2;
    else
        z_entry = z_down + floor(params.nrlets / 2) * ...
            (params.rlet_diam * params.rlet_exc + params.rlet_gap) + ...
            params.rlet_diam * params.rlet_exc / 2;
    end
    
    % Compute angular direction of the entry point. It is the angular
    % direction of the farthest point from the z-axis of the DH at
    % z=z_entry. This point is obtained as a weighted average of those
    % farthest points from the z-axis of the DH in the cross-sections
    % surrounding z_entry.
    [w_up, w_down] = this.sc.get_weights(z_entry);
    DH_up = this.sc.CS{idx}.DH.contour;
    DH_down = this.sc.CS{idx + 1}.DH.contour;
    p_up = Geom.get_farthest_point_from_zaxis(DH_up);
    p_down = Geom.get_farthest_point_from_zaxis(DH_down);
    tmp = w_up * p_up(1 : 2) + w_down * p_down(1 : 2);
    alpha_entry = atan2(tmp(2), tmp(1));

    % Compute virtual WM point in the direction alpha_entry at z_entry.
    WM_up = this.sc.get_contour('WM', z_up);
    WM_down = this.sc.get_contour('WM', z_down);
    p_up = Geom.get_point_from_angle(WM_up, alpha_entry); 
    p_down = Geom.get_point_from_angle(WM_down, alpha_entry);
    p = w_up * p_up + w_down * p_down;

    % The entry point.
    this.LD.p_entry = [p(1 : 2) * params.eps_p_entry, z_entry];

end
