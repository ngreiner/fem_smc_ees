function set_Lspinerv_idxs_CS(this)
    % Set the attribute Lspinerv.idxCS.
    %
    % Usage:
    %   rt.set_Lspinerv_idxs_CS()
    %
    % Description:
    %   Lspinerv.idxs_CS stores the indexes of the points of the
    %   Lspinerv.GC which should serve as centers for the Lspinerv
    %   cross-sections. 
    %   First, the number of cross-sections need be defined: it is
    %   determined from the lineic density of cross-sections per unit arc
    %   length of guide curve and from the guide curve arc length.
    %   Second, the indexes of the guide curve points serving as centers
    %   for the cross-sections are selected based on the following
    %   criteria:
    %       1. the first and last points should be selected;
    %       2. the selected points should be as uniformly spaced as
    %       possible.
    %   However, the first cross-section of the Lspinerv (which should
    %   normally be centered around the first point of the Lspinerv.GC) is
    %   going to coincide with the last cross-sections of the LV and LD.
    %   Thus, the actual number of cross-sections of the Lspinerv is
    %   reduced by 1 unit compared to the above prescription, and the
    %   index 1 not included in the collection Lspinerv.idxs_CS.
    
    % Unpack attributes in variables for convenience.
    spinerv_length = this.sc.roots_params.spinerv_length;
    CS_dens = this.sc.roots_params.CS_dens;
    
    % Number of cross-sections.
    nCS = floor(CS_dens * spinerv_length) - 1;
    
    % Select indexes of guide curve points.
    idxs_CS = zeros(nCS, 1);
    L = size(this.Lspinerv.GC, 1);
    q = floor(L / nCS);
    r = floor(mod(L, nCS) / 2);
    
    % We discard 1 as an index.
    
    % The last index should be the last index of the Lspinerv GC.
    idxs_CS(end) = L;
    
    % The other ones should be evenly spaced.
    for i = 1 : nCS - 1
        idxs_CS(i) = q * i + 1 + r;
    end
    
    % Assign attributes.
    this.Lspinerv.nCS = nCS;
    this.Lspinerv.idxs_CS = idxs_CS;
    
end
