function scale(this, scale_factors)
    % Scale the geometric attributes of the Root.
    %
    % Usage:
    %   rt.scale(scale_factors)
    %
    % Description:
    %   TO BE COMPLETED.
    
    scale_mat = diag(scale_factors);
    
    this.LD.p_entry = this.LD.p_entry * scale_mat;
    this.LD.p_exit_IF = this.LD.p_exit_IF * scale_mat;
    this.LD.p_exit_CSF = this.LD.p_exit_CSF * scale_mat;
    this.LD.GC = this.LD.GC * scale_mat;
    this.LD.CS = cellfun(@(x) x * scale_mat, this.LD.CS, 'UniformOutput', false);
    this.LD.Gen = cellfun(@(x) x * scale_mat, this.LD.Gen, 'UniformOutput', false);
    
    this.LV.p_entry = this.LV.p_entry * scale_mat;
    this.LV.p_exit_IF = this.LV.p_exit_IF * scale_mat;
    this.LV.p_exit_CSF = this.LV.p_exit_CSF * scale_mat;
    this.LV.GC = this.LV.GC * scale_mat;
    this.LV.CS = cellfun(@(x) x * scale_mat, this.LV.CS, 'UniformOutput', false);
    this.LV.Gen = cellfun(@(x) x * scale_mat, this.LV.Gen, 'UniformOutput', false);
    
    if this.with_spinerv()
        this.Lspinerv.GC = this.Lspinerv.GC * scale_mat;
        this.Lspinerv.CS = cellfun(@(x) x * scale_mat, this.Lspinerv.CS, 'UniformOutput', false);
        this.Lspinerv.Gen = cellfun(@(x) x * scale_mat, this.Lspinerv.Gen, 'UniformOutput', false);
    end
    
    if this.with_rlets()
        for i = 1 : this.sc.roots_params.nrlets
            this.LD.rlets{i}.GC = this.LD.rlets{i}.GC * scale_mat;
            this.LD.rlets{i}.CS = cellfun(@(x) x * scale_mat, this.LD.rlets{i}.CS, 'UniformOutput', false);
            this.LD.rlets{i}.Gen = cellfun(@(x) x * scale_mat, this.LD.rlets{i}.Gen, 'UniformOutput', false);
            this.LV.rlets{i}.GC = this.LV.rlets{i}.GC * scale_mat;
            this.LV.rlets{i}.CS = cellfun(@(x) x * scale_mat, this.LV.rlets{i}.CS, 'UniformOutput', false);
            this.LV.rlets{i}.Gen = cellfun(@(x) x * scale_mat, this.LV.rlets{i}.Gen, 'UniformOutput', false);
        end
    end
    
    for i = 1 : this.LD.n_full_branches
        this.LD.full_branches{i}.GC = this.LD.full_branches{i}.GC * scale_mat;
        this.LD.full_branches{i}.CS = cellfun(@(x) x * scale_mat, this.LD.full_branches{i}.CS, 'UniformOutput', false);
        this.LD.full_branches{i}.Gen = cellfun(@(x) x * scale_mat, this.LD.full_branches{i}.Gen, 'UniformOutput', false);
    end
    
    for i = 1 : this.LV.n_full_branches
        this.LV.full_branches{i}.GC = this.LV.full_branches{i}.GC * scale_mat;
        this.LV.full_branches{i}.CS = cellfun(@(x) x * scale_mat, this.LV.full_branches{i}.CS, 'UniformOutput', false);
        this.LV.full_branches{i}.Gen = cellfun(@(x) x * scale_mat, this.LV.full_branches{i}.Gen, 'UniformOutput', false);
    end
    
end
