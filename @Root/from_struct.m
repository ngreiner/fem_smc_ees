function from_struct(this, struc)
    % Set the Root object from the the input struc.
    %
    % Usage:
    %   root.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : fields are the same as the Root class properties.
    this.name = struc.name;
    this.LD = struc.LD;
    this.LV = struc.LV;
    this.Lspinerv = struc.Lspinerv;
    this.Ltip = struc.Ltip;
end
