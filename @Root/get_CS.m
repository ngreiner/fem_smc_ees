function CS = get_CS(this, sub_root_label, idx_branch, varargin)
    % Return the CS of branch #idx_branch of sub_root (LV or LD).
    %
    % Usage:
    %   CS = rt.get_CS(sub_root_label)
    %   CS = rt.get_CS(sub_root_label, idx_branch)
    %   CS = rt.get_CS(sub_root_label, idx_branch, 'without_spinerv')
    %
    % Parameters:
    %   sub_root_label : string : 'LD' or 'LV'.
    %   idx_branch : int : index of full branch of sub_root.
    %
    % Description:
    %   sub_root_label should be either 'LV' or 'LD'. The returned
    %   cross-sections are those of the full branch #idx_branch of the
    %   accordingly LV or LD.
    %   If idx_branch is not specified, it defaults to 1.
    
    sub_root = this.(sub_root_label);
    
    if nargin == 2
        idx_branch = 1;
    end
    
    if any(strcmp(varargin, 'without_spinerv'))
        if this.with_rlets()
            CS = [sub_root.rlets{idx_branch}.CS; sub_root.CS];
        else
            CS = sub_root.CS;
        end
        
    else
        CS = sub_root.full_branches{idx_branch}.CS;
        
    end
    
end
