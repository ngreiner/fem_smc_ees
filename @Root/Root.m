classdef Root < handle
    
    % Properties defined here:
    %
    %   name : string :
    %       Name of the Root.
    %
    %   sc : SCModel object :
    %       The SCModel to which the Root belongs, and from which it draws
    %       its parameters.
    %
    %   LD : struct :
    %       Descriptor of the left ventral root. Fields are p_entry,
    %       p_exit_IF, p_exit_CSF, GC, nCS, CS, Gen and rlets.
    %
    %   LV : struct :
    %       Descriptor of the left ventral root. Fields are p_entry,
    %       p_exit_IF, p_exit_CSF, GC, nCS, idxs_CS, CS, Gen and rlets.
    %
    %   Lspinerv : struct :
    %       Descriptor of the left spinal nerve. Fields are GC, idxs_CS, CS
    %       and Gen.
    %
    %   Ltip : 3*1 double :
    %       The coordinates of the tip of the left root.
    
    properties
        name        % string
        sc          % SCModel object
        LV          % struct
        LD          % struct
        Lspinerv    % struct
        Ltip        % 3*1 double
    end
    
    methods
        
        function this = Root(name, sc)
            % Class constructor method.
            %
            % Usage:
            %   rt = Root()
            %   rt = Root(name, sc)
            %
            % Parameters:
            %   name: str: name of the Root.
            %   sc: SCModel object: spinal cord model.
            %
            % Description:
            %   The Root class attemps to represent the geometrical
            %   features of the quartet of spinal roots innervating a
            %   spinal segment (2 dorsal roots -- 1 left, 1 right -- and 2
            %   ventral roots -- 1 left, 1 right).
            %
            %   In the current implementation, only the left dorsal and
            %   ventral spinal roots are represented by a Root object.
            %   It is assumed that an application requiring a
            %   representation of the right dorsal and ventral roots would 
            %   simply mirror their left counterparts. However, independent
            %   or identical implementation of attributes of the Root class
            %   can easily be achieved to represent them in their own
            %   right.
            %
            %   If provided, the name of the root and the SCModel to which
            %   it is attached suffice to completely set the Root.
            
            if nargin == 0
                return
                
            else
                this.name = name;
                this.sc = sc;
                this.build();
                
            end
            
        end
        
    end

    methods
        build(this)
        outbool = eq(obj1, obj2)
        from_struct(this, struc)
        CS = get_CS(this, sub_root_label, idx_branch, varargin)
        GC = get_GC(this, sub_root_label, idx_branch, varargin)
        Gen = get_Gen(this, sub_root_label, idx_branch, varargin)
        plot(this, varargin)
        set_LD_CS(this)
        set_LD_full_branches(this)
        set_LD_GC(this)
        set_LD_GC_generic(this)
        set_LD_GC_spline(this)
        set_LD_Gen(this)
        set_LD_idxs_CS(this)
        set_LD_p_entry(this)
        set_LD_p_exit_CSF(this)
        set_LD_p_exit_IF(this)
        set_LD_rlets_CS(this)
        set_LD_rlets_GC(this)
        set_LD_rlets_Gen(this)
        set_Lspinerv_CS(this)
        set_Lspinerv_Gen(this)
        set_Lspinerv_idxs_CS(this)
        set_LV_full_branches(this)
        set_LV_CS(this)
        set_LV_GC(this)
        set_LV_GC_generic(this)
        set_LV_GC_spline(this)
        set_LV_Gen(this)
        set_LV_idxs_CS(this)
        set_LV_p_entry(this)
        set_LV_p_exit_CSF(this)
        set_LV_p_exit_IF(this)
        set_LV_rlets_CS(this)
        set_LV_rlets_GC(this)
        set_LV_rlets_Gen(this)
        set_Ltip(this)
        struc = to_struct(this)
        bool = with_rlets(this)
        bool = with_spinerv(this)
    end
    
end
