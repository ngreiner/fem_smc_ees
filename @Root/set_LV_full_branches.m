function set_LV_full_branches(this)
    % Set the left ventral full branches.
    %
    % Usage:
    %   rt.set_LV_full_branches()
    %
    % Description:
    %   There are as many left ventral full branches as there are left
    %   ventral rootlets: they encompass the spinal nerve, main branch
    %   and one rootlet each.
    %   If the spinal nerve or rootlets are not present in the model, they
    %   are not accounted for in the full branches. Their presence might
    %   affect the number of cross-sections of the full branches.
    %   The full branches are formed by concatenating the cross-sections of
    %   the rootlets, main branch, and spinal nerve.
    
    % Unpack attributes in convenient variables.
    nCS = this.LV.nCS;
    nrlets = this.sc.roots_params.nrlets;
    rlets_nCS = this.sc.roots_params.LV_rlets_nCS;
    spinerv_nCS = this.Lspinerv.nCS;
    
    % Derive number of left dorsal full branches. It is equal to the
    % number of rootlets or to 1.
    if this.with_rlets()
        nFB = nrlets;
    else
        nFB = 1;
    end
    
    % Derive number of cross-sections of left dorsal full branches.
    nFBCS = nCS;
    if this.with_rlets()
        nFBCS = nFBCS + rlets_nCS;
    end
    if this.with_spinerv()
        nFBCS = nFBCS + spinerv_nCS;
    end
    
    % Initialize full branches array.
    this.LV.full_branches = cell(nFB, 1);
    
    %Form full-branches' cross-section arrays.
    for i = 1 : nFB
        
        % Initialize cell-array of CS of full-branch #i.
        this.LV.full_branches{i}.CS = cell(nFBCS, 1);
        
        % If neither rootlets nor the spinal nerve are included, the CS are
        % readily given by LV.CS.
        if ~ this.with_rlets() && ~ this.with_spinerv()
            this.LV.full_branches{i}.CS = this.LV.CS;
            
        % If rootlets are included but not the spinal nerve:
        elseif this.with_rlets() && ~ this.with_spinerv()
            this.LV.full_branches{i}.CS = [this.LV.rlets{i}.CS; this.LV.CS];
            
        % If spinerv is included but not the rootlets:
        elseif ~ this.with_rlets() && this.with_spinerv()
            this.LV.full_branches{i}.CS = [this.LV.CS; this.Lspinerv.CS];
            
        % If rootlets and spinal nerve are both included:
        else
            this.LV.full_branches{i}.CS = [this.LV.rlets{i}.CS; this.LV.CS; this.Lspinerv.CS];
            
        end
        
    end

    % Form full-branches generatrices.
    for i = 1 : nFB
        
        % Initialize cell-array of generatrices of full-branch #i.
        this.LV.full_branches{i}.Gen = cell(4, 1);
        
        % Retrieve CS of full-branch #i.
        tmp = this.LV.full_branches{i}.CS;
        
        % Form generatrices.
        for j = 1 : 4
            this.LV.full_branches{i}.Gen{j} = zeros(nFBCS, 3);
            for k = 1 : nFBCS
                this.LV.full_branches{i}.Gen{j}(k, :) = tmp{k}(1 + floor((j - 1) * size(tmp{k}, 1) / 4), :);
            end
        end
        
    end
    
    % Form full branches guide curves.
    for i = 1 : nFB
        
        % If neither rootlets nor the spinal nerve are included, the GC is
        % readily given by LV.GC.
        if ~ this.with_rlets() && ~ this.with_spinerv()
            this.LV.full_branches{i}.GC = this.LV.GC;
            
        % If rootlets are included but not the spinal nerve:
        elseif this.with_rlets() && ~ this.with_spinerv()
            this.LV.full_branches{i}.GC = [this.LV.rlets{i}.GC; this.LV.GC(2 : end, :)];
            
        % If spinerv is included but not the rootlets:
        elseif ~ this.with_rlets() && this.with_spinerv()
            this.LV.full_branches{i}.GC = [this.LV.GC(1 : end - 1, :); this.Lspinerv.GC];
            
        % If rootlets and spinal nerve are both included:
        else
            this.LV.full_branches{i}.GC = [this.LV.rlets{i}.GC; this.LV.GC(2 : end - 1, :); this.Lspinerv.GC];
            
        end
        
    end
    
    % Assign attributes.
    this.LV.n_full_branches = nFB;
    this.LV.full_branches_nCS = nFBCS;
    
end
