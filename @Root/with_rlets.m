function bool = with_rlets(this)
    % Return the attribute sc.roots_params.with_rlets.
    %
    % Usage:
    %   bool = rt.with_rlets()
    %
    % Return:
    %   bool : logical.
    bool = this.sc.roots_params.with_rlets;
end
