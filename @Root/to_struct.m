function struc = to_struct(this)
	% Return a matlab struct equivalent to the Root object.
    %
    % Usage:
    %   struc = root.to_struct()
    %
    % Return: 
    %   struc : struct : with same fields as the Root object.
    struc.name = this.name;
    struc.LD = this.LD;
    struc.LV = this.LV;
    struc.Lspinerv = this.Lspinerv;
    struc.Ltip = this.Ltip;
end
