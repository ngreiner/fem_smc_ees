function set_LD_rlets_Gen(this)
    % Set the attribute LD.rlets{:}.Gen
    %
    % Usage:
    %   rt.set_LD_rlets_Gen()
    %
    % Description:
    %   LD.rlets{k}.Gen is the cell-array of generatrices of the left
    %   dorsal rootlet #k.
    %   See method 'set_LD_Gen'.
    
    % Unpack attributes in convenient variables.
    nrlets = this.sc.roots_params.nrlets;
    nCS = this.sc.roots_params.LD_rlets_nCS;
    
    % Build generatrices.
    for i = 1 : nrlets
    
        % Initialize array of generatrices.
        this.LD.rlets{i}.Gen = cell(4, 1);
        
        % Unpack rlet CS.
        CS = this.LD.rlets{i}.CS;

        % Form generatrices.
        for j = 1 : 4
            this.LD.rlets{i}.Gen{j} = zeros(nCS, 3);
            for k = 1 : nCS
                this.LD.rlets{i}.Gen{j}(k, :) = CS{k}(1 + floor((j - 1) * size(CS{k}, 1) / 4), :);
            end
        end
        
    end
    
end
