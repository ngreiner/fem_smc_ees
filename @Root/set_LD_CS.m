function set_LD_CS(this)
    % Set the attribute LD.CS.
    %
    % Usage:
    %   rt.set_LD_CS()
    %
    % Description:
    %   LD.CS gives the cross-sections of the left dorsal root.
    %   It is a cell-array of double-arrays of coordinates. Each CS is an
    %   ellipse centered around a point of the LD.GC, lying in a plane
    %   somewhat orthogonal to the direction of the guide curve at that
    %   point.
    
    % Unpack attributes in convenient variables.
    nCS = this.LD.nCS;
    x = this.LD.GC(:, 1);
    y = this.LD.GC(:, 2);
    z = this.LD.GC(:, 3);
    idxs_CS = this.LD.idxs_CS;
    params = this.sc.roots_params;
    
    % Initialize cross-sections array.
    CS = cell(nCS, 1);    
    
    % Define cross-sections.
    for i = 1 : nCS
        
        % The center of the CS.
        center = [x(idxs_CS(i)), y(idxs_CS(i)), z(idxs_CS(i))];

        % The 1st CS should be orthogonal to the vector joining the 1st two
        % points of the LDGC. In fact, we want it to be vertical, so the
        % 3rd coordinate of this vector should be imposed =0.
        if i == 1
            u1 = x(2) - x(1);
            u2 = y(2) - y(1);
            u3 = 0;
        % The last CS should be orthogonal to the vector joining the last
        % two points of the GC.
        elseif i == nCS
            u1 = x(end) - x(end - 1);
            u2 = y(end) - y(end - 1);
            u3 = z(end) - z(end - 1);
        % Any other CS should be orthogonal to the vector joining the
        % two points surrounding the CS center on the GC.
        else
            u1 = x(idxs_CS(i) + 1) - x(idxs_CS(i) - 1);
            u2 = y(idxs_CS(i) + 1) - y(idxs_CS(i) - 1);
            u3 = z(idxs_CS(i) + 1) - z(idxs_CS(i) - 1);
        end
        
        % Set u and beta.
        u = [u1, u2, u3];
        beta = acos(u1 / norm([u1, u2]));
        if u2 < 0;
            beta = -beta;
        end
        
        % Build CS.
        % It is an ellipse lying in a plane orthogonal to vector u, with
        % excentricity exc, centered around the point center.
        diam = params.diam;
        
        % In fact, we want to smooth the way the ellipses get bent along
        % the path of the guide curve, that is, deviate from strict
        % orthoganility of the ellipses with respect to vector u.
        % This is achieved using the parameter w in the function
        % bent_ellipse. The strategy is as follows: since the 1st CS
        % is vertical, it is orthogonal to a vector whose 3rd coordinate is
        % =0. For the subsequent CS, instead of using the 3rd coordinate of
        % the computed vector u, u(3), we would use the value w*u(3). The
        % parameter w should be close to 0 for the first cross-sections,
        % and progressively reach 1 for the last cross-sections.
        w = (i - 1) / (nCS - 1);
                
        % At the root entry zone, the excentricity is higher to represent
        % the flat aspect of the rootlets bundle. This increased
        % excentricity decreases linearly until reaching the standard
        % root excentricity at the last cross-section.
        exc = params.excREZ + (i - 1) / (nCS - 1) * (params.exc - params.excREZ);
        
        % Build ellipse.
        E = Geom.build_ellipse(diam, exc);
        E = Geom.rotate_curve(E, beta);
        E = Geom.bend_ellipse(E, u, w);
        E = Geom.move_curve(E, center);
        CS{i} = E;
        
    end
    
    % Assign attribute.
    this.LD.CS = CS;

end
