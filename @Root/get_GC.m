function GC = get_GC(this, sub_root_label, idx_branch, varargin)
    % Return the GC of branch #idx_branch of sub_root (LV or LD).
    %
    % Usage:
    %   GC = rt.get_GC(sub_root_label)
    %   GC = rt.get_GC(sub_root_label, idx_branch)
    %   GC = rt.get_GC(sub_root_label, idx_branch, 'without_spinerv')
    %
    % Parameters:
    %   sub_root_label : string : 'LD' or 'LV'.
    %   idx_branch : int : index of full branch of sub_root.
    %
    % Description:
    %   sub_root_label should be either 'LV' or 'LD'. The returned
    %   guide curve is that of the full branch #idx_branch of the
    %   accordingly LV or LD.
    %   If idx_branch is not specified, it defaults to 1.
    
    sub_root = this.(sub_root_label);
    
    if nargin == 2
        idx_branch = 1;
    end
    
    if any(strcmp(varargin, 'without_spinerv'))
        if this.with_rlets()
            GC = [sub_root.rlets{idx_branch}.GC; sub_root.GC(2 : end, :)];
        else
            GC = sub_root.GC;
        end
        
    else
        GC = sub_root.full_branches{idx_branch}.GC;
        
    end
    
end
