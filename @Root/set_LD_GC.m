function set_LD_GC(this)
    % Set the guide curve of the left dorsal root (attribute LD.GC).
    %
    % Usage:
    %   rt.set_LD_GC()
    %
    % Description:
    %   The construction of the guide curve depends upon the type of
    %   guide curve, which is controlled by the attribute
    %   sc.roots_params.GC_type.
    
    if strcmp(this.sc.roots_params.GC_type, 'generic')
        this.set_LD_GC_generic();
        
    elseif strcmp(this.sc.roots_params.GC_type, 'spline')
        this.set_LD_GC_spline();
        
    else
        warning('Wrong value for attribute sc.roots_params.GC_type. Using ''spline'' option.')
        this.set_LD_GC_spline();
        
    end

end
