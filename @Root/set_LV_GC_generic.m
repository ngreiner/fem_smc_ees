function set_LV_GC_generic(this)
    % Build the left ventral guide curve.
    %
    % Usage:
    %   rt.set_LV_GC_generic()
    %
    % Description:  
    %   This method is called to construct the LV.GC when the attribute
    %   sc.roots_params.GC_type is 'generic'.
    %
    %   This method builds the LV.GC starting from a generic 2D-curve.
    %   The strategy is to dilate this curve, rotate it, bend it in 3D and
    %   translate it such that the first point of the resulting path
    %   coincides with the entry point of the GC in the WM, and its last
    %   point with the exit point of the GC at the IF.
    %
    %   This generic 2D-curve is to be found in dirs.resources at
    %   'geom/curve_ventral_generic.txt'.
    %   It is a curve lying in the upper left quadrant with initial point
    %   (0, 0) and last point (-1, 0). Its shape is such that, after
    %   dilatation, planar rotation, and bending in 3D, the obtained
    %   path should remain within the CSF and outside the WM.

    % Load generic 2D-coordinates of ventral curve.
    fileID = fopen(fullfile(this.dirs.resources, 'geom', 'curve_ventral_generic.txt'));
    CV0 = textscan(fileID, '%f %f');
    CV0 = [CV0{1}, CV0{2}];
    fclose(fileID);
    
    % First transformation to apply: anisotropic dilatation.
    % Vector joining entry point to exit point.
    u = this.LV.p_exit_IF - this.LV.p_entry;
    
    % Point of CSF lying in direction this.alphaV at z-level of this.VPi.
    CSF_up = this.sc.get_contour('CSF', this.LV.p_entry(3), 'above');
    CSF_down = this.sc.get_contour('CSF', this.LV.p_entry(3), 'below');
    [w_up,w_down] = this.getWeights(this.LV.p_entry(3));
    p_up = Geom.get_point_from_angle(CSF_up, this.sc.roots_params.alphaV);
    p_down = Geom.get_point_from_angle(CSF_down, this.sc.roots_params.alphaV);
    p_CSF = p_up * w_up + p_down * w_down;
    
    % Vector joining entry point to previous point.
    v = p_CSF - this.LV.p_entry;
    
    % Dilatation coefficients.
    lx = norm(u);
    ly = norm(v) / max(CV0(:, 2)) / 2.0;
    
    % Dilatation.
    CV = Geom.dilate_curve(CV0, lx, ly);
    
    % Second transformation: xy-planar rotation.
    % Direction of u in the xy-plane.
    beta = acos(u(1) / norm([u(1), u(2)]));
    if u(2) < 0
        beta = -beta;
    end
    
    % Rotation.
    CV = Geom.rotate_curve(CV, pi + beta);
    
    % Third transformation: 3D-bending.
    % Initiliaze third component of curve.     
    CV(:, 3) = zeros(size(CV(:, 1)));
    CV = Geom.bend_curve(CV, u);
    
    % Final transformation: translation.
    this.LV.GC = Geom.move_curve(CV, this.LV.Pi);
    
end
