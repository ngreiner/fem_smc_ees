function set_LD_Gen(this)
    % Set the 4 generatrices of the left dorsal root.
    %
    % Usage:
    %   rt.set_LD_gen()
    %
    % Description:
    %   The generatrices are a set of 4 curves following somewhat a course
    %   parallel to that of the LD.GC. Each of them connects topologically
    %   coincident points of the root cross-sections.
    %   The first generatrix connects all the first points of each
    %   cross-section. 
    %   The second generatrix connects all the n-th/4 points of each
    %   cross-section, where the n-th/4 point is the point whose index
    %   is 1/4 of the total number of points in the cross-section.
    %   The third generatrix connects all the 2*n-th/4 points of each
    %   cross-section.
    %   The fourth generatrix connects all the 3*n-th/4 points of each
    %   cross-section.
    
    % Recover cross-sections.
    CS = this.LD.CS;
    
    % Initialize array of generatrices.
    this.LD.Gen = cell(4, 1);
    
    % Build generatrices.
    for i = 1 : 4
        this.LD.Gen{i} = zeros(size(CS, 1), 3);
        for k = 1 : size(CS, 1)
            this.LD.Gen{i}(k, :) = CS{k}(1 + floor((i - 1) * size(CS{k}, 1) / 4), :);
        end
    end
    
end
