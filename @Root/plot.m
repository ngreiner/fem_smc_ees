function plot(this, varargin)
    % Produce a plot displaying all the cross-sections, generatrices and
    % guide curves, entry and exit points of the root.
    
    % Parse inputs.
    dors = false;
    vent = false;
    mb = false;
    rlet = false;
    spi = false;
    fb = false;
    while ~ isempty(varargin)
        switch varargin{1}
            case 'dorsal'
                dors = true;
            case 'vent'
                vent = true;
            case {'mb', 'MB', 'main_branch'}
                mb = true;
            case 'rlet'
                rlet = true;
            case {'spi', 'spinerv'}
                spi = true;
            case {'fb', 'FB', 'full_branches'}
                fb = true;
            case 'all'
                dors = true;
                vent = true;
                mb = true;
                rlet = true;
                spi = true;
                fb = true;
        end
        varargin(1) = [];
    end
                

    % Open figure. Define axes.
    figure
    hold on
    
    
    % Plot entry and exit points of root.
    scatter3(this.LV.p_entry(1), this.LV.p_entry(2), this.LV.p_entry(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red')
    scatter3(this.LD.p_entry(1), this.LD.p_entry(2), this.LD.p_entry(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue')
    scatter3(this.LV.p_exit_IF(1), this.LV.p_exit_IF(2), this.LV.p_exit_IF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red')
    scatter3(this.LD.p_exit_IF(1), this.LD.p_exit_IF(2), this.LD.p_exit_IF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue')
    scatter3(this.LV.p_exit_CSF(1), this.LV.p_exit_CSF(2), this.LV.p_exit_CSF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red')
    scatter3(this.LD.p_exit_CSF(1), this.LD.p_exit_CSF(2), this.LD.p_exit_CSF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue')

    
    % Plot main branches.
    if mb

        % Guide curves.
        plot3(this.LV.GC(:, 1), this.LV.GC(:, 2), this.LV.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red')
        plot3(this.LD.GC(:, 1), this.LD.GC(:, 2), this.LD.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'blue')

        % Cross-sections.
        for k = 1 : this.LV.nCS
            plot3(this.LV.CS{k}(:, 1), this.LV.CS{k}(:, 2), this.LV.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'red')
        end
        for k = 1 : this.LD.nCS
            plot3(this.LD.CS{k}(:, 1), this.LD.CS{k}(:, 2), this.LD.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'blue')
        end

        % Generatrices.
        for k = 1 : 4
            plot3(this.LV.Gen{k}(:, 1), this.LV.Gen{k}(:, 2), this.LV.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'red')
            plot3(this.LD.Gen{k}(:, 1), this.LD.Gen{k}(:, 2), this.LD.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'blue')
        end
        
    end
    

    % Plot rootlets.
    if rlet && this.with_rlets()

        % Loop over rootlets.
        for k = 1 : this.sc.roots_params.nrlets

            % Guide curves.
            plot3(this.LV.rlets{k}.GC(:, 1), this.LV.rlets{k}.GC(:, 2), this.LV.rlets{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red')
            plot3(this.LD.rlets{k}.GC(:, 1), this.LD.rlets{k}.GC(:, 2), this.LD.rlets{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'blue')

            % Cross-sections.
            for j = 1 : this.sc.roots_params.LV_rlets_nCS
                plot3(this.LV.rlets{k}.CS{j}(:, 1), this.LV.rlets{k}.CS{j}(:, 2), this.LV.rlets{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'red')
            end
            for j = 1 : this.sc.roots_params.LD_rlets_nCS
                plot3(this.LD.rlets{k}.CS{j}(:, 1), this.LD.rlets{k}.CS{j}(:, 2), this.LD.rlets{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'blue')
            end

            % Generatrices.
            for j = 1 : 4
                plot3(this.LV.rlets{k}.Gen{j}(:, 1), this.LV.rlets{k}.Gen{j}(:, 2), this.LV.rlets{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'red')
                plot3(this.LD.rlets{k}.Gen{j}(:, 1), this.LD.rlets{k}.Gen{j}(:, 2), this.LD.rlets{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'blue')
            end

        end

    end
    

    % Plot spinal nerve.
    if spi && this.with_spinerv()

        % Guide curve.
        plot3(this.Lspinerv.GC(:, 1), this.Lspinerv.GC(:, 2), this.Lspinerv.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'magenta')

        % Cross-sections.
        for k = 1 : this.Lspinerv.nCS
            plot3(this.Lspinerv.CS{k}(:, 1), this.Lspinerv.CS{k}(:, 2), this.Lspinerv.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'magenta')
        end

        % Generatrices.
        for k = 1 : 4
            plot3(this.Lspinerv.Gen{k}(:, 1), this.Lspinerv.Gen{k}(:, 2), this.Lspinerv.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'magenta')
        end

    end
    

    % Plot full branches.
    if fb

        if dors
            
            % Loop over dorsal full branches.
            for k = 1 : this.LD.n_full_branches

                % Guide curves.
                plot3(this.LD.full_branches{k}.GC(:, 1), this.LD.full_branches{k}.GC(:, 2), this.LD.full_branches{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'blue')

                % Cross-sections.
                for j = 1 : this.LD.full_branches_nCS
                    plot3(this.LD.full_branches{k}.CS{j}(:, 1), this.LD.full_branches{k}.CS{j}(:, 2), this.LD.full_branches{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'blue')
                end

                % Generatrices.
                for j = 1 : 4
                    plot3(this.LD.full_branches{k}.Gen{j}(:, 1), this.LD.full_branches{k}.Gen{j}(:, 2), this.LD.full_branches{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'blue')
                end

            end
            
        end
        
        if vent
        
            % Loop over ventral full branches.
            for k = 1 : this.LV.n_full_branches

                % Guide curves.
                plot3(this.LV.full_branches{k}.GC(:, 1), this.LV.full_branches{k}.GC(:, 2), this.LV.full_branches{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red')

                % Cross-sections.
                for j = 1 : this.LV.full_branches_nCS
                    plot3(this.LV.full_branches{k}.CS{j}(:, 1), this.LV.full_branches{k}.CS{j}(:, 2), this.LV.full_branches{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'red')
                end

                % Generatrices.
                for j = 1 : 4
                    plot3(this.LV.full_branches{k}.Gen{j}(:, 1), this.LV.full_branches{k}.Gen{j}(:, 2), this.LV.full_branches{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'red')
                end

            end
            
        end

    end
    

end
