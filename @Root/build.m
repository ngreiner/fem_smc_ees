function build(this)
    % Build the Root. 
    %
    % Usage:
    %   rt.build()
    %
    % Description:
    %   1. The entry points in the WM and the exit points through the CSF
    %   and the intervertebral foramen of the left ventral and dorsal roots
    %   are determined.
    %   2. The guide curves of the left ventral and dorsal roots are then
    %   computed.
    %   3. The indexes of points of the guide curves where cross-sections
    %   should be centered are next identified and the cross-sections
    %   effectively built.
    %   4. Then, if the spinerv should be included, its cross-sections are
    %   built similarly.
    %   5. If the roolets of the left ventral and dorsal roots should be
    %   represented, they are built similarly.
    %   6. The full branches of the left dorsal and ventral are finally
    %   formed, encompassing the rootlets, the main branches and the spinal
    %   nerve.
    this.set_LD_p_exit_IF();
    this.set_LV_p_exit_IF();
    this.set_LD_p_entry();
    this.set_LV_p_entry();
    this.set_LD_p_exit_CSF();
    this.set_LV_p_exit_CSF();
    this.set_LD_GC();
    this.set_LV_GC();
    this.set_LD_idxs_CS();
    this.set_LV_idxs_CS();
    this.set_LD_CS();
    this.set_LV_CS();
    this.set_LD_Gen();
    this.set_LV_Gen();
    if this.with_spinerv()
        this.set_Lspinerv_idxs_CS();
        this.set_Lspinerv_CS();
        this.set_Lspinerv_Gen();
    end
    if this.with_rlets()
        this.set_LD_rlets_GC();
        this.set_LD_rlets_CS();
        this.set_LD_rlets_Gen();
        this.set_LV_rlets_GC();
        this.set_LV_rlets_CS();
        this.set_LV_rlets_Gen();
    end
    this.set_LD_full_branches();
    this.set_LV_full_branches();
    this.set_Ltip();
end
