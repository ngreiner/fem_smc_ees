function set_LD_p_exit_CSF(this)
    % Set the attribute LD.p_exit_CSF
    %
    % Usage:
    %   rt.set_LD_p_exit_CSF()
    %
    % Description: 
    %   LD.p_exit_CSF stores the position of the exit point of the LD
    %   through the CSF.
    %   LD.p_exit_CSF is obtained from the LD.p_exit_IF as follows:
    %       1. It lies in the same direction as the latter.
    %       2. It lies on the border of the CSF.
    %       3. Its elevation z_p_exit_CSF is such that:
    %           z_p_exit_CSF - z_p_entry = ...
    %                   eps_p_exit_CSF * (z_p_exit_IF - z_p_entry)

    % Convenient variables.
    x = this.LD.p_exit_IF(1);
    y = this.LD.p_exit_IF(2);
    z = this.LD.p_exit_IF(3);
    z_entry = this.LD.p_entry(3);
    eps_exit = this.sc.roots_params.eps_p_exit_CSF;

    % Compute virtual point of CSF lying in the direction of p_exit_IF with
    % a rectified z-coordinate.
    z = z_entry + eps_exit * (z - z_entry);
    z_up = this.sc.get_z_above(z);
    z_down = this.sc.get_z_below(z);    
    CSF_up = this.sc.get_contour('CSF', z_up);
    CSF_down = this.sc.get_contour('CSF', z_down);
    [w_up, w_down] = this.sc.get_weights(z);
    eta = atan2(y, x);
    p_CSF_up = Geom.get_point_from_angle(CSF_up, eta);
    p_CSF_down = Geom.get_point_from_angle(CSF_down, eta);
    d = w_up * norm(p_CSF_up(1 : 2)) + w_down * norm(p_CSF_down(1 : 2));

    % The CSF exit point.
    this.LD.p_exit_CSF = [d * cos(eta), d * sin(eta), z];
    
end
