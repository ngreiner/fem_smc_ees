function set_Lspinerv_CS(this)
    % Set the cross-sections of the left spinal nerve (Lspinerv.CS).
    %
    % Usage:
    %   rt.set_Lspinerv_CS()
    %
    % Description:
    %   See for instance 'set_LD_CS' method.
    
    % Unpack attributes in convenient variables.
    nCS = this.Lspinerv.nCS;
    x = this.Lspinerv.GC(:, 1);
    y = this.Lspinerv.GC(:, 2);
    z = this.Lspinerv.GC(:, 3);
    idxs_CS = this.Lspinerv.idxs_CS;
    params = this.sc.roots_params;
    
    % Initialize cross-sections array.
    CS = cell(nCS, 1);    
    
    % Define cross-sections.
    for i = 1 : nCS
        
        % The center of the CS.
        center = [x(idxs_CS(i)), y(idxs_CS(i)), z(idxs_CS(i))];

        % Each CS should be orthogonal to the vector joining the point
        % preceding its center on the GC and its center.
        u1 = x(idxs_CS(i)) - x(idxs_CS(i) - 1);
        u2 = y(idxs_CS(i)) - y(idxs_CS(i) - 1);
        u3 = z(idxs_CS(i)) - z(idxs_CS(i) - 1);
        u = [u1, u2, u3];
        beta = acos(u1 / norm([u1, u2]));
        if u2 < 0;
            beta = -beta;
        end
        
        % Build CS.
        % It is an ellipse lying in a plane orthogonal to vector u, with
        % excentricity spinerv_exc.
        
        % Build ellipse.
        E = Geom.build_ellipse(params.spinerv_diam, params.spinerv_exc);
        E = Geom.rotate_curve(E, beta);
        E = Geom.bend_ellipse(E, u);
        E = Geom.move_curve(E, center);
        CS{i} = E;
        
    end
    
    % Assign attribute.
    this.Lspinerv.CS = CS;

end
