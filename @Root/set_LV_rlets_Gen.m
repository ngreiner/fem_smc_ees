function set_LV_rlets_Gen(this)
    % Set the attribute LV.rlets{:}.Gen
    %
    % Usage:
    %   rt.set_LV_rlets_Gen()
    %
    % Description:
    %   LV.rlets{k}.Gen is the cell-array of generatrices of the left
    %   ventral rootlet #k.
    %   See method 'set_LD_Gen'.
    
    % Unpack attributes in convenient variables.
    nrlets = this.sc.roots_params.nrlets;
    nCS = this.sc.roots_params.LV_rlets_nCS;
    
    % Build generatrices.
    for i = 1 : nrlets
    
        % Initialize array of generatrices.
        this.LV.rlets{i}.Gen = cell(4, 1);
        
        % Unpack rlet CS.
        CS = this.LV.rlets{i}.CS;

        % Form generatrices.
        for j = 1 : 4
            this.LV.rlets{i}.Gen{j} = zeros(nCS, 3);
            for k = 1 : nCS
                this.LV.rlets{i}.Gen{j}(k, :) = CS{k}(1 + floor((j - 1) * size(CS{k}, 1) / 4), :);
            end
        end
        
    end
    
end
