function set_LV_rlets_CS(this)
    % Set the attribute LV.rlets{:}CS.
    %
    % Usage:
    %   rt.set_LV_rlets_CS()
    %
    % Description:
    %   LV.rlets{k}.CS is the cell-array of cross-sections of the left
    %   ventral rootlet #k.
    %   These CS are formed using a similar algorithm as that used for
    %   LD.CS.
    
    % Unpack some attributes in convenient variables.
    nrlets = this.sc.roots_params.nrlets;
    nCS = this.sc.roots_params.LV_rlets_nCS;
    idxs_CS = this.LV.idxs_CS;
    params = this.sc.roots_params;
    
    % Loop over rootlets.
    for i = 1 : nrlets
    
        % Initialize array of cross-sections arrays.
        this.LV.rlets{i}.CS = cell(nCS, 1);
        
        % Unpack LD.GC coordinates in convenient variables.
        x = this.LV.rlets{i}.GC(:, 1);
        y = this.LV.rlets{i}.GC(:, 2);
        z = this.LV.rlets{i}.GC(:, 3);

        % Define cross-sections. They should have the diameter and
        % excentricity prescribed for the rootlets.
        for k = 1 : nCS
            
            % The center of the CS.
            center = [x(idxs_CS(k)), y(idxs_CS(k)), z(idxs_CS(k))];

            % The 1st CS should be orthogonal to the vector joining the 1st
            % two points of the GC. We also want it vertical, so the
            % 3rd coordinate of this vector should be imposed =0.
            if k == 1
                u1 = x(2) - x(1);
                u2 = y(2) - y(1);
                u3 = 0;

            % The last cross-section should be orthogonal to the vector
            % joining the penultimate to the last points of the GC.
            elseif k == nCS
                u1 = x(end) - x(end - 1);
                u2 = y(end) - y(end - 1);
                u3 = z(end) - z(end - 1);

            % The intermediary ones should be orthogonal to the vector
            % joining the two points surrounding the center of the CS on
            % the GC.
            else
                u1 = x(idxs_CS(k) + 1) - x(idxs_CS(k) - 1);
                u2 = y(idxs_CS(k) + 1) - y(idxs_CS(k) - 1);
                u3 = z(idxs_CS(k) + 1) - z(idxs_CS(k) - 1);
                
            end
            
            % Set u and beta.
            u = [u1, u2, u3];
            beta = acos(u1 / norm([u1, u2]));
            if u2 < 0;
                beta = -beta;
            end
            
            % Build CS.
            CS = this.build_ellipse(params.rlet_diam, params.rlet_exc);
            CS = this.rotate_curve(CS, beta);
            CS = this.bend_ellipse(CS, u);
            CS = this.move_curve(CS, center);
            
            % Assign to rlets attribute.
            this.LV.rlets{i}.CS{i} = CS;
            
        end
        
    end
    
end
