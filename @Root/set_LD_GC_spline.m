function set_LD_GC_spline(this)
    % Build the left dorsal guide curve.
    %
    % Usage:
    %   rt.set_LD_GC_spline()
    %
    % Description:
    %   This method is called to construct the LD.GC when the attribute
    %   sc.roots_params.GC_type is 'spline'.
    %
    %   The strategy is to distribute points along a spline running through
    %   a series of interpolation points.
    
    %   The first interpolation is readily given by LD.p_entry, while the
    %   last interpolation point is readily given by LD.p_exit_IF. The
    %   total number of interpolation points is given by the attribute
    %   sc.roots_params.LD_GC_ninter_points, and is a sensitive parameter
    %   of the algorithm.
    %
    %   The intermediary interpolation points are regularly spaced both
    %   angularly and along the z-axis between LD.p_entry and LD.p_exit_IF.
    %
    %   Concerning their distances to the z-axis, for the first half of
    %   these points, this distance  is obtained as the distance of the
    %   point of the WM lying in the same direction, to which we add a
    %   fixed amount given by the attribute sc.roots_params.GC_gap. This
    %   amount should be such that after building the cross-sections of
    %   the LD along its guide curve, these cross-sections will not overlap
    %   with the WM (they should be strictly outside of the WM convex
    %   hull).
    %   For the subsequent points, they are readily obtained as weighted
    %   sums between the exit point LD.p_exit_IF and the last interpolation
    %   point defined using the rule described above. The weights are
    %   adjusted so as for these subsequent points to progressively reach
    %   the exit point.
    %
    %   If the spinerv is included, the strategy is slightly modified.
    %   For the guide curve of the LD to be smoothly prolonged by that of
    %   the spinerv, we extend the collection of inter_points with an
    %   additional point representing the tip of the spinerv. By
    %   interpolating this extended collection of interpolation points with
    %   a spline and distributing points along this spline, we will build a
    %   guide curve that encompasses both the LD guide curve and the
    %   spinerv  guide curve. It will remain to split this global guide
    %   curve into two pieces, one corresponding to the LD guide curve and
    %   one to the spinerv.
    
    % Convenient variables.
    params = this.sc.roots_params;
    nip = params.LD_GC_ninter_points;

    % Recover entry and exit points.
    x1 = this.LD.p_entry;
    x2 = this.LD.p_exit_IF;

    % Angles of x1 and x2.
    alpha1 = atan2(x1(2), x1(1));
    if x2(1) < 0
        alpha2 = atan(x2(2) / x2(1)) + pi;
    elseif x2(1) > 0
        alpha2 = atan(x2(2) / x2(1));
    else
        if x2(2) > 0
            alpha2 = pi/2;
        else
            alpha2 = -pi/2;
        end
    end
    
    % Regress alpha2 - alpha1 in interval ]-pi, pi].
    theta = alpha2 - alpha1;
    while theta > pi
        theta = theta - 2*pi;
    end
    while theta <= -pi
        theta = theta + 2*pi;
    end
    
    % Define z-levels of interpolation points.
    dz = (x2(3) - x1(3)) / (nip - 1);
    zip = zeros(nip - 2, 1);
    for i = 1 : nip - 2
        zip(i) = x1(3) + dz * i;
    end

    % Define angles of interpolation points. 
    dalpha = theta / (nip - 1);
    angles = zeros(nip - 2, 1);
    for i = 1 : nip - 2
        angles(i) = alpha1 + dalpha * i;
        while angles(i) > pi
            angles(i) = angles(i) - 2 * pi;
        end
        while angles(i) <= -pi
            angles(i) = angles(i) + 2 * pi;
        end
    end
    
    % Index of point at which a switch in the way to compute the distance
    % of the interpolation points is necessary. It is arbitrarily defined
    % to be the index of the point where the y=0 plane is crossed in the 
    % positive direction. The y=0 plane corresponds to the frontal plane,
    % ie it divides the anterior half of the spinal cord from its posterior
    % half. 
    idx = find(angles > 0, 1);

    % Define interpolation points.
    inter_points = zeros(nip, 3);
    inter_points(1, :) = x1;
    inter_points(end, :) = x2;
    for i = 1 : nip - 2
        if i < idx - 1
            p_WM_above = Geom.get_point_from_angle(this.sc.get_contour('WM', zip(i), 'above'), angles(i));
            p_WM_below = Geom.get_point_from_angle(this.sc.get_contour('WM', zip(i), 'below'), angles(i));
            [w_above, w_below] = this.sc.get_weights(zip(i));
            d_WM_p = w_above * norm(p_WM_above(1 : 2)) + w_below * norm(p_WM_below(1 : 2));
            d = d_WM_p + params.GC_gap;
            inter_points(i + 1, :) = [d * cos(angles(i)), d * sin(angles(i)), zip(i)];
        else
            w = (i - idx + 2) / (nip - idx + 1);
            inter_points(i + 1, :) = (1 - w) * inter_points(idx - 1, :) + w * x2;
        end
    end
    
    % If spinerv is included, we need to extend the collection of
    % inter_points with an additional point, representing the tip of the
    % spinerv. This point is lying at a distance spinerv_length from the
    % last inter_point (which currently is LD.p_exit_IF), in the direction
    % that is the mean between the directions joining LD.p_entry and
    % LD.p_exit_IF on one hand, and LV.p_entry and LD.p_exit_IF on the
    % other hand (recall that LD.p_exit_IF and LV.p_exit_IF are identical
    % when spinerv is included).
    if this.with_spinerv()
        u1 = x2 - this.LV.p_entry;
        u2 = x2 - x1;
        u = 0.5 * u1 / norm(u1) + 0.5 * u2 / norm(u2);
        u = u / norm(u);
        lp = x2 + params.spinerv_length * u;
        inter_points(end + 1, :) = lp;
    end
    
    % Build spline interpolating the previous interpolation points and
    % distribute points along it. This is all achieved by the function
    % interparc.
    np = floor(params.GC_pdens * Geom.get_arc_length(inter_points));
    GC = Geom.interparc(np, inter_points(:, 1), inter_points(:, 2), inter_points(:, 3), 'spline');
    % If spinerv is included, we need to split the previous GC among
    % the LD guide curve and the spinerv guide curve. Up to p_exit_IF, the
    % points belong to the LD guide curve. After, they belong to the
    % spinerv guide curve.
    % However, the points composing the GC computed above actually do
    % not contain p_exit_IF, though the spline from which they were drawn
    % does interpolate p_exit_IF. We thus need to replace p_exit_IF by the
    % point of the GC that is closest to p_exit_IF. 
    if this.with_spinerv()
        [idx, x2] = Geom.find_closest(x2, GC);
        this.LD.p_exit_IF = x2;
        this.LV.p_exit_IF = x2;
        this.set_LD_p_exit_CSF();
        this.set_LV_p_exit_CSF();
        this.Lspinerv.GC = GC(idx : end, :);
        GC = GC(1 : idx, :);
    end
    
    % Assign attributes.
    this.LD.GC = GC;

end
