function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if ~ strcmp(obj1.name, obj2.name)
        fprintf('Attributes name are different.\n')
        return
        
    elseif ~ isequal(obj1.LV, obj2.LV)
        fprintf('Attributes LV are different.\n')
        return
        
    elseif ~ isequal(obj1.LD, obj2.LD)
        fprintf('Attributes LD are different.\n')
        return
        
    elseif ~ isequal(obj1.Lspinerv, obj2.Lspinerv)
        fprintf('Attributes Lspinerv are different.\n')
        return
        
    elseif ~ isequal(obj1.Ltip, obj2.Ltip)
        fprintf('Attributes Ltip are different.\n')
        return
    end
    
    outbool = true;
    
end
