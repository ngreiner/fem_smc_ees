function set_LD_p_exit_IF(this)
    % Set the attribute LD.p_exit_IF
    %
    % Usage:
    %   rt.set_LD_p_exit_IF()
    %
    % Description: 
    %   LD.p_exit_IF stores the position of the exit point of the LD
    %   through its associated intervertebral foramina.
    %   If spinerv is included, this point is the point where the LD.GC and
    %   the LV.GC meet and where the spinerv starts.
    %   Its coordinates are readily contained in the 1st, 2nd and 3rd
    %   columns of the attribute sc.geom.roots_map in the row corresponding
    %   to the present root.
    %   Those are previously obtained by manual identification of the
    %   intervertebral foramina in a 3D-model of the vertebral column
    %   corresponding to the spinal cord model in which the present root is
    %   embedded.
    %   If spinerv is not included, LD.p_exit_IF is deflected compared to
    %   the previous point, so as to manage a space between the LD and LV.

    % Recover intervertebral foramina position from roots_map.
    roots_map = this.sc.geom.roots_map;
    idx = find(strcmp(this.sc.morpho.segs_with_roots, this.name));
    x = roots_map(idx, 1);
    y = roots_map(idx, 2);
    z = roots_map(idx, 3);
    
    if this.with_spinerv()
        % LD.p_exit_IF is readily obtained.
        this.LD.p_exit_IF = [x, y, z];
        
    else
        % Deflect previous point to manage a space between the ventral
        % and dorsal exit points. This space is defined to be half the
        % diameter of the root.
        theta = atan2(y, x);
        d = norm([x, y]);
        beta = 0.5 * this.sc.roots_params.diam / d;
        this.LD.p_exit_IF = [d * cos(theta + beta), d * sin(theta + beta), z];
        
    end
    
end
