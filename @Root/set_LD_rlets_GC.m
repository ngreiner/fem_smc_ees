function set_LD_rlets_GC(this)
    % Set the attribute LD.rlets{:}.GC
    %
    % Usage:
    %   rt.set_LD_rlets_GC()
    %
    % Description:
    %   LD.rlets{k}.GC is the guide curve of the left dorsal roolet #k.
    %   It is derived from the main guide curve LD.GC as follows:
    %       1. The guide curve of the central rootlet coincides with the
    %       portion of the guide curve LD.GC between indexes 1 and
    %       LD.idxs_CS(sc.roots_params.LD_rlets_nCS) (which corresponds to
    %       the point of the LD.GC where the last CS of the central rootlet
    %       should be centered).
    %       2. The others are obtained by replicating the central rootlet
    %       guide curve with a deviation along the z-axis.
    
    % Convenient variables.
    z_entry = this.LD.p_entry(3);
    GC = this.LD.GC;
    idxs_CS = this.LD.idxs_CS;
    params = this.sc.roots_params;
    nCS = params.LD_rlets_nCS;
    nrlets = params.nrlets;

    % Initialize array of rootlets.
    this.LD.rlets = cell(nrlets, 1);
    
    % Derive elevations of rootlet entry points from the elevation of the
    % central rootlet entry point and from the rootlet diameter and
    % inter-rootlet gap.
    zs = zeros(nrlets, 1);
    for i = 1 : nrlets
        zs(i) = z_entry + (i - floor((nrlets + 1) / 2)) * (params.rlet_diam * params.rlet_exc + params.rlet_gap);
    end
    
    % Compute rootlet guide curves.
    idx = idxs_CS(nCS);
    z_common = GC(idx, 3);
    for i = 1 : nrlets
        curve = GC(1 : idx, :);
        curve(:, 3) = linspace(zs{i}, z_common, idx);
        this.LD.rlets{i}.GC = curve;
    end
    
    % Transfer idxs_CS of rootlets to new attribute to reflect the true
    % organisation of the root.
    this.LD.rlets_idxs_CS = idxs_CS(1 : nCS);
    
    % Remove initial portion of LD.GC and initial LD.CS, which are now
    % replaced by the rootlets.
    this.LD.GC = GC(idx : end, :);
    this.LD.CS(1 : nCS) = [];
    this.LD.idxs_CS(1 : nCS) = [];
    this.LD.nCS = this.LD.nCS - nCS;
    
end
