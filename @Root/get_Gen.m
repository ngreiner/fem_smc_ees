function Gen = get_Gen(this, sub_root_label, idx_branch, varargin)
    % Return the Gen of branch #idx_branch of sub_root (LV or LD).
    %
    % Usage:
    %   Gen = rt.get_Gen(sub_root_label)
    %   Gen = rt.get_Gen(sub_root_label, idx_branch)
    %   Gen = rt.get_Gen(sub_root_label, idx_branch, 'without_spinerv')
    %
    % Parameters:
    %   sub_root_label : string : 'LD' or 'LV'.
    %   idx_branch : int : index of full branch of sub_root.
    %
    % Description:
    %   sub_root_label should be either 'LV' or 'LD'. The returned
    %   generatrices are those of the full branch #idx_branch of the
    %   accordingly LV or LD.
    %   If idx_branch is not specified, it defaults to 1.
    
    sub_root = this.(sub_root_label);
    
    if nargin == 2
        idx_branch = 1;
    end
    
    if any(strcmp(varargin, 'without_spinerv'))
        if this.with_rlets()
            Gen = [sub_root.rlets{idx_branch}.Gen; sub_root.Gen];
        else
            Gen = sub_root.Gen;
        end
        
    else
        Gen = sub_root.full_branches{idx_branch}.Gen;
        
    end
    
end
