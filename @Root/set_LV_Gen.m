function set_LV_Gen(this)
    % Set the 4 generatrices of the left ventral root.
    %
    % Usage:
    %   rt.set_LV_gen()
    %
    % Description:
    %   See set_LV_Gen.
    
    % Recover cross-sections.
    CS = this.LV.CS;
    
    % Initialize array of generatrices.
    this.LV.Gen = cell(4, 1);
    
    % Build generatrices.
    for i = 1 : 4
        this.LV.Gen{i} = zeros(size(CS, 1), 3);
        for k = 1 : size(CS, 1)
            this.LV.Gen{i}(k, :) = CS{k}(1 + floor((i - 1) * size(CS{k}, 1) / 4), :);
        end
    end
    
end
