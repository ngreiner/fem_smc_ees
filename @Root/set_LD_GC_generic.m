function set_LD_GC_generic(this)
    % Build the left dorsal guide curve.
    %
    % Usage:
    %   rt.set_LD_GC_generic()
    %
    % Description:  
    %   This method is called to construct the LD.GC when the attribute
    %   sc.roots_params.GC_type is 'generic'.
    %
    %   This method builds the LD.GC starting from a generic 2D-curve.
    %   The strategy is to dilate this curve, rotate it, bend it in 3D and
    %   translate it such that the first point of the resulting path
    %   coincides with the entry point of the GC in the WM, and its last
    %   point with the exit point of the GC at the IF.
    %
    %   This generic 2D-curve is to be found in sc.dirs.resources at
    %   'geom/curve_dorsal_generic.txt'.
    %   It is a curve lying in the lower left quadrant with initial point
    %   (0, 0) and last point (-1, 0). Its shape is such that, after
    %   dilatation, planar rotation, and bending in 3D, the obtained
    %   path should remain within the CSF and outside the WM.

    % Load generic 2D-coordinates of dorsal curve.
    file_path = fullfile(this.dirs.resources, 'geom', 'curve_dorsal_generic.txt');
    fileID = fopen(file_path);
    CD0 = textscan(fileID, '%f %f');
    CD0 = [CD0{1}, CD0{2}];
    fclose(fileID);

    % First transformation to apply: anisotropic dilatation.
    
    % Vector joining entry point to exit point.   
    u = this.LD.p_exit_IF - this.LD.p_entry;
    
    % Point of CSF lying in direction this.alphaD at z-level of this.DPi.
    CSF_up = this.sc.get_contour('CSF', this.LD.p_entry(3), 'above');
    CSF_down = this.sc.get_contour('CSF', this.LD.p_entry(3), 'below');
    [w_up,w_down] = this.getWeights(this.LD.p_entry(3));
    p_up = Geom.get_point_from_angle(CSF_up, this.sc.roots_params.alphaD);
    p_down = Geom.get_point_from_angle(CSF_down, this.sc.roots_params.alphaD);
    p_CSF = p_up * w_up + p_down * w_down;
    
    % Vector joining entry point to previous point.
    v = p_CSF - this.LD.p_entry;
    
    % Dilatation coefficients.
    lx = norm(u);
    ly = norm(v) / abs(min(CD0(:, 2))) / 1.9;
    
    % Dilatation.
    CD = Geom.dilate_curve(CD0, lx, ly);
    
    % Second transformation to apply: xy-planar rotation.
    
    % Direction of u in the xy-plane.
    beta = acos(u(1) / norm([u(1), u(2)]));
    if u(2) < 0
        beta = -beta;
    end
    % Rotation.
    CD = Geom.rotate_curve(CD, pi + beta);
    
    % Third transformation to apply: 3D-bending.
    CD(:, 3) = zeros(size(CD(:, 1)));    
    CD = Geom.bend_curve(CD, u);
    
    % Final transformation: translation.
    this.LD.GC = Geom.move_curve(CD, this.LD.p_entry);
    
end
