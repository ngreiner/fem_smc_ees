function set_LV_idxs_CS(this)
    % Set the attribute LV.idxs_CS.
    %
    % Usage:
    %   rt.set_LV_idxs_CS()
    %
    % Description:
    %   LV.idxs_CS stores the indexes of the points of the LV.GC which
    %   should serve as centers for the LV cross-sections.
    %   First, the number of cross-sections need be defined: it is
    %   determined from the lineic density of cross-sections per unit arc
    %   length of guide curve and from the guide curve arc length.
    %   Second, the indexes of the guide curve points serving as centers
    %   for the cross-sections are selected based on the following
    %   criteria:
    %       1. the first and last points should be selected;
    %       2. the selected points should be as uniformly spaced as
    %       possible.
    
    % Unpack attributes in variables for convenience.
    GC = this.LV.GC;
    CS_dens = this.sc.roots_params.CS_dens;
        
    % Number of cross-sections.
    nCS = max(floor(CS_dens * Geom.get_arc_length(GC)), 2);

    % Select indexes of guide curve points.
    idxs_CS = zeros(nCS, 1);
    L = size(GC, 1);
    q = floor(L / (nCS - 1));
    r = floor(mod(L, nCS - 1) / 2);

    % The first index should be 1.
    idxs_CS(1) = 1;

    % The last one should be the last index of GC.
    idxs_CS(end) = L;

    % The other ones should be evenly spaced.
    for i = 2 : nCS - 1
        idxs_CS(i) = q * (i - 1) + 1 + r;
    end
    
    % Assign attributes.
    this.LV.nCS = nCS;
    this.LV.idxs_CS = idxs_CS;
    
end
