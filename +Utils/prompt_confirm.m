function answer = prompt_confirm(msg)
    % Prompt confirmation of user to perform action.
    %
    % Usage:
    %   answer = Utils.prompt_confirm(msg)
    %
    % Parameters:
    %   msg: string: confirmation question to prompt to the user.
    %
    % Return:
    %   answer: bool: 1 if answer if yes, 0 if the answer is no.
    %
    % Description: Void.
    prompt = sprintf('%s (y/n)\n', msg);
    answer = input(prompt, 's');
    while ~any(strcmp(answer, {'y'; 'Y'; 'yes'; 'Yes'; 'YES'; 'n'; 'N'; 'no'; 'No'; 'NO'}))
        answer = input(prompt);
    end
    if strcmp(answer, {'y'; 'Y'; 'yes'; 'Yes'; 'YES'})
        answer = 1;
    else
        answer = 0;
    end
end
