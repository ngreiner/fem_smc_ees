function file_names = scan_files(folder)
    % Return cell-array of names of files contained in the input folder.
    %
    % Usage:
    %   file_names = Utils.scan_files(folder)
    %
    % Parameters:
    %   folder : string : path to folder to be scanned.
    %
    % Return:
    %   file_names : N*1 cell : names of files in input folder.
    
    % Scan folder.
    file_names = dir(folder);
    
    % Extract subfolders.
    ifile = ~[file_names(:).isdir];
    file_names = {file_names(ifile).name}';
    
    % Remove '.DS_Store'.
    file_names(ismember(file_names, {'.DS_Store'})) = [];
    
end
