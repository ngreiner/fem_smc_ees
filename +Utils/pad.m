function newstr = pad(str, side, symbol, lgth)
    % This adds as many occurences of `symbol` on each side of the input
    % str as necessary so that the resulting string be `length`-long.
    %
    % Inputs: - str: char array: the string to pad.
    %         - side: char array: can be 'right', 'left' or 'both'.
    %         - symbol: char array: the symbol to add.
    %         - length: int: the target length.
    %
    % Outputs: - newstr: char array: the padded str.
    %
    % Actions: None.
    %
    % Description: Void.
    
    % Init newstr.
    newstr = str;
    
    dl = lgth-length(newstr);
    
    if strcmp(side, 'left')
        while dl > 0
            newstr = [symbol, newstr];
            dl = dl-1;
        end
    elseif strcmp(side, 'right')
        while dl > 0
            newstr = [newstr, symbol];
            dl = dl-1;
        end
    else
        lside = 1;
        while dl > 0
            if lside
                newstr = [symbol, newstr];
            else
                newstr = [newstr, symbol];
            end
            dl = dl-1;
            lside = ~lside;
        end
    end        
    
end