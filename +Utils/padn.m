function newstr = padn(str, side, symbol, lgth)
    % This adds as many occurences of `symbol` on any required side of the
    % input str as necessary so that the resulting string be `length`-long.
    %
    % Inputs: - str: char array: the string to pad.
    %         - side: char array: can be 'right', 'left' or 'both'.
    %         - symbol: char array: the symbol to add.
    %         - length: int: the target length.
    %
    % Outputs: - newstr: char array: the padded str.
    %
    % Actions: None.
    %
    % Description: Do the same as pad, but add a \n at the end of the
    % returned string.
    newstr = [Utils.pad(str, side, symbol, lgth), '\n'];
end