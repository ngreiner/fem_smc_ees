function seed = gen_seed(varargin)
    % Return an integer encrypting the input sequence of string/integer.
    %
    % Usage:
    %   seed = Utils.gen_seed(arg1, arg2, ...)
    %
    % Parameters:
    %   arg1, arg2, ... : string / integer.
    %
    % Return:
    %   seed : integer.
    
    seed = 1;
    for i = 1 : size(varargin, 2)
        seed = seed * sum(double(varargin{i}));
    end 
    
end
