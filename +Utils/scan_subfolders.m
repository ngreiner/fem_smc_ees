function subfolders = scan_subfolders(folder)
    % Return cell-array of names of folders contained in the input folder.
    %
    % Usage:
    %   subfolders = Utils.scan_subfolders(folder)
    %
    % Parameters:
    %   folder: string: path to folder to be scanned.
    %
    % Return:
    %   subfolders: N*1 cell-array of strings: names of folders in input
    %              folder.
    %
    % Description: Void.
    
    % Scan folder.
    subfolders = dir(folder);
    % Extract subfolders.
    idir = [subfolders(:).isdir];
    subfolders = {subfolders(idir).name}';
    % Remove '.' and '..'.
    subfolders(ismember(subfolders, {'.', '..'})) = [];
    
end
