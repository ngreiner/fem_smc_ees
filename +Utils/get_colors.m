function colors = get_colors(n, varargin)
    % Return a cell-array of colors.
    %
    % Usage:
    %   colors = Utils.get_colors(n)
    %   colors = Utils.get_colors(n, 'cmap', cmap)
    %   colors = Utils.get_colors(n, 'lthresh', lthresh)
    %   colors = Utils.get_colors(n, 'hthresh', hthresh)
    %
    % Parameters:
    %   n    : integer    : number of colors to return.
    %
    % Optional parameters:
    %   cmap    : string : name of colormap.
    %   lthresh : double : between 0 and 1 : lower bound of colormap.
    %   hthresh : double : between 0 and 1 : upper bound of colormap.
    %
    % Return:
    %   colors : n*1 cell : n colors.
    %
    % Description:
    %   If cmap is not specified, the method uses the jet colormap instead.

    % Parse inputs.
    % Colormap.
    idx = find(strcmp(varargin, 'cmap'));
    if idx
        if strcmp(varargin{idx + 1}, 'inferno')
            cmap = Utils.inferno;
        else
            cmap = jet;
        end
    else
        cmap = jet;
    end
    
    % Low threshold.
    idx = find(strcmp(varargin, 'lthresh'));
    if idx
        lthresh = varargin{idx + 1};
    else
        lthresh = 0;
    end
    il = 1 + floor(lthresh * size(cmap, 1));
    
    % High threshold.
    idx = find(strcmp(varargin, 'hthresh'));
    if idx
        hthresh = varargin{idx + 1};
    else
        hthresh = 1;
    end
    ih = ceil(hthresh * size(cmap, 1));
    
    % Build and return colors.
    cmap = cmap(il : ih, :);
    R = ones(size(cmap, 1), 1);
    colors = mat2cell(cmap, R);
    idx = floor(linspace(1, size(colors, 1), n));
    colors = colors(idx, :);

end
