function export_final_geom(this, varargin)
	% Export final geometry of CM.
    %
    % Usage:
    %   fem.export_final_geom()
    %   fem.export_final_geom('file_name', file_name, ...)
    %   fem.export_final_geom('dir_export', dir_export, ...)
    %
    % Parameters:
    %   file_name : string [optional] :
    %       Name of file without extension. Defaults to 'final_geom'.
    %
    %   dir_export : string [optional] :
    %       Directory where to export the geometry file. Defaults to
    %       dirs.comsol_CAD.
    %
    % Description:
    %   The resulting file is $dir_export/$file_name.mphbin.
    
    idx = find(strcmp(varargin, 'file_name'));
    if idx
        file_name = varargin{idx + 1};
    else
        file_name = 'final_geom';
    end
    
    idx = find(strcmp(varargin, 'dir_export'));
    if idx
        dir_export = varargin{idx + 1};
    else
        dir_export = this.dirs.comsol_CAD;
    end

    this.CM.geom('geom1').exportFinal(fullfile(dir_export, file_name));
    
end

