function make_unif_surf(this, tag, color, varargin)
	% Create a uniformly colored surface in CM.
    %
    % Usage:
    %   fem.make_unif_surf(tag, radius, color)
    %   fem.make_unif_surf(tag, radius, color, 'pg', pg)
    %
    % Parameters:
    %   tag : string :
    %       Tag of a boundary selection.
    %
    %   color : string or 1*3 cell :
    %       Color name or RGB-triplet.
    %
    %   pg : string :
    %       Tag of a plot group.
    %
    % Description:
    %   If pg is not specified, the colored line is created in plot group
    %   'pg1'.
    
    idx = find(strcmp('pg', varargin));
    if isempty(idx)
        pg = 'pg1';
    else
        pg = varargin{idx + 1};
    end
    
    pg_tags = cell(this.CM.result.tags);
    if ~any(strcmp(pg, pg_tags))
        this.CM.result.create(pg, 'PlotGroup3D');
        this.CM.result(pg).set('edges', 'off');
    end
    
    this.make_dataset(sprintf('dset_%s', tag), tag);
    
    pg_tags = cell(this.CM.result('pg1').feature.tags);
    if ~any(strcmp(sprintf('surf_%s', tag), pg_tags))
        this.CM.result(pg).create(sprintf('surf_%s', tag), 'Surface');
    end
    
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('data', sprintf('dset_%s', tag));
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('coloring', 'uniform');
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('customcolor', color);
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('color', 'custom');
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('expr', '1');
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('descr', '1');
    this.CM.result(pg).feature(sprintf('surf_%s', tag)).set('resolution', 'normal');
    
end
