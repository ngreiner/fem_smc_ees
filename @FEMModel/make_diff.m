function make_diff(this, tag, label, keep, intbnd, input1, input2)
	% Build a Difference feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_diff(tag, label, keep, intbnd, input1, input2)
    %
    % Parameters:
    %   tag : string : tag of feature.
    %	label : string : label of feature.
    %	keep : string : 'on' or 'off' : indicates wether to keep input
    %                   objects or not. 
    %   intbnd : string : 'on' or 'off': indicates wether to keep interior
    %                     boundaries or not.
    %	input1 : 1*N cell of strings : tags of objects to add.
    %	input2 : 1*M cell of strings : tags of objects to remove.
    %
    % Description:
    %   Objects in input2 are substracted from the combination of objects
    %   in input1.
    %   If keep is 'off', all the objects of input1 and input2 are
    %   deleted once the difference is constructed (equivalent to a Delete
    %   feature).
    %   If intbnd is off, the internal boundaries between two input
    %   objects remaining in the unified object are deleted.
    
    fprintf('Making ''%s'' Difference feature...\n', tag)
    fprintf('Added objects: ')
    cellfun(@(x) fprintf('%s, ', x), input1)
    fprintf('\nSubtracted objects: ')
    cellfun(@(x) fprintf('%s, ', x), input2)
    fprintf('\nKeeping input objects: %s\n', keep)
    
    this.CM.geom('geom1').create(tag, 'Difference');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('keep', keep);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('intbnd', intbnd);
    this.CM.geom('geom1').feature(tag).selection('input').set(input1);
    this.CM.geom('geom1').feature(tag).selection('input2').set(input2);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('... Done.\n\n')
    
end
