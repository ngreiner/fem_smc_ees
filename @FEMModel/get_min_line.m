function min_line = get_min_line(this, tag, quantity, sol)
    % Return the min of a quantity over a line.
    %
    % Usage:
    %   min_line = fem.get_min_line(tag, quantity, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the line over which the min should be computed.
    %
    %   quantity : string :
    %       Identifier of the quantity whose min should be computed.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    
    % Make MinLine.
    fprintf('Making MinLine: %s ...\n', tag)
    
    % Make table to store numerical result.
    this.make_table(sprintf('tbl_min_%s_%s', tag, sol));
    
    % Make dataset.
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    % Make MinLine.
    num_tag = sprintf('min_%s_%s', tag, sol);
    num_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(num_tag, num_tags))
        this.CM.result.numerical.create(num_tag, 'MinLine');
    end
    this.CM.result.numerical(num_tag).set('data', sprintf('dset_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).selection.named(tag);
    
    % Attach MinLine to table.
    this.CM.result.numerical(num_tag).set('table', sprintf('tbl_min_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).set('expr', {quantity});
    this.CM.result.numerical(num_tag).setResult;
    
    fprintf('... Done.\n')
    
    % Fetch value.
    min_line = this.CM.result.numerical(num_tag).getReal();

end
