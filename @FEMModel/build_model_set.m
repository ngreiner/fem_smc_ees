function build_model_set(this, AS_idxs)
	% Build a model set. 
    %
    % Usage:
    %   fem.build_model_set()
    %
    % Description:
    %   We call model set the collection of models obtained when using all
    %   the available active sites of the electrode array, activating them
    %   one at a time consecutively.
    
    % Parse inputs.
    if nargin == 1
        AS_idxs = (1 : this.elec.AS.count)';
    end
    
    % Initialize Comsol Model.
    this.build_CM();

    % Run Mesh and Curvilinear Coordinates nodes once for all the
    % active sites.
    this.run_mesh();
    this.run_CC();
    
    % Loop over the active sites and compute the set of models.
    for i = 1 : size(AS_idxs, 1)
        this.update_AS(AS_idxs(i));
        this.run_EC();
        this.save_CM();
    end
    
end
