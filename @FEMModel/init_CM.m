function init_CM(this)
	% Initialize the comsol model CM.
    %
    % Usage:
    %   fem.init_CM()
    fprintf('Initializing Comsol Model...\n')
    import com.comsol.model.*
    import com.comsol.model.util.*
    % Create model and component
    this.CM = ModelUtil.create('Model');
    this.CM.modelPath('.');
    this.CM.modelNode.create('comp1');
    fprintf('Initialized.\n')
end
