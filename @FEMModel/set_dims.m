function set_dims(this)
	% Set various geometrical parameters of the FEMModel.
    %
    % Usage:
    %   fem.set_dims()
    %
    % Description:
    %
    %   This method defines geom.z_parts := [z1, z2, z3, z4] where z1, z2,
    %   z3 and z4 are as depicted on the following sketch:
    %
    %   -l_exts                  0        seg_length     seg_length+l_exts
    %      |                     |            |                     |
    %   ---|------------------|--|------------|--|------------------|--->
    %      |                  |                  |                  |
    %      z1                 z2                 z3                 z4
    %    
    %   It also redefines params.bone.radius in case the latter was defined
    %   by any of 'small', 'medium', 'large' or left unspecified in the
    %   configuration file.
    %
    %   It defines similarly params.saline.radius.
    
    % z_parts.
	this.params.geom.z_parts = zeros(4, 1);
    this.params.geom.z_parts(1) = - this.sc.morpho.l_exts * 1e-3;
    this.params.geom.z_parts(2) = - this.sc.morpho.seg_length / 10 * 1e-3;
    this.params.geom.z_parts(3) = 11 / 10 * this.sc.morpho.seg_length * 1e-3;
    this.params.geom.z_parts(4) = (this.sc.morpho.seg_length + this.sc.morpho.l_exts) * 1e-3;
    
    % bone.radius.
    r0 = max(this.sc.morpho.fat_max_trans_width, this.sc.morpho.fat_max_sag_width) / 2 * 1e-3;
    r = this.params.bone.radius;
    if strcmp(r, 'small')
        r = 1.15 * r0;
    elseif strcmp(r, 'medium')
        r = 1.30 * r0;
    elseif strcmp(r, 'large')
        r = 1.45 * r0;
    elseif isnan(r)
        r = 1.30 * r0;
    end
    this.params.bone.radius = r;
    
    % saline.radius.
    s = this.params.saline.size;
    if strcmp(s, 'small')
        r = 4 * 1.15 * r0;
    elseif strcmp(s, 'medium')
        r = 4 * 1.30 * r0;
    elseif strcmp(s, 'large')
        r = 4 * 1.45 * r0;
    else
        r = 4 * 1.30 * r0;
    end
    this.params.saline.radius = r;
    
end
