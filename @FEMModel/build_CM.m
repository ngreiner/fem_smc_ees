function build_CM(this)
	% Build the comsol model CM. 
    %
    % Usage:
    %   fem.build_CM()
    %
	% Description:
    %   Builds CM according to the attributes of the FEMModel.
    %   Specifically, this method builds the geometry, defines the
    %   selections, assign the materials, set-up the parameters to compute
    %   the curvilinear coordinates and the electric potential, set-up the
    %   parameters to build the mesh, and prepare the plots.
    
    % Initialize comsol model object.
    this.init_CM();
    
    % Build geometry.
    this.build_geom();
    
    % Make selections.
    this.build_sel();
    
    % Set materials.
    this.build_mat();
    
    % Set curvilinear coordinates node.
    this.build_CC();
    
    % Set electric currents node.
    this.build_EC();
    
    % Set mesh.
    this.build_mesh();
    
    % Set studies.
    this.build_studies();
    
    % Set plots.
    this.build_plots();
    
end
