function make_polygon(this, tag, label, path)
	% Make a Polygon feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_polygon(tag, label, path)
    %
    % Parameters:
    %   tag : string :
    %       Tag of feature.
    %
    %	label : string :
    %       Label of feature.
    %
    %	path : N*3 double :
    %       Polygon endpoints coordinates.
    
    fprintf('Making ''%s'' Polygon feature...\n', tag)
    
    this.CM.geom('geom1').create(tag, 'Polygon');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('x', path(:, 1));
    this.CM.geom('geom1').feature(tag).set('y', path(:, 2));
    this.CM.geom('geom1').feature(tag).set('z', path(:, 3));
    
    fprintf('... Done.\n\n')
    
end
