function parse_cond_file(this, file_name)
	% Parse the input conductivities file.
    %
    % Usage:
    %   fem.parse_cond_file(file_name)
    %
    % Parameters:
    %   file_name : string : conductivities file name.
    %
    % Description:
    %   The full path to the conductivities file is obtained either as
    %   $dirs.top/$cond_file_name or  $dirs.resources/$cond_file_name.

    % Open conductivities file.
    if exist(fullfile(this.dirs.top, file_name), 'file')
        fileID = fopen(fullfile(this.dirs.top, file_name));
    elseif exist(fullfile(this.dirs.resources, file_name), 'file')
        fileID = fopen(fullfile(this.dirs.resources, file_name));
    else
        error('%s: file not found.', file_name);
    end

    % Read file content as a two-columns file.
	C = textscan(fileID, '%s %f');
    
    % Close file.
	fclose(fileID);
    
    % Parse conductivities.
	this.params.cond.GM = C{2}(strcmp(C{1}, 'GM'));
	this.params.cond.WM_longi = C{2}(strcmp(C{1}, 'WM_longi'));
	this.params.cond.WM_trans = C{2}(strcmp(C{1}, 'WM_trans'));
	this.params.cond.CSF = C{2}(strcmp(C{1}, 'CSF'));
	this.params.cond.dura = C{2}(strcmp(C{1}, 'dura'));
	this.params.cond.fat = C{2}(strcmp(C{1}, 'fat'));
	this.params.cond.elec = C{2}(strcmp(C{1}, 'elec'));
	this.params.cond.paddle = C{2}(strcmp(C{1}, 'paddle'));
	this.params.cond.bone = C{2}(strcmp(C{1}, 'bone'));
	this.params.cond.saline = C{2}(strcmp(C{1}, 'saline'));
    
end
