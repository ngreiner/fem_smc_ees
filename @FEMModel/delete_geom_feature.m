function delete_geom_feature(this, tag)
    % Remove the specified feature from the geometry of CM.
    %
    % Usage:
    %   fem.delete_geom_feature(tag)
    %
    % Parameters:
    %   tag : string : tag of the geometry feature to remove.
    this.CM.geom('geom1').feature().remove(tag);
end
