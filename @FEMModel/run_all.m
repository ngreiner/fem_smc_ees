function run_all(this)
	% Run the Mesh node, the Curvilinear Coordinates node and the Electric
	% Currents node of CM.
    %
    % Usage:
    %   fem.run_all()
	this.CM.mesh('mesh1').run;
	this.CM.sol('sol1').runAll;
	this.CM.sol('sol2').runAll;
    this.set_AS_area();
end
