function make_table(this, tag)
	% Make a table in CM.
    %
    % Usage:
    %   fem.make_table()
    %
    % Parameters:
    %   tag : str :
    %       Tag of the table.
    %
    % Description:
    %   Tables can store numerical results.
    %   If a table with specified tag already exist, this method does
    %   nothing.
    
    tbl_tags = cell(this.CM.result.table.tags);
    if any(strcmp(tag, tbl_tags))
        return
    end
    
    this.CM.result.table.create(tag, 'Table');
    
end
