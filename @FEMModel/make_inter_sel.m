function make_inter_sel(this, dim, tag, inputs)
    % Create an intersection selection between input selections.
    %
    % Usage:
    %   fem.make_inter_sel(dim, tag, inputs)
    %
    % Parameters:
    %   dim     : double : entity level indicator (3=domains, 2=boundaries,
    %                      1=edges, 0=points).
    %   tag     : string : tag of the intersection selection.
    %   input1  : 1*N cell  : tags of selections to intersect.
    
    fprintf('Making Intersection selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Input selections to intersect:\n')
    cellfun(@(x) fprintf('%s: %s\n', x, this.CM.selection(x).toString.toCharArray), inputs)
    
    this.CM.selection.create(tag, 'Intersection');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('input', inputs);
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
