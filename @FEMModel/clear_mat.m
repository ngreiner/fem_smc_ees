function clear_mat(this)
	% Clear the Material node of CM.
	%
	% Usage:
    %   fem.clearMat()
	%
	% Description:
    %   Clears the Material node for a fresh new start.
    this.CM.material.clear()
end
