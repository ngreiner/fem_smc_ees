function build_CSF(this)
    % Build the geometry feature representing the cerebro-spinal fluid.
    %
    % Usage:
    %   fem.build_CSF()
    %
    % Description:
    %   Imports the appropriate CAD file, representing the cerebro-spinal
    %   fluid, and remove from it the features already built that are
    %   intersecting it by boolean difference.
    
    fprintf('Building CSF...\n')
    
    if strcmp(this.params.sc.CAD_file_format, 'step')
        this.make_import('tmp_CSF', 'tmp_CSF', fullfile(this.dirs.step, 'CSF.step'), 'type', 'cad');
        
    elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
        this.make_import('tmp_CSF', 'tmp_CSF', fullfile(this.dirs.comsol_CAD, 'CSF.mphbin'), 'type', 'native');
        
    end
    
    this.make_diff('CSF', 'CSF', 'on', 'off', {'tmp_CSF'}, {'GM', 'WM_with_roots'});
    this.make_delete('del_CSF', 'del_CSF', {'tmp_CSF'});
    
end
