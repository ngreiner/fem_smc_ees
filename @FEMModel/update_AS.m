function update_AS(this, AS_idx)
    % Update the selections related to the stimulation active site.
    %
    % Usage:
    %   fem.update_AS(AS_idx)
    %
    % Parameters:
    %   AS_idx : double : index of the new stimulating active site.
    %           
    % Description:
    %   Updates the selections 'stimu_boundary' and 'insulating_plates'
    %   (when applicable) to refer to the new active site.
    
    % Update attribute ASindex.
    this.AS.idx = AS_idx;
    
    % Update stimu_boundary selection.
    x = this.elec.AS.coords{AS_idx}{1} * 1e-3;
    y = this.elec.AS.coords{AS_idx}{2} * 1e-3;
    z = this.elec.AS.coords{AS_idx}{3} * 1e-3;
    if ~ strcmp(this.elec.paddle.flag, 'in_fat')
        this.update_box_sel(2, 'box_stimu_boundary', x, y, z, this.elec.AS.thick / 2 * 1e-3);
    else
        this.update_box_sel(2, 'box_stimu_boundary', x, y, z, 2 * this.elec.AS.thick / 3 * 1e-3);
    end
    
end
