function run_geom_node(this)
    % Run the Geometry node of CM.
    %
    % Usage:
    %   fem.run_geom_node()
    this.CM.geom('geom1').run;
end
