function make_compl_sel(this, dim, tag, inputs)
    % Create a complement selection.
    %
    % Usage:
    %   fem.make_compl_sel(dim, tag, inputs)
    %
    % Parameters:
    %   dim     : double : entity level indicator (3=domains, 2=boundaries,
    %                      1=edges, 0=points).
    %   tag     : string : tag of the complement selection.
    %   inputs  : 1*N cell  : names of selections to invert.
    %
    % Description:
    %   The complement selection selects all entities which are not already
    %   selected by the selections inputs.
    
    fprintf('Making Complement selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Input selections:\n')
    cellfun(@(x) fprintf('%s: %s\n', x, this.CM.selection(x).toString.toCharArray), inputs)
    
    this.CM.selection.create(tag, 'Complement');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('input', inputs);
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
