function max_line = get_max_line(this, tag, quantity, sol)
    % Return the max of a quantity over a line.
    %
    % Usage:
    %   max_line = fem.get_max_line(tag, quantity, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the line over which the max should be computed.
    %
    %   quantity : string :
    %       Identifier of the quantity whose max should be computed.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    
    fprintf('Making max_line: %s ...\n', tag)
    
    % Make table to store numerical result.
    this.make_table(sprintf('tbl_max_%s_%s', tag, sol));
    
    % Make dataset.
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    % Make MaxLine.
    num_tag = sprintf('max_%s_%s', tag, sol);
    num_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(num_tag, num_tags))
        this.CM.result.numerical.create(num_tag, 'MaxLine');
    end
    this.CM.result.numerical(num_tag).set('data', sprintf('dset_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).selection.named(tag);
    
    % Attach MaxLine to table.
    this.CM.result.numerical(num_tag).set('table', sprintf('tbl_max_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).set('expr', {quantity});
    this.CM.result.numerical(num_tag).setResult;
    
    fprintf('... Done.\n')
    
    % Fetch value.
    max_line = this.CM.result.numerical(num_tag).getReal();
    
end
