function clear_physics(this)
	% Clear the Physics node of CM.
	%
	% Usage:
    %   fem.clear_physics()
	%
	% Description: 
    %   Clears the Physics node for a fresh new start.
    this.CM.physics.clear()
end
