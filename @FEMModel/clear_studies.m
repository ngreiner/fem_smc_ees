function clear_studies(this)
	% Clear the Study nodes of CM.
	%
	% Usage:
    %   fem.clear_studies()
	%
	% Description:
    %   Clears the Study nodes for a fresh new start.
    this.CM.study.clear()
end
