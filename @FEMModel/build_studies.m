function build_studies(this)
	% Build the Study nodes of CM.
	%
	% Usage:
    %   fem.build_studies()
	%
    % Description: 
    %   2 studies are defined so as to run the Curvilinear Coordinates
    %   node alone first, and then the Electric Currents node alone using
    %   the results of the first study (e.g. the curvilinear coordinates
    %   system).

    fprintf('Building Studies...\n')
    
    % Case where curvilinear coordinate are enabled.
    if this.params.physics.with_curvi_coords

        % Create study for curvilinear coordinates only.
        this.CM.study.create('std1');
        this.CM.study('std1').create('stat', 'Stationary');
        this.CM.study('std1').feature('stat').set('showdistribute', false);
        this.CM.study('std1').feature('stat').set('physselection', 'cc');
        this.CM.study('std1').feature('stat').set('activate', {'cc' 'on' 'ec' 'off'});

        % Create study for electric currents only. Requires that the variables
        % not solved for be the ones computed in study1, that is, the 
        % curvilinear coordinates.
        this.CM.study.create('std2');
        this.CM.study('std2').create('stat', 'Stationary');
        this.CM.study('std2').feature('stat').set('showdistribute', false);
        this.CM.study('std2').feature('stat').set('physselection', 'cc');
        this.CM.study('std2').feature('stat').set('activate', {'cc' 'off' 'ec' 'on'});
        this.CM.study('std2').feature('stat').set('usesol', 'on');
        this.CM.study('std2').feature('stat').set('notsolmethod', 'sol');
        this.CM.study('std2').feature('stat').set('notstudy', 'std1');


        % Create solutions to store the results of the previous studies.
        % Create solution for study1 (cc).
        this.CM.sol.create('sol1');
        this.CM.sol('sol1').study('std1');

        this.CM.study('std1').feature('stat').set('notlistsolnum', 1);
        this.CM.study('std1').feature('stat').set('notsolnum', '1');
        this.CM.study('std1').feature('stat').set('listsolnum', 1);
        this.CM.study('std1').feature('stat').set('solnum', '1');

        this.CM.sol('sol1').create('st1', 'StudyStep');
        this.CM.sol('sol1').feature('st1').set('study', 'std1');
        this.CM.sol('sol1').feature('st1').set('studystep', 'stat');
        this.CM.sol('sol1').create('v1', 'Variables');
        this.CM.sol('sol1').feature('v1').set('control', 'stat');
        this.CM.sol('sol1').create('s1', 'Stationary');
        this.CM.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
        this.CM.sol('sol1').feature('s1').feature('fc1').set('linsolver', 'dDef');
        this.CM.sol('sol1').feature('s1').feature.remove('fcDef');
        this.CM.sol('sol1').attach('std1');

        % Create solution for study2 (ec).
        this.CM.sol.create('sol2');
        this.CM.sol('sol2').study('std2');

        this.CM.study('std2').feature('stat').set('notlistsolnum', 1);
        this.CM.study('std2').feature('stat').set('notsolnum', 'auto');
        this.CM.study('std2').feature('stat').set('listsolnum', 1);
        this.CM.study('std2').feature('stat').set('solnum', '1');

        this.CM.sol('sol2').create('st1', 'StudyStep');
        this.CM.sol('sol2').feature('st1').set('study', 'std2');
        this.CM.sol('sol2').feature('st1').set('studystep', 'stat');
        this.CM.sol('sol2').create('v1', 'Variables');
        this.CM.sol('sol2').feature('v1').set('control', 'stat');
        this.CM.sol('sol2').create('s1', 'Stationary');
        this.CM.sol('sol2').feature('s1').create('fc1', 'FullyCoupled');
        this.CM.sol('sol2').feature('s1').create('i1', 'Iterative');
        this.CM.sol('sol2').feature('s1').feature('i1').set('linsolver', 'cg');
        this.CM.sol('sol2').feature('s1').feature('i1').create('mg1', 'Multigrid');
        this.CM.sol('sol2').feature('s1').feature('i1').feature('mg1').set('prefun', 'amg');
        this.CM.sol('sol2').feature('s1').feature('fc1').set('linsolver', 'i1');
        this.CM.sol('sol2').feature('s1').feature.remove('fcDef');
        this.CM.sol('sol2').attach('std2');

    % Case where curvilinear coordinates are disabled.
    else

        % Create study for electric currents.
        this.CM.study.create('std2');
        this.CM.study('std2').create('stat', 'Stationary');
        this.CM.study('std2').feature('stat').set('showdistribute', false);
        this.CM.study('std2').feature('stat').set('physselection', 'ec');
        this.CM.study('std2').feature('stat').set('activate', {'ec' 'on'});

        % Create solution for study2 (ec).
        this.CM.sol.create('sol2');
        this.CM.sol('sol2').study('std2');

        this.CM.study('std2').feature('stat').set('notlistsolnum', 1);
        this.CM.study('std2').feature('stat').set('notsolnum', '1');
        this.CM.study('std2').feature('stat').set('listsolnum', 1);
        this.CM.study('std2').feature('stat').set('solnum', '1');

        this.CM.sol('sol2').create('st1', 'StudyStep');
        this.CM.sol('sol2').feature('st1').set('study', 'std2');
        this.CM.sol('sol2').feature('st1').set('studystep', 'stat');
        this.CM.sol('sol2').create('v1', 'Variables');
        this.CM.sol('sol2').feature('v1').set('control', 'stat');
        this.CM.sol('sol2').create('s1', 'Stationary');
        this.CM.sol('sol2').feature('s1').create('fc1', 'FullyCoupled');
        this.CM.sol('sol2').feature('s1').create('i1', 'Iterative');
        this.CM.sol('sol2').feature('s1').feature('i1').set('linsolver', 'cg');
        this.CM.sol('sol2').feature('s1').feature('i1').create('mg1', 'Multigrid');
        this.CM.sol('sol2').feature('s1').feature('i1').feature('mg1').set('prefun', 'amg');
        this.CM.sol('sol2').feature('s1').feature('fc1').set('linsolver', 'i1');
        this.CM.sol('sol2').feature('s1').feature.remove('fcDef');
        this.CM.sol('sol2').attach('std2');

    end


    fprintf('Studies are built.\n\n')
    
end
