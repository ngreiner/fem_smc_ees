function build_CC(this)
	% Build the Curvilinear Coordinates node in CM.
	%
	% Usage:
    %   fem.build_CC()
	%
	% Description: 
    %   The Curvilinear Coordinates node of Comsol enables to build a
    %   curvilinear coordinate system over a given volume by solving a
    %   heat-diffusion equation over the desired volume.
	%   To do so, this method implements the following features:
    %       - the 'inlet'  : set of boundaries where T=1;
    %       - the 'outlet' : set of boundaries where T=0;
    %       - the 'wall'   : set of boundaries where adiabatic conditions
    %                        are applied.

    if this.params.physics.with_curvi_coords

        fprintf('Building Curvilinear Coordinates Node...\n')

        % Create Node.
        this.CM.physics.create('cc', 'CurvilinearCoordinates', 'geom1');
        
        % Enable creation of coordinate system basis.
        this.CM.physics('cc').prop('Settings').set('CreateBasis', true);
        
        % Apply only to WM_ with_roots.
        this.CM.physics('cc').selection.named('WM_with_roots');
        
        % Use diffusion method on all domains
        this.CM.physics('cc').create('diff1', 'DiffusionMethod', 3);
        this.CM.physics('cc').feature('diff1').selection.named('WM_with_roots');
        
        % Set roots surfacic intersections as walls.
        switch this.params.roots.repr
            case 'dorsal_ventral'
                for i = 1 : this.sc.morpho.nroots - 1
                    this.CM.physics('cc').feature('diff1').create(sprintf('wall%d', i + 1), 'Wall', 2);
                    this.CM.physics('cc').feature('diff1').feature(sprintf('wall%d', i + 1)).selection.named(sprintf('geom1_intsel%d', i));
                end
            case 'left_right'
                fprintf('No wall feature in cc for root surfacic intersections.\n')
        end
        
        % Set Inlet.
        this.CM.physics('cc').feature('diff1').create('Inlet', 'Inlet', 2);
        this.CM.physics('cc').feature('diff1').feature('Inlet').selection.named('inlet_cc');
        
        % Set Outlet.
        this.CM.physics('cc').feature('diff1').create('Outlet', 'Outlet', 2);
        this.CM.physics('cc').feature('diff1').feature('Outlet').selection.named('outlet_cc');

        fprintf('Curvilinear Coordinates Node is built.\n\n')

    else
        
        fprintf('Curvilinear Coordinates are disabled.\n\n')

    end
    
end
