function build_sel(this)
    % Build selections of domains, boundaries and points in CM.
    %
    % Usage:
    %   fem.build_sel()
    %           
    % Description : 
    %   Selections are sets of domains or boundaries, which one can refer
    %   to for building the mesh, defining physics properties or setting
    %   the materials of the geometry. 
    %   This method creates :
    %       - domain selections for material settings;
    %       - domain selections for mesh settings;
    %       - domain selections for view settings;
    %       - boundary selections for Curvilinear Coordinates settings;
    %       - boundary selections for Electric Currents settings;
    %       - boundary selections for view settings.
    
    fprintf('Making selections...\n')
    
    % Domain selections for materials.
    this.make_union_sel(3, 'GM', {'geom1_GM_dom'});
    this.make_union_sel(3, 'WM_with_roots', {'geom1_WM_with_roots_dom'});
    this.make_union_sel(3, 'CSF', {'geom1_CSF_dom'});
    this.make_union_sel(3, 'dura', {'geom1_dura_dom'});
    this.make_union_sel(3, 'elec', {'geom1_elec_dom'});
    if ~ strcmp(this.elec.paddle.flag, 'off')
        this.make_union_sel(3, 'paddle', {'geom1_paddle_dom'});
        this.make_union_sel(3, 'array', {'elec', 'paddle'});
    else
        this.make_union_sel(3, 'array', {'elec'});
    end
    this.make_union_sel(3, 'fat', {'geom1_fat_dom'});
    this.make_union_sel(3, 'bone', {'geom1_bone_dom'});
    if ~ strcmp(this.params.saline.repr, 'single_filled_cylinder')
        this.make_diff_sel(3, 'saline', {'geom1_part1_dom' 'geom1_part2_dom' 'geom1_part3_dom'}, {'bone', 'fat', 'array', 'dura', 'CSF', 'WM_with_roots', 'GM'});
    else
        this.make_diff_sel(3, 'saline', {'geom1_part1_dom'}, {'bone', 'fat', 'array', 'dura', 'CSF', 'WM_with_roots', 'GM'});
    end
    
    % Domain selections for views.
    if strcmp(this.params.saline.repr, 'single_filled_cylinder')
        this.make_union_sel(3, 'all_doms', {'geom1_part1_dom'});
    else
        this.make_union_sel(3, 'all_doms', {'geom1_part1_dom', 'geom1_part2_dom', 'geom1_part3_dom'});
    end
    this.make_diff_sel(3, 'all_but_WM_with_roots', {'all_doms'}, {'WM_with_roots'});
    
    % Boundary selections for Curvilinear Coordinates.
    % Inlet boundary.
    % It is the upper surface of the WM. To get it we use a box-selection
    % which will intersect this specific boundary. We look for the point
    % P(x,y,z) of the uppermost cross- section contour of the WM with
    % maximum y-coordinate. Then we create a cubic box around this point
    % which corners are Pmin(x-dx,y-3dy,z-dz) and Pmax(x+dx,y-dy,z+dz).
    % If dx,dy,dz are sufficiently small, the box intersects only the
    % desired boundary.
    WM = this.sc.eCS{1}.WM;
    [~, idx] = max(WM(:, 2));
    x = WM(idx, 1) * 1e-3;
    y = WM(idx, 2) * 1e-3 - 2e-5;
    z = WM(idx, 3) * 1e-3;
    this.make_box_sel(2, 'inlet_cc', x, y, z);
    % Outlet boundaries. 
    % They are the last cross-section of each root, plus the lower surface
    % of the WM. To get them, we use box-selections as above.
    % We store the names of these individual selections to unify them at
    % the end.
    % For each root, the point whose coordinates will be used is the tip
    % of the root.
    % For the lower surface of the WM, we do as for the upper one (see
    % above).
    feats = cell(this.sc.morpho.nroots + 1, 1);    
    for j = 1 : this.sc.morpho.nroots
        Ltip = this.sc.roots{j}.Ltip;
        x = Ltip(1) * 1e-3;
        y = Ltip(2) * 1e-3;
        z = Ltip(3) * 1e-3;
        this.make_box_sel(2, sprintf('outlet_cc_%d', 2 * j - 1), x, y, z);
        feats{2 * j - 1} = sprintf('outlet_cc_%d', 2 * j - 1);
        this.make_box_sel(2, sprintf('outlet_cc_%d', 2 * j), -x, y, z);
        feats{2 * j} = sprintf('outlet_cc_%d', 2 * j);
    end
    WM = this.sc.eCS{end}.WM;
    [~, idx] = max(WM(:, 2));
    x = WM(idx(1), 1) * 1e-3;
    y = WM(idx(1), 2) * 1e-3 - 2e-5;
    z = WM(idx(1), 3) * 1e-3;
    this.make_box_sel(2, 'outlet_cc_bottom', x, y, z);
    feats{2 * this.sc.morpho.nroots + 1} = 'outlet_cc_bottom';
    this.make_union_sel(2, 'outlet_cc', feats);

    % Boundary selections for Electric Currents.
    % Border boundaries.
    % We first select all the inner boundaries using a cylinder-selection.
    % The charachteristics of the cylinder are the following : 
    %   - the radius is 0.95 times the radius of the saline;
    %   - the height is the mean between the total length of the structure
    %     (central part plus extended parts) and the length of the central
    %     part alone;
    %	- the position (z-coordinate of base-point) is the mean betwen the
    %	  z-coordinates of the lower and upper surfaces of the lower part. 
    % Like this we make sure that we take all boundaries except the ones of
    % the border.
    % We get the boundaries of the border using a complement selection.
    r = 0.95 * this.params.saline.radius;
    zmin = (this.params.geom.z_parts(1) + this.params.geom.z_parts(2)) / 2;
    h = (this.params.geom.z_parts(4) - this.params.geom.z_parts(1)) / 2 + (this.params.geom.z_parts(3) - this.params.geom.z_parts(2)) / 2;
    this.make_cyl_sel(2, 'inner_boundaries', r, h, {0, 0, zmin});
    this.make_compl_sel(2, 'border', {'inner_boundaries'});
    % Boundary for the current/voltage source.
    % It is the boundary of the 1st active site facing the spinal cord.
    % We obtain it using a box selection around an identified point. 
    % First we look for the center of the block defining the active site.
    % Then, if the paddle is not included:
    % The point we're looking for  lies in the same direction as the center
    % of the block but at a distance from the z-axis reduced by half the
    % width of the active site.
    % Otherwise:
    % The block initially defining the active site was cropped by the
    % paddle, hence the point we're looking for is the center of the block
    % defining the active site, but we use a larger box selection to make
    % sure we get the inner boundary.
    % If the paddle is sticked on the dura (paddleFlag = on_dura), then we
    % need to restrain the previous selection to the set of active sites
    % boundaries to discard the boundary of the dura.
    x = this.elec.AS.coords{this.AS.idx}{1} * 1e-3;
    y = this.elec.AS.coords{this.AS.idx}{2} * 1e-3;
    z = this.elec.AS.coords{this.AS.idx}{3} * 1e-3;
    if ~ strcmp(this.elec.paddle.flag, 'in_fat')
        this.make_box_sel(2, 'box_stimu_boundary', x, y, z, this.elec.AS.thick / 2 * 1e-3);
    else
        this.make_box_sel(2, 'box_stimu_boundary', x, y, z, 2 * this.elec.AS.thick / 3 * 1e-3);
    end    
    this.make_inter_sel(2, 'stimu_boundary', {'box_stimu_boundary' 'geom1_elec_bnd'});
    % Boundaries for electric insulation if paddle is not included.
    % They are the boundaries of the  active site not concerned by the
    % current source condition.
    if strcmp(this.elec.paddle.flag, 'off')
        this.make_diff_sel(2, 'insulating_plates', {'geom1_as1_bnd'}, {'stimu_boundary'}); % <- need modif
    end    
    % Point selection for ground.
    if this.params.physics.with_ground_point
        this.make_union_sel(0, 'ground_point', {'geom1_ground_point_pnt'});
    end

end
