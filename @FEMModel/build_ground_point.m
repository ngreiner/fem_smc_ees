function build_ground_point(this)
	% Build the point feature representing the electrical ground.
	%
	% Usage:
    %   fem.build_ground_point()
	%
	% Description:
    %   The ground point is placed in the saline cylinder, on the opposite
    %   side of the spinal cord compared to the electrode array (i.e.
    %   ventrally).
    %   It is used to implement a Dirichlet condition to mimic the return
    %   of the current to a virtual IPG case.
    
    fprintf('Building ground point...\n')

    if this.params.physics.with_ground_point

        this.make_point('ground_point', 'ground_point', [0, 0.95 * this.params.saline.radius, 0]);
        fprintf('Ground point built.\n\n')

    else

    	fprintf('ground_point: off. Returned without making ground point.\n')
        
    end

end
