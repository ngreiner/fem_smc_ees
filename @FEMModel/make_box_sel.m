function make_box_sel(this, dim, tag, x, y, z, width)
    % Create a box selection around the point [x, y, z].
    %
    % Usage:
    %   fem.make_box_sel(dim, tag, x, y, z)
    %   fem.make_box_sel(dim, tag, x, y, z, width)
    %
    % Parameters:
    %   dim     : double : entity level indicator (3=domains, 2=boundaries,
    %                      1=edges, 0=points). 
    %   tag     : string : tag of the box selection.
    %   x       : double : box center x-coordinate (in m).
    %   y       : double : box center y-coordinate (in m).
    %   z       : double : box center z-coordinate (in m).
    %   width   : double : width of the box.
    
    % Parse inputs.
    if nargin == 6
        width = 2e-5;
    end

    % Check existence of box selection.
    tags = cell(this.CM.selection.tags);
    if any(strcmp(tag, tags))
        fprintf('Box Selection %s already exists.\n', tag)
        this.update_box_sel(tag, x, y, z, width);
        return
    end
    
    % Make box selection.
    fprintf('Making Box selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Box center x-coordinate: %f\n', x)
    fprintf('Box center y-coordinate: %f\n', y)
    fprintf('Box center z-coordinate: %f\n', z)
    fprintf('Box half-side: %f\n', width)
    
    this.CM.selection.create(tag, 'Box');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('xmin', sprintf('%f', x - width / 2));
    this.CM.selection(tag).set('xmax', sprintf('%f', x + width / 2));
    this.CM.selection(tag).set('ymin', sprintf('%f', y - width / 2));
    this.CM.selection(tag).set('ymax', sprintf('%f', y + width / 2));
    this.CM.selection(tag).set('zmin', sprintf('%f', z - width / 2));
    this.CM.selection(tag).set('zmax', sprintf('%f', z + width / 2));
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
