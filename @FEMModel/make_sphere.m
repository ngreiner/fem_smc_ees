function make_sphere(this, tag, label, r, pos)
	% Create a Sphere feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_sphere(tag, label, r, pos)
    %
    % Parameters:
    %   tag : string :
    %       Comsol tag of feature.
    %
    %	label : string :
    %       Comsol label of feature.
    %
    %	r : double/string :
    %       Radius of sphere.
    %
    %   pos : 1*3 cell :
    %       Coordinates of block's base point.
    
    r = num2str(r);
    pos = {sprintf('%f', pos(1)), sprintf('%f', pos(2)), sprintf('%f', pos(3))};
    
    fprintf('Making ''%s'' Sphere feature...\n', tag)
    fprintf('Sphere radius: %s\n', r)
    fprintf('Sphere position:\n')
    fprintf('x = %s\n', pos{1})
    fprintf('y = %s\n', pos{2})
    fprintf('z = %s\n', pos{3})
    
    this.CM.geom('geom1').create(tag, 'Sphere');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('r', r);
    this.CM.geom('geom1').feature(tag).set('pos', pos);
    
    fprintf('... Done.\n\n')
    
end
