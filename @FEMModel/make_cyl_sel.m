function make_cyl_sel(this, dim, tag, r, h, pos)
    % Create a cylinder selection.
    %
    % Parameters:
    %   dim : double : 
    %       Entity level indicator (3=domains, 2=boundaries, 1=edges,
    %       0=points).
    %
    %   tag : string :
    %       Tag of the cylinder selection.
    %
    %   r : double :
    %       Radius of cylinder.
    %
    %	h : double/string :
    %       Height of cylinder.
    %
    %   pos : 1*3 cell :
    %       Coordinates of cylinder's base point.
    %
    % Description:
    %   The cylinder selection selects all entities intersecting the
    %   specified cylinder.
    %   The cylinders specified with this method have a vertical axis.
    
    r = num2str(r);
    h = num2str(h);
    pos = cellfun(@(x) num2str(x), pos, 'UniformOutput', false);
    
    fprintf('Making Cylinder selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Cylinder radius: %s\n', r)
    fprintf('Cylinder height: %s\n', h)
    fprintf('Cylinder position:\n')
    fprintf('x = %s\n', pos{1})
    fprintf('y = %s\n', pos{2})
    fprintf('z = %s\n', pos{3})
    
    this.CM.selection.create(tag, 'Cylinder');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('r', r);
    this.CM.selection(tag).set('pos', pos);
    this.CM.selection(tag).set('top', h);
    this.CM.selection(tag).set('bottom', 0);
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
