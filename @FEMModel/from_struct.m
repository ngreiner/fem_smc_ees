function from_struct(this, struc)
    % Set the FEMModel object from the input struc.
    %
    % Usage:
    %   fem.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with same fields as the FEMModel class.
    this.name = struc.name;
    this.params = struc.params;
    this.sc = struc.sc;
    this.sc = SCModel();
    this.sc.set_dirs(this.dirs.top);
    this.sc.from_struct(struc.sc);
    this.elec = Elec();
    this.elec.sc = this.sc;
    this.elec.from_struct(struc.elec);
    this.AS = struc.AS;
end
