function build_elec(this)
    % Build the geometry features representing the electrode array.
    %
    % Usage:
    %   fem.build_elec()
    %
    % Description:
    %   There are three options:
    %       1. If paddle.flag is 'off', the electrode array only consists
    %          in a collection of active sites. These are blocks, either
    %          fully included in the fat, either in contact with the dura. 
    %       2. If paddle.flag is 'on_dura', the electrode array consists in 
    %          a paddle and the active sites, in contact with the dura.
    %       3. If paddle.flag is 'in_fat', the electrode array consists in 
    %          a paddle and the active sites, completely enclosed in the
    %          fat.

    fprintf('Building electrode array...\n')
    
    % Unpack active site parameters in convenient variables.
    AS = this.elec.AS.coords;
    nAS = this.elec.AS.count;
    length = this.elec.AS.length * 1e-3;
    width = this.elec.AS.width * 1e-3;
    thick = this.elec.AS.thick * 1e-3;
    
    % If paddle.flag is 'off': build directly active sites as Blocks.
    if strcmp(this.elec.paddle.flag, 'off')
        fprintf('paddle.flag: off. Paddle not included. Building active sites...\n')
        feats = cell(1, nAS);
        for i = 1 : nAS
            this.make_block(sprintf('as%d', i), sprintf('as%d', i), {width, thick, length}, {AS{i}{1} * 1e-3, AS{i}{2} * 1e-3, AS{i}{3} * 1e-3}, AS{i}{4}, 'center');
            feats{i} = sprintf('as%d', i);
        end
        this.make_diff('elec', 'elec', 'on', 'off', feats, {'GM', 'WM_with_roots', 'CSF', 'dura'});
        for i = 1 : nAS
            this.make_delete(sprintf('del_as%d', i), sprintf('del_AS%d', i), {sprintf('as%d', i)});
        end
        
    % If paddle.flag is 'on_dura':
    % First, we build the active sites as blocks which we crop, keeping
    % only the volume outside of the dura.
    % Second we import the paddle from which we remove every other
    % compartments.
    elseif strcmp(this.elec.paddle.flag, 'on_dura')
        fprintf('paddle.flag: on_dura. Paddle is in direct contact with the dura, with no space in between.\n')
        fprintf('Building active sites...')
        feats = cell(1, nAS);
        for i = 1 : nAS
            this.make_block(sprintf('as%d', i), sprintf('as%d', i), {width, thick, length}, {AS{i}{1} * 1e-3, AS{i}{2} * 1e-3, AS{i}{3} * 1e-3}, AS{i}{4}, 'center');
            feats{i} = sprintf('as%d', i);
        end
        this.make_diff('elec', 'elec', 'on', 'off', feats, {'GM', 'WM_with_roots', 'CSF', 'dura'});
        for i = 1 : nAS
            this.make_delete(sprintf('del_as%d', i), sprintf('del_as%d', i), {sprintf('as%d', i)});
        end
        fprintf('Building paddle...\n')
        if strcmp(this.params.elec.CAD_file_format, 'step')
            this.make_import('tmp_paddle', 'tmp_paddle', fullfile(this.dirs.step, 'paddle.step'), 'type', 'cad');
        elseif strcmp(this.params.elec.CAD_file_format, 'comsol')
            this.make_import('tmp_paddle', 'tmp_paddle', fullfile(this.dirs.comsol_CAD, 'paddle.mphbin'), 'type', 'native');
        end
        this.make_diff('paddle', 'paddle', 'on', 'off', {'tmp_paddle'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec'});
        this.make_delete('del_paddle', 'del_paddle', {'tmp_paddle'});
        
    % Otherwise, paddle.flag is 'in_fat'. There is a space filled with fat
    % between the dura and the paddle.
    % First we import the paddle.
    % Second we build the active sites as blocks which we intersect with
    % the paddle so as to keep only the volume inside the paddle.
    % Finally the paddle is edited to remove from it the volumes of the 
    % active sites.
    else
        fprintf('paddle.flag: in_fat. Paddle is embedded in the fat, spaced from the dura.\n')
        fprintf('Importing paddle...\n')
        if strcmp(this.params.elec.CAD_file_format, 'step')
            this.make_import('tmp_paddle', 'tmp_paddle', fullfile(this.dirs.step, 'paddle.step'), 'type', 'cad');
        elseif strcmp(this.params.elec.CAD_file_format, 'comsol')
            this.make_import('tmp_paddle', 'tmp_paddle', fullfile(this.dirs.comsol_CAD, 'paddle.mphbin'), 'type', 'native');
        end
        fprintf('Building active sites...\n')
        feats = cell(1, nAS);
        for i = 1 : nAS
            this.make_block(sprintf('as%d', i), sprintf('as%d', i), {width, thick, length}, {AS{i}{1} * 1e-3, AS{i}{2} * 1e-3, AS{i}{3} * 1e-3}, AS{i}{4}, 'center');
            this.make_inter(sprintf('as%d', i), sprintf('as%d', i), 'on', 'off', {sprintf('tmp_as%d', i), 'tmp_paddle'});
            this.make_delete(sprintf('del_as%d', i), sprintf('as%d', i), {sprintf('tmp_as%d', i)});
            feats{i} = sprintf('as%d', i);
        end
        this.make_union('elec', 'elec', 'off', 'off', feats);
        fprintf('Editing paddle...')
        this.make_diff('paddle', 'paddle', 'on', 'off', {'tmp_paddle'}, {'elec'});
        this.make_delete('del_paddle', 'del_paddle', {'tmp_paddle'});
    end
    
end
