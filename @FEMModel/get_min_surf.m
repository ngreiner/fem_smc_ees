function min_surf = get_min_surf(this, tag, quantity, sol)
    % Return the min of a quantity over a surface.
    %
    % Usage:
    %   min_surf = fem.get_min_surf(tag, quantity, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the surface over which the min should be computed.
    %
    %   quantity : string :
    %       Identifier of the quantity whose min should be computed.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    
    fprintf('Making MinSurf: %s ...\n', tag)
    
    % Make table to store numerical result.
    this.make_table(sprintf('tbl_min_%s_%s', tag, sol));
    
    % Make dataset.
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    % Make MinSurf.
    num_tag = sprintf('min_%s_%s', tag, sol);
    num_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(num_tag, num_tags))
        this.CM.result.numerical.create(num_tag, 'MinSurface');
    end
    this.CM.result.numerical(num_tag).set('data', sprintf('dset_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).selection.named(tag);
    
    % Attach MinSurf to table.
    this.CM.result.numerical(num_tag).set('table', sprintf('tbl_min_%s_%s', tag, sol));
    this.CM.result.numerical(num_tag).set('expr', {quantity});
    this.CM.result.numerical(num_tag).setResult;
    
    fprintf('... Done.\n')
    
    % Fetch value.
    min_surf = this.CM.result.numerical(num_tag).getReal();

end
