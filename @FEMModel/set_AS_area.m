function set_AS_area(this)
	% Set the attribute AS.area.
    %
    % Usage:
    %   fem.set_AS_area()
    this.AS.area = this.get_AS_area();
end
