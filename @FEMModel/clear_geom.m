function clear_geom(this)
	% Clear the Geometry node of CM.
	%
	% Usage:
    %   fem.clear_geom()
	%
	% Description:
    %   Clears the geometry for a fresh new start.
    this.CM.geom.clear()
end
