function build_plots(this)
    % Build plots for visualizing results in CM.
    %
    % Usage:
    %   fem.build_plots()
    %
    % Description:
    %   The plots are the followings :
    %   
    %       - several slice-plots displaying the electric potential
    %         distribution together with the current densities (both along
    %         and orthogonal to the plane of the slice) at various
    %         z-coordinates; 
    %
    %       - 1 3D-plot displaying the streamlines of the curvilinear
    %         coordinates system in the roots.
    %
    %   In the process, the method also defines 2 views and several
    %   cut-planes.

    fprintf('Building Plots...\n')
    
    % Unpack attributes in convenient variables.
    rows = this.elec.AS.rows;

    % Make views.
    view_tags = cell(this.CM.view.tags);

    % View 1: hide saline domains and hide boundaries at the entrance
    % of the roots in the WM.
    view1_tags = cell(this.CM.view('view1').hideEntities.tags);
    if ~ any(strcmp('hide1', view1_tags));
        this.CM.view('view1').hideEntities.create('hide1');
        this.CM.view('view1').hideEntities('hide1').geom(3);
        this.CM.view('view1').hideEntities('hide1').named('saline');
    end

    % View 2: keep only WM and roots as domains and hide boundaries at
    % the entrance of the roots in the WM.
    if ~ any(strcmp('view2', view_tags));
        this.CM.view().create('view2', 'geom1');
        this.CM.view('view2').hideEntities.create('hide1');
        this.CM.view('view2').hideEntities('hide1').geom(3);
        this.CM.view('view2').hideEntities('hide1').named('all_but_WM_with_roots');
    end

    % View 3: all unhidden.
    if ~ any(strcmp('view3', view_tags));
        this.CM.view().create('view3', 'geom1');
    end

    % Make cutplanes datasets for results visualization.
    dset_tags = cell(this.CM.result.dataset.tags);
    zCP = [0; cellfun(@(x) x.z, rows); this.sc.morpho.seg_length] * 1e-3;
    for i = 1 : size(zCP, 1)
        if ~ any(strcmp(sprintf('cpl_V_slice%d', i), dset_tags));
            this.CM.result.dataset.create(sprintf('cpl_V_slice%d', i), 'CutPlane');
            this.CM.result.dataset(sprintf('cpl_V_slice%d', i)).set('quickplane', 'xy');
            this.CM.result.dataset(sprintf('cpl_V_slice%d', i)).set('quickz', sprintf('%f', zCP(i)));
            this.CM.result.dataset(sprintf('cpl_V_slice%d', i)).set('data', 'dset2');
        end
    end

    % Make Potential-Current slice plots.
    plot_tags = cell(this.CM.result.tags);
    for i = 1 : size(zCP, 1)
        if ~ any(strcmp(sprintf('V_slice_%d', i), plot_tags));
            this.CM.result.create(sprintf('V_slice_%d', i), 'PlotGroup3D');
            this.CM.result(sprintf('V_slice_%d', i)).label(sprintf('V_slice_%d', i));
            this.CM.result(sprintf('V_slice_%d', i)).set('data', sprintf('cpl_V_slice%d', i));
            this.CM.result(sprintf('V_slice_%d', i)).set('view', 'view1');
            this.CM.result(sprintf('V_slice_%d', i)).create('surf1', 'Surface');
            this.CM.result(sprintf('V_slice_%d', i)).feature('surf1').set('colortable', 'RainbowLight');
            this.CM.result(sprintf('V_slice_%d', i)).feature('surf1').set('expr', 'V');
            this.CM.result(sprintf('V_slice_%d', i)).create('arwv1', 'ArrowVolume');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('color', 'red');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('arrowlength', 'logarithmic');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('expr', {'ec.Jx' 'ec.Jy' '0'});
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('descr', 'Current density');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('xnumber', '64');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('ynumber', '64');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv1').set('znumber', '1');
            this.CM.result(sprintf('V_slice_%d', i)).create('arwv2', 'ArrowVolume');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('color', 'yellow');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('arrowlength', 'logarithmic');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('expr', {'0' '0' 'ec.Jz'});
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('descr', 'Current density');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('xnumber', '64');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('ynumber', '64');
            this.CM.result(sprintf('V_slice_%d', i)).feature('arwv2').set('znumber', '1');
        end
    end

    % Make curvilinear coordinates plot.
    if ~ any(strcmp('vector_field', plot_tags));
        this.CM.result.create('vector_field', 'PlotGroup3D');
        this.CM.result('vector_field').label('Vector_Field');
        this.CM.result('vector_field').set('data', 'dset1');
        this.CM.result('vector_field').set('view', 'view2');
        this.CM.result('vector_field').create('str1', 'Streamline');
        this.CM.result('vector_field').feature('str1').set('posmethod', 'selection');
        this.CM.result('vector_field').feature('str1').selection.named('outlet_cc');
        this.CM.result('vector_field').feature('str1').set('tuberadiusscaleactive', true);
        this.CM.result('vector_field').feature('str1').set('tuberadiusscale', 1);
        this.CM.result('vector_field').feature('str1').set('radiusexpr', '0.00005');
    end
    
    fprintf('Plots are built.\n\n')
    
end
