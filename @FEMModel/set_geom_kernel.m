function set_geom_kernel(this)
	% Set the geometry kernel used by comsol.
	% 
	% Usage:
    %   fem.set_geom_kernel()
	%
	% Description:
    %   The geometry kernel used by Comsol is switched to 'comsol' if
    %   possible, that is, if no import of CAD files other than .mphbin is
    %   required.
    
    bool = strcmp(this.params.sc.CAD_file_format, 'comsol');
    bool = bool && strcmp(this.params.roots.CAD_file_format, 'comsol');
    bool = bool && strcmp(this.params.elec.CAD_file_format, 'comsol');
    bool = bool && strcmp(this.params.bone.CAD_file_format, 'comsol');
    
    if bool
        this.CM.geom('geom1').geomRep('comsol');
    end
    
end
