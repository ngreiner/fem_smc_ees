function make_union(this, tag, label, keep, intbnd, input)
	% Build a Union feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_union(tag, label, keep, intbnd, input)
    %
    % Parameters:
    %   tag : string :
    %       Tag of feature.
    %
    %	label : string :
    %       Label of feature.
    %
    %	keep : string :
    %       'on' or 'off': indicates wether to keep input objects.
    %
    %   intbnd : string :
    %       'on' or 'off': indicates wether to keep interior
    %       boundaries.
    %
    %	input : 1*N cell of strings :
    %       Tags of objects to unify.
    %
    % Description:
    %   If keep is 'off', all the objects of input1 and input2 are
    %   deleted once the difference is constructed (equivalent to a Delete
    %   feature).
    %   If intbnd is 'off', the internal boundaries between two input
    %   objects remaining in the unified object are deleted.
    
    fprintf('Making ''%s'' Union feature...\n', tag)
    fprintf('Unified objects: ')
    cellfun(@(x) fprintf('%s, ', x), input)    
    fprintf('\nKeeping input objects: %s\n', keep)
    fprintf('Keeping interior boundaries: %s\n', intbnd)
    
    this.CM.geom('geom1').create(tag, 'Union');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('keep', keep);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('intbnd', intbnd);
    this.CM.geom('geom1').feature(tag).selection('input').set(input);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('... Done.\n\n')
    
end
