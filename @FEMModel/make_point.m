function make_point(this, tag, label, pos)
	% Create a Point feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_point(tag, label, pos)
    %
    % Parameters:
    %   tag     : string        : tag of feature.
    %	label   : string        : label of feature.
    %	pos     : 1*3 double    : point coordinates.
    
    fprintf('Making ''%s'' Point feature...\n', tag)
    fprintf('Point coordinates:\n')
    fprintf('x: %f\n', pos(1))
    fprintf('y: %f\n', pos(2))
    fprintf('z: %f\n', pos(3))
    
    this.CM.geom('geom1').create(tag, 'Point')
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('p', pos);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    
    fprintf('... Done.\n\n')
    
end
