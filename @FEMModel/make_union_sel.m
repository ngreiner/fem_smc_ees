function make_union_sel(this, dim, tag, input)
    % Create a union selection.
    %
    % Usage:
    %   fem.make_union_sel(dim, tag, input)
    %
    % Parameters:
    %   dim     : double : entity level indicator (3=domains, 2=boundaries,
    %                      1=edges, 0=points).
    %   tag     : string : tag of the union selection.
    %   input   : 1*N cell  : tags of selections to unify.
    
    fprintf('Making Union selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Input selections:\n')
    cellfun(@(x) fprintf('%s: %s\n', x, this.CM.selection(x).toString.toCharArray), input)
    
    this.CM.selection.create(tag, 'Union');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('input', input);
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
