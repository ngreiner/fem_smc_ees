function set_elec(this)
	% Set the attribute elec.
    %
    % Usage:
    %   fem.set_elec()
    %
    % Description:
    %   See class Elec.
	this.elec = Elec(this.sc, this.params.elec.config_file_name);
end
