function save_CM(this, file_name)
	% Save CM.
    %
    % Usage:
    %   fem.save_CM()
    %   fem.save_CM(file_name)
    %
    % Parameters:
    %   file_name : string [optional] :
    %       Name of file (.mph extension).
    %
    % Description:
    %   If no file name is provided, the name of the file is obtained
    %   by appending '_AS#i' to the FEMModel name, where #i denotes the
    %   index of the active site currently accessed by the FEMModel, ie it
    %   denotes AS.idx.
    %   The directory where the file is saved is always dirs.comsol_models.
    
    if nargin == 1
        file_path = fullfile(this.dirs.comsol_models, sprintf('%s_AS%d', this.name, this.AS.idx));
    else
        file_path = fullfile(this.dirs.comsol_models, file_name);
    end
    
    % Make comsol models directory if needed.
    if ~ exist(this.dirs.comsol_models, 'dir')
        mkdir(this.dirs.comsol_models);
    end
    
    this.CM.save(Utils.get_full_path(file_path));
    
end
