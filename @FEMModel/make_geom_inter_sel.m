function make_geom_inter_sel(this, tag, label, dim, input1)
	% Build an IntersectionSelection in the Geometry of CM.
    %
    % Usage:
    %   fem.make_geom_inter_sel(tag, label, dim, input1)
    %
    % Parameters:
    %   tag    : string : tag of feature.
    %	label  : string : label of feature.
    %	dim    : int    : dimension of intersection selection.
    %	input1 : 1*N cell of strings : tags of objects to intersect.
    
    fprintf('Making ''%s'' IntersectionSelection feature...\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Intersected objects: ')
    cellfun(@(x) fprintf('%s, ', x), input1)
    
    this.CM.geom('geom1').create(tag, 'IntersectionSelection');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('entitydim', dim);
    this.CM.geom('geom1').feature(tag).set('input', input1);
    
    fprintf('\n... Done.\n\n')
    
end  
