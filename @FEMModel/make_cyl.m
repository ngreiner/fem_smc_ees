function make_cyl(this, tag, label, r, h, pos)
	% Create a Cylinder feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_cyl(tag, label, r, h, pos)
    %
    % Parameters:
    %   tag     : string        : Comsol tag of Cylinder feature.
    %	label   : string        : Comsol label of Cylinder feature.
    %	r       : double/string : radius of cylinder.
    %	h       : double/string : height of cylinder.
    %   pos     : 1*3 cell      : coordinates of cylinder's base point.
    %
    % Description:
    %   The cylinders constructed with this method have a vertical axis.
    
    r = num2str(r);
    h = num2str(h);
    pos = cellfun(@(x) num2str(x), pos, 'UniformOutput', false);
    
    fprintf('Making ''%s'' Cylinder feature...\n', tag)
    fprintf('Cylinder radius: %s\n', r)
    fprintf('Cylinder height: %s\n', h)
    fprintf('Cylinder position:\n')
    fprintf('x = %s\n', pos{1})
    fprintf('y = %s\n', pos{2})
    fprintf('z = %s\n', pos{3})
    
    this.CM.geom('geom1').create(tag, 'Cylinder');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('r', r);
    this.CM.geom('geom1').feature(tag).set('h', h);
    this.CM.geom('geom1').feature(tag).set('pos', pos);
    
    fprintf('... Done.\n\n')
    
end
