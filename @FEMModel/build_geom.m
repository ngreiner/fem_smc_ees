function build_geom(this, varargin)
	% Build the geometry of the comsol model CM.
	% 
	% Usage:
    %   fem.build_geom()
    %   fem.build_geom('nosaline')
    %   fem.build_geom('norun')
	%
	% Description:
    %   This geometry is built as follows:
	%       1. Build GM;
	%       2. Build WM_with_roots;
	%       3. Build CSF;
    %       4. Build dura;
    %       5. Build elec;
    %       6. Build bone and fat;
	%       7. Build saline;
	%       8. Build ground point.
    %   The option 'nosaline' bypasses the construction of the saline.
    %   The option 'norun' prevents to run the Geom node at the end of the
    %   construction process.

	fprintf('Building Geometry...\n')

	% Create geometry node.
    this.build_geom_node();
    
    % Build GM.
    this.build_GM();
    
    % Build WM_with_roots.
    this.build_WM_with_roots();
    
    % Build CSF.
    this.build_CSF();
    
    % Build dura.
    this.build_dura();
    
    % Build elec.
    this.build_elec();
    
    % Build bone and fat.
    this.build_bone_fat();
    
    % Build saline.
    if any(strcmp('nosaline', varargin))
        fprintf('''nosaline'' specified in varargin. I don''t build it.\n\n')
    else
        this.build_saline();
    end
    
    % Build ground point in saline.
    this.build_ground_point();
    
    % Set geom kernel to 'comsol' if possible.
    this.set_geom_kernel();    
    
    % Build the geometry.
    if any(strcmp('norun', varargin))
        fprintf('''norun'' specified in varargin. I don''t run geom node.\n\n')
    else
        this.run_geom_node();
    end

    fprintf('Geometry: built with no errors.\n\n')
    
end
