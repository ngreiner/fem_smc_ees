function build_mesh(this)
	% Build the Mesh node of CM.
	%
	% IUsage:
    %   fem.build_mesh()
	%
	% Description:
    %   The implemented mesh is overall composed of either one or three
    %   parts, depending upon the option chosen for the saline. 
	%
    %   If the saline is in three parts, so is the mesh and:
	%       - The central part is a tetrahedric mesh where:
	%           * the GM subpart is 'Extra Fine';
	%           * the WM and roots subpart  is 'Extra Fine';
	%           * the CSF subpart is 'Extra Fine';
	%           * the dura subpart is 'Extremely Fine';
	%           * the elec subpart is 'Extremely Fine';
	%           * the paddle subpart is 'Extra Fine';
	%           * the fat subpart is 'Extra Fine';
	%           * the bone subpart is 'Finer';
	%           * the saline is 'Fine'.
	%
	%       - The upper (resp. lower) part can be either:
    %           * a swept mesh obtained by extrusion of the meshing pattern
    %             sketched on the upper (resp. lower) surface of the
    %             central part 
    %           * a free-tetrahedric mesh
    %
    %   If the saline is in one part, so is the mesh, built exactly as the
    %   central part of the three-parts mesh.

	fprintf('Building Mesh...\n')
    
    % Create Mesh node.
    this.CM.mesh.create('mesh1', 'geom1');
    
    % Create WM_with_roots mesh.
    this.CM.mesh('mesh1').create('WM_with_roots', 'FreeTet');
    this.CM.mesh('mesh1').feature('WM_with_roots').selection.geom('geom1', 3);
    this.CM.mesh('mesh1').feature('WM_with_roots').selection.named('WM_with_roots');
    this.CM.mesh('mesh1').feature('WM_with_roots').create('size1', 'Size');
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('custom', 'on');
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('hmax', this.params.mesh.WM_with_roots.hmax);
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('hmin', this.params.mesh.WM_with_roots.hmin);
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('hgrad', this.params.mesh.WM_with_roots.hgrad);
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('hcurve', this.params.mesh.WM_with_roots.hcurve);
    this.CM.mesh('mesh1').feature('WM_with_roots').feature('size1').set('hnarrow', this.params.mesh.WM_with_roots.hnarrow);
    
    % Create elec mesh.
    this.CM.mesh('mesh1').create('elec', 'FreeTet');
    this.CM.mesh('mesh1').feature('elec').selection.geom('geom1', 3);
    this.CM.mesh('mesh1').feature('elec').selection.named('elec');
    this.CM.mesh('mesh1').feature('elec').create('size1', 'Size');
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('custom', 'on');
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('hmax', this.params.mesh.elec.hmax);
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('hmin', this.params.mesh.elec.hmin);
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('hgrad', this.params.mesh.elec.hgrad);
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('hcurve', this.params.mesh.elec.hcurve);
    this.CM.mesh('mesh1').feature('elec').feature('size1').set('hnarrow', this.params.mesh.elec.hnarrow);
    
    % Create remaining geometry mesh.
    this.CM.mesh('mesh1').create('remaining', 'FreeTet');
    this.CM.mesh('mesh1').feature('remaining').create('size1', 'Size');
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('custom', 'on');
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('hmax', this.params.mesh.remaining.hmax);
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('hmin', this.params.mesh.remaining.hmin);
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('hgrad', this.params.mesh.remaining.hgrad);
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('hcurve', this.params.mesh.remaining.hcurve);
    this.CM.mesh('mesh1').feature('remaining').feature('size1').set('hnarrow', this.params.mesh.remaining.hnarrow);

    fprintf('Mesh is built.\n\n')
    
end
