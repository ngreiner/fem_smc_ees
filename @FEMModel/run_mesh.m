function run_mesh(this)
	% Run the Mesh node of CM.
    %
    % Usage:
    %   fem.run_mesh()
	this.CM.mesh('mesh1').run;
end
