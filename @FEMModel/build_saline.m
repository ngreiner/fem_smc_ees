function build_saline(this)
	% Build the geometry features representing the saline.
	%
	% Usage:
    %   fem.build_saline()
	%
	% Description:
    %   The Saline can be constructed in three ways:
    %       a. 3 filled contiguous cylinders
    %       b. 3 hollow contiguous cylinders
    %       c. 1 filled cylinder
    %   For the options a. and c., 'filled' means that the other geometry
    %   compartments are not removed by boolean difference from the
    %   constructed cylinders.
    %   For option b., 'hollow' means the exact opposite.
    %   The selected option is imposed by this.params.saline.repr.

    fprintf('Building Saline...\n')

    % Build saline cylinders.
    switch this.params.saline.repr
        
        % Filled cylinders.
        case 'multiple_filled_cylinder'
            fprintf('saline.repr = multiple_filled_cylinder\n')
            for i = 1 : 3
                this.make_cyl(sprintf('part%d', i), sprintf('part%d', i), this.params.saline.radius, this.params.geom.z_parts(i + 1) - this.params.geom.z_parts(i), {0, 0, this.params.geom.z_parts(i)});
            end
            
        % Hollow cylinders.
        case 'multiple_hollow_cylinders'
            fprintf('saline.repr = multiple_hollow_cylinders\n')
            for i = 1 : 3
                this.make_cyl(sprintf('tmp_part%d', i), sprintf('tmp_part%d', i), this.params.saline.radius, this.params.geom.z_parts(i + 1) - this.params.geom.z_parts(i), {0, 0, this.params.geom.z_parts(i)});
                if ~ strcmp(this.elec.paddle.flag, 'off')
                    this.make_diff(sprintf('part%d', i), sprintf('part%d', i), 'on', 'off', {sprintf('part%d', i)}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle', 'fat', 'bone'});
                else
                    this.make_diff(sprintf('part%d', i), sprintf('part%d', i), 'on', 'off', {sprintf('part%d', i)}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'fat', 'bone'});
                end
                this.make_delete(sprintf('del_part%d', i), sprintf('del_part%d', i), {sprintf('tmp_part%d', i)});
            end
            
        % Individual single filled cylinder.
        case 'single_filled_cylinder'
            fprintf('saline.repr = single_filled_cylinders\n')
            this.make_cyl('part1', 'part1', this.params.saline.radius, this.params.geom.z_parts(4) - this.params.geom.z_parts(1), {0, 0, this.params.geom.z_parts(1)});
    end

end
