function build(this)
	% Build the FEMModel object.
    %
    % Usage:
    %   fem.build()
    %
    % Description:
    %   1. Builds the spinal cord model of the FEMModel.
    %   2. Builds the electrode array of the FEMModel.
    %   3. Configures various geometrical parameters.
    this.set_sc();
    this.set_elec();
    this.set_dims();
end
