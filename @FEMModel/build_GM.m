function build_GM(this)
    % Build the geometry feature representing the grey matter.
    %
    % Usage:
    %   fem.build_GM()
    %
    % Description:
    %   Imports the appropriate CAD file, representing the grey matter.
    
    fprintf('Building GM...\n')
    
    if strcmp(this.params.sc.CAD_file_format, 'step')
        this.make_import('GM', 'GM', fullfile(this.dirs.step, 'GM.step'), 'type', 'cad');
        
    elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
        this.make_import('GM', 'GM', fullfile(this.dirs.comsol_CAD, 'GM.mphbin'), 'type', 'native');
        
    end
    
end
