function clear_sel(this)
	% Clear the Selection node of CM.
	%
	% Usage:
    %   fem.clear_sel()
	%
	% Description:
    %   Clears the Selection node for a fresh new start.
    this.CM.selection.clear();
end
