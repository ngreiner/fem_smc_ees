function make_block(this, tag, label, dims, pos, rot, base)
	% Create a Block feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_block(tag, label, dims, pos, rot, base)
    %
    % Parameters:
    %   tag     : string    : Comsol tag of feature.
    %	label   : string    : Comsol label of feature.
    %	dims    : 1*3 cell  : dimensions of block.
    %   pos     : 1*3 cell  : coordinates of block's base point.
    %	rot     : double/string : rotation angle of block.
    %	base    : string    : 'center' or 'corner': indicates the base
    %                         point type.
    %
    % Description:
    %   Dimensions and coordinates (pos) should be of numerical type:
    %   they are converted to string within the course of the method
    %   execution.
    %   The rotation takes place around the vertical axis passing through
    %   the block center (only tested with base='center').
    
    dims = cellfun(@(x) num2str(x), dims, 'UniformOutput', false);
    pos = cellfun(@(x) num2str(x), pos, 'UniformOutput', false);
    rot = num2str(rot);
    
    fprintf('Making ''%s'' Block feature...\n', tag)
    
    fprintf('Block dimensions: ')
    cellfun(@(x) fprintf('%s, ', x), dims)
    fprintf('Block position: ')
    cellfun(@(x) fprintf('%s, ', x), pos)
    fprintf('\nBlock rotation angle: %s\n', rot)
    fprintf('Block base point type: %s', base)
    
    this.CM.geom('geom1').create(tag,'Block');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('size', dims);
    this.CM.geom('geom1').feature(tag).set('pos', pos);
    this.CM.geom('geom1').feature(tag).set('rot', rot);
    this.CM.geom('geom1').feature(tag).set('base', base);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('... Done.\n\n')
    
end
