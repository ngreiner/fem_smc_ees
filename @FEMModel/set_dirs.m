function set_dirs(this, topdir)
	% Set the attribute dirs.
    %
    % Usage:
    %   fem.set_dirs(topdir)
    %
    % Parameters:
    %   topdir : string : path to top-level directory.
    %
    % Description:
    %   The directories are:
    %       top           : top-level directory.
    %       comsol_models : to read and write models.
    %       resources     : lots of resources there.
    %       CAD           : regroups the CAD resources.
    %       stl           : directory for .stl files.
    %       comsol_CAD    : directory for .mphbin files.
    %       step          : directory for .step files.
    this.dirs.top = topdir;
    this.dirs.comsol_models = fullfile(this.dirs.top, 'comsol_models');
    this.dirs.resources = fullfile(this.dirs.top, 'resources');
    this.dirs.CAD = fullfile(this.dirs.resources, 'CAD');
    this.dirs.stl = fullfile(this.dirs.CAD, 'stl');
    this.dirs.comsol_CAD = fullfile(this.dirs.CAD, 'comsol');
    this.dirs.step = fullfile(this.dirs.CAD, 'step');
end
