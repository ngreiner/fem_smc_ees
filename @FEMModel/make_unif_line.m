function make_unif_line(this, tag, radius, color, varargin)
	% Create a uniformly colored line in CM.
    %
    % Usage:
    %   fem.make_unif_line(tag, radius, color)
    %   fem.make_unif_line(tag, radius, color, 'pg', pg)
    %
    % Parameters:
    %   tag : str :
    %       Tag of an edge selection.
    %
    %   radius : double :
    %       Radius of the tube materializing the line.
    %
    %   color : string or 1*3 cell :
    %       Color name or RGB-triplet.
    %
    %   pg : string :
    %       Tag of a plot group.
    %
    % Description:
    %   If pg is not specified, the colored line is created in plot group
    %   'pg1'.
    
    idx = find(strcmp('pg', varargin));
    if isempty(idx)
        pg = 'pg1';
    else
        pg = varargin{idx + 1};
    end
    
    pg_tags = cell(this.CM.result.tags);
    if ~ any(strcmp(pg, pg_tags))
        this.CM.result.create(pg, 'PlotGroup3D');
        this.CM.result(pg).set('edges', 'off');
    end
    
    this.make_dataset(sprintf('dset_%s', tag), tag);
    this.CM.result(pg).create(sprintf('line_%s', tag), 'Line');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('data', sprintf('dset_%s', tag));
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('linetype', 'tube');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('radiusexpr', sprintf('%f', radius));
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('tuberadiusscaleactive', 'on');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('tuberadiusscale', '1');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('descr', '1');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('expr', '1');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('unit', '1');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('coloring', 'uniform');
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('customcolor', color);
    this.CM.result(pg).feature(sprintf('line_%s', tag)).set('resolution', 'normal');
    
end
