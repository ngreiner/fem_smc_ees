function parse_config_file(this, file_path)
	% Parse the configuration file at file_path.
    %
    % Usage:
    %   fem.parse_config_file(file_path)
    %
    % Parameters:
    %   file_path : string : path to configuration file.
    
    % Open file.
   if exist(file_path, 'file')
        fileID = fopen(file_path);
    else
        error('%s: file not found.', file_path);
    end
    
    % Read file lines.
    C = textscan(fileID, '%s', 'Delimiter', '\n');
    C = C{1};
    
    % Close file.
    fclose(fileID);
    
    % Parse config.
    while ~ isempty(C)
        
        if isempty(C{1}) || strcmp(C{1}(1), '#')
            C(1) = [];
            continue
        end
        
        words = strsplit(C{1});
        
        switch words{1}
            
            case 'name'
                this.name = words{3};
                
            case 'AS_idx'
                this.AS.idx = str2double(words{3});

            case 'sc_config_file_name'
                this.params.sc.config_file_name = words{3};

            case 'sc_scale'
                this.params.sc.scale = strcmp(words{3}, 'true');

            case 'sc_scale_factors'
                try
                    this.params.sc.scale_factors = [str2double(words{3}), str2double(words{4}), str2double(words{5})];
                catch
                    this.params.sc.scale_factors = [];
                end

            case 'sc_CAD_file_format'
                this.params.sc.CAD_file_format = words{3};
            
            case 'roots_repr'
                this.params.roots.repr = words{3};
                
            case 'roots_CAD_file_format'
                if ~ strcmp(this.params.roots.repr, 'none')
                    this.params.roots.CAD_file_format = words{3};
                end
                
            case 'roots_CAD_file_dorsal_suffix'
                if regexp(this.params.roots.repr, 'dorsal')
                    this.params.roots.CAD_file_dorsal_suffix = words{3};
                end
                
            case 'roots_CAD_file_ventral_suffix'
                if regexp(this.params.roots.repr, 'ventral')
                    this.params.roots.CAD_file_ventral_suffix = words{3};
                end
                
            case 'roots_CAD_file_left_suffix'
                if regexp(this.params.roots.repr, 'left')
                    this.params.roots.CAD_file_left_suffix = words{3};
                end
                
            case 'roots_CAD_file_right_suffix'
                if regexp(this.params.roots.repr, 'right')
                    this.params.roots.CAD_file_right_suffix = words{3};
                end
                
            case 'roots_mirror'
                if ~ strcmp(this.params.roots.repr, 'none')
                    this.params.roots.mirror = words{3};
                end
                
            case 'roots_make_diff'
                if ~strcmp(this.params.roots.repr, 'none')
                    this.params.roots.make_diff = words{3};
                end
                
            case 'bone_repr'
                this.params.bone.repr = words{3};
                
            case 'bone_comsol_CAD_file_name'
                this.params.bone.comsol_CAD_file_name = words{3};
                
            case 'bone_CAD_file_format'
                this.params.bone.CAD_file_format = words{3};
                
            case 'bone_nvertebrae'
                if strcmp(this.params.bone.repr, 'multiple_vertebrae_files')
                    this.params.bone.nvertebrae = str2double(words{3});
                end
                
            case 'bone_vertebrae'
                if strcmp(this.params.bone.repr, 'multiple_vertebrae_files')
                    this.params.bone.vertebrae = words(3 : end)';
                end
                
            case 'bone_radius'
                if strcmp(words{3}, {'small', 'medium', 'large'})
                    this.params.bone.radius = words{3};
                else
                    this.params.bone.radius = str2double(words{3});
                end
                
            case 'cond_file_name'
                this.parse_cond_file(words{3});
                
            case 'mesh_fineness_type'
                this.params.mesh.fineness_type = words{3};
                
            case 'mesh_fineness'
                this.params.mesh.fineness = words{3};
                
            case 'mesh_type_exts'
                this.params.mesh.type_exts = words{3};
                
            case 'mesh_WM_with_roots_hmax'
                this.params.mesh.WM_with_roots.hmax = words{3};
                
            case 'mesh_WM_with_roots_hmin'
                this.params.mesh.WM_with_roots.hmin = words{3};
                
            case 'mesh_WM_with_roots_hgrad'
                this.params.mesh.WM_with_roots.hgrad = words{3};
                
            case 'mesh_WM_with_roots_hcurve'
                this.params.mesh.WM_with_roots.hcurve = words{3};
                
            case 'mesh_WM_with_roots_hnarrow'
                this.params.mesh.WM_with_roots.hnarrow = words{3};
                
            case 'mesh_elec_hmax'
                this.params.mesh.elec.hmax = words{3};
                
            case 'mesh_elec_hmin'
                this.params.mesh.elec.hmin = words{3};
                
            case 'mesh_elec_hgrad'
                this.params.mesh.elec.hgrad = words{3};
                
            case 'mesh_elec_hcurve'
                this.params.mesh.elec.hcurve = words{3};
                
            case 'mesh_elec_hnarrow'
                this.params.mesh.elec.hnarrow = words{3};
                
            case 'mesh_remaining_hmax'
                this.params.mesh.remaining.hmax = words{3};
                
            case 'mesh_remaining_hmin'
                this.params.mesh.remaining.hmin = words{3};
                
            case 'mesh_remaining_hgrad'
                this.params.mesh.remaining.hgrad = words{3};
                
            case 'mesh_remaining_hcurve'
                this.params.mesh.remaining.hcurve = words{3};
                
            case 'mesh_remaining_hnarrow'
                this.params.mesh.remaining.hnarrow = words{3};

            case 'elec_config_file_name'
                this.params.elec.config_file_name = words{3};

            case 'elec_CAD_file_format'
                this.params.elec.CAD_file_format = words{3};
                
            case 'saline_repr'
                this.params.saline.repr = words{3};
                
            case 'saline_size'
                this.params.saline.size = words{3};
                
            case 'physics_with_curvi_coords'
                this.params.physics.with_curvi_coords = strcmp(words{3}, 'true');
                
            case 'physics_stim_type'
                this.params.physics.stim_type = words{3};
                
            case 'physics_border_condition'
                this.params.physics.border_condition = words{3};
                
            case 'physics_with_ground_point'
                this.params.physics.with_ground_point = strcmp(words{3}, 'true');

        end
        
        C(1) = [];

    end
    
end
