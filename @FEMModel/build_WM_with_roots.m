function build_WM_with_roots(this)
    % Build the geometry feature representing the WM and the roots.
    %
    % Usage:
    %   fem.build_WM_with_roots()
    %
    % Description:
    %   A single geometry feature (resulting in a single calculation
    %   domain) is built to represent both the white matter and the spinal
    %   roots.
    %   The white matter and the spinal roots are nonetheless imported from
    %   separate .step or .mphbin files.
    %   The attribute params.root.repr specifies how to build this feature.

    fprintf('Building WM_with_roots...\n')

    % Build WM.
    if strcmp(this.params.sc.CAD_file_format, 'step')
        this.make_import('WM', 'WM', fullfile(this.dirs.step, 'WM.step'), 'type', 'cad');
        
    elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
        this.make_import('WM', 'WM', fullfile(this.dirs.comsol_CAD, 'WM.mphbin'), 'type', 'native');
        
    end

    % Build Roots.
    switch this.params.roots.repr

        % If 'none', exit method.
        case 'none'
            fprintf('Roots are disabled. Returned without imports.\n')

        % If 'dorsal_ventral':
        % We import successively the dorsal and ventral roots, taking care
        % to remove any overlapping between them, and between them and the
        % WM. Any boundary that is shared by more than two roots is labeled
        % for later definition of an electric insulation.
        case 'dorsal_ventral'
            fprintf('roots.repr: dorsal_ventral.\n')
            
            % Import dorsal roots.
            root_feats = cell(1, this.sc.morpho.nroots);
            if strcmp(this.params.roots.CAD_file_format, 'step')
                this.make_import('tmp_dr1', 'tmp_dr1', fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{1}.name, this.params.roots.CAD_file_dorsal_suffix)), 'type', 'cad');
            elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                this.make_import('tmp_dr1', 'tmp_dr1', fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{1}.name, this.params.roots.CAD_file_dorsal_suffix)), 'type', 'native');
            end
            this.make_diff('dr1', 'dr1', 'on', 'off', {'tmp_dr1'}, {'WM'});
            root_feats{1} = 'dr1';
            for j = 2 : this.sc.morpho.nroots
                if strcmp(this.params.roots.CAD_file_format, 'step')
                    this.make_import(sprintf('tmp_dr%d', j), sprintf('tmp_dr%d', j), fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{j}.name, this.params.roots.CAD_file_dorsal_suffix)), 'type', 'cad');
                elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                    this.make_import(sprintf('tmp_dr%d', j), sprintf('tmp_dr%d', j), fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{j}.name, this.params.CAD_file_roots.dorsal_suffix)), 'type', 'native');
                end
                this.make_diff(sprintf('dr%d', j), sprintf('dr%d', j), 'on', 'off', {sprintf('tmp_dr%d', j)}, {'WM', sprintf('dr%d', j - 1)});
                root_feats{j} = sprintf('dr%d', j);
            end
            for j = 1 : this.sc.morpho.nroots - 1 
                this.make_geom_inter_sel(sprintf('dr%d_bnd_int_dr%d', j, j + 1), sprintf('drR%d_bnd_int_dr%d', j, j + 1), 2,  {sprintf('dr%d',j) sprintf('dr%d', j + 1)});
            end
            for j = 1 : this.sc.morpho.nroots
                this.make_delete(sprintf('del_dr%d', j), sprintf('del_dr%d', j), 3, {sprintf('tmp_dr%d', j)});
            end
            this.make_union('droots', 'droots', 'off', 'off', root_feats);
            
            % Import ventral roots.
            root_feats = cell(1, this.sc.morpho.nroots);
            if strcmp(this.params.roots.CAD_file_format, 'step')
                this.make_import('tmp_vr1', 'tmp_vr1', fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{1}.name, this.params.roots.CAD_file_ventral_suffix)), 'type', 'cad');
            elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                this.make_import('tmp_vr1', 'tmp_vr1', fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{1}.name, this.params.roots.CAD_file_ventral_suffix)), 'type', 'native');
            end
            this.make_diff('vr1', 'vr1', 'on', 'off', {'tmp_vr1'}, {'WM'});
            root_feats{1} = 'vr1';
            for j = 2 : this.sc.morpho.nroots
                if strcmp(this.params.roots.CAD_file_format, 'step')
                    this.make_import(sprintf('tmp_vr%d', j), sprintf('tmp_vr%d', j), fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{j}.name, this.params.roots.CAD_file_ventral_suffix)), 'type', 'cad');
                elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                    this.make_import(sprintf('tmp_vr%d', j), sprintf('tmp_vr%d', j), fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{j}.name, this.params.roots.CAD_file_ventral_suffix)), 'type', 'native');
                end
                this.make_diff(sprintf('vr%d', j), sprintf('vr%d', j), 'on', 'off', {sprintf('tmp_vr%d', j)}, {'WM', sprintf('vr%d', j - 1)});
                root_feats{j} = sprintf('vr%d', j);
            end
            for j = 1 : this.sc.morpho.nroots - 1 
                this.make_geom_inter_sel(sprintf('vr%d_bnd_int_vr%d', j, j + 1), sprintf('vrR%d_bnd_int_vr%d', j, j + 1), 2,  {sprintf('vr%d',j) sprintf('vr%d', j + 1)});
            end
            for j = 1 : this.sc.morpho.nroots
                this.make_delete(sprintf('del_vr%d', j), sprintf('del_vr%d', j), 3, {sprintf('tmp_vr%d', j)});
            end
            this.make_union('vroots', 'vroots', 'off', 'off', root_feats);
            
            % Unify dorsal and ventral roots.
            this.make_union('roots', 'roots', 'off', 'off', {'droots', 'vroots'});

        % If 'left_right':
        % We import successively the left and right roots. If the mirror
        % option is activated, the right roots are obtained by reflexion
        % about the yz-plane from the left roots. 
        case 'left_right'
            fprintf('roots.repr: left_right.\n')
            
            % Import left roots.
            root_feats = cell(1, this.sc.morpho.nroots);
            for j = 1 : this.sc.morpho.nroots
                if strcmp(this.params.roots.CAD_file_format, 'step')
                    this.make_import(sprintf('lr%d', j), sprintf('lr%d', j), fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{j}.name, this.params.roots.CAD_file_left_suffix)), 'type', 'cad');
                elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                    this.make_import(sprintf('lr%d', j), sprintf('lr%d', j), fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{j}.name, this.params.roots.CAD_file_left_suffix)), 'type', 'native');
                end
                root_feats{j} = sprintf('lr%d', j);
            end
            this.make_union('lroots', 'lroots', 'off', 'off', root_feats);
            
            % If roots.mirror is false, import right roots.
            if ~ this.params.roots.mirror
                root_feats = cell(1, this.sc.morpho.nroots);
                for j = 1 : this.sc.morpho.nroots
                    if strcmp(this.params.roots.CAD_file_format, 'step')
                        this.make_import(sprintf('rr%d', j), sprintf('rr%d', j), fullfile(this.dirs.step, sprintf('%s%s.step', this.sc.roots{j}.name, this.params.roots.CAD_file_right_suffix)), 'type', 'cad');
                    elseif strcmp(this.params.roots.CAD_file_format, 'comsol')
                        this.make_import(sprintf('rr%d', j), sprintf('rr%d', j), fullfile(this.dirs.comsol_CAD, sprintf('%s%s.mphbin', this.sc.roots{j}.name, params.roots.CAD_file_right_suffix)), 'type', 'native');
                    end
                    root_feats{j} = sprintf('rr%d', j);
                end
                this.make_union('rroots', 'rroots', 'off', 'off', root_feats);
            
            % Otherwise, mirror left roots about yz-plane.
            else
                this.make_mirror('rroots', 'rroots', 'on', {'lroots'}, {'1' '0' '0'});
                
            end
            
            % Unify left and right roots.
            this.make_union('roots', 'roots', 'off', 'off', {'lroots', 'rroots'});
    end

    % Unify roots and WM (if roots included) while subtracting GM.
    if strcmp(this.params.roots.repr, 'none')
        this.make_diff('WM_with_roots', 'WM_with_roots', 'on', 'off', {'WM'}, {'GM'});
        this.make_delete('del_WM', 'del_WM', {'WM'});
    else
        this.make_diff('WM_with_roots', 'WM_with_roots', 'on', 'off', {'WM', 'roots'}, {'GM'});
        this.make_delete('del_WM', 'del_WM', {'WM'});
        this.make_delete('del_roots', 'del_roots', {'roots'});
    end
    
end
