function make_mat(this, tag, cond_tensor)
    % Create a material for a specific geometric feature.
    %
    % Usage:
    %   fem.make_mat(tag, cond_tensor)
    %
    % Parameters:
    %   tag         : string    : tag of a geometric feature.
    %   cond_tensor : 3*1 cell  : diagonal conductivity tensor.
    %
    % Desription:
    %   cond_tensor should be an array of doubles.
    %   The 1st element of cond_tensor should be the conductivity along
    %   the 1st-axis of the coordinate system(c.s.) used for the domains of
    %   the specified selection, the 2nd element the conductivity along the
    %   2nd-axis, and the 3rd for the 3rd.
    %   If the coordinate system to use is the default cartesian c.s.,
    %   the 1st element of cond_tensor is thus the conductivity along the
    %   x-axis. 
    %   If the c.s. is user-defined (e.g. curvilinear), the 1st element is
    %   the conductivity along the main axis of that c.s. and the other two
    %   are the conductivities along the basis vectors of the transverse
    %   planes.
    
    cond_tensor = cellfun(@(x) num2str(x), cond_tensor, 'UniformOutput', false);
    
    fprintf('Making Material feature: %s...\n', tag)
    fprintf('Conductivity along 1st-axis: %s (S/m)\n', cond_tensor{1})
    fprintf('Conductivity along 2nd-axis: %s (S/m)\n', cond_tensor{2})
    fprintf('Conductivity along 3rd-axis: %s (S/m)\n', cond_tensor{3})
    
    this.CM.material.create(tag, 'Common', 'comp1');
    this.CM.material(tag).propertyGroup('def').set('electricconductivity', {cond_tensor{1} '0' '0' '0' cond_tensor{2} '0' '0' '0' cond_tensor{3}});
    this.CM.material(tag).propertyGroup('def').set('relpermittivity', {'1' '0' '0' '0' '1' '0' '0' '0' '1'});
    this.CM.material(tag).selection.named(tag);
    
    fprintf('... Done.\n\n')
    
end
