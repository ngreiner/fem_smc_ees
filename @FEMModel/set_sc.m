function set_sc(this)
	% Set the attribute sc.
    %
    % Usage:
    %   fem.set_sc()
    %
    % Description:
    %   See class SCModel.
    
	this.sc = SCModel('topdir', this.dirs.top, 'config_file_name', this.params.sc.config_file_name);
    
    % Eventually scale SCModel.
    if this.params.sc.scale
        this.sc.scale(this.params.sc.scale_factors);
    end
    
end
