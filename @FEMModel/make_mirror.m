function make_mirror(this, tag, label, keep, input, plane)
	% Build a Mirror feature in the Geometry of CM.
    %
    % Parameters:
    %   tag     : string : tag of feature.
    %	label   : string : label of feature.
    %	keep    : string : 'on' or 'off': indicates wether to keep input
    %                                     objects.
    %	input   : 1*N cell of strings   : tags of objects to mirror.
    %	plane   : 1*3 cell of strings   : coordinates of normal vector to
    %                                     plane of reflexion.
    %
    % Description:
    %   If keep is 'off', all the input objects are deleted once the
    %   mirrored object is constructed.
    
    fprintf('Making ''%s'' Mirror feature...\n', tag)
    fprintf('Mirrored objects: ')
    cellfun(@(x) fprintf('%s, ', x), input)
    fprintf('\nKeeping input objects: %s\n', keep)
    fprintf('Normal to plane of reflexion: (%s, %s, %s).\n', plane{1}, plane{2}, plane{3})
    
    this.CM.geom('geom1').create(tag, 'Mirror');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('keep', keep);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).selection('input').set(input);
    this.CM.geom('geom1').feature(tag).set('axis', plane);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('Done.\n\n')
    
end
