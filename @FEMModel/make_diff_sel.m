function make_diff_sel(this, dim, tag, input1, input2)
    % Create a difference selection between input selections.
    %
    % Usage:
    %   fem.make_diff_sel(dim, tag, input1, input2)
    %
    % Parameters:
    %   dim     : double : entity level indicator (3=domains, 2=boundaries,
    %                      1=edges, 0=points).
    %   tag     : string : tag of the difference selection.
    %   input1  : 1*N cell  : tags of selections to add.
    %   input2  : 1*M cell  : tags of selections to remove.
    
    fprintf('Making Difference selection: %s\n', tag)
    fprintf('Dimension: %d\n', dim)
    fprintf('Input selections to add:\n')
    cellfun(@(x) fprintf('%s: %s\n', x, this.CM.selection(x).toString.toCharArray), input1)
    fprintf('Input selections to remove:\n')
    cellfun(@(x) fprintf('%s: %s\n', x, this.CM.selection(x).toString.toCharArray), input2)
    
    this.CM.selection.create(tag, 'Difference');
    this.CM.selection(tag).label(tag);
    this.CM.selection(tag).set('entitydim', dim);
    this.CM.selection(tag).set('add', input1);
    this.CM.selection(tag).set('subtract', input2);
    
    fprintf('Resulting selection:\n')
    fprintf('%s: %s\n\n', tag, this.CM.selection(tag).toString.toCharArray)
    
end
