function clear_mesh(this)
	% Clear the Mesh node of CM.
	%
	% Usage:
    %   fem.clear_mesh()
	%
	% Description:
    %   Clears the Mesh node for a fresh new start.
    this.CM.mesh.clear()
end
