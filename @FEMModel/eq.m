function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".

    outbool = false;
    
    if ~ strcmp(obj1.name, obj2.name)
        fprintf('Attributes name are different.\n')
        return
    end
    
    if ~ isequal(obj1.dirs, obj2.dirs)
        fprintf('Attributes dirs are different.\n')
        return
    end
    
    if ~ isequal(obj1.params, obj2.params)
        fprintf('Attributes params are different.\n')
        return
    end
    
    if ~ (obj1.sc == obj2.sc)
        fprintf('Attributes sc are different.\n')
        return
    end
    
    if ~ (obj1.elec == obj2.elec)
        fprintf('Attributes elec are different.\n')
        return
    end
    
    if ~ isequal(obj1.AS, obj2.AS)
        fprintf('Attributes AS are different.\n')
        return
    end
    
    outbool = true;
    
end
