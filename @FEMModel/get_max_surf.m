function max_surf = get_max_surf(this, tag, quantity, sol)
    % Return the max of a quantity over a surface.
    %
    % Usage:
    %   max_surf = fem.get_max_surf(tag, quantity, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the surface over which the max should be computed.
    %
    %   quantity : string :
    %       Identifier of the quantity whose max should be computed.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    
    fprintf('Making MaxSurf: %s ...\n', tag)
    
    % Make table to store numerical result.
    this.make_table(sprintf('tbl_max_%s_%s', tag, sol));
    
    % Make dataset.
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    % Make MaxSurf.
    numt_tag = sprintf('max_%s_%s', tag, sol);
    num_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(numt_tag, num_tags))
        this.CM.result.numerical.create(numt_tag, 'MaxSurface');
    end
    this.CM.result.numerical(numt_tag).set('data', sprintf('dset_%s_%s', tag, sol));
    this.CM.result.numerical(numt_tag).selection.named(tag);
    
    % Attach MaxSurf to table.
    this.CM.result.numerical(numt_tag).set('table', sprintf('tbl_max_%s_%s', tag, sol));
    this.CM.result.numerical(numt_tag).set('expr', {quantity});
    this.CM.result.numerical(numt_tag).setResult;
    
    fprintf('... Done.\n')
    
    % Fetch value.
    max_surf = this.CM.result.numerical(numt_tag).getReal();
    
end
