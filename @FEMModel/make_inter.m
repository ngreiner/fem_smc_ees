function make_inter(this, tag, label, keep, intbnd, input1)
	% Create an Intersection feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_inter(tag, label, keep, intbnd, input1)
    %
    % Parameters:
    %   tag : string :
    %       Tag of feature.
    %
    %	label : string :
    %       Label of feature.
    %
    %	keep : string :
    %       'true' or 'false': indicates wether to keep input objects.
    %
    %   intbnd : string :
    %       'on' or 'off': indicates wether to keep interior
    %       boundaries.
    %
    %	input1 : 1*N cell of strings :
    %       Tags of objects to intersect.
    %
    % Description:
    %   If keep is 'off', all the objects of input1 and input2 are
    %   deleted once the intersection is constructed (equivalent to a
    %   Delete feature).
    %   If intbnd is 'off', the internal boundaries between two input
    %   objects remaining in the intersected object are deleted.
    
    fprintf('Making ''%s'' Intersection feature...\n', tag)
    fprintf('Intersected objects: ')
    cellfun(@(x) fprintf('%s, ', x), input1)
    fprintf('\nKeeping input objects: %s\n', keep)
    
    this.CM.geom('geom1').create(tag, 'Intersection');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('keep', keep);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('intbnd', intbnd);
    this.CM.geom('geom1').feature(tag).selection('input').set(input1);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('... Done.\n\n')
    
end
