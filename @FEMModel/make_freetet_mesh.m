function make_freetet_mesh(this, tag, fineness)
    % Create a tetrahedric mesh for a specific domain selection.
    %
    % Usage:
    %   fem.make_freetet_mesh(tag, fineness)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the domain selection for which we build a mesh.
    %
    %   fineness : double :
    %       Mesh fineness indicator.
    %
    % Description:
    %   Currently, the fineness parameter is used only when the FEMModel
    %   attribute params.mesh.fineness_type is 'built_in'.
    %   If it is 'custom' instead, a fixed element size range is used.
    %   This should be addressed in the future.

    fprintf('Making FreeTet Mesh for feature: %s...\n', tag)
    fprintf('Fineness: %d\n', fineness)
    
    this.CM.mesh('mesh1').create(tag, 'FreeTet');
    
    % Find out whether mesh is for specific domain or for the remaining of
    % the geometry:
    idx = find(strcmp(varargin, 'tag'));
    if idx
    this.CM.mesh('mesh1').feature(tag).create('size1', 'Size');
    
    if strcmp(this.params.mesh.fineness_type, 'built_in')
        % Parse inputs.
        idx = find(strcmp(varargin, 'fineness'));
        if idx
            fineness = varargin{idx + 1};
        end
        % Assign fineness.
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hauto', fineness);
        
    elseif strcmp(this.mesh.finenessType, 'custom')
        % Parse inputs.
        idx = find(strcmp(varargin, 'hmax'));
        if idx
            hmax = varargin{idx + 1};
        end
        idx = find(strcmp(varargin, 'hmin'));
        if idx
            hmin = varargin{idx + 1};
        end
        idx = find(strcmp(varargin, 'hgrad'));
        if idx
            hgrad = varargin{idx + 1};
        end
        idx = find(strcmp(varargin, 'hcurve'));
        if idx
            hcurve = varargin{idx + 1};
        end
        idx = find(strcmp(varargin, 'hnarrow'));
        if idx
            hnarrow = varargin{idx + 1};
        end
        % Assign size parameters
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('custom', 'on');
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hmaxactive', true);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hmax', hmax);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hminactive', true);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hmin', hmin);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hgradactive', true);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hgrad', hgrad);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hcurveactive', true);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hcurve', hcurve);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hnarrowactive', true);
        this.CM.mesh('mesh1').feature(tag).feature('size1').set('hnarrow', hnarrow);
        
    end
    
    fprintf('... Done.\n\n')
    
end
