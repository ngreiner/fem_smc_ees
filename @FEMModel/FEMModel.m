classdef FEMModel < handle
    
    % Properties defined here:
    %
    %   name : string :
    %       Name of the FEMModel.
    %
    %   dirs : struct :
    %       Paths to directories necessary to build the FEMModel. Fields
    %       are top, comsol_models, resources, CAD, comsol_CAD, step, stl).
    %
    %   params : struct :
    %       Parameters of the FEMModel. Fields are: sc, roots, bone, cond,
    %       mesh, elec, saline, physics, geom.
    %
    %   sc : SCModel object :
    %       Describing the spinal cord involved in the FEM calculations.
    %
    %   elec : Elec object :
    %       Describing the electrode array used for stimulation of the
    %       spinal cord.
    %
    %   CM : comsol model object :
    %       Actual finite element model performing the electric potential
    %       distributions calculations.
    %
    %   AS : struct :
    %       Describing the active site of the electrode array currently
    %       accessed by the comsol model CM.

	properties
        name    % string
		dirs    % struct
        params  % struct
        sc      % SCModel object
        elec    % Elec object
        CM      % comsol model object
        AS      % struct
	end

	methods
        
		function this = FEMModel(varargin)
            % Class constructor method.
            %
            % Usage:
            %   fem = FEMModel()
            %   fem = FEMModel('topdir', topdir)
            %   fem = FEMModel('config_file_name', config_file_name)
            %   fem = FEMModel('name', name)
            %
            % Parameters:
            %   topdir : string : 
            %       Path to top-level directory.
            %
            %   config_file_name : string :
            %       Name of configuration file.
            %
            %   name : string :
            %       Name of FEMModel.
            %
            % Description:
            %   The arguments above can be used in combination when
            %   constructing a new object.
            %   If topdir is not specified, it defaults to the directory
            %   where the code is executed.
            %   If config_file_name is specified, the path to the
            %   configuration file is obtained as 
            %   $topdir/FEM_config_files/$config_file_name.
            %   Such a configuration file should specify the name of the
            %   FEMModel, but if the parameter name is specified in the
            %   construction declaration, it has precedence.
            %   This machinery allows to only specify a name and a topdir
            %   so as to load an existing FEMModel saved on disk.
            
            idx = find(strcmp('topdir', varargin));
            if ~ isempty(idx)
                this.set_dirs(varargin{idx + 1});
            else
                this.set_dirs(pwd);
            end
            
            idx = find(strcmp('config_file_name', varargin));
            if ~ isempty(idx)
                file_path = fullfile(this.dirs.resources, 'FEM_config_files', varargin{idx + 1});
                this.parse_config_file(file_path);
                this.build();
            end
            
            idx = find(strcmp('name', varargin));
            if ~ isempty(idx)
                this.name = varargin{idx + 1};
            end
            
        end
        
	end

	methods
        build(this)
        build_bone_fat(this)
        build_CC(this)
        build_CM(this)
        build_CSF(this)
        build_dura(this)
        build_EC(this)
        build_elec(this)
        build_geom(this, varargin)
        build_geom_node(this)
        build_GM(this)
        build_ground_point(this)
        build_mat(this)
        build_mesh(this)
        build_model_set(this, AS_idxs)
        build_plots(this)
        build_saline(this)
        build_sel(this)
        build_studies(this)
        build_WM_with_roots(this)        
        clear_geom(this)
        clear_mat(this)
        clear_mesh(this)
        clear_physics(this)
        clear_sel(this)
        clear_studies(this)
        delete_geom_feature(this, tag)
        bool = eq(obj1, obj2)
        export_final_geom(this, varargin)
        from_struct(this, struc)
        area = get_AS_area(this)
        max_line = get_max_line(this, tag, quantity, sol)
        max_surf = get_max_surf(this, tag, quantity, sol)
        mean_surf = get_mean_surf(this, tag, quantity, sol)
        min_line = get_min_line(this, tag, quantity, sol)
        min_surf = get_min_surf(this, tag, quantity, sol)
		init_CM(this)
        load_CM(this, CM_id, perm)
        make_block(this, tag, label, dims, pos, rot, base)
        make_box_sel(this, dim, tag, x, y, z, width)
        make_compl_sel(this, dim, tag, inputs)
        make_cyl(this, tag, label, r, h, pos)
        make_cyl_sel(this, dim, tag, r, h, pos)
        make_data_line(this, tag, quantity, sol, varargin)
        make_data_surf(this, tag, selection, sol, varargin)
        make_dataset(this, tag, selection, sol)
        make_delete(this, tag, label, input1)
        make_diff(this, tag, label, keep, intbnd, input1, input2)
        make_diff_sel(this, dim, tag, input1, input2)
        make_freetet_mesh(this, tag, fineness)
        make_geom_inter_sel(this, tag, label, dim, input1)
        make_geom_node(this)
        make_import(this, tag, label, file_path, varargin)
        make_inter(this, tag, label, keep, intbnd, input1)
        make_inter_sel(this, dim, tag, inputs)
        make_mat(this, tag, cond_tensor)
        make_mirror(this, tag, label, keep, input1, plane)
        make_point(this, tag, label, pos)
        make_polygon(this, tag, label, path)
        make_sphere(this, tag, label, r, pos)
        make_swept_mesh(this, tag, sourceface, fineness)
        make_table(this, tag)
        make_unif_line(this, tag, radius, color, varargin)
        make_unif_surf(this, tag, color, varargin)
        make_union(this, tag, label, keep, intbnd, input1)
        make_union_sel(this, dim, unionSelName, inputs)
        parse_cond_file(this, file_path)
        parse_config_file(this, file_path)
        run_all(this)
		run_CC(this)
		run_EC(this)
        run_geom_node(this)
		run_mesh(this)
		save_CM(this, file_name)
        set_AS_area(this)
        set_dirs(this, topdir)
        set_dims(this, topdir)
		set_elec(this)
        set_geom_kernel(this)
		set_sc(this)
        struc = to_struct(this)
        update_AS(this, ASindex)
        update_box_sel(this, dim, tag, x, y, z, width)
    end

end
