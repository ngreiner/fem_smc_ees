function make_dataset(this, tag, selection, sol)
	% Make a dataset in CM.
    %
    % Usage:
    %   fem.make_dataset(tag, selection)
    %   fem.make_dataset(tag, selection, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the dataset.
    %
    %   selection : string :
    %       Identifier of a selection of objects.
    %
    %   sol : string :
    %       Identifier of a solution.
    %
    % Description:
    %   If a dataset with specified tag already exist, this method does
    %   nothing.
    %   If the parameter sol is not specified, no solution is attached to
    %   the dataset.
    
    dset_tags = cell(this.CM.result.dataset.tags);
    if any(strcmp(tag, dset_tags))
        return
    end
    
    this.CM.result.dataset.create(tag, 'Solution');
    this.CM.result.dataset(tag).selection.named(selection);
    
    if nargin == 4
        this.CM.result.dataset(tag).set('solution', sol);
    end
    
end
