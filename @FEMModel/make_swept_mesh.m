function make_swept_mesh(this, tag, sourceface, fineness)
    % Create a swept mesh for a specific domain selection.
    %
    % Usage:
    %   fem.make_swept_mesh(tag, sourceface, fineness)
    %
    % Parameters:
    %   tag : string :
    %       Tag of a domain selection.
    %
    %   sourceface : string :
    %       Tag of a boundary selection.
    %
    %   fineness : double :
    %       Mesh fineness indicator.
    %
    % Description:
    %   The selected domain (tag) should be the extrusion of the selected
    %   boundary (sourceface).
    %   The mesh drawn on sourceface is thus extruded and extended to the
    %   domain tag.
    %   The fineness of the mesh (specified by fineness) corresponds to a
    %   built-in fineness of Comsol.
    %   Custom definition of the fineness is currently not implemented.
    %   This should be addressed in the future.
    
    this.CM.mesh('mesh1').create(tag, 'Sweep');
    this.CM.mesh('mesh1').feature(tag).selection.geom('geom1', 3);
    this.CM.mesh('mesh1').feature(tag).selection.named(tag);
    this.CM.mesh('mesh1').feature(tag).create('size1', 'Size');
    this.CM.mesh('mesh1').feature(tag).feature('size1').set('hauto', fineness);
    this.CM.mesh('mesh1').feature(tag).selection('sourceface').named(sourceface);
    
    % The code below is left incomplete for now. It can be used to enable
    % custom specifications for the mesh.
    if 0
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('custom', 'on');
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hgradactive', true);
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hcurveactive', true);
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hnarrowactive', true);
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hminactive', true);
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hmaxactive', true);
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hmin', '0.00005');
        this.CM.mesh('mesh1').feature(featureName).feature('size1').set('hmax', '0.004');
    end
    
end
