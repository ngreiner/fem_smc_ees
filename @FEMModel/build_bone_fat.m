function build_bone_fat(this)
	% Build the geometry features representing the bone and fat.
	%
	% Usage:
    %   fem.build_bone_fat()
	%
	% Description:
    %   The features representing the bone and fat depend upon the choice
	%   made for the attribute params.bone.repr. See code below.

	display('Building bone and fat...')

    switch this.params.bone.repr

        % If params.bone.repr is set on 'cylinder', import fat first and
        % build bony cylinder around it. The fat is removed from the bony
        % cylinder by boolean difference.
        % All the features already built and intersecting the newly built
        % features are also removed by boolean difference.
        case 'cylinder'
            fprintf('bone.repr: cylinder\n')
            if strcmp(this.params.sc.CAD_file_format, 'step')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.step, 'fat.step'), 'type', 'cad');
            elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.comsol_CAD, 'fat.mphbin'), 'type', 'native');
            end
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle'});
            else
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec'});
            end
            this.make_delete('del_fat', 'del_fat', {'tmp_fat'});
            this.make_cyl('tmp_bone', 'tmp_bone', this.params.bone.radius, this.params.geom.z_parts(4) - this.params.geom.z_parts(1), {0, 0, this.params.geom.z_parts(1)});
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('bone', 'bone', 'on', 'off', {'tmp_bone'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle', 'fat'});
            else
                this.make_diff('bone', 'bone', 'on', 'off', {'tmp_bone'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'fat'});
            end
            this.make_delete('del_bone', 'del_bone', {'tmp_bone'});

		% If bone.repr is set on 'single_comsol_CAD_file', build bone first
		% from single comsol CAD file, and then build fat. The bone is
		% removed from the fat by boolean difference.
        % All the features already built and intersecting the newly built
        % features are also removed by boolean difference.
        case 'single_comsol_CAD_file'
        	fprintf('bone.repr: single_comsol_CAD_file\n')
            this.make_import('tmp_bone', 'tmp_bone', fullfile(this.dirs.comsol_CAD, sprintf('%s.mphbin', this.params.bone.comsol_CAD_file_name)), 'type', 'native');
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('bone', 'bone', 'on', 'off', {'tmp_bone'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle'});
            else
                this.make_diff('bone', 'bone', 'on', 'off', {'tmp_bone'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec'});
            end
            this.make_delete('del_bone', 'del_bone', {'tmp_bone'});
            if strcmp(this.params.sc.CAD_file_format, 'step')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.step, 'fat.step'), 'type', 'cad');
            elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.comsol_CAD, 'fat.mphbin'), 'type', 'native');
            end
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle', 'bone'});
            else
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'bone'});
            end
            this.make_delete('del_fat', 'del_fat', {'tmp_fat'});

		% If bone.repr is set on 'multiple_vertebrae', build vertebrae
		% from separate files, and then build fat. The vertebrae are
		% removed from the fat by boolean difference.
        % All the features already built and intersecting the newly built
        % features are also removed by boolean difference.
        case 'multiple_vertebrae'
        	fprintf('bone.repr: multiple_vertebrae\n')
            feats = cell(1, this.params.bone.nvertebrae);
            for i = 1 : this.params.bone.nvertebrae
                if strcmp(this.params.bone.CAD_file_format, 'step')
                    this.make_import(sprintf('vert%d', i), sprintf('vert%d', i), fullfile(this.dirs.step, sprintf('%s_vertebra.step', this.params.bone.vertebrae{i})), 'type', 'cad');
                else
                    this.make_import(sprintf('vert%d', i), sprintf('vert%d', i), fullfile(this.dirs.comsol_CAD, sprintf('%s_vertebra.mphbin', this.params.bone.vertebrae{i})), 'type', 'native');
                end
                feats{i} = sprintf('vert%d', i);
            end
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('bone', 'bone', 'on', 'off', feats, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle'});
            else
                this.make_diff('bone', 'Bone', 'on', 'off', feats, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec'});
            end
            for i = 1 : this.params.boner.nvertebrae
                this.make_delete(sprintf('del_vert%d', i), sprintf('del_vert%d', i), {sprintf('vert%d', i)});
            end
            if strcmp(this.params.sc.CAD_file_format, 'step')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.step, 'fat.step'), 'type', 'cad');
            elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
                this.make_import('tmp_fat', 'tmp_fat', fullfile(this.dirs.ComsolCAD, 'fat.mphbin'), 'type', 'native');
            end
            if ~ strcmp(this.elec.paddle.flag, 'off')
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'paddle', 'bone'});
            else
                this.make_diff('fat', 'fat', 'on', 'off', {'tmp_fat'}, {'GM', 'WM_with_roots', 'CSF', 'dura', 'elec', 'bone'});
            end
            this.make_delete('del_fat', 'del_fat', {'tmp_fat'});
    end

end
