function make_import(this, tag, label, file_path, varargin)
	% Create an Import feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_import(tag, label, file_path)
    %   fem.make_import(tag, label, file_path, 'type', type)
    %
    % Parameters:
    %   tag       : string : tag of feature.
    %	label     : string : label of feature.
    %	file_path : string : path to CAD file to import.
    %   type      : string : type of file ('cad', 'native', ...).
    
    fprintf('Making ''%s'' Import feature...\n', tag)
    fprintf('Importing from file: %s\n', Utils.get_full_path(file_path))
    
    this.CM.geom('geom1').create(tag, 'Import');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).set('selresult', 'on');
    this.CM.geom('geom1').feature(tag).set('selresultshow', 'all');
    this.CM.geom('geom1').feature(tag).set('filename', Utils.get_full_path(file_path));
    idx = find(strcmp(varargin, 'type'));
    if idx
        this.CM.geom('geom1').feature(tag).set('type', varargin{idx + 1});
    end
    this.CM.geom('geom1').feature(tag).importData;
    this.CM.geom('geom1').runCurrent;
    
    fprintf('... Done.\n\n')
    
end
