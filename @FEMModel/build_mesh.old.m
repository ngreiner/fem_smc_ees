function build_mesh(this)
	% Build the Mesh node of CM.
	%
	% IUsage:
    %   fem.build_mesh()
	%
	% Description:
    %   The implemented mesh is overall composed of either one or three
    %   parts, depending upon the option chosen for the saline. 
	%
    %   If the saline is in three parts, so is the mesh and:
	%       - The central part is a tetrahedric mesh where:
	%           * the GM subpart is 'Extra Fine';
	%           * the WM and roots subpart  is 'Extra Fine';
	%           * the CSF subpart is 'Extra Fine';
	%           * the dura subpart is 'Extremely Fine';
	%           * the elec subpart is 'Extremely Fine';
	%           * the paddle subpart is 'Extra Fine';
	%           * the fat subpart is 'Extra Fine';
	%           * the bone subpart is 'Finer';
	%           * the saline is 'Fine'.
	%
	%       - The upper (resp. lower) part can be either:
    %           * a swept mesh obtained by extrusion of the meshing pattern
    %             sketched on the upper (resp. lower) surface of the
    %             central part 
    %           * a free-tetrahedric mesh
    %
    %   If the saline is in one part, so is the mesh, built exactly as the
    %   central part of the three-parts mesh.

	fprintf('Building Mesh...\n')

    this.CM.mesh.create('mesh1', 'geom1');
    
    this.make_freetet_mesh('GM_center', 2);
    this.make_freetet_mesh('WM_with_roots_center', 2);
    this.make_freetet_mesh('CSF_center', 2);
    this.make_freetet_mesh('dura_center', 1);
    this.make_freetet_mesh('elec_center', 1);
    if ~ strcmp(this.elec.paddle.flag, 'off')
        this.make_freetet_mesh('paddle_center', 2);
    end
    this.make_freetet_mesh('fat_center', 2);
    this.make_freetet_mesh('bone_center', 3);
    this.make_freetet_mesh('saline_center', 4);
    
    if ~ strcmp(this.params.saline.repr, 'single_filled_cylinder')
        if strcmp(this.params.mesh.type_exts, 'sweep')
            this.make_swept_mesh('ext_up', 'inter_up', 4);
            this.make_swept_mesh('ext_down', 'inter_down', 4);
        elseif strcmp(this.params.mesh.type_exts, 'freetet')
            this.make_freetet_mesh('ext_up', 4);
            this.make_freetet_mesh('ext_down', 4);
        end
    end

    fprintf('Mesh is built.\n\n')
    
end
