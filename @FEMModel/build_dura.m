function build_dura(this)
    % Build the geometry feature representing the dura matter.
    %
    % Usage:
    %   fem.build_dura()
    %
    % Description:
    %   Imports the appropriate CAD file, representing the dura matter, and
    %   remove from it the features already built that are intersecting it
    %   by boolean difference.
    
    fprintf('Building dura...\n')
    
    if strcmp(this.params.sc.CAD_file_format, 'step')
        this.make_import('tmp_dura', 'tmp_dura', fullfile(this.dirs.step, 'dura.step'), 'type', 'cad');
        
    elseif strcmp(this.params.sc.CAD_file_format, 'comsol')
        this.make_import('tmp_dura', 'tmp_dura', fullfile(this.dirs.comsol_CAD, 'dura.mphbin'), 'type', 'native');
        
    end
    
    this.make_diff('dura', 'dura', 'on', 'off', {'tmp_dura'}, {'GM', 'WM_with_roots', 'CSF'});
    this.make_delete('del_dura', 'del_dura', {'tmp_dura'});
    
end
