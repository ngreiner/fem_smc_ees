function build_geom_node(this)
    % Build the Geometry node in CM.
    %
    % Usage:
    %   fem.build_geom_node()
    this.CM.geom.create('geom1', 3);
end
