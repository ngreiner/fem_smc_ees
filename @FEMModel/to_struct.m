function struc = to_struct(this)
	% Return a matlab struct equivalent to the FEMModel.
    %
    % Usage:
    %   struc = fem.to_struct()
    %
    % Return: 
    %   struc : struct : with same fields as the FEMModel object.
    struc.name = this.name;
    struc.params = this.params;
    struc.sc = this.sc.to_struct();
    struc.elec = this.elec.to_struct();
    struc.AS = this.AS;
end
