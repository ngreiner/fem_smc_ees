function make_data_line(this, tag, quantity, sol, varargin)
	% Make the features necessary to plot a 3d-line colored so as to
	% represent the intensity of a computed quantity.
    %
    % Usage:
    %   fem.make_data_line(tag, quantity, sol)
    %   fem.make_data_line(tag, quantity, sol, 'colormin', colormin)
    %   fem.make_data_line(tag, quantity, sol, 'colormax', colormax)
    %   fem.make_data_line(tag, quantity, sol, 'radius', radius)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the edge selection over which the quantity should
    %       be plotted.
    %
    %   quantity : string :
    %       Identifier of the quantity whose intensity should be
    %       represented.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    %
    %   colormin : double :
    %       Quantity value represented by the minimal value of the color
    %       range.
    %
    %   colormax : double :
    %       Quantity value represented by the maximal value of the color
    %       range.
    %
    %   radius : double :
    %       Radius of the tube materializing the line.
    %
    % Description:
    %   If radius is specified, the lines are materialized by solid tubes
    %   instead of thin lines.
    
    pg_tags = cell(this.CM.result.tags);
    if ~ any(strcmp('pg1', pg_tags))
        this.CM.result.create('pg1', 'PlotGroup3D');
        this.CM.result('pg1').set('edges', 'off');
    end
    
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    pg_tags = cell(this.CM.result('pg1').feature.tags);
    if ~ any(strcmp(sprintf('line_%s', tag), pg_tags))
        this.CM.result('pg1').create(sprintf('line_%s', tag), 'Line');
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('data', sprintf('dset_%s_%s', tag, sol));
    end
    
    idx = find(strcmp('colormin', varargin));
    if idx
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('rangecoloractive', 'on');
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('rangecolormin', varargin{idx + 1});
    end

    idx = find(strcmp('colormax', varargin));
    if idx
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('rangecoloractive', 'on');        
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('rangecolormax', varargin{idx + 1});
    end
    
    idx = find(strcmp('radius', varargin));
    if idx
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('linetype', 'tube');
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('tuberadiusscaleactive', true);
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('radiusexpr', sprintf('%f', varargin{idx + 1}));
    end
    
    idx = find(strcmp('colortable', varargin));
    if idx
        colortable = varargin{idx + 1};
    else
        colortable = 'Thermal';
    end
    this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('colortable', colortable);
    
    idx = find(strcmp('colortablerev', varargin));
    if idx
        this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('colortablerev', 'on');
    end
    
    this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('colorlegend', false);
    this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('expr', quantity);
    this.CM.result('pg1').feature(sprintf('line_%s', tag)).set('resolution', 'normal');
    
end
