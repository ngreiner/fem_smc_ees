function mean_surf = get_mean_surf(this, tag, quantity, sol)
    % Return the mean of a quantity over a surface.
    %
    % Usage:
    %   mean_surf = fem.get_mean_surf(tag, quantity, sol)
    %
    % Parameters:
    %   tag : string :
    %       Tag of the surface over which the mean should be computed.
    %
    %   quantity : string :
    %       Identifier of the quantity whose mean should be computed.
    %
    %   sol : string :
    %       Tag of the solution from which the quantity should be
    %       estimated.
    
    fprintf('Making MeanSurf: %s ...\n', tag)
    
    % Make table to store numerical result.
    this.make_table(sprintf('tbl_mean_%s_%s', tag, sol));
    
    % Make dataset.
    this.make_dataset(sprintf('dset_%s_%s', tag, sol), tag, sol);
    
    % Make MaxSurf.
    numt_tag = sprintf('mean_%s_%s', tag, sol);
    num_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(numt_tag, num_tags))
        this.CM.result.numerical.create(numt_tag, 'AvSurface');
    end
    this.CM.result.numerical(numt_tag).set('data', sprintf('dset_%s_%s', tag, sol));
    this.CM.result.numerical(numt_tag).selection.named(tag);
    
    % Attach MaxSurf to table.
    this.CM.result.numerical(numt_tag).set('table', sprintf('tbl_mean_%s_%s', tag, sol));
    this.CM.result.numerical(numt_tag).set('expr', {quantity});
    this.CM.result.numerical(numt_tag).setResult;
    
    fprintf('... Done.\n')
    
    % Fetch value.
    mean_surf = this.CM.result.numerical(numt_tag).getReal();
    
end
