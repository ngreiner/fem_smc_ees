function build_EC(this)
	% Build the Electric Currents node in CM.
	%
	% Usage:
    %   fem.build_EC()
	%
	% Description:
    %   This method implements the following features:
    %       - Von Neumann or Dirichlet condition at the border;
    %       - boundary current or voltage source at the stimulation active
    %         site; 
	%       - Von Neumann condition at the other boundaries of the
	%         stimulation active site if applicable;  
    %       - Dirichlet condition at the ground point if applicable;
    %       - Current conservation using curvilinear coordinates in the WM
    %         and the roots. 

    fprintf('Building Electric Currents Node...\n')

    % Create Node
    this.CM.physics.create('ec', 'ConductiveMedia', 'geom1');
    
    % Set roots surfacic intersections as Electric Insulations.
    switch this.params.roots.repr
        case 'dorsal_ventral'
            for i = 1 : this.sc.morpho.nroots - 1
                this.CM.physics('ec').create(sprintf('el_in%d', i), 'ElectricInsulation', 2);
                this.CM.physics('ec').feature(sprintf('el_in%d', i)).selection.named(sprintf('geom1_intsel%d',i));
            end
        case 'left_right'
            fprintf('No electric insulation features for root surfacic intersections.\n')
    end
    
    % Impose use of curvilinear coordinates in the WM and roots.
    this.CM.physics('ec').feature.create('AnisotropicConduction', 'CurrentConservation', 3);
    this.CM.physics('ec').feature('AnisotropicConduction').selection.named('WM_with_roots');
    this.CM.physics('ec').feature('AnisotropicConduction').setIndex('coordinateSystem', 'cc_cs', 0);
    
    % Set condition at the border.
    switch this.params.physics.border_condition
        case 'von_neumann'
            this.CM.physics('ec').feature.create('VonNeumanCondition', 'ElectricInsulation', 2);
            this.CM.physics('ec').feature('VonNeumanCondition').selection.named('border');
        case 'dirichlet'
            this.CM.physics('ec').feature.create('DirchletCondition', 'Ground', 2);
            this.CM.physics('ec').feature('DirchletCondition').selection.named('border');
    end
    
    % Set Dirichlet condition on the ground point if applicable.
    if this.params.physics.with_ground_point
        this.CM.physics('ec').create('Ground', 'Ground', 0);
        this.CM.physics('ec').feature('Ground').selection.named('ground_point');
    end
    
    % Set boundary condition at the active site.
    switch this.params.physics.stim_type
        case 'current'
            this.CM.physics('ec').feature.create('StimuBoundary', 'BoundaryCurrentSource', 2);
            this.CM.physics('ec').feature('StimuBoundary').selection.named('stimu_boundary');
            this.CM.physics('ec').feature('StimuBoundary').set('Qjs', '1');
        case 'voltage'
            this.CM.physics('ec').feature.create('StimuBoundary', 'BoundaryVoltageSource', 2);
            this.CM.physics('ec').feature('StimuBoundary').selection.named('stimu_boundary');
            this.CM.physics('ec').feature('StimuBoundary').set('V0', '1');
    end
    
    % If no paddle is included, set electric insulation on the other faces
    % of the stimulation active site.
    if strcmp(this.elec.paddle.flag, 'off')
        this.CM.physics('ec').feature.create('ElectricInsulation', 'ElectricInsulation', 2);
        this.CM.physics('ec').feature('ElectricInsulation').selection.named('insulating_plates');
    end

    fprintf('Electric Currents Node is built.\n\n')
    
end
