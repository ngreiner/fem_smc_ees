function load_CM(this, CM_id, perm)
	% Load the comsol model identified by CM_id.
    %
    % Usage:
    %   fem.load_CM(CM_id)
    %   fem.load_CM(CM_id, perm)
    %
    % Parameters:
    %   CM_id : int/string :
    %       If int, index of active site; if string, full name of a .mph
    %       file.
    %
    %   perm : int :
    %       Loading permission level.
    %
    % Description:
    %   If perm is 1, loading takes place.
    %   If perm is 0, loading takes place:
    %       1. systematically when a full file name is given as CM_id;
    %       2. only when the integer given as CM_id does not coincide with
    %          the active site index of a model already loaded. If not,
    %          the attribute AS.idx is updated and the corresponding model
    %          loaded.
    
    if (nargin < 3)
        perm = 0;
    end
    
    if ischar(CM_id)
        fprintf('Loading comsol model %s ...\n', CM_id)
        file_path = fullfile(this.dirs.comsol_models, CM_id);
    	this.CM = mphload(file_path);
    
    elseif isnumeric(CM_id)
        idx = CM_id;
        fprintf('Loading comsol model %s_AS%d ...\n', this.name, idx)
        % Check wether loading the model is superfluous.
        if (this.AS.idx == idx) && ~ isempty(this.CM) && ~ perm
            fprintf('comsol model with similar AS.idx already loaded. Exited method without overloading.\n\n');
            return
        end
        % If not, load Comsol Model and update ASindex.
        this.AS.idx = idx;
        file_path = fullfile(this.dirs.comsol_models, sprintf('%s_AS%d', this.name, this.AS.idx));
        this.CM = mphload(file_path);
    
    end
    
    fprintf('... Done.\n\n')
    
end
