function run_EC(this)
	% Run the Electric Currents node of CM.
    %
    % Usage:
    %   fem.run_EC()
	this.CM.sol('sol2').runAll;
    this.set_AS_area();
end
