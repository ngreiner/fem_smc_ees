function area = get_AS_area(this)
	% Return the area of the stimulation active site (in m^2).
    %
    % Usage:
    %   area = fem.get_AS_area()
    %
    % Return:
    %   area : double : stimulation active site area.
    %
    % Description:
    %   Fetching the value of the stimulation active site area requires to
    %   create a table and a an 'IntSurface' numerical object in the Comsol
    %   Model, which is achieved by the code below.
    %   If that table and numerical object already exist, the method simply
    %   fetches the stored value.
    
    tbl_tags = cell(this.CM.result.table.tags);
    if ~ any(strcmp('tbl_int', tbl_tags))
        this.CM.result.table.create('tbl_int', 'Table');
    end
    
    int_AS_tag = sprintf('int_as%d', this.AS.idx);
    int_tags = cell(this.CM.result.numerical.tags);
    if ~ any(strcmp(int_AS_tag, int_tags));
        this.CM.result.numerical.create(int_AS_tag, 'IntSurface');
		this.CM.result.numerical(int_AS_tag).selection.named('stimu_boundary');
		this.CM.result.numerical(int_AS_tag).set('probetag', 'none');
		this.CM.result.numerical(int_AS_tag).set('table', 'tbl_int');
		this.CM.result.numerical(int_AS_tag).set('unit', {'m^2'});
		this.CM.result.numerical(int_AS_tag).set('expr', {'1'});
		this.CM.result.numerical(int_AS_tag).setResult;
    end
    
    area = this.CM.result.numerical(int_AS_tag).getReal();
    
end
