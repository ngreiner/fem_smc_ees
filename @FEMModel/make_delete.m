function make_delete(this, tag, label, input)
	% Build a Delete feature in the Geometry of CM.
    %
    % Usage:
    %   fem.make_delete(tag, label, input)
    %
    % Parameters:
    %   tag   : string : Comsol tag of feature.
    %	label : string : Comsol label of feature.
    %	input : 1*M cell of strings : tags of objects to delete.
    %
    % Description:
    %   A Delete feature allows to delete another feature from the geometry
    %   without deleting the features which are derived from it.
    
    fprintf('Making ''%s'' Delete feature...\n', tag)
    fprintf('Deleted objects:\n')
    cellfun(@(x) fprintf('%s, ', x), input)
    
    this.CM.geom('geom1').create(tag, 'Delete');
    this.CM.geom('geom1').feature(tag).label(label);
    this.CM.geom('geom1').feature(tag).selection('input').init;
    this.CM.geom('geom1').feature(tag).selection('input').set(input);
    this.CM.geom('geom1').runCurrent;
    
    fprintf('\n... Done.\n\n')
    
end
