function run_CC(this)
	% Run the Curvilinear Coordinates node of CM.
    %
    % Usage:
    %   fem.run_CC()
	this.CM.sol('sol1').runAll;
end
