function build_mat(this)
	% Build the Materials node of CM.
	%
	% Usage:
    %   fem.build_mat()
    %
    % Description:
    %   Assigns an electric conductivity tensor to every domain in the
    %   geometry, using the values stored in the attribute params.cond and
    %   the selections of CM.

    fprintf('Building Materials...\n')

	% Create GM material. It is isotropic.
    this.make_mat('GM', {this.params.cond.GM; this.params.cond.GM; this.params.cond.GM});

    % Create WM material. It is anisotropic, defined using a curvilinear
    % coordinate system.
    this.make_mat('WM_with_roots', {this.params.cond.WM_longi; this.params.cond.WM_trans; this.params.cond.WM_trans});

    % Create CSF material. It is isotropic.
    this.make_mat('CSF', {this.params.cond.CSF; this.params.cond.CSF; this.params.cond.CSF});

    % Create dura material. It is isotropic.
    this.make_mat('dura', {this.params.cond.dura; this.params.cond.dura; this.params.cond.dura});

    % Create fat material. It is isotropic.
    this.make_mat('fat', {this.params.cond.fat; this.params.cond.fat; this.params.cond.fat});

    % Create bone material. It is isotropic.
    this.make_mat('bone', {this.params.cond.bone; this.params.cond.bone; this.params.cond.bone});

    % Create elec material. It is isotropic.
    this.make_mat('elec', {this.params.cond.elec; this.params.cond.elec; this.params.cond.elec});
    
    % When applicable, create paddle material. It is isotropic.
    if ~ strcmp(this.elec.paddle.flag, 'off')
        this.make_mat('paddle', {this.params.cond.paddle; this.params.cond.paddle; this.params.cond.paddle});
    end
    
    % Create saline material. It is isotropic.
    this.make_mat('saline', {this.params.cond.saline; this.params.cond.saline; this.params.cond.saline});

    fprintf('Material is built.\n\n')
    
end
