function outbool = is_interp(this, AS_idx)
    % Return a logical indicating whether the Soma is already well
    % interpolated for the input AS_idx. 
    %
    % Parameters:
    %   AS_idx : int : index of active site.
    %
    % Return:
    %   outbool : logical.
    outbool = ~ isempty(this.V{AS_idx});
end
