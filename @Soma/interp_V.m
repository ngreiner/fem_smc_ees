function interp_V(this, smcd, perm)
    % Interpolate the electric potential at the Soma using the Comsol Model
    % attached to the smcd.
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Sets the fields V{AS_idx} of the Soma (where AS_idx is
    %   smcd.fem.AS.idx). 
    %   Potentials are interpolated in the Comsol model thanks to the
    %   coordinates of the Soma.
    
    % Unpack attributes in convenient variables.
    idx = smcd.fem.AS.idx;
    CM = smcd.fem.CM;
    if smcd.fem.params.physics.with_curvi_coords
        dset = 'dset2';
    else
        dset = 'dset1';
    end
    
    % If Soma's potentials already interpolated, interpolate anew only
    % with permission. 
    if this.is_interp(idx)
        fprintf('%s already interpolated for ASindex #%d.\n', class(this), idx)
        if (perm == 0) || (perm == 1)
            fprintf('Writing permission = %d. Exiting method without interpolating.\n', perm)
            return
        elseif strcmp(perm, 'i') && ~ Utils.prompt_confirm('Do you want to overwrite?')
            fprintf('Exiting method without interpolating.\n')
            return
        end
        fprintf('Overwriting fibers''potentials.\n')
    end
    
    % Interpolate.
    V = mphinterp(CM, 'V', 'coord', [this.coords(1) * 1e-3; this.coords(2) * 1e-3; this.coords(3)* 1e-3], 'dataset', dset);

    % Rectify potential so that it is expressed in mV and for a
    % stimulation amplitude of 1uA and store in dedicated arrays.
    V = V / smcd.fem.AS.area * 1e-3;
    this.V{idx} = V;
    
end
