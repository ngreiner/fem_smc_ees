function from_struct(this, struc)
	% Set the Soma from the input struct.
    %
    % Usage:
    %   soma.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with same fields as the Soma.
    this.diam = struc.diam;
    this.coords = struc.coords;
    this.V = struc.V;
end
