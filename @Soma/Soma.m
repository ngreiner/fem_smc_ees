classdef Soma < handle

    % Properties defined here:
    %
    %   diam : double :
    %       Diameter of the Soma (in um).
    %
    %   coords : 1*3 double :
    %       Coordinates of the Soma (in mm).
    %
    %   V : K*1 cell :
    %       Electric potentials obtained for each of K active sites.
    
    properties
        diam        % double 
        coords      % 1*3 double
        V           % K*1 cell
    end
    
    methods
        function this = Soma()
        end
    end

    methods
        from_struct(this, struc)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx)
        struc = to_struct(this)
        translate(this, u)
    end

end
