function struc = to_struct(this)
	% Return a matlab struct equivalent to the Soma.
    %
    % Usage:
    %   struc = soma.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the Soma.
    struc.diam = this.diam;
    struc.coords = this.coords;
    struc.V = this.V;
end
