function translate(this, u)
    % Translate the Soma.
    %
    % Usage:
    %   soma.translate()
    %
    % Parameters:
    %   u : 1*3 double : translation vector.
    this.coords = this.coords + u;
end
