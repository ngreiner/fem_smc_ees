function interp_V(this, smcd, perm)
    % Interpolate the electric potential at the DendSecs of the
    % DendTree using the Comsol Model attached to the smcd.
    %
    % Usage:
    %   dtree.interp_V(smcd, perm)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Work is delegated to homonymous method of DendSec class.    
    for i = 1 : size(this.dsecs)
        this.dsecs{i}.interp_V(smcd, perm);
    end
end
