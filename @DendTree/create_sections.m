function create_sections(this)
    % Create the dendritic sections composing the dendritic tree.
    %
    % Usage:
    %   dtree.create_sections()
    %
    % Description:
    %   The dendritic tree's sections are created after a swc file (see
    %   http://www.neuronland.org/NLMorphologyConverter/...
    %                                   ...MorphologyFormats/SWC/Spec.html
    %   for the swc format specifications).
    %   The package +swc provides the functions to read the swc file and
    %   exploit its enclosed information.
    %   We don't go into the details of how the branches and bifurcations
    %   are retrieved from the swc file: patient inspection of the code
    %   below and of the +swc package functions will bring answers to the
    %   onlookers.
    %   Let us only mention that:
    %       1. At a bifurcation, which always result in the splitting of a
    %          parent branch into exactly two children branches, the two
    %          children branches, represented by DendSec objects, become
    %          left and right children of their parent branch, represented
    %          by yet another DendSec object.
    %       2. There is a special case where the first sample of a child
    %          branch is attached to the first sample of a stem branch: in
    %          those instances, the child DendSec is appended to the array
    %          of 'siblings' of the concerned stem branch. 

    ndend = 1;

    [sids, stypes, xs, ys, zs, ds, pids] = swc.read_file(this.swc_file, this.diam);

    % Dendritic sections are constructed by considering the stems
    % sequentially.
    for i = 1 : size(sids, 1)
        dsid = sids(i);
        stype = stypes(i);
        pid = pids(i);

        % The initial samples of the dendritic stems (stype=3) are
        % attached to the soma (pid=1).
        if (stype ~= 3) || (pid ~= 1)
            continue
        end

        % Retrieve branches of current dendrite.
        brs = swc.get_branches(dsid, pids);

        % We need to link the branches to their corresponding dendritic
        % section and the samples id to their corresponding branch.
        br2sec = cell(0, 2);
        sid2br = cell(0, 2);

        % Branch 1 is treated separately: it is the dendrite stem.
        br0 = brs{1};
        name = sprintf('dend%d_0', ndend);
        stem = DendSec('name', name);  % create stem
        stem.coords = [xs(br0), ys(br0), zs(br0)];
        stem.ds = ds(br0);
        sid2br{end + 1, 1} = dsid;  % link first sid of branch to branch
        sid2br{end, 2} = br0;
        sid2br{end + 1, 1} = br0(end);  % link last sid of branch to branch
        sid2br{end, 2} = br0;
        br2sec{end + 1, 1} = br0;  % store tuple (br0, stem) in index
        br2sec{end, 2} = stem;

        this.dsecs{end + 1, 1} = stem;

        % Subsequent branches are treated sequentially.
        for j = 2 : size(brs, 1)
            br = brs{j};
            pbr = sid2br{cellfun(@(x) x == br(1), sid2br(:, 1)), 2};
            psec = br2sec{cellfun(@(x) isequal(x, pbr), br2sec(:, 1)), 2};
            % Derive name of new section.
            % If it is a sibling of a stem, its name is obtained by
            % replacing the last character ('0') of the stem's name by
            % the current number of siblings of the stem plus 1. 
            if pids(br(1)) == br0(1)
                name = [psec.name(1 : end - 1), num2str(size(psec.siblings, 1) + 1)];
            % If the parent section of the new section does not have a
            % lchild yet, then the new section is its lchild and its
            % name is obtained by appending a 0 at the end of its
            % parent's name. 
            elseif isempty(psec.lchild)
                name = [psec.name, '0'];
            % Otherwise the new section is the rchild of its parent
            % section, and its name is obtained by appending a 1 at the
            % end of its parent's name. 
            else
                name = [psec.name, '1'];
            end
            % Create new section.
            csec = DendSec('par', psec, 'name', name);
            csec.coords = [xs(br), ys(br), zs(br)];
            csec.ds = ds(br);
            sid2br{end + 1, 1} = br(end);  % link last sid of branch to branch
            sid2br{end, 2} = br;
            % Connect new section to its parent section.
            if br(1) == br0(1)  % current section is a sibling of its parent
                psec.siblings{end + 1, 1} = csec;
            elseif isempty(psec.lchild)  % parent section doesn't have a left child yet
                psec.lchild = csec;
            elseif isempty(psec.rchild)  % parent section already has a left child
                psec.rchild = csec;
            else
                error('Problem: too much children.')
            end
            br2sec{end + 1, 1} = br;
            br2sec{end, 2} = csec;
            
            this.dsecs{end + 1, 1} = csec;
            
        end
            
        % Increment dendrite counter.
        ndend = ndend + 1;
        
    end

end
