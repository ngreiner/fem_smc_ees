function stems = get_stems(this)
    % Return a cell-array of references to the  stems of the DendTree.
    %
    % Usage:
    %   stems = dtree.get_stems()
    %
    % Return:
    %   stems : K*1 cell : DendSec objects.
    %
    % Description:
    %   The stems are those dsecs who does not have any parent, hence
    %   dsec.par is empty.
    stems = this.dsecs(cellfun(@(x) isempty(x.par), this.dsecs));
end
