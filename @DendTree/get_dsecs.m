function dsecs = get_dsecs(this, depth)
    % Return a cell-array of references to the DendSecs down to the input
    % depth.
    %
    % Usage:
    %   dsecs = dtree.get_dsecs()
    %
    % Parameters:
    %   depth : integer : depth until which to fetch the DendSecs.
    %
    % Return:
    %   dsecs : K*1 cell of DendSec objects.
    stems = this.get_stems();
    sibs = cellfun(@(x) x.siblings, stems, 'UniformOutput', false);
    tmp = vertcat(stems, sibs{:});
    dsecs = tmp;
    for i = 1 : depth
        ldsecs = cellfun(@(x) x.lchild, tmp, 'UniformOutput', false);
        rdsecs = cellfun(@(x) x.rchild, tmp, 'UniformOutput', false);
        tmp = [ldsecs; rdsecs];
        tmp = tmp(~cellfun(@isempty, tmp));
        dsecs = [dsecs; tmp];
    end
end
