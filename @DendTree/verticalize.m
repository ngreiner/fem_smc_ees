function verticalize(this)
    % Verticalize the DendTree.
    %
    % Usage:
    %   dtree.verticalize()
    %
    % Description:
    %   See homonymous method of DendSec class.
    cellfun(@(x) x.verticalize(), this.get_stems());    
end
