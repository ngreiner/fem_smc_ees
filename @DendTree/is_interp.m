function outbool = is_interp(this, AS_idx)
    % Return a logical indicating whether the DendTree is already well
    % interpolated for the input AS_idx.
    %
    % outbool = dtree.is_interp(AS_idx)
    %
    % Parameters:
    %   AS_idx : double : active site index.
    %
    % Return:
    %   outbool : logical.
    %
    % Description:
    %   The DendTree is well interpolated if all its DendSecs are.
    outbool = all(cellfun(@(x) x.is_interp(AS_idx), this.dsecs));          
end
