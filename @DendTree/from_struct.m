function from_struct(this, struc)
	% Set the DendTree from the input struct.
    %
    % Usage:
    %   dtree.from_struct(struc)
    %
    % Parameters:
    %   struc: struct: with same fields as the DendTree.
    this.diam = struc.diam;
    this.swc_file = struc.swc_file;
    if isfield(struc, 'stems')
        stems = cell(size(struc.stems));
        for i = 1 : size(struc.stems, 1)
            stems{i} = DendSec('name', sprintf('dend%d_0', i));
            stems{i}.from_struct(struc.stems{i});
        end
        offs = cellfun(@(x) x.get_offspring(), stems, 'UniformOutput', false);
        this.dsecs = vertcat(offs{:});
    end
end
