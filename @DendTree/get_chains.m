function chains = get_chains(this, chains)
    % Return a cell-array of unbranching chains of DendSecs.
    %
    % Usage:
    %   chains = dtree.get_chains()
    %   chains = dtree.get_chains(chains)
    %
    % Parameters:
    %   chains: K*1 cell: already formed chains.
    %
    % Description:
    %   The chains are constructed recursively starting from the stems.
    %       1. each stem constitutes the first link of a chain;
    %       2. their left children are added to these initial chains;
    %       3. their right children initiate new chains.
    %   The process at rank #n is repeated by considering the last links of
    %   all chains formed at rank #n-1:
    %       1. their left children are added to their chains;
    %       2. their right children initiate new chains.
    
    if nargin == 1
        dsecs = this.get_stems();
        chains = cellfun(@(x) {x}, dsecs, 'UniformOutput', false);
    end
    
    newchains = {};
    for i = 1 : size(chains, 1)
        last_link = chains{i}{end};
        if ~ isempty(last_link.lchild)
            chains{i}{end + 1, 1} = last_link.lchild;
        end
        if ~ isempty(last_link.rchild)
            newchains{end + 1, 1} = {last_link.rchild};
        end
        for j = 1 : size(last_link.siblings, 1)
            newchains{end + 1, 1} = last_link.siblings(j);
        end
    end
    
    if ~ isempty(newchains)
        chains = [chains; newchains];
    end
    
    if any(cellfun(@(x) ~isempty(x{end}.lchild), chains))
        chains = this.get_chains(chains);
    end
    
end
