function rotate(this, ax, ay, az)
    % Rotate the dendritic tree.
    %
    % Usage:
    %   dtree.rotate()
    %
    % Parameters:
    %   ax, ay, az : rotation angles around the x, y and z axes.
    %
    % Description:
    %   Rotates each dendritic section.
    cellfun(@(x) x.rotate(ax, ay, az), this.dsecs);
end
