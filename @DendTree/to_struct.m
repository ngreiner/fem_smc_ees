function struc = to_struct(this)
	% Return a matlab struct equivalent to the DendTree.
    %
    % Usage:
    %   struc = dtree.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the DendTree. 
    struc.diam = this.diam;
    struc.swc_file = this.swc_file;
    if ~ isempty(this.dsecs)
        stems = this.get_stems();
        struc.stems = cellfun(@(x) x.to_struct(), stems, 'UniformOutput', false);
    end
end
