classdef DendTree < handle

    % Properties defined here:
    %
    %   diam : double :
    %       Diameter of the soma of the motoneuron to which the DendTree
    %       belongs (in um).
    %
    %   swc_file : string :
    %       Path to .swc file describing the DendTree.
    %
    %   dsecs : K*1 cell :
    %       DendSec objects composing the DendTree.
    
    properties
        diam        % double
        swc_file    % string
        dsecs       % K*1 cell
    end
    
    methods
        function this = DendTree()
        end
    end

    methods
        create_sections(this)
        chains = get_chains(this, chains)
        dsecs = get_dsecs(this, depth)
        from_struct(this, struc)
        stems = get_stems(this)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx)
        rotate(this, ax, ay, az)
        struc = to_struct(this)
        translate(this, u)
        verticalize(this)
    end

end
