function translate(this, u)
    % Translate the DendTree.
    %
    % Usage:
    %   dtree.translate()
    %
    % Parameters:
    %   u : 1*3 double : translation vector.
    %
    % Description: 
    %   Translates all the dendritic sections.
    cellfun(@(x) x.translate(u), this.dsecs);
end
