function struc = to_struct(this)
	% Return a matlab struct equivalent to the DRFiber.
    %
    % Usage:
    %   struc = dr.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the DRFiber object.
    struc = to_struct@Fiber(this);
    struc.DRbranch = this.DRbranch.to_struct();
end
