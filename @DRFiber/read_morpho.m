function read_morpho(this, folder)
	% Read the DRFiber morphology information from textfile.
    %
    % Usage:
    %   dr.read_morpho(folder)
    %
    % Parameters:
    %   folder : string : path to DRFiber folder.
    %
    % Description:
    %   The textfile from which to read is found in the input folder at
    %   'morpho.txt'. It is organized as follows:
    %       1. diam
    %       2. nN
    
    % Read file.
    file_path = fullfile(folder, 'morpho.txt');
    fileID = fopen(file_path);
    morpho = textscan(fileID, '%s');
    fclose(fileID);

    % Assign appropriate attributes.
    this.DRbranch = Branch();
    this.DRbranch.diam = str2double(morpho{1}{1});
    this.DRbranch.nN = str2double(morpho{1}{2});
    this.DRbranch.nP1 = 2 * (this.DRbranch.nN - 1);
    this.DRbranch.nP2 = 2 * (this.DRbranch.nN - 1);
    this.DRbranch.nI = 6 * (this.DRbranch.nN - 1);
    
end
