function from_struct(this, struc)
	% Set the the DRFiber from the input struct.
    %
    % Usage:
    %   dr.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with the same as the DRFiber.
    from_struct@Fiber(this, struc);
    this.DRbranch = Branch();
    this.DRbranch.from_struct(struc.DRbranch);
end