function set_DRbranch(this, smcd)
    % Set the dorsal root branch 'DRbranch' of the DRFiber.
    %
    % Usage:
    %   dr.set_Dbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The dorsal root branch of the DRFiber runs in the dorsal root
    %   hosting the DRFiber and terminates in the ventral horn of the
    %   grey matter.
    %   The contours materializing the dorsal root and the ventral horn of
    %   the grey matter are contained in the spinal cord model attached to
    %   the input SMCDModel.
    %   The trajectory of the dorsal root branch is simply a cubic spline
    %   running through these contours.
    %   Its diameter is that of the DRFiber.
    %   Its compartments' coordinates are finally distributed along its
    %   trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting DR branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.DRbranch = Branch();
    this.DRbranch.label = 'DRbranch';
    this.DRbranch.model = this.model;

    % The diameter of the DRbranch is the diameter of the  DRfiber.
    this.DRbranch.diam = this.diam;
    
    % Recover root's CS, and build path in the root.
    CS = this.get_DRCS(smcd);
    path = Geom.build_constrained_path(CS, smcd.params.pdens);
    
    % Generate interpolation points in the middle region of the GM and in
    % the ventral horn of the GM.
    P3 = smcd.fem.sc.gen_VQ_points(this.seg, 1);
    if P3(3) > path(1, 3)
        direction = 'below';
    else
        direction = 'above';
    end
    P2 = smcd.fem.sc.gen_MR_point(P3(3), direction);
        
    % Generate full path of DRbranch.
    inter_points = [P3; P2; path];
    np = round(smcd.params.pdens * Geom.get_arc_length(inter_points));
    this.DRbranch.path = Geom.interparc(np, inter_points(:, 1), inter_points(:, 2), inter_points(:, 3), 'verbose', smcd.params.verbose);
    
    % Derive and build branch's compartments' coordinates.
    this.DRbranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.DRbranch.VN = cell(smcd.get_nAS(), 1);
    this.DRbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.DRbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.DRbranch.VI = cell(smcd.get_nAS(), 1);

    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting DR branch. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
