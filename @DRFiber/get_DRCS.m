function CS = get_DRCS(this, smcd)
    % Get and return the DRCS of the DRFiber.
    %
    % Usage:
    %   CS = dr.get_DRCS(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Return:
    %   CS : K*1 cell : each cell is a N*3 double.
    %
    % Description:
    %   'DRCS' = 'Dorsal Root Cross-Sections'.
    %   The DRCS are the cross-sectional contours of the dorsal root
    %   through which the dorsal root branch of the DRFiber shall run.
    %   They are retrieved from the spinal cord model attached to the
    %   SMCDModel passed to the method.

    % Unpack attributes in convenient variables.
    rts = smcd.fem.sc.roots;
    
    % Retrieve root of interest.
    idx = cellfun( @(x) strcmp(x.name, this.seg), rts );
    rt = rts{idx};

    % Retrieve cross-sectional contours.
    CS = rt.LD.full_branches{1}.CS;
    
    % Flip x-coordinates if muscle is right-sided.
    if strcmp(this.musc(end - 1 : end), '_r')
        CS = Geom.flip_x_coords(CS);
    end
    
end
