function write_morpho(this, dir_export, perm)
    % Write the DRFiber morphology information to textfile.
    %
    % Usage:
    %   dr.write_morpho(dir_export)
    %   dr.write_morpho(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string : path to top-level export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   A single textfile is written, the lines of which are:
    %       1. this.diam
    %       2. this.DRbranch.nN
    
    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    folder = fullfile(dir_export, this.musc, this.seg, sprintf('%s%d', class(this), this.fidx));
    file_path = fullfile(folder, 'morpho.txt');

    if perm || ~ exist(file_path, 'file')

        % Write fiber morphology info.
        morpho_text = sprintf('%f\n', this.diam);
        morpho_text = [morpho_text sprintf('%d\n', this.DRbranch.nN)];

        % Write info to file.
        fileID = fopen(file_path, 'w');
        fprintf(fileID, morpho_text);
        fclose(fileID);
        
    end
    
end
