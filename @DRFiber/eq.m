function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = eq@Fiber(obj1, obj2);
    if ~ outbool
        return
    else
        outbool = false;
    end
    
    if ~ (obj1.DRbranch == obj2.DRbranch)
        fprintf('Attributes `DRbranch` are different.\n')
        return
    end
    
    outbool = true;
    
end