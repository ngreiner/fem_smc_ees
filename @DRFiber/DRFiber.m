classdef DRFiber < Fiber

    % Properties defined here:
    %
    %   DRbranch : Branch object :
    %       Dorsal root branch of the DRFiber.
    
    properties
        DRbranch     % Branch object
    end
    
    methods
        function this = DRFiber()
        end
    end
    
    methods
        from_struct(this, struc)
        branches = get_branches(this)
        CS = get_DRCS(this, smcd)
        read_morpho(this, folder)
        set_DRbranch(this, smcd)
        struc = to_struct(this)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
