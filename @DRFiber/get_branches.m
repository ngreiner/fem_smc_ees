function branches = get_branches(this)
    % Return the cell-array of all the branches of the DRFiber.
    %
    % Usage:
    %   branches = dr.get_branches()
    %
    % Return:
    %   branches : K*1 cell : the DRFiber's branches.
    %
    % Description:
    %   The branches are stored as follows in the output array:
    %       1. DRbranch
    branches = {this.DRbranch};
end
