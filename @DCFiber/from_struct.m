function from_struct(this, struc)
	% Set the the DCFiber from the input struct.
    %
    % Usage:
    %   dc.from_struct(struc)
    %
    % Return:
    %   struc : struct :
    %       Fields are the same as the DCFiber class properties.
    from_struct@Fiber(this, struc);
    this.DCbranch = Branch();
    this.cols = cellfun(@(x) Branch(), struc.cols, 'UniformOutput', false);
    this.DCbranch.from_struct(struc.DCbranch);
    cellfun(@(x, y) x.from_struct(y), this.cols, struc.cols, 'UniformOutput', false);
end
