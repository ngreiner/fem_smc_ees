classdef DCFiber < Fiber

    % Properties defined here.
    %
    %   DCbranch : Branch object :
    %       Dorsal columns branch of the DCFiber.
    %
    %   cols : N*1 cell :
    %       Collaterals (Branch objects) of the DCFiber.
    
    properties
        DCbranch    % Branch object
        cols        % N*1 cell
    end
    
    methods
        function this = DCFiber()
        end
    end
    
    methods
        from_struct(this, struc)
        ncols = gen_ncols(this, varargin)
        branches = get_branches(this)
        struc = to_struct(this)
        read_morpho(this, folder)
        set_DCbranch(this, smcd)
        write_morpho(this, dir_export, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
