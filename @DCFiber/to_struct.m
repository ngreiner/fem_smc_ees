function struc = to_struct(this)
	% Return a matlab struct equivalent to the DCFiber.
    %
    % Usage:
    %   struc = dc.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the DCFiber object.
    struc = to_struct@Fiber(this);
    struc.DCbranch = this.DCbranch.to_struct();
    struc.cols = cellfun(@(x) x.to_struct(), this.cols, 'UniformOutput', false);
end
