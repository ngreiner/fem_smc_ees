function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;

    if ~ eq@Fiber(obj1, obj2);
        return
    else
        
    end
    
    if ~ (obj1.DCbranch == obj2.DCbranch)
        fprintf('Attributes `DRbranch` are different.\n')
        return
    end
    
    if ~ all(cellfun(@(x, y) x == y, obj1.cols, obj2.cols))
        fprintf('Attributes `cols` are different.\n')
        return
    end
    
    outbool = true;
    
end