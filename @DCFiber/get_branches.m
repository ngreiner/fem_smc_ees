function branches = get_branches(this)
    % Return the cell-array of all the branches of the DCFiber.
    %
    % Usage:
    %   branches = dcf.get_branches()
    %
    % Return:
    %   branches : K*1 cell : the DCFiber's branches.
    %
    % Description:
    %   The branches are stored as follows in the output array:
    %       1. DCbranch
    %       2. cols{1}
    %       3. cols{2}
    %       4. cols{3}
    %       5. ...
    branches = [{this.DCbranch}; this.cols];
end
