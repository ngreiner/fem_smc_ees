function set_DCbranch(this, smcd)
    % Set the dorsal column branch of the DCcBFiber.
    %
    % Usage:
    %   dc.set_DCbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The dorsal column branch of the DC fiber runs in the dorsal
    %   columns (!!).
    %   The contours materializing the dorsal columns are contained in the
    %   cross-sections of the spinal cord model attached to the input smcd.
    %   They are either the contours of the 'OD' (the outer dorsal columns
    %   compartments) either the contours of the total dorsal columns
    %   compartments (See CrossSection class).
    %   The trajectory of the DC branch is simply a cubic spline running
    %   through these contours.
    %   The coordinates of the DC branch's compartments are finally
    %   distributed along this trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting DC branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.DCbranch = Branch();
    this.DCbranch.label = 'DCbranch';
    this.DCbranch.model = this.model;
    
    % The diameter of the DCbranch is the diameter of the DCcB fiber.
    this.DCbranch.diam = this.diam;
    
    % Recover dorsal columns compartment CS, and build path of DCbranch.
    idx = find(strcmp(smcd.morpho.DCcB.comp, {'OD1'; 'OD2'; 'OD3'; 'OD4'; 'OD5'; 'OD6'}));
    if ~ isempty(idx)
        CS = cellfun(@(x) x.OD{idx}, flipud(smcd.fem.sc.CS), 'UniformOutput', false);
    else
        CS = cellfun(@(x) x.DC.contour, flipud(smcd.fem.sc.CS), 'UniformOutput', false);
    end
    this.DCbranch.path = Geom.build_unconstrained_path(CS, smcd.params.pdens, 'refine_interp');
    
    % Derive branch's compartments' coordinates.
    this.DCbranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.DCbranch.VN = cell(smcd.get_nAS(), 1);
    this.DCbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.DCbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.DCbranch.VI = cell(smcd.get_nAS(), 1);
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting A branch. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
