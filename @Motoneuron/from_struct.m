function from_struct(this, struc)
	% Set the Motoneuron from the input struct.
    %
    % Usage:
    %   mn.from_struct(struc) 
    %
    % Parameters:
    %   struc : struct : with same fields as the Motoneuron.
    this.musc = struc.musc;
    this.seg = struc.seg;
    this.fidx = struc.fidx;
    this.gidx = struc.gidx;
    this.idx_Ias = struc.idx_Ias;
    this.idx_IIs = struc.idx_IIs;
    this.diam = struc.diam;
    this.soma = Soma();
    this.soma.from_struct(struc.soma);
    if isfield(struc, 'dtree')
        this.dtree = DendTree();
        this.dtree.from_struct(struc.dtree);
    end
    if isfield(struc, 'axon')
        this.axon = MotorAxon();
        this.axon.from_struct(struc.axon);
    end
end