function set_soma(this, smcd, coords)
    % Set the soma of the Motoneuron.
    %
    % Usage:
    %   mn.set_soma(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   coords : 1*3 double : soma center coordinates (in mm).
    %
    % Description:
    %   The soma is represented by a single compartment with diameter the
    %   diameter of the Motoneuron.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting soma  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.soma = Soma();
    this.soma.diam = this.diam;
    this.soma.coords = coords;
    this.soma.V = cell(smcd.get_nAS(), 1);

    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting soma. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
