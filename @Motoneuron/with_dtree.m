function outbool = with_dtree(this)
    % Return a logical indicating whether the Motoneuron has a dtree.
    %
    % Usage:
    %   outbool = mn.with_dtree()
    %
    % Return:
    %   outbool : logical.
    outbool = ~ isempty(this.dtree);
end
