function struc = to_struct(this)
	% Return a matlab struct equivalent to the Motoneuron.
    %
    % Usage:
    %   struc = mn.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the Motoneuron.
    struc.musc = this.musc;
    struc.seg = this.seg;
    struc.fidx = this.fidx;
    struc.gidx = this.gidx;
    struc.idx_Ias = this.idx_Ias;
    struc.idx_IIs = this.idx_IIs;
    struc.diam = this.diam;
    struc.soma = this.soma.to_struct();
    if ~ isempty(this.dtree)
        struc.dtree = this.dtree.to_struct();
    end
    if ~ isempty(this.axon)
        struc.axon = this.axon.to_struct();
    end
end
