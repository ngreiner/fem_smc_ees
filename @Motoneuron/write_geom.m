function write_geom(this, dir_export, perm)
    % Write the Motoneuron geometry information to textfiles.
    %
    % Usage:
    %   mn.write_geom(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string : path to top-level export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   A textfile for each of the soma, axon and dtree is written.
    %
    %   For the soma, it is a one line file, with three real values
    %   separated by single whitespaces ' ': the coordinates in mm of the
    %   soma.
    %
    %   For the axon, it is also composed of three columns of real values
    %   separated by single whitespaces ' ', but with multiple rows
    %   organized as:
    %       1. coordinates of the initial segments (first 3 rows)
    %       2. coordinates of the nodes (next nN rows)
    %       3. coordinates of the paranodes1 (next nP1 rows)
    %       4. coordinates of the paranodes2 (next nP2 rows)
    %       5. coordinates of the internodes (next nI rows)
    %       6. a single row: 'nP  NaN  NaN' where nP indicates the number
    %          of points (hence number of rows) composing the path
    %       7. coordinates of the path (next nP rows)
    %
    %   For the dtree, a new swc file is written via a call to the method
    %   write_swc().

    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    % Deal with soma.
    % Soma coordinates file path.
    soma_folder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'soma');
    file_path = fullfile(soma_folder, 'geom.txt');
    % Write coordinates to textfile.
    if perm || ~ exist(file_path, 'file')
        text = sprintf('%f %f %f', (this.soma.coords)');
        fileID = fopen(file_path, 'w');
        fprintf(fileID, text);
        fclose(fileID);
        fprintf('Writing `geom.txt` at `%s`.\n', soma_folder)
    end
    
    % Deal with axon.
    if ~ isempty(this.axon)
        % Axon coordinates file path.
        axon_folder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'axon');
        file_path = fullfile(axon_folder, 'geom.txt');
        % Write coordinates to textfile.
        axon = this.axon;
        if perm || ~ exist(file_path, 'file')
            geom_text = '';
            % Write inisegs, nodesCNS, paranodes1CNS, paranodes2CNS,
            % internodesCNS, nodesPNS, paranodes1PNS, paranodes2PNS and
            % internodesPNS' coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.IScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.NCNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.P1CNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.P2CNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.ICNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.NPNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.P1PNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.P2PNScoords)')];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.IPNScoords)')];
            % Write line indicating the number of points in the path and
            % write path coordinates.
            geom_text = [geom_text sprintf('%d NaN NaN\n', size(axon.path, 1))];
            geom_text = [geom_text sprintf('%f %f %f\n', (axon.path)')];
            % Write textfile.
            fileID = fopen(file_path, 'w');
            fprintf(fileID, geom_text);
            fclose(fileID);
            % Console message.
            fprintf('Writing ''geom.txt'' at ''%s''.\n', axon_folder)
        end
    end
    
    % Deal with dendtree. Geom textfile of dtree is a new SWC file.
    if ~ isempty(this.dtree)
        % SWCfile path.
        dtree_folder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'dtree');
        [~, swc_file_name, ~] = fileparts(this.dtree.swc_file);
        file_path = fullfile(dtree_folder, sprintf('%s.swc', swc_file_name));
        % Write file.
        if perm || ~ exist(file_path, 'file')
            % Write text.
            geom_text = this.write_swc();
            % Write textfile.
            fileID = fopen(file_path, 'w');
            fprintf(fileID, geom_text);
            fclose(fileID);
            % Console message.
            fprintf('Writing ''%s.swc'' at ''%s''.\n', swc_file_name, dtree_folder)
        end
    end

end
