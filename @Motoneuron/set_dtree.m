function set_dtree(this, smcd)
    % Set the DendTree (attribute dtree) of the Motoneuron.
    %
    % Usage:
    %   mn.set_dtree(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The dendritic tree of the Motoneuron is built from a swc file (see
    %   DendTree class).
    %   This swc file is chosen at random from a collection held by the
    %   input SMCDModel.
    %   The original motoneuron diameter embedded in the selected swc file
    %   is overriden by the diameter of the Motoneuron (which should be
    %   defined before calling the present method) and the dendritic tree
    %   constructed accordingly.
    %   Once constructed from the swc file, the dendritic tree is rotated
    %   at random, verticalized, and translated at the position of the
    %   Motoneuron soma.
    
    if smcd.params.verbose
        fprintf(Utils.padn('  Setting axon  ', 'both', '-', 70))
        t0 = cputime;
    end
    
    % Initialize DendTree.
    this.dtree = DendTree();
    idx = randi(size(smcd.morpho.MN.swc_files, 1));
    this.dtree.swc_file = smcd.morpho.MN.swc_files{idx};
    
    % The diameter of the DendTree is the diameter of the MN.
    this.dtree.diam = this.diam;
    
    % Build sections from swc file, rotate and translate.
    this.dtree.create_sections();
    this.dtree.rotate(rand(1) * 360, rand(1) * 360, rand(1) * 360);
    this.dtree.translate(this.soma.coords);
    this.dtree.verticalize();
    
    % Initialize cell-arrays for axon's compartments potentials.
    for i = 1 : size(this.dtree.dsecs, 1)
        this.dtree.dsecs{i}.V = cell(smcd.get_nAS(), 1);
    end
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting axon. (Time spent: %f s.)  ', cputime-t0), 'both', '-', 70))
    end

end
