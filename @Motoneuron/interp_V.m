function interp_V(this, smcd, perm)
    % Interpolate the electric potential at the Motoneuron compartments.
    %
    % Usage:
    %   mn.interp_V(smcd, perm)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Work is delegated to homonymous methods of Soma, DendTree and
    %   MotorAxon classes (the two latter are called if and only if the
    %   Motoneuron's dtree and axon are non-empty). 
    
    fprintf('BEGIN: interp_V. %s #%d | musc: %s | seg: %s | AS_idx #%d ...\n', class(this), this.fidx, this.musc, this.seg, smcd.fem.AS.idx) 
    t0 = cputime;
    
    % Call Soma method.
    this.soma.interp_V(smcd, perm);
        
    % Call DendTree method if Motoneuron has a DendTree.
    if ~ isempty(this.dtree)
        this.dtree.interp_V(smcd, perm);
    end
        
    % Call MotorAxon method if Motoneuron has a MotorAxon.
    if ~ isempty(this.axon)
        this.axon.interp_V(smcd, perm);
    end
    
    fprintf('END. (Time spent: %d s.)\n', cputime - t0)

end
