function read_geom(this, folder)
	% Read the Motoneuron geometry information from textfile.
    %
    % Usage:
    %   mn.read_geom(folder)
    %
    % Parameters:
    %   folder : str : path to Motoneuron folder.
    %
    % Description:
    %   Not implemented.
end
