function write_V(this, dir_export, perm)
    % Export the Motoneuron electric potentials information to textfiles.
    %
    % Parameters:
    %   dir_export : string : path to top-level export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   Textfiles describing the electric potentials of each of the soma,
    %   axon and dtree are written separately, and this for several
    %   active sites.
    %
    %   For the soma, the textfiles for each active site contain a single
    %   real value: the value at the soma.
    %
    %   For the axon, the textfiles for each active site contain a single
    %   line, with real values separated by single whitespaces ' ' giving
    %   the potentials at each compartment of the axon as:
    %       VIS(1) VIS(2) VIS(3) VP1CNS(1) VP2CNS(1) VICNS(1) ... 
    %                                               VP1PNS(end) VNNS(end)
    %   
    %   For the dtree, the electric potentials of each DendSec is exported
    %   along with its coordinates, to enable later interpolation at
    %   arbitrary positions in the vicinity of the DendSecs.
    %   Thus the textfiles for each active site are composed of 4 columns
    %   of real values, the first 3 of which are coordinates (in um), and
    %   the 4th column the corresponding electric potentials.
    %   The coordinates are expressed in reference to a soma positioned at
    %   (0, 0, 0).
    
    if nargin == 2
        perm = 1;
    end

    % Deal with soma.
    % Make potentials directory if necessary.
    Vfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'soma', 'potentials');
    if ~ exist(Vfolder, 'dir')
        mkdir(Vfolder);
    end
    % Loop over the active sites.
    for i = 1 : size(this.soma.V, 1)
        % Check if AS file shall be built.
        file_path = fullfile(Vfolder, sprintf('elec%d.txt', i));
        makeAS = this.soma.is_interp(i) && (perm || ~ exist(file_path, 'file'));
        if ~ makeAS
            continue
        end
        % Write file.
        fileID = fopen(file_path, 'w');
        fprintf(fileID, '%f', this.soma.V{i});
        fclose(fileID);
        % Console message.
        fprintf('Writing ''elec%d.txt'' at ''%s''.\n', i, Vfolder)
    end

    % Deal with axon.
    if ~ isempty(this.axon)
        % Make potentials directory if necessary.
        Vfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'axon', 'potentials');
        if ~ exist(Vfolder, 'dir')
            mkdir(Vfolder);
        end
        % Unpack axon.
        axon = this.axon;
        % Loop over the active sites.
        for i = 1 : size(axon.VNCNS, 1)
            % Check if AS file shall be built.
            file_path = fullfile(Vfolder, sprintf('elec%d.txt', i));
            makeAS = axon.is_interp(i) && (perm || ~ exist(file_path, 'file'));
            if ~ makeAS
                continue
            end
            % Unpack potentials.
            VIS = axon.VIS{i};
            VNCNS = axon.VNCNS{i};
            VP1CNS = axon.VP1CNS{i};
            VP2CNS = axon.VP2CNS{i};
            VICNS = axon.VICNS{i};
            VNPNS = axon.VNPNS{i};
            VP1PNS = axon.VP1PNS{i};
            VP2PNS = axon.VP2PNS{i};
            VIPNS = axon.VIPNS{i};
            % Write potentials.
            AStext = sprintf('%f %f %f ', VIS(1), VIS(2), VIS(3));
            for n = 1 : axon.nNCNS
                AStext = [AStext sprintf('%f ', VP1CNS(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VP2CNS(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 3))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 4))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 5))];
                AStext = [AStext sprintf('%f ', VICNS(6 * (n - 1) + 6))];
                AStext = [AStext sprintf('%f ', VP2CNS(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VP1CNS(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VNCNS(n))];
            end
            for n = 1 : axon.nNPNS
                AStext = [AStext sprintf('%f ', VP1PNS(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VP2PNS(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 3))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 4))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 5))];
                AStext = [AStext sprintf('%f ', VIPNS(6 * (n - 1) + 6))];
                AStext = [AStext sprintf('%f ', VP2PNS(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VP1PNS(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VNPNS(n))];
            end
            AStext = [AStext '\n'];
            % Write file.
            fileID = fopen(file_path, 'w');
            fprintf(fileID, AStext);
            fclose(fileID);
            % Console message.
            fprintf('Writing ''elec%d.txt'' at ''%s''.\n', i, Vfolder)
        end
    end

    % Deal with dendtree.
    if ~ isempty(this.dtree)
        % Make potentials directory if necessary.
        Vfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'dtree', 'potentials');
        if ~ exist(Vfolder, 'dir')
            mkdir(Vfolder);
        end
        % Unpack dendtree.
        dtree = this.dtree;
        % Loop over the active sites.
        for i = 1 : size(dtree.dsecs{1}.V, 1)
            % Check if AS file shall be built.
            file_path = fullfile(Vfolder, sprintf('elec%d.txt', i));
            makeAS = this.is_interp(i) && (perm || ~ exist(file_path, 'file'));
            if ~ makeAS
                continue
            end
            AStext = [];
            % Write potentials together with geometric coordinates of the center of mass
            % of each dendsec of the dendtree.
            for j = 1 : size(dtree.dsecs, 1)
                dsec = dtree.dsecs{j};
                com = (mean(dsec.coords, 1) - this.soma.coords) * 1e3;
                AStext = [AStext sprintf('%f %f %f %f\n', com(1), com(2), com(3), dsec.V{i})];
            end
            % Write file.
            fileID = fopen(file_path, 'w');
            fprintf(fileID, AStext);
            fclose(fileID);
            % Console message.
            fprintf('Writing ''elec%d.txt'' at ''%s''.\n', i, Vfolder)
        end
    end

end
