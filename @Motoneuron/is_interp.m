function outbool = is_interp(this, AS_idx)
    % Return a logical indicating whether the Motoneuron is already well
    % interpolated for the input AS_idx.
    %
    % Usage:
    %   outbool = mn.is_interp(AS_idx)
    %
    % Parameters:
    %   AS_idx : double.
    %
    % Return:
    %   outbool : logical.
    %
    % Description:
    %   The Motoneuron is well interpolated if its soma, axon and dtree
    %   are. 
    %   If the Motoneuron doesn't possess an axon or a dtree, the criteria
    %   are relaxed for the structures at concern.
    
    outbool = this.soma.is_interp(AS_idx);
    
    if ~ isempty(this.axon)
        outbool = outbool && this.axon.is_interp(AS_idx);
    end
    
    if ~ isempty(this.dtree)
        outbool = outbool && this.dtree.is_interp(AS_idx);
    end
    
end
