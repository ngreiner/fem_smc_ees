function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if ~ strcmp(obj1.musc, obj2.musc)
        fprintf('Attributes `musc` are different.\n')
        return
    end
    
    if ~ strcmp(obj1.seg, obj2.seg)
        fprintf('Attributes `seg` are different.\n')
        return
    end
    
    if ~ (obj1.fidx == obj2.fidx)
        fprintf('Attributes `fidx` are different.\n')
        return
    end
    
    if ~ (obj1.gidx == obj2.gidx)
        fprintf('Attributes `gidx` are different.\n')
        return
    end
    
    if ~ (abs(obj1.diam-obj2.diam) < 1e-6)
        fprintf('Attributes `diam` are different.\n')
        fprintf('%s | %s | %d\n', obj1.musc, obj1.seg, obj1.idx)
        return
    end
    
    if ~ isequal(obj1.soma, obj2.soma)
        fprintf('Attributes `soma` are different.\n')
        return
    end
    
    if ~ isequal(obj1.dtree, obj2.dtree)
        fprintf('Attributes `dtree` are different.\n')
        return
    end
    
    if ~ isequal(obj1.axon, obj2.axon)
        fprintf('Attributes `axon` are different.\n')
        return
    end
    
    outbool = true;
    
end
