function make_folder(this, dir_export)
    % Make an export folder for the Motoneuron.
    %
    % Usage:
    %   mn.make_folder(dir_export)
    %
    % Parameters:
    %   dir_export : string : path to global export directory.
    %
    % Description:
    %   A Motoneuron is rarely exported alone, but rather along with an
    %   entire sensorimotor circuit. Thus, motoneurons are regrouped by
    %   muscle-segment pairs.
    %   Within the top-level export directory of the sensorimotor circuit,
    %   we thus find muscle folders, and within the muscle folders, segment 
    %   folders: there we find Motoneuron folders.
    %   Motoneuron folders' names take the form 'MNx', where x is the
    %   family index of the Motoneuron.
    %   A Motoneuron folder contains subfolders for its soma, axon and
    %   dtree.
    %   This method creates all these folders if they do not yet exist.
    
    if ~ exist(dir_export, 'dir')
        mkdir(dir_export);
    end
    
    muscfolder = fullfile(dir_export, this.musc);
    if ~ exist(muscfolder, 'dir')
        mkdir(muscfolder);
    end
    
    segfolder = fullfile(dir_export, this.musc, this.seg);
    if ~ exist(segfolder, 'dir')
        mkdir(segfolder);
    end
    
    MNfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx));
    if ~ exist(MNfolder, 'dir')
        mkdir(MNfolder);
    end
    
    soma_folder = fullfile(MNfolder, 'soma');
    if ~ exist(soma_folder, 'dir')
        mkdir(soma_folder);
    end
    
    if ~ isempty(this.axon)
        axon_folder = fullfile(MNfolder, 'axon');
        if ~ exist(axon_folder, 'dir')
            mkdir(axon_folder);
        end
    end
    
    if ~ isempty(this.dtree)
        dtree_folder = fullfile(MNfolder, 'dtree');
        if ~ exist(dtree_folder, 'dir')
            mkdir(dtree_folder);
        end
    end
    
end
