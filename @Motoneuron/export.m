function export(this, dir_export, perm)
    % Export the Motoneuron information to textfiles.
    %
    % Usage:
    %   mn.export(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string : path to export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   See methods used below.
    this.make_folder(dir_export);
    this.write_geom(dir_export, perm);
    this.write_morpho(dir_export, perm);
    this.write_V(dir_export, perm);
end
