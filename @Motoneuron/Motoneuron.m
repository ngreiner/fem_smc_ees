classdef Motoneuron < handle

    % Properties defined here:
    %
    %   musc : string :
    %       Name of muscle to which the Motoneuron belongs.
    %
    %   seg : string :
    %       Name of segment in which the Motoneuron is located.
    %
    %   fidx : integer :
    %       Index of Motoneuron in the family (muscle, seg).
    %
    %   gidx : integer :
    %       Global index of Motoneuron in a SMCD.
    %
    %   idx_Ias : N*1 integer :
    %       Indexes of the IaFibers innervating the Motoneuron.
    %
    %   idx_IIs : N*1 integer :
    %       Indexes of the IIFibers innervating the Motoneuron.
    %
    %   diam : double :
    %       Diameter of the soma of the Motoneuron (in um).
    %
    %   soma : Soma object :
    %       Describing the soma of the Motoneuron.
    %
    %   dtree : DendTree object :
    %       Describing the dendritic tree of the Motoneuron.
    %
    %   axon : MotorAxon object :
    %       Describing the axon of the Motoneuron.
    
    properties
        musc        % string
        seg         % string
        fidx        % integer
        gidx        % integer
        idx_Ias     % N*1 double
        idx_IIs     % M*1 double
        diam        % double
        soma        % Soma object
        dtree       % DendTree object
        axon        % MotorAxon object
    end
    
    methods
        function this = Motoneuron()
        end
    end
    
    methods
        export(this, dir_export, perm)
        from_struct(this, struc)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx, varargin)
        make_folder(this, dir_export)
        read_geom(this, folder)
        read_morpho(this, folder)
        read_V(this, folder, nAS)
        set_axon(this, smcd)
        set_dtree(this, smcd)
        set_soma(this, smcd, coords)
        struc = to_struct(this)
        outbool = with_axon(this)
        outbool = with_dtree(this)
        write_geom(this, dir_export, perm)
        write_morpho(this, dir_export, perm)
        write_V(this, dir_export, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
