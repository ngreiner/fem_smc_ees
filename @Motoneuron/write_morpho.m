function write_morpho(this, dir_export, perm)
    % Writes the Motoneuron morphology information to textfiles.
    %
    % Usage:
    %   mn.write_morpho(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string : path to top-level export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   Two textfiles are written:
    %       1 giving the diameter of the Motoneuron;
    %       1 giving the diameter and number of nodes of its axon.

    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    % Deal with Motoneuron's global morphology.
    % Morphology file path.
    MNfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx));
    file_path = fullfile(MNfolder, 'morpho.txt');
    % Morphology to textfile.
    if perm || ~ exist(file_path, 'file')
        fileID = fopen(file_path, 'w');
        fprintf(fileID, '%f', this.diam);
        fclose(fileID);
        % Console message.
        fprintf('Writing ''morpho.txt'' at ''%s''.\n', MNfolder)
    end
    
    % Deal with axon.
    if ~ isempty(this.axon)
        % Axon morphology file path.
        axonfolder = fullfile(dir_export, this.musc, this.seg, sprintf('MN%d', this.fidx), 'axon');
        file_path = fullfile(axonfolder, 'morpho.txt');
        % Axon morphology to textfile.
        axon = this.axon;
        if perm || ~ exist(file_path, 'file')
            morpho_text = '';
            morpho_text = [morpho_text, sprintf('%f\n', axon.diam)];
            morpho_text = [morpho_text, sprintf('%d\n', axon.nNCNS + axon.nNPNS)];
            fileID = fopen(file_path, 'w');
            fprintf(fileID, morpho_text);
            fclose(fileID);
            % Console message.
            fprintf('Writing ''morpho.txt'' at `%s`.\n', axonfolder)
        end
    end

end
