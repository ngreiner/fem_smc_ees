function set_axon(this, smcd)
    % Set the axon of the Motoneuron.
    %
    % Usage:
    %   mn.set_axon(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The axon of the Motoneuron runs in the ventral root of the segment
    %   hosting the Motoneuron.
    %   The contours materializing the ventral root are contained in the
    %   spinal cord model attached to the input SMCDModel.
    %   The trajectory of the axon is simply a cubic spline running through
    %   these contours.
    %   Its diameter is derived from the diameter of the Motoneuron by
    %   linear scaling, using the parameters of the SMCDModel.
    %   Its compartments' coordinates are finally distributed along its
    %   trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting axon  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.axon = MotorAxon();
    this.axon.label = 'Axon';
    this.axon.model = smcd.morpho.MA.model;
    
    % The diameter of the axon is linearly scaled  from the MN diameter.
    MN_M = smcd.morpho.MN.max_diam;
    MN_m = smcd.morpho.MN.min_diam;
    MA_M = smcd.morpho.MA.max_diam;
    MA_m = smcd.morpho.MA.min_diam;
    this.axon.diam = (MA_M - MA_m) / (MN_M - MN_m) * (this.diam - MN_m) + MA_m;
    
    % Recover ventral root's CS, and build path of axon.
    CS = this.get_VRCS(smcd);
    this.axon.path = Geom.build_constrained_path(CS, smcd.params.pdens, 'starting_point', this.soma.coords, 'refine_interp');
    
    % Derive and build axon's compartments' coordinates.
    this.axon.build_comps_coords();
    
    % Initialize cell-arrays for axon's compartments potentials.
    this.axon.VIS = cell(smcd.get_nAS(), 1);
    this.axon.VNCNS = cell(smcd.get_nAS(), 1);
    this.axon.VP1CNS = cell(smcd.get_nAS(), 1);
    this.axon.VP2CNS = cell(smcd.get_nAS(), 1);
    this.axon.VICNS = cell(smcd.get_nAS(), 1);
    this.axon.VNPNS = cell(smcd.get_nAS(), 1);
    this.axon.VP1PNS = cell(smcd.get_nAS(), 1);
    this.axon.VP2PNS = cell(smcd.get_nAS(), 1);
    this.axon.VIPNS = cell(smcd.get_nAS(), 1);

    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting axon. (Time spent: %f s.)  ', cputime-t0), 'both', '-', 70))
    end

end
