function outbool = with_axon(this)
    % Return a logical indicating whether the Motoneuron has an axon.
    %
    % Usage:
    %   outbool = mn.with_axon()
    %
    % Return:
    %   outbool : logical.
    outbool = ~ isempty(this.axon);
end
