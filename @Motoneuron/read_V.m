function read_V(this, folder, nAS)
	% Read the Motoneuron electric potentials information from textfiles.
    %
    % Usage:
    %   mn.read_V(folder, nAS)
    %
    % Parameters:
    %   folder : string :
    %       Path to Motoneuron folder.
    %
    %   nAS : integer :
    %       Total number of active sites to store the potentials from.
    %
    % Description:
    %   Not implemented.
end
