function CS = get_VRCS(this, smcd)
    % Get and return the VRCS of the Motoneuron.
    %
    % Usage:
    %   CS = mn.get_VRCS(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Return:
    %   CS : K*1 cell : each cell is a a N*3 double.
    %
    % Description:
    %   'VRCS' = 'Ventral Root Cross-Sections'.
    %   The VRCS are the cross-sectional contours of the ventral root
    %   through which the axon of the Motoneuron shall run.
    %   They are retrieved from the spinal cord model attached to the
    %   SMCDModel passed to the method.


    % Unpack attributes in convenient variables.
    rts = smcd.fem.sc.roots;
    
    % Retrieve root of interest.
    idx = cellfun( @(x) strcmp(x.name, this.seg), rts );
    rt = rts{idx};

    % Retrieve cross-sectional contours.
    CS = rt.LV.full_branches{1}.CS;
    
    if strcmp(this.musc(end - 1 : end), '_r')
        CS = Geom.flip_x_coords(CS);
    end
    
end
