function read_morpho(this, folder)
    % Read the Motoneuron morphology information from textfile.
    %
    % Usage:
    %   mn.read_morpho(folder)
    %
    % Parameters:
    %   folder : str : path to Motoneuron folder.
    %
    % Description:
    %   Not implemented.
end
