function text = write_swc(this)
    % Compose and return a swc-formatted string describing the Motoneuron's
    % dendritic tree.
    %
    % Usage:
    %   text = mn.write_swc()
    %
    % Return:
    %   text : string :
    %       swc-formatted descriptor of the Motoneuron's dendritic tree.
    %
    % Description:
    %   The algorithm implemented here is reciprocal to the algorithm
    %   implemented in the method create_sections of the DendTree class.
    %   The collection of interconnected (via parenthood relationships)
    %   DendSecs composing the Motoneuron's dendritic tree is sequentially
    %   converted in a series of samples enclosing: type, coordinates,
    %   radius, and sample-parenthood information.
    %   The coordinates are expressed in um and in reference to a soma
    %   positioned at (0, 0, 0).

    lines = {};
    
    % Write lines for soma.
    r = this.diam / 2 * 1e3;
    lines{1, 1} = sprintf('1  1  0.000000  0.000000  0.000000  %f  -1', r);
    lines{2, 1} = sprintf('2  1  0.000000  %f  0.000000  %f  1', r, r);
    lines{3, 1} = sprintf('3  1  0.000000  -%f  0.000000  %f  1', r, r);
    
    % Write lines for dsecs.
    sid = 4;
    coords2sid = cell(0, 2);
    for i = 1 : size(this.dtree.dsecs, 1)
        
        % Unpack dsec.
        dsec = this.dtree.dsecs{i};
        
        % Express dsec's coords
        % (i) in the coordinate system whose origin is at the soma
        % (ii) in um instead of mm
        coords = dsec.coords;
        for j = 1 : size(coords, 1)
            coords(j, :) = (coords(j, :) - this.soma.coords) * 1e3;
        end
        rs = this.dtree.dsecs{i}.ds / 2 * 1e3;
        
        % Dendrite stems need special treatment.
        % Their first sample's pid (parent id) is 1 (=soma), and their
        % first sample's coordinates need be stored in the coords2sid table
        % to be retrieved during the writing of their siblings.
        if isempty(dsec.par)
            coords2sid(end + 1, :) = {coords(1, :), sid};
            lines{sid, 1} = sprintf('%d  3  %f  %f  %f  %f  1', sid, coords(1, :), rs(1));
            sid = sid + 1;
            coords(1, :) = [];
            rs(1) = [];
            while ~ isempty(coords)
                coord = coords(1, :);
                lines{sid, 1} = sprintf('%d  3  %f  %f  %f  %f  %d', sid, coord, rs(1), sid - 1);
                sid = sid + 1;
                coords(1, :) = [];
                rs(1) = [];
            end
            coords2sid(end + 1, :) = {coord, sid - 1};
          
        % Non-stem dendsecs.
        % The id of the parent sample of their first sample need be
        % retrieved in the coords2sid table, and their first sample's
        % coordinates is coords(2,:), not coords(1,:), because coords(1,:)
        % is identical to their first sample's parent sample's coordinates.
        else
            idx = cellfun(@(x) isequal(x, coords(1, :)), coords2sid(:, 1));
            pid = coords2sid{idx, 2};
            coords(1, :) = [];
            rs(1) = [];
            coord = coords(1, :);
            lines{sid, 1} = sprintf('%d  3  %f  %f  %f  %f  %d', sid, coords(1, :), rs(1), pid);
            sid = sid + 1;
            coords(1, :) = [];
            rs(1) = [];
            while ~ isempty(coords)
                coord = coords(1, :);
                lines{sid, 1} = sprintf('%d  3  %f  %f  %f  %f  %d', sid, coord, rs(1), sid - 1);
                sid = sid + 1;
                coords(1, :) = [];
                rs(1) = [];
            end
            coords2sid(end + 1, :) = {coord, sid - 1};
        end
    end
    
    % Assemble SWC file text.
    text = strjoin(lines, '\n');

end
