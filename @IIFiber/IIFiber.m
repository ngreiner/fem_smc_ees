classdef IIFiber < IaFiber

	% This class is a near-perfect copy of the IaFiber class.
	% Its purpose is to represent group-II fibers.
	%
	% The only minor difference lies in the assign_MNs method, which
	% extracts different parameters from the input smcd for the IIFiber
	% and IaFiber classes.
    
    properties
    end
    
    methods
        function this = IIFiber()
        end
    end
    
    methods
        assign_MNs(this, smcd)
    end
    
end