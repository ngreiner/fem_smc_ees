function assign_MNs(this, smcd)
    % Assign a collection of MNs to the IIFiber (set attribute idx_MNs).
    %
    % Usage:
    %   ii.assign_MNs(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The assigned MNs are selected at random among the homonymous and
    %   heteronymous MNs of the IIFiber, using the appropriate proportions
    %   (p_homo and p_hete) given by the input SMCDModel. 

    if smcd.params.verbose
        fprintf(Utils.padn('  Assigning MNs  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Homonymous MNs.
    idx_MNs = find(cellfun(@(x) strcmp(x.musc, this.musc), smcd.entities.MNs));
    nhomo = size(idx_MNs, 1);
    idx_homo = idx_MNs(randsample(nhomo, round(nhomo * smcd.connec.II.p_homo)));

    % Heteronymous MNs.
    hete_muscles = smcd.connec.II.synergies{strcmp(smcd.compo.muscles, this.musc)};
    idx_hete = [];
    for i = 1 : size(hete_muscles, 1)
        idx_MNs = find(cellfun(@(x) strcmp(x.musc, hete_muscles{i}), smcd.entities.MNs));
        n = size(idx_MNs, 1);
        idx_MNs = idx_MNs(randsample(n, round(n * smcd.connec.II.p_hete)));
        idx_hete = union(idx_hete, idx_MNs);
    end

    % Complete list of MN indexes.
    this.idx_MNs = union(idx_homo, idx_hete);
    
    % Print exit message.
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished assigning MNs. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
