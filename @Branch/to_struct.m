function struc = to_struct(this)
	% Return a matlab struct equivalent to the Branch object.
    %
    % Usage:
    %   struc = branch.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the Branch object.
    struc.label = this.label;
    struc.model = this.model;
    struc.diam = this.diam;
    struc.path = this.path;
    struc.nN = this.nN;
    struc.nP1 = this.nP1;
    struc.nP2 = this.nP2;
    struc.nI = this.nI;
    struc.Ncoords = this.Ncoords;
    struc.P1coords = this.P1coords;
    struc.P2coords = this.P2coords;
    struc.Icoords = this.Icoords;
    struc.VN = this.VN;
    struc.VP1 = this.VP1;
    struc.VP2 = this.VP2;
    struc.VI = this.VI;
    struc.p_attach = this.p_attach;
end
