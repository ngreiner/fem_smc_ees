function outbool = is_interp(this, AS_idx, varargin)
    % Return a logical indicating whether the Branch is already well
    % interpolated for the input AS_idx.
    %
    % Usage:
    %   outbool = branch.is_interp(AS_idx)
    %   outbool = branch.is_interp(AS_idx, 'N')
    %   outbool = branch.is_interp(AS_idx, 'P1')
    %   outbool = branch.is_interp(AS_idx, 'P2')
    %   outbool = branch.is_interp(AS_idx, 'I')
    %
    % Parameters:
    %   AS_idx : double.
    %
    % Return:
    %   outbool : logical.
    %
    % Description:
    %   If the branch lacks the potential array for one of its nodes,
    %   paranodes1, paranodes2 or internodes, the branch is not well
    %   interpolated.
    %   If the branch doesn't possess any paranodes or internodes, the
    %   previous requirement is relaxed for the paranodes/internodes at
    %   concern.
    %
    %   If one of 'N', 'P1', 'P2' or 'I' is given in varargin, the test
    %   should be performed solely on the nodes/paranodes1/paranodes2/
    %   internodes respectively.
    %
    %   Otherwise it is the conjunction of all of them.
    
    if strcmp('N', varargin)
        outbool = ~isempty(this.VN{AS_idx});
        return
    elseif strcmp('P1', varargin)
        outbool = ~isempty(this.VP1{AS_idx}) || (this.nP1 == 0);
        return
    elseif strcmp('P2', varargin)
        outbool = ~isempty(this.VP2{AS_idx}) || (this.nP2 == 0);
        return
    elseif strcmp('I', varargin)
        outbool = ~isempty(this.VI{AS_idx}) || (this.nI == 0);
        return
    end
    
    outbool = this.is_interp(AS_idx, 'N', varargin{:}) && ...
              this.is_interp(AS_idx, 'P1', varargin{:}) && ...
              this.is_interp(AS_idx, 'P2', varargin{:}) && ...
              this.is_interp(AS_idx, 'I', varargin{:});
          
end