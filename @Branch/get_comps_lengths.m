function [Nlength, P1length, P2length, Ilength] = get_comps_lengths(this)
    % Return the lengths of the nodes, paranodes1, paranodes2 and
    % internodes of the Branch.
    %
    % Usage:
    %   [Nlength, P1length, P2length, Ilength] = branch.get_comps_lengths()
    %
    % Return:
    %   Nlength: double: length of branch's nodes.
    %   P1length: double: length of branch's paranodes1.
    %   P2length: double: length of branch's paranodes2.
    %   Ilength: double: length of branch's internodes.
    %
    % Description:
    %   The compartments lengths are derived from the MRG model of
    %   myelinated mammalian nerve fibers (Richardson et al., 2000).
    %   This publication gives the lengths of the compartments for a finite
    %   set of fiber diameters.
    %   We implemented piecewise-linear interpolation from this database to
    %   estimate the value of these lengths for any fiber diameter value.
    %   This is used when model is 'ModelC'.
    %   An alternative possibility for model is 'ModelB', in which
    %   case all the internodal compartments have equal lengths.
    
    % McIntyre's database for ModelC fiber models.
    diams = [0; 5.7; 7.3; 8.7; 10; 11.5; 12.8; 14; 15; 16] * 1e-3;
    P2lengths = [0; 35; 38; 40; 46; 50; 54; 56; 58; 60] * 1e-3;
    deltaXs = [0; 500; 750; 1000; 1150; 1250; 1350; 1400; 1450; 1500] * 1e-3;

    % Piecewise linear interpolants for P2length and deltaX.
    PP2length = mkpp(diams, [diff(P2lengths) ./ diff(diams), P2lengths(1 : end - 1)]);
    PdeltaX = mkpp(diams, [diff(deltaXs) ./ diff(diams), deltaXs(1 : end - 1)]);
    
    % Interpolate compartments lengths according to fiber model.
    Nlength = 1e-3;
    deltaX = ppval(PdeltaX, this.diam);
    if regexp(this.model, 'ModelC|ModelD')
        P1length = 3e-3;
        P2length = ppval(PP2length, this.diam);
        Ilength = (deltaX - 2 * P1length - 2 * P2length - Nlength) / 6;
    elseif strcmp(this.model, 'ModelB')
        P1length = deltaX / 10;
        P2length =  deltaX / 10;
        Ilength =  deltaX / 10;
    else
        error('Unknown model.')
    end
    
end
