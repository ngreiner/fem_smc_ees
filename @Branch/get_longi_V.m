function [longi_abs, longi_V] = get_longi_V(this, AS_idx, varargin)
    % Interpolates the electric potential at the points of the Branch path.
    %
    % Parameters:
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   varargin : fields can be :
    %       'pstep': followed by integer : step to roam the potentials
    %       vectors.
    %
    % Description:
    %   The potentials at the points of the path are obtained by linear
    %   interpolation from the values at the nodes/paranodes/internodes.

    pstep = 1;
    idx = find(strcmp('pstep', varargin));
    if idx
        pstep = varargin{idx + 1};
    end
    
    % Unpack coordinates and potentials of the nodes/paranodes/internodes.
    npath = size(this.path, 1);
    nN = this.nN;
    VN = this.VN{AS_idx};
    VP1 = this.VP1{AS_idx};
    VP2 = this.VP2{AS_idx};
    VI = this.VI{AS_idx};
    [Nlength, P1length, P2length, Ilength] = this.get_comps_lengths();

    % Form potential and longitudinal abscissa vectors.
    n = 11 * (nN - 1) + 1;
    Vs = zeros(n, 1);
    xs = zeros(n, 1);
    Vs(1) = VN(1);
    for i = 1 : nN - 1
        Vs(11 * (i - 1) + 2) = VP1(2 * (i - 1) + 1);
        Vs(11 * (i - 1) + 3) = VP2(2 * (i - 1) + 1);
        Vs(11 * (i - 1) + 4) = VI(6 * (i - 1) + 1);
        Vs(11 * (i - 1) + 5) = VI(6 * (i - 1) + 2);
        Vs(11 * (i - 1) + 6) = VI(6 * (i - 1) + 3);
        Vs(11 * (i - 1) + 7) = VI(6 * (i - 1) + 4);
        Vs(11 * (i - 1) + 8) = VI(6 * (i - 1) + 5);
        Vs(11 * (i - 1) + 9) = VI(6 * (i - 1) + 6);
        Vs(11 * (i - 1) + 10) = VP2(2 * (i - 1) + 2);
        Vs(11 * (i - 1) + 11) = VP1(2 * (i - 1) + 2);
        Vs(11 * (i - 1) + 12) = VN(i + 1);

        xs(11 * (i - 1) + 2) = xs(11 * (i - 1) + 1) + Nlength / 2 + P1length / 2;
        xs(11 * (i - 1) + 3) = xs(11 * (i - 1) + 2) + P1length / 2 + P2length / 2;
        xs(11 * (i - 1) + 4) = xs(11 * (i - 1) + 3) + P2length / 2 + Ilength / 2;
        xs(11 * (i - 1) + 5) = xs(11 * (i - 1) + 4) + Ilength;
        xs(11 * (i - 1) + 6) = xs(11 * (i - 1) + 5) + Ilength;
        xs(11 * (i - 1) + 7) = xs(11 * (i - 1) + 6) + Ilength;
        xs(11 * (i - 1) + 8) = xs(11 * (i - 1) + 7) + Ilength;
        xs(11 * (i - 1) + 9) = xs(11 * (i - 1) + 8) + Ilength;
        xs(11 * (i - 1) + 10) = xs(11 * (i - 1) + 9) + Ilength / 2 + P2length / 2;
        xs(11 * (i - 1) + 11) = xs(11 * (i - 1) + 10) + P2length / 2 + P1length / 2;
        xs(11 * (i - 1) + 12) = xs(11 * (i - 1) + 11) + P1length / 2 + Nlength / 2;
    end
    
    % Interpolate potentials of points of Branch's path.
    np = floor(npath / pstep);
    tmp = Geom.interparc(np, xs, Vs, 'linear');
    longi_abs = tmp(:, 1);
    longi_V = tmp(:, 2);
    
end
