function interp_V(this, smcd, perm)
    % Interpolate the Branch compartments' electric potentials.
    %
    % Usage:
    %   branch.interp_V(smcd)
    %   branch.interp_V(smcd, perm)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Potentials are interpolated in the Comsol Model attached to the
    %   input SMCDModel thanks to the coordinates of the compartments of
    %   the MotorAxon.
    %   Depending upon the value of smcd.params.interpType, the potentials
    %   at the compartments are either all directly obtained by
    %   interpolation in the Comsol Model, either are obtained from Comsol
    %   for some of them and the others by linear interpolation from the
    %   first ones.
    %
    %   This method sets the fields VN{AS_idx}, VP1{AS_idx}, VP2{AS_idx}
    %   and VI{AS_idx} of the Branch (where AS_idx = smcd.fem.AS.idx).
        
    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    % Unpack attributes in convenient variables.
    idx = smcd.fem.AS.idx;
    CM = smcd.fem.CM;
    if smcd.fem.params.physics.with_curvi_coords
        dset = 'dset2';
    else
        dset = 'dset1';
    end
    
    % If Branch's potentials already interpolated, interpolate anew only
    % with permission. 
    if this.is_interp(idx)
        fprintf('%s already interpolated for AS_idx #%d.\n', class(this), idx)
        if (perm == 0) || (perm == 1)
            fprintf('Writing permission = %d. Exiting method without interpolating.\n', perm)
            return
        elseif strcmp(perm, 'i') && ~ Utils.prompt_confirm('Do you want to overwrite?')
            fprintf('Exiting method without interpolating.\n')
            return
        end
        fprintf('Overwriting fibers''potentials.\n')
    end
        
    switch smcd.params.interp_type

        case 'full'
            % Branch nodes potentials.
            VN = zeros(this.nN, 1);
            for k = 1 : this.nN
                VN(k) = mphinterp(CM, 'V', 'coord', [this.Ncoords(k, 1) * 1e-3; this.Ncoords(k, 2) * 1e-3; this.Ncoords(k, 3)* 1e-3], 'dataset', dset);
            end
            % Branch paranodes1 potentials.
            VP1 = zeros(this.nP1, 1);
            for k = 1 : this.nP1
                VP1(k) = mphinterp(CM, 'V', 'coord', [this.P1coords(k, 1) * 1e-3; this.P1coords(k, 2) * 1e-3; this.P1coords(k, 3) * 1e-3], 'dataset', dset);
            end
            % Branch paranodes2 potentials.
            VP2 = zeros(this.nP2,1);
            for k = 1 : this.nP2
                VP2(k) = mphinterp(CM, 'V', 'coord', [this.P2coords(k, 1) * 1e-3; this.P2coords(k, 2) * 1e-3; this.P2coords(k, 3) * 1e-3], 'dataset', dset);
            end
            % Branch internodes potentials.
            VI = zeros(this.nI,1);
            for k = 1 : this.nI
                VI(k) = mphinterp(CM, 'V', 'coord', [this.Icoords(k, 1) * 1e-3; this.Icoords(k, 2) * 1e-3; this.Icoords(k, 3) * 1e-3], 'dataset', dset);
            end

        case 'mixed'
            [Nlength, P1length, P2length, Ilength] = this.get_comps_lengths();

            % Branch nodes potentials. Interpolate in Comsol Model.
            VN = zeros(this.nN, 1);
            for k = 1 : this.nN
                VN(k) = mphinterp(CM, 'V', 'coord', [this.Ncoords(k, 1) * 1e-3; this.Ncoords(k, 2) * 1e-3; this.Ncoords(k, 3) * 1e-3], 'dataset', dset);
            end
            % Branch nodes potentials.
            VP2 = zeros(this.nP2, 1);
            for k = 1 : this.nP2
                VP2(k) = mphinterp(CM, 'V', 'coord', [this.P2coords(k, 1) * 1e-3; this.P2coords(k, 2) * 1e-3; this.P2coords(k, 3) * 1e-3], 'dataset', dset);
            end        
            % Branch paranodes1 potentials. Interpolate from neighboring nodes
            % and paranodes2.
            VP1 = zeros(this.nP1, 1);
            w1 = (P1length / 2 + P2length / 2) / (Nlength / 2 + P1length + P2length / 2);
            w2 = (P1length / 2 + Nlength / 2) / (Nlength / 2 + P1length + P2length / 2);
            for k = 1 : this.nN - 1
                VP1(2 * k - 1) = VN(k) * w1 + VP2(2 * k - 1) * w2;
                VP1(2 * k) = VN(k + 1) * w1 + VP2(2 * k) * w2;
            end
            % Branch internodes potentials. Interpolate in Comsol Model for the
            % 2nd and 5th internodes of each internodal section. Interpolate
            % from neighboring compartments for 1st, 3rd, 4th and 6th.
            VI = zeros(this.nI, 1);
            w1 = (Ilength / 2 + P2length / 2) / (P2length / 2 + 3 * Ilength / 2);
            w2 = Ilength / (P2length / 2 + 3 * Ilength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nN - 1
                VI(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', [this.Icoords(6 * (k - 1) + 2, 1) * 1e-3; this.Icoords(6 * (k - 1) + 2, 2) * 1e-3; this.Icoords(6 * (k - 1) + 2, 3) * 1e-3], 'dataset', dset);
                VI(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', [this.Icoords(6 * (k - 1) + 5, 1) * 1e-3; this.Icoords(6 * (k - 1) + 5, 2) * 1e-3; this.Icoords(6 * (k - 1) + 5, 3) * 1e-3], 'dataset', dset);
                VI(6 * (k - 1) + 1) = VP2(2 * k - 1) * w2 + VI(6 * (k - 1) + 2) * w1;
                VI(6 * (k - 1) + 3) = VI(6 * (k - 1) + 2) * w4 + VI(6 * (k - 1) + 5) * w3;
                VI(6 * (k - 1) + 4) = VI(6 * (k - 1) + 2) * w3 + VI(6 * (k - 1) + 5) * w4;
                VI(6 * (k - 1) + 6) = VP2(2 * k) * w2 + VI(6 * (k - 1) + 5) * w1;
            end

        case 'poor'
            [Nlength, P1length, P2length, Ilength] = this.get_comps_lengths();

            % Branch nodes potentials. Interpolate in Comsol Model.
            VN = zeros(this.nN, 1);
            for k = 1 : this.nN
                VN(k) = mphinterp(CM, 'V', 'coord', [this.Ncoords(k, 1) * 1e-3; this.Ncoords(k, 2) * 1e-3; this.Ncoords(k, 3) * 1e-3], 'dataset', dset);
            end
            % Branch internodes potentials. Interpolate in Comsol Model for
            % the 2nd and 5th internodes of each internodal section.
            % Interpolate from 2nd and 5th for 1st, 3rd, 4th and 6th.
            VI = zeros(this.nI, 1);
            w1 = (Nlength / 2 + P1length + P2length + Ilength / 2) / (Nlength / 2 + P1length + P2length + 3 * Ilength / 2);
            w2 = Ilength / (Nlength / 2 + P1length + P2length + 3 * Ilength / 2);
            w3 = 1 / 3;
            w4 = 2 / 3;
            for k = 1 : this.nN - 1
                VI(6 * (k - 1) + 2) = mphinterp(CM, 'V', 'coord', [this.Icoords(6 * (k - 1) + 2, 1) * 1e-3; this.Icoords(6 * (k - 1) + 2, 2) * 1e-3; this.Icoords(6 * (k - 1) + 2, 3) * 1e-3], 'dataset', dset);
                VI(6 * (k - 1) + 5) = mphinterp(CM, 'V', 'coord', [this.Icoords(6 * (k - 1) + 5, 1) * 1e-3; this.Icoords(6 * (k - 1) + 5, 2) * 1e-3; this.Icoords(6 * (k - 1) + 5, 3) * 1e-3], 'dataset', dset);
                VI(6 * (k - 1) + 1) = VN(k) * w2 + VI(6 * (k - 1) + 2) * w1;
                VI(6 * (k - 1) + 3) = VI(6 * (k - 1) + 2) * w4 + VI(6 * (k - 1) + 5) * w3;
                VI(6 * (k - 1) + 4) = VI(6 * (k - 1) + 2) * w3 + VI(6 * (k - 1) + 5) * w4;
                VI(6 * (k - 1) + 6) = VN(k + 1) * w2 + VI(6 * (k - 1) + 5) * w1;
            end
            % Branch paranodes1 potentials. Interpolate from neighboring
            % nodes and internodes.
            VP1 = zeros(this.nP1, 1);
            w1 = (Nlength / 2 + P1length / 2) / (Nlength / 2 + P1length + P2length + Ilength / 2);
            w2 = (Ilength / 2 + P2length + P1length / 2) / (Nlength / 2 + P1length + P2length + Ilength / 2);
            for k = 1 : this.nN - 1
                VP1(2 * k - 1) = VN(k) * w2 + VI(6 * (k - 1) + 1) * w1;
                VP1(2 * k) = VN(k + 1) * w2 + VI(6 * (k - 1) + 6) * w1;
            end
            % Branch paranodes2 potentials. Interpolate from neighboring
            % nodes and internodes.
            VP2 = zeros(this.nP2, 1);
            w1 = (Nlength / 2 + P1length + P2length / 2) / (Nlength / 2 + P1length + P2length + 3 * Ilength / 2);
            w2 = (3 * Ilength / 2 + P2length / 2) / (Nlength / 2 + P1length + P2length + 3 * Ilength / 2);
            for k = 1 : this.nN - 1
                VP2(2 * k - 1) = VN(k) * w2 + VI(6 * (k - 1) + 2) * w1 ;
                VP2(2 * k) = VN(k + 1) * w2 + VI(6 * (k - 1) + 5) * w1;
            end

    end

    % Rectify potentials so that they are expressed in mV and for a
    % stimulation amplitude of 1uA and store in dedicated arrays.
    VN = VN / smcd.fem.AS.area * 1e-3;
    VP1 = VP1 / smcd.fem.AS.area * 1e-3;
    VP2 = VP2 / smcd.fem.AS.area * 1e-3;
    VI = VI / smcd.fem.AS.area * 1e-3;
    
    % Assign to Branch attributes.
    if ~ isempty(VN)
        this.VN{idx} = VN;
        this.VP1{idx} = VP1;
        this.VP2{idx} = VP2;
        this.VI{idx} = VI;
    end

end
