function path_V = get_path_V(this, AS_idx, varargin)
    % Interpolates the electric potential at the points of the Branch path.
    %
    % Parameters:
    %   AS_idx : integer :
    %       Index of active site.
    %
    %   varargin : fields can be :
    %       'pstep': followed by integer : step to roam the potentials
    %       vectors.
    %
    % Description:
    %   The potentials at the points of the path are obtained by linear
    %   interpolation from the values at the nodes/paranodes/internodes.

    pstep = 1;
    idx = find(strcmp('pstep', varargin));
    if idx
        pstep = varargin{idx + 1};
    end
    
    % Unpack coordinates and potentials of the nodes/paranodes/internodes.
    comp_coords = [this.Ncoords; this.P1coords; this.P2coords; this.Icoords];
    comp_V = [this.VN{AS_idx}; this.VP1{AS_idx}; this.VP2{AS_idx}; this.VI{AS_idx}];

    % Interpolate potentials of points of Branch's path.
    new_path = this.path(1 : pstep : end, :);
    np = size(new_path, 1);
    path_V = zeros(np, 1);
    for i = 1 : np
        p = new_path(i, :);
        [idx, D] = knnsearch(comp_coords, p, 'k', 2);
        w = D(1) / sum(D);
        path_V(i) = w * comp_V(idx(2)) + (1 - w) * comp_V(idx(1));
    end
    
end
