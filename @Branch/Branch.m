classdef Branch < handle

    % Properties defined here:
    %
    %   label : string :
    %       Label of Branch (e.g. 'DRbranch', 'Abranch', ...).
    %
    %   model : string :
    %       Indicator of fiber/branch model.
    %
    %   diam : double :
    %       Branch diameter (in um).
    %
    %   path : N*3 double :
    %       Branch endpoints' coordinates (in mm).
    %
    %   nN : integer :
    %       Number of nodes on the Branch.
    %
    %   nP1 : integer :
    %       Number of paranodes1 on the Branch.
    %
    %   nP2 : integer :
    %       Number of paranodes2 on the Branch.
    %
    %   nI : integer :
    %       Number of internodes on the Branch.
    %
    %   Ncoords : nN*3 double :
    %       Nodes coordinates (in mm).
    %
    %   P1coords : nP1*3 double :
    %       Paranodes1 coordinates (in mm).
    %
    %   P2coords : nP2*3 double :
    %       Paranodes2 coordinates (in mm).
    %
    %   Icoords : nI*3 double :
    %       Internodes coordinates (in mm).
    %
    %   VN : K*1 cell :
    %       Nodes potentials for each of K active sites (in mV).
    %
    %   VP1 : K*1 cell :
    %       Paranodes1 potentials for each of K active sites (in mV).
    %
    %   VP2 : K*1 cell :
    %       Paranodes2 potentials for each of K active sites (in mV).
    %
    %   VI : K*1 cell :
    %       Internodes potentials for each of K active sites (in mV).
    %
    %   p_attach : string :
    %       Indicator of node of attachment on another branch.
    
    properties
        label       % string
        model       % string
        diam        % double
        path        % N*3 double
        nN          % integer
        nP1         % integer
        nP2         % integer
        nI          % integer
        Ncoords     % nN*3 double
        P1coords    % nP1*3 double
        P2coords    % nP2*3 double
        Icoords     % nI*3 double
        VN          % K*1 cell
        VP1         % K*1 cell
        VP2         % K*1 cell
        VI          % K*1 cell
        p_attach    % string
    end
    
    methods
        function this = Branch()
        end
    end
    
    methods
        build_comps_coords(this, rnd_offset)
        outbool = eq(obj1, obj2)
        from_struct(this, struc)
        [Nlength, P1length, P2length, Ilength] = get_comps_lengths(this)
        [longi_abs, longi_V] = get_longi_V(this, AS_idx, varargin)
        path_V = get_path_V(this, AS_idx, varargin)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx, varargin)
        plot_V(this, AS_idx, varargin)
        struc = to_struct(this)
    end
    
end
