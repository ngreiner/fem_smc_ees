function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if abs(obj1.diam - obj2.diam) >= 1e-6
        fprintf('Attributes `diam` are different.\n')
        return
    end
    
    if xor(isempty(obj1.path), isempty(obj2.path)) || max(max(abs(obj1.path - obj2.path))) >= 1e-6;
        fprintf('Attributes `path` are different.\n')
        return
    end

    if ~ (obj1.nN == obj2.nN)
        fprintf('Attributes `nN` are different.\n')
        return
    end

    if ~ (obj1.nP1 == obj2.nP1)
        fprintf('Attributes `nP1` are different.\n')
        return
    end

    if ~ (obj1.nP2 == obj2.nP2)
        fprintf('Attributes `nP2` are different.\n')
        return
    end

    if ~ (obj1.nI == obj2.nI)
        fprintf('Attributes `nI` are different.\n')
        return
    end
    
    if xor(isempty(obj1.Ncoords), isempty(obj2.Ncoords)) || ~ isempty(obj1.Ncoords) && max(max(abs(obj1.Ncoords - obj2.Ncoords))) >= 1e-6;
        fprintf('Attributes `Ncoords` are different.\n')
        return
    end
    
    if xor(isempty(obj1.P1coords), isempty(obj2.P1coords)) || ~ isempty(obj1.P1coords) && max(max(abs(obj1.P1coords - obj2.P1coords))) >= 1e-6;
        fprintf('Attributes `P1coords` are different.\n')
        return
    end
    
    if xor(isempty(obj1.P2coords), isempty(obj2.P2coords)) || ~ isempty(obj1.P2coords) && max(max(abs(obj1.P2coords - obj2.P2coords))) >= 1e-6;
        fprintf('Attributes `P2coords` are different.\n')
        return
    end
    
    if xor(isempty(obj1.Icoords), isempty(obj2.Icoords)) || ~ isempty(obj1.Icoords) && max(max(abs(obj1.Icoords - obj2.Icoords))) >= 1e-6;
        fprintf('Attributes `Icoords` are different.\n')
        return
    end
    
    if any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VN, obj2.VN));
        fprintf('Attributes `VN` are different.\n')
        return
    end
    
    if any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP1, obj2.VP1));
        fprintf('Attributes `VP1` are different.\n')
        return
    end
    
    if any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VP2, obj2.VP2));
        fprintf('Attributes `VP2` are different.\n')
        return
    end
    
    if any(cellfun(@(x, y) xor(isempty(x), isempty(y)) || ~ isempty(x) && max(abs(x - y)) >= 1e-6, obj1.VI, obj2.VI));
        fprintf('Attributes `VI` are different.\n')
        return
    end

    if (~ isempty(obj1.p_attach) || ~ isempty(obj2.p_attach)) && ~ strcmp(obj1.p_attach, obj2.p_attach);
        fprintf('Attributes `p_attach` are different.\n')
        return
    end
    
    outbool = true;
    
end
