function NI2I5V = getNI2I5V(this, ASindex)
    % This method returns the electric potentials at the nodes, 2nd 
    % internodes and 5th internodes of the branch for the active site
    % `ASindex`.
    %
    % Inputs: - ASindex: int: index of active site.
    %
    % Outputs: None.
    %
    % Actions: None.
    %
    % Description: Void.
    NI2I5V = zeros(3 * (this.nN - 1) + 1, 1);
    for i = 1:this.nN - 1
        NI2I5V(3 * (i - 1) + 1) = this.VN{ASindex}(i);
        NI2I5V(3 * (i - 1) + 2) = this.VI{ASindex}(6 * (i - 1) + 2);
        NI2I5V(3 * (i - 1) + 3) = this.VI{ASindex}(6 * (i - 1) + 5);
    end
    NI2I5V(end) = this.VN{ASindex}(end);
end
