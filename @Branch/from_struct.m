function fromStruct(this, struc)
	% Set the Branch object from the input struct.
    %
    % Usage:
    %   branch.from_struct(struc)
    %
    % Parameters: 
    %   struc : struct : fields are those of the Branch class.
    this.label = struc.label;
    this.model = struc.model;
    this.diam = struc.diam;
    this.path = struc.path;
    this.nN = struc.nN;
    this.nP1 = struc.nP1;
    this.nP2 = struc.nP2;
    this.nI = struc.nI;
    this.Ncoords = struc.Ncoords;
    this.P1coords = struc.P1coords;
    this.P2coords = struc.P2coords;
    this.Icoords = struc.Icoords;
    this.VN = struc.VN;
    this.VP1 = struc.VP1;
    this.VP2 = struc.VP2;
    this.VI = struc.VI;
    this.p_attach = struc.p_attach;
end
