function build_comps_coords(this, rndOffset)
    % Build the coordinates of the compartments of the Branch.
    %
    % Usage:
    %   branch.build_comps_coords()
    %   branch.build_comps_coords(rndOffset)
    %
    % Parameters:
    %   rndOffset : double : 1 if a random offset for the first node
    %                        should be applied, 0 otherwise.
    %
    % Description:
    %   The Branch is representing a portion of nerve fiber, which can be
    %   seen as the repetition of the following sequence of 11
    %   compartments:
    %       N - P1 - P2 - I - I - I - I - I - I - P2 - P1
    %   In this sequence, N stands for 'node', P1 stands for 'paranode1',
    %   P2 stands for 'paranode2' and I stands for 'internode'.
    %   The sequence repetition is finally completed by a terminal node.
    %   
    %   Here, only the positions of the compartments matter. They are
    %   determined by following the path of the Branch and distributing
    %   the compartments along it on the basis of their lengths.
    %
    %   This method thus sets the attributes nN, Ncoords, nP1, P1coords,
    %   nP2, P2coords, nI, and Icoords.
    
    % Unpack branch path.
    path = this.path;

    % Get compartments lengths.
    [Nlength, P1length, P2length, Ilength] = this.get_comps_lengths();
    deltaX = Nlength + 2 * P1length + 2 * P2length + 6 * Ilength;

    % Compute length of each path segment, cumulated length and total
    % length. 
    seg_lengths = zeros(size(path, 1) - 1, 1);
    cum_lengths = zeros(size(path, 1) -1, 1);
    tot_length = 0;
    for i = 1 : size(seg_lengths, 1)
        seg_lengths(i) = norm(path(i + 1, :) - path(i, :));
        tot_length = tot_length + seg_lengths(i);
        cum_lengths(i) = tot_length;
    end

    % Compute distance of each compartment from path origin.
    % Distance of first node.
    if rndOffset
        x = unifrnd(Nlength / 2, Nlength / 2 + deltaX);
    else
        x = Nlength / 2;
    end
    % Number of nodes. The remaining path length divided by the internodal
    % distance.
    nN = floor((tot_length - x) / deltaX);
    if nN * deltaX + Nlength < tot_length - x
        nN = nN + 1;
    end
    % Derive number of paranodes1, paranodes2, and internodes.
    nP1 = 2 * (nN - 1);
    nP2 = 2 * (nN - 1);
    nI = 6 * (nN - 1);
    % Compute distances.
    Ndists = zeros(nN, 1);
    P1dists = zeros(nP1, 1);
    P2dists = zeros(nP2, 1);
    Idists = zeros(nI, 1);
    Ndists(1) = x;
    for i = 1 : nN - 1
        P1dists(2 * i - 1) = Ndists(i) + Nlength / 2 + P1length / 2;
        P2dists(2 * i - 1) = P1dists(2 * i - 1) + P1length / 2 + P2length / 2;
        Idists(6 * i - 5) = P2dists(2 * i - 1) + P2length / 2 + Ilength / 2;
        Idists(6 * i - 4) = Idists(6 * i - 5) + Ilength;
        Idists(6 * i - 3) = Idists(6 * i - 4) + Ilength;
        Idists(6 * i - 2) = Idists(6 * i - 3) + Ilength;
        Idists(6 * i - 1) = Idists(6 * i - 2) + Ilength;
        Idists(6 * i) = Idists(6 * i - 1) + Ilength;
        P2dists(2 * i) = Idists(6 * i) + Ilength / 2 + P2length / 2;
        P1dists(2 * i) = P2dists(2 * i) + P2length / 2 + P1length / 2;
        Ndists(i + 1) = P1dists(2 * i) + P1length / 2 + Nlength / 2;
    end
    
    % Compute the compartments coordinates.
    Ncoords = zeros(nN, 3);
    P1coords = zeros(nP1, 3);
    P2coords = zeros(nP2, 3);
    Icoords = zeros(nI, 3);
    
    % We need 1 variable to track the current segment we are roaming, and 1
    % variable to store the unitary vector of this segment (uvect, defined
    % in the loop).
    cseg = 1;
    
    for i = 1 : nN - 1
        
        % Node(i)
        while Ndists(i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - Ndists(i));
        Ncoords(i, :) = path(cseg, :) + uvect * rdist;
        
        % Paranode1(2 * i - 1)
        while P1dists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1dists(2 * i - 1));
        P1coords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        
        % Paranode2(2 * i - 1)
        while P2dists(2 * i - 1) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2dists(2 * i - 1));
        P2coords(2 * i - 1, :) = path(cseg, :) + uvect * rdist;
        
        % Internodes
        for k = 5 : -1 : 0
            while Idists(6 * i - k) >= cum_lengths(cseg)
                cseg = cseg + 1;
            end
            uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
            rdist = seg_lengths(cseg) - (cum_lengths(cseg) - Idists(6 * i - k));
            Icoords(6 * i - k, :) = path(cseg, :) + uvect * rdist;
        end
        
        % Paranode2(2 * i)
        while P2dists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P2dists(2 * i));
        P2coords(2 * i, :) = path(cseg, :) + uvect * rdist;
        
        % Paranode1(2 * i)
        while P1dists(2 * i) >= cum_lengths(cseg)
            cseg = cseg + 1;
        end
        uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
        rdist = seg_lengths(cseg) - (cum_lengths(cseg) - P1dists(2 * i));
        P1coords(2 * i, :) = path(cseg, :) + uvect * rdist;
    end
    
    % Last Node.
    while Ndists(nN) >= cum_lengths(cseg)
        cseg = cseg + 1;
    end
    uvect = (path(cseg + 1, :) - path(cseg, :)) / seg_lengths(cseg);
    rdist = seg_lengths(cseg) - (cum_lengths(cseg) - Ndists(nN));
    Ncoords(nN, :) = path(cseg, :) + uvect * rdist;
    
    % Assign values to attributes.
    this.nN = nN;
    this.Ncoords = Ncoords;
    this.nP1 = nP1;
    this.P1coords = P1coords;
    this.nP2 = nP2;
    this.P2coords = P2coords;
    this.nI = nI;
    this.Icoords = Icoords;

end
