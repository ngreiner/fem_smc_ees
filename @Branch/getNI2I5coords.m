function NI2I5coords = getNI2I5coords(this)
    % This method returns the coordinates of the nodes, 2nd internodes and
    % 5th internodes of the Branch.
    %
    % Inputs: None.
    %
    % Outputs: None.
    %
    % Actions: None.
    %
    % Description: Void.
    NI2I5coords = zeros(3 * (this.nN - 1) + 1, 3);
    for i = 1:this.nN - 1
        NI2I5coords(3 * (i - 1) + 1, :) = this.NCoords(i, :);
        NI2I5coords(3 * (i - 1) + 2, :) = this.ICoords(6 * (i - 1) + 2, :);
        NI2I5coords(3 * (i - 1) + 3, :) = this.ICoords(6 * (i - 1) + 5, :);
    end
    NI2I5coords(end, :) = this.NCoords(end, :);
end
