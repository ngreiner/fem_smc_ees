classdef DRcAFiber < DRFiber

	% This class is a perfect copy of the DRFiber class.
	% They only differ by their name, which influences the reading
	% and writing of files in the file system.
    
    methods
        function this = DRcAFiber()
        end
    end
    
end