function set_CSTbranch(this, smcd)
    % Set the cortico-spinal branch of the CSTFiber.
    %
    % Usage:
    %   cst.set_CSTbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The cortico-spinal tract branch of the CST fiber runs in the
    %   cortico-spinal tract (!!).
    %   The contours materializing the cortico-spinal tract are contained
    %   in the cross-sections of the spinal cord model attached to the
    %   input smcd.
    %   The trajectory of the CST branch is simply a cubic spline running
    %   through these contours - but only down to those belonging to
    %   segments where there are MNs innervated by the CST fiber.
    %   The coordinates of the CST branch's compartments are finally
    %   distributed along this trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting CST branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.CSTbranch = Branch();
    this.CSTbranch.label = 'CSTbranch';
    this.CSTbranch.model = this.model;
    
    % The diameter of the CSTbranch is the diameter of the CSTFiber.
    this.CSTbranch.diam = this.diam;
    
    % Recover cortico-spinal tract CS, and build path of CSTbranch.
    % First, retrieve minimal z-level reached by the MNs innervated by the
    % CSTFiber. Next, find index of spinal cord cross-section below this
    % minimal z-level. The CST CS are taken subsequently in each spinal
    % cord cross-section up to the previous index.
    zmin = min(cellfun(@(x) x.soma.coords(3), smcd.entities.MNs(this.idx_MNs)));
    CS_names = smcd.fem.sc.morpho.CS_names;
    idx = find(strcmp(smcd.fem.sc.get_CS_below(zmin).name, CS_names));
    CS = cellfun(@(x) x.CT.contour, smcd.fem.sc.CS(1 : idx), 'UniformOutput', false);
    this.CSTbranch.path = Geom.build_unconstrained_path(CS, smcd.params.pdens, 'refine_interp');
    
    % Derive branch's compartments' coordinates.
    this.CSTbranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.CSTbranch.VN = cell(smcd.get_nAS(), 1);
    this.CSTbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.CSTbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.CSTbranch.VI = cell(smcd.get_nAS(), 1);
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting CST branch. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
