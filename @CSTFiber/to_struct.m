function struc = to_struct(this)
	% Return a matlab struct equivalent to the CSTFiber.
    %
    % Usage:
    %   struc = cstf.to_struct()
    %
    % Return:
    %   strc : struct : with same fields as the CSTFiber.
    struc = to_struct@Fiber(this);
    struc.idx_MNs = this.idx_MNs;
    struc.MNfamilies = this.MNfamilies;
    struc.CSTbranch = this.CSTbranch.to_struct();
    struc.cols = cellfun(@(x) x.to_struct(), this.cols, 'UniformOutput', false);
end
