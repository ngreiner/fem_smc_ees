function from_struct(this, struc)
	% Set the CSTFiber from the input struct.
    %
    % Usage:
    %   cstf.from_struct(struc)
    %
    % Parameters:
    %   struc : struct :
    %       Fields are the same as the CSTFiber class properties.
    from_struct@Fiber(this, struc);
    this.idx_MNs = struc.idx_MNs;
    this.MNfamilies = struc.MNfamilies;
    this.CSTbranch = Branch();
    this.cols = cellfun(@(x) Branch(), struc.cols, 'UniformOutput', false);
    this.CSTbranch.from_struct(struc.CSTbranch);
    cellfun(@(x,y) x.from_struct(y), this.cols, struc.cols, 'UniformOutput', false);
end
