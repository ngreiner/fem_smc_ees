function read_morpho(this, folder)
	% Read the CSTFiber morphology information from textfile.
    %
    % Usage:
    %   cst.read_morpho(folder)
    %
    % Parameters:
    %   folder : string : path to CSTFiber folder.
    %
    % Description:
    %   The textfile from which to read is found in the input folder at
    %   'morpho.txt'. It is organized as follows:
    %       1. diam
    %       2. ncols
    %       3. CSTbranch.diam
    %       4. CSTbranch.nN
    %       9. cols{1}.diam
    %       5. cols{1}.nN
    %       6. cols{1}.p_attach
    %       7. cols{2}.diam
    %       8. cols{2}.nN
    %       9. cols{2}.p_attach
    %            .
    %            .
    %       n. cols{m}.diam
    %   (n+1). cols{m}.nN
    %   (n+2). cols{m}.p_attach
    
    % Read file.
    file_path = fullfile(folder, 'morpho.txt');
    fileID = fopen(file_path);
    morpho = textscan(fileID, '%s');
    fclose(fileID);

    % Assign appropriate attributes.
    this.diam = str2double(morpho{1}{1});
    this.cols = cell(str2double(morpho{1}{2}), 1);
    this.CSTbranch = Branch();
    this.CSTbranch.diam = str2double(morpho{1}{3});
    this.CSTbranch.nN = str2double(morpho{1}{4});
    this.CSTbranch.nP1 = 2 * (this.CSTbranch.nN - 1);
    this.CSTbranch.nP2 = 2 * (this.CSTbranch.nN - 1);
    this.CSTbranch.nI = 6 * (this.CSTbranch.nN - 1);
    for n = 1 : size(this.cols, 1)
        this.cols{n} = Branch();
        this.cols{n}.diam = str2double(morpho{1}{5 + 3 * (n - 1)});
        this.cols{n}.nN = str2double(morpho{1}{6 + 3 * (n - 1)});
        this.cols{n}.nP1 = 2 * (this.cols{n}.nN - 1);
        this.cols{n}.nP2 = 2 * (this.cols{n}.nN - 1);
        this.cols{n}.nI = 6 * (this.cols{n}.nN - 1);
        this.cols{n}.p_attach = morpho{1}{7 + 3 * (n - 1)};
    end
    
end
