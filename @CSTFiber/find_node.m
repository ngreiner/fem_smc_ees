function [P, loc] = find_node(this, z)
    % Find the node of the CST branch closest to the input z-coordinate.
    %
    % Usage:
    %   [P, loc] = cstf.find_node(z)
    %
    % Parameters:
    %   z : double : target z-coordinate.
    %
    % Return:
    %   P : 1*3 double :
    %       Desired node's coordinates.
    %
    %   loc : string :
    %       Indicator of node identity. The first 3 characters are 'CST'.
    %       The subsequent characters indicate the node index on the CST
    %       branch. Nodes are indexed starting from 0.
    %
    % Description:
    %   Looks for the node of the cortico-spinal tract branch that is
    %   just above the input z-coordinate (more precisely, for a node that
    %   would be 100um to 300um above the input z-coordinate).
    %   The last node of the CST branch (the caudalmost) is discarded
    %   from the selection: in practice, no z-coordinate below this node
    %   will be passed to this method.
    %   The method returns the coordinates of the selected node and a
    %   string identifying its position on the CST branch.
    
    % Positive shift from z.
    dz = 0.1 + rand() * 0.2;
    
    Ncoords = this.CSTbranch.Ncoords(1 : end - 1, :);
    zs = Ncoords(:, 3) - (z + dz);
    [~, idx] = min(abs(zs));
    
    P = Ncoords(idx, :);
    loc = sprintf('CST%d', idx);
    
end
