classdef CSTFiber < Fiber

    % Properties defined here:
    %
    %   idx_MNS : N*1 double :
    %       Indexes of the MNs innervated by the CSTFiber.
    %
    %   MNfamilies : ncols*1 cell :
    %       Families of MNs innervated by the CSTFiber's collaterals.
    %
    %   CSTbranch : Branch object :
    %       Cortico-spinal tract branch of the CSTFiber.
    %
    %   cols : ncols*1 cell :
    %       Collaterals (Branch objects) of the CSTFiber.
    
    properties
        idx_MNs     % N*1 double
        MNfamilies  % K*1 cell
        CSTbranch   % Branch object
        cols        % N*1 cell
    end
    
    methods
        function this = CSTFiber()
        end
    end
    
    methods
        assign_MNs(this, smcd)
        [P, loc] = find_node(this, z)
        from_struct(this, struc)
        ncols = gen_ncols(this, varargin)
        branches = get_branches(this)
        nMNs = get_nMNs(this)
        read_connec(this, folder)
        read_morpho(this, folder)
        set_cols(this, smcd)
        set_CSTbranch(this, smcd)
        struc = to_struct(this)
        write_morpho(this, dir_export, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end