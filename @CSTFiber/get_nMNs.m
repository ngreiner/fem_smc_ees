function nMNs = get_nMNs(this)
    % Get and return the number of MNs innervated by the CSTFiber.
    %
    % Usage:
    %   nMNs = cst.get_nMNs()
    %
    % Return:
    %   nMNs : integer : size of the attribute idx_MNs.
    nMNs = size(this.idx_MNs, 1);    
end
