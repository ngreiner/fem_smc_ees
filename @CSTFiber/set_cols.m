function set_cols(this, smcd)
    % Set the collaterals 'cols' of the CSTFiber.
    %
    % Usage: 
    %   cst.set_cols(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The collaterals of the CST fiber are generated as follows.
    %   First the number of them is generated from a statistical model
    %   based on the data obtained by (Brown and Fyffe, 1978), originally
    %   describing the morphology of cat hindlimb Ia fibers.
    %   Then the families of MNs innervated by each collateral are
    %   determined.
    %   The collaterals run from a node on the CST branch towards the
    %   center of mass of their target MN family.
    %   Additional points in the middle region of the GM serves for
    %   interpolation of cubic splines materializing the pathways of the
    %   collaterals.
    %   The coordinates of the collaterals' compartments are finally
    %   distributed along these trajectories.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting collaterals  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Parameters for the collaterals' diameters.
    mu = smcd.morpho.CST.mu_cols;
    sig = smcd.morpho.CST.sig_cols;
    
    % Generate theoretical number of collaterals.
    ncols = this.gen_ncols();
    
    % Split MNs innervated by the CSTFiber into ncols families
    % corresponding to each collateral.
    MNfamilies = this.gen_MN_families(smcd, ncols);
    
    % Generate interpolation points for the collaterals' pathways.
    % In so doing, rectify number of collaterals so that no 2 collaterals
    % originate from the same node of Ranvier. If ever that would be the
    % case, decrement the total number of collaterals, regenerate the
    % families of MNs, and start over the process of generation of
    % interpolation points.
    P3s = [];
    P2s = [];
    P1s = [];
    locs = {};
    i = 1;
    while i <= ncols
        P3 = mean(cell2mat(cellfun(@(x) x.soma.coords, smcd.entities.MNs(MNfamilies{i}), 'UniformOutput', false)), 1);
        P2 = smcd.fem.sc.gen_MR_point(P3(3), 'above');
        if strcmp(this.musc(end - 1 : end), '_r')
            P2(1) = - P2(1);
        end
        [P1, loc] = this.find_node(P2(3));
        if (i > 1) && isequal(P1, P1s(i - 1, :))
            ncols = ncols - 1;
            P3s = [];
            P2s = [];
            P1s = [];
            locs = {};
            i = 1;
            MNfamilies = this.gen_MN_families(smcd, ncols);
        else
            P2 = 0.25 * P2 + 0.75 * (0.5 * P1 + 0.5 * P3);
            P3s = [P3s; P3];
            P2s = [P2s; P2];
            P1s = [P1s; P1];
            locs{end + 1, 1} = loc;
            i = i + 1;
        end
    end
    this.MNfamilies = MNfamilies;

    % Generate cols.
    cols = cell(ncols, 1);
    for i = 1 : ncols
        
        if smcd.params.verbose
            fprintf(Utils.padn(sprintf('  collateral  #%d  ', i), 'both', ':', 40))
        end
        
        % Initialize branch.
        cols{i} = Branch();
        cols{i}.label = sprintf('col%d', i);
        cols{i}.model = this.model;
        
        % Generate random diameter.
        cols{i}.diam = lognrnd(mu, sig);
        
        % Generate path.
        intP = [P1s(i, :); P2s(i, :); P3s(i, :)];
        d = norm(P2s(i, :) - P1s(i, :)) + norm(P3s(i, :) - P2s(i, :));
        nP = round(smcd.params.pdens * d);
        cols{i}.path = Geom.interparc(nP, intP(:, 1), intP(:, 2), intP(:, 3), 'verbose', smcd.params.verbose);
        
        % Derive and build branch's compartments' coordinates.
        cols{i}.build_comps_coords(0);
        
        % Initialize cell-arrays for branch's compartments potentials.
        cols{i}.VN = cell(smcd.get_nAS(), 1);
        cols{i}.VP1 = cell(smcd.get_nAS(), 1);
        cols{i}.VP2 = cell(smcd.get_nAS(), 1);
        cols{i}.VI = cell(smcd.get_nAS(), 1);
        
        % Store attachment point.
        cols{i}.p_attach = locs{i};
        
    end
    
    % Return.
    this.cols = cols;
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting collaterals. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end
    
end
