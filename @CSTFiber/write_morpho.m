function write_morpho(this, dir_export, perm)
    % Write the CSTFiber morphology information to textfile.
    %
    % Usage:
    %   cst.write_morpho(dir_export)
    %   cst.write_morpho(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string :
    %       Path to global export directory.
    %
    %   perm : integer :
    %       Writing permission level.
    %
    % Description: 
    %   A single textfile is written, the lines of which are organized as
    %   follows: 
    %       1. diam
    %       2. ncols
    %       3. CSTbranch.diam
    %       4. CSTbranch.nN
    %       5. cols{1}.diam
    %       6. cols{1}.nN
    %       7. cols{1}.pAttach
    %       8. cols{2}.diam
    %       9. cols{2}.nN
    %      10. cols{2}.pAttach
    %             .
    %             .
    %       n. cols{m}.diam
    %   (n+1). cols{m}.nN
    %   (n+2). cols{m}.pAttach
    %
    %   Permission to write (perm=1) is granted by default.
    
    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    folder = fullfile(dir_export, this.musc, this.seg, sprintf('CSTFiber%d', this.fidx));
    file_path = fullfile(folder, 'morpho.txt');

    if perm || ~ exist(file_path, 'file')

        % Write fiber morphology info.
        morpho_text = sprintf('%f\n', this.diam);
        morpho_text = [morpho_text sprintf('%d\n', size(this.cols, 1))];

        % Write CSTbranch morphology file.
        morpho_text = [morpho_text sprintf('%f\n', this.CSTbranch.diam)];
        morpho_text = [morpho_text sprintf('%d\n', this.CSTbranch.nN)];

        % Write Collaterals morphology file.
        for n = 1 : size(this.cols, 1)
            morpho_text = [morpho_text sprintf('%f\n', this.cols{n}.diam)];
            morpho_text = [morpho_text sprintf('%d\n', this.cols{n}.nN)];
            morpho_text = [morpho_text sprintf('%s\n', this.cols{n}.p_attach)];
        end

        % Write info to file.
        fileID = fopen(file_path, 'w');
        fprintf(fileID, morpho_text);
        fclose(fileID);
        
    end
    
end
