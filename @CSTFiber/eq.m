function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = eq@Fiber(obj1, obj2);
    if ~ outbool
        return
    else
        outbool = false;
    end
    
    if ~ (obj1.CSTbranch == obj2.CSTbranch)
        fprintf('Attributes `CSTbranch` are different.\n')
        return
    end
    
    if ~ all(cellfun(@(x, y) x == y, obj1.cols, obj2.cols))
        fprintf('Attributes `cols` are different.\n')
        return
    end
    
    outbool = true;
    
end
