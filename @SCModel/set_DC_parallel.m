function set_DC_parallel(this)
	% Set the attribute DC when morpho.DC_repr is 'PARALLEL'.
	%
	% Usage:
    %   sc.set_DC_parallel()
	%
	% Description:
	%   This method is called by the method set_DC when morpho.DC_repr is
	%   'PARALLEL'.
    %   It computes the DC compartments as parallel arrangements of
    %   subregions of the total DC region in each cross-section.
    %   This parallelism is materialized by parallel lines separating
    %   neighboring DC compartments from one another.
    %   Refer to method 'set_DC' for explanations on attribute DC and to
    %   the corpus of the present method to understand the algorithm
    %   employed.
    
    % Unpack attributes in convenient variables.
    CS = this.CS;
    nCS = this.morpho.nCS;
    
    % Initialize DC array. As mentionned in method 'set_DC', there are as
    % many DC compartments as there are CS in the SCModel.
    % The array DC shall contain 3 columns, again as explained in method
    % 'set_DC'.
    DC = cell(nCS, 3);
    
    % The second column of this array shall itself be a cell-array with 3
    % columns. The lengths of these inner arrays are the numbers of CS in
    % which the concerned DC compartments have contours.
    % For instance, the first DC compartment (index=1 in the array DC) has
    % contours in each CS, while the last DC compartment has contours only
    % in the rostral-most CS (cf 'set_DC' method).
    for i = 1 : nCS
        DC{i, 2} = cell(nCS - i + 1, 3);
    end
    
    % Slope between the x-axis and the DC compartment contours' oblique
    % lines.
    slope = this.morpho.DC_slope;
    
    % Build DC.
    
    % The general strategy is as follows: we proceed iteratively by
    % looping over the CS. For each CS, we define the contour and area of
    % every DC compartment that has a contour in that CS.
    % We start with the most caudal CS.
    % The index of the currently accessed CS is stored in idxCS.
    idxCS = nCS;
    
    % In this CS, the total DC region coincides with the 1st DC compartment
    % because it is the only one present in that CS.
    DC{1, 1} = CS{idxCS}.name;
    DC{1, 2}{1, 1} = CS{idxCS}.name;
    DC{1, 2}{1, 2} = CS{idxCS}.DC.contour;
    DC{1, 2}{1, 3} = CS{idxCS}.DC.area;
    DC{1, 3} = CS{idxCS}.DC.area;
    
    % We store the index of the lastly created DC compartment in idxDC.
    idxDC = 1;    
    
    % We will need to store the areas of the DC compartment contours in the
    % current CS before moving to the next CS, in order to compute the
    % contours of the very same DC compartments in the next CS.
    % Here it consists only in the area of the unique DC compartment
    % present in that CS.
    pre_DCCC_areas = CS{idxCS}.DC.area;
    
    % For each subsequent CS, the strategy is the following:
    % 1. isolate the subregion allocated to the DC compartment that takes
    %    origin in the CS out of the total DC region of the CS (it is the
    %    lateral-most subregion of the total DC region of the CS)
    % 2. split and distribute the complementary medial subregion among the
    %    previous DC compartments
    % Let's go.
    while idxCS >= 2
        
    	% Decrement CS index.
    	idxCS = idxCS - 1;
        
    	% Increment DC index: a new compartment takes origin in the newly
    	% accessed CS.
    	idxDC = idxDC + 1;
        
    	% If needed, rectify the areas of the previous DC compartments
        % contours in previous CS, so that they can fit in the newly
        % accessed CS.
    	pre_DCCC_areas = Geom.rectify_areas(pre_DCCC_areas, CS{idxCS}.DC.area);
        pre_DCCC_total_area = sum(pre_DCCC_areas);
        
    	% Extract contour and area of new DC compartment.
    	[new_DCCC, new_DCCC_area, rem_contour, ~] = this.get_new_DCCC(CS{idxCS}, pre_DCCC_total_area);
        
    	% Split and distribute the complementary medial contour among the
    	% previous DC compartments.
    	[pre_DCCCs, pre_DCCC_areas] = Geom.split_parallel(rem_contour, pre_DCCC_areas, slope);
        
    	% Fill object DC with newly computed contours and areas.
        
        % Fisrt, the new DC compartment.
        DC{idxDC, 1} = CS{idxCS}.name;
        DC{idxDC, 2}{1, 1} = CS{idxCS}.name;
    	DC{idxDC, 2}{1, 2} = new_DCCC;
    	DC{idxDC, 2}{1, 3} = new_DCCC_area;
    	DC{idxDC, 3} = CS{idxCS}.DC.area;
        
        % Second, the previous DC compartments.
        for i = 1 : idxDC - 1
            DC{i, 2}{idxDC - i + 1, 1} = CS{idxCS}.name;
            DC{i, 2}{idxDC - i + 1, 2} = pre_DCCCs{end - i + 1};
    		DC{i, 2}{idxDC - i + 1, 3} = pre_DCCC_areas(end - i + 1);
        end
        
        % Update variable preDCCCareas for next CS.
        pre_DCCC_areas = [new_DCCC_area; pre_DCCC_areas];
        
    end
    
    % Assign attribute.
    this.DC = DC;
    
end
