function export_roots_coords(this)
    % Write the roots coordinates to textfiles.
    %
    % Usage:
    %   sc.export_roots_coords()
    %
    % Description:
    %   1 textfile for each CS and each generatrix of each left ventral and
    %   dorsal root full branches are written.
    %   The textfiles are written in subfolder 'roots' of dirs.coords.
    
    % Make dirs.coords directory if needed.
    if ~ exist(this.dirs.coords, 'dir')
        mkdir(this.dirs.coords);
    end
    
    % Make 'roots' subfolder. Erase previous version if already present in
    % the file system.
    dir_roots = fullfile(this.dirs.coords, 'roots');
    if exist(dir_roots, 'dir')
        rmdir(dir_roots, 's');
    end
    mkdir(dir_roots);
    
    % Loop over roots.
    for i = 1 : this.morpho.nroots
        root = this.roots{i};
        
        % Dorsal full branches.
        for j = 1 : root.LD.n_full_branches
            
            % Cross-sections.
            CS = root.get_CS('LD', j);
            for k = 1 : root.LD.full_branches_nCS
                fileID = fopen(fullfile(dir_roots, sprintf('%s_left_dorsal_branch_%d_CS_%d.txt', root.name, j, k)), 'w');
                fprintf(fileID, '%.14f %.14f %.14f\n', CS{k}');
                fclose(fileID);
            end
            
            % Generatrices.
            Gen = root.get_Gen('LD', j);
            for k = 1 : 4
                fileID = fopen(fullfile(dir_roots, sprintf('%s_left_dorsal_branch_%d_Gen_%d.txt', root.name, j, k)), 'w');
                fprintf(fileID, '%.14f %.14f %.14f\n', Gen{k}');
                fclose(fileID);
            end
            
            % Guide curve.
            GC = root.get_GC('LD', j);
            fileID = fopen(fullfile(dir_roots, sprintf('%s_left_dorsal_branch_%d_GC.txt', root.name, j)), 'w');
            fprintf(fileID, '%.14f %.14f %.14f\n', GC');
            fclose(fileID);
            
        end
        
        % Ventral full branches.
        for j = 1 : root.LV.n_full_branches
            
            % Cross-sections.
            CS = root.get_CS('LV', j, 'without_spinerv');
            for k = 1 : size(CS, 1)
                fileID = fopen(fullfile(dir_roots, sprintf('%s_left_ventral_branch_%d_CS_%d.txt', root.name, j, k)), 'w');
                fprintf(fileID, '%.14f %.14f %.14f\n', CS{k}');
                fclose(fileID);
            end
            
            % Generatrices.
            Gen = root.get_Gen('LV', j, 'without_spinerv');
            for k = 1 : 4
                fileID = fopen(fullfile(dir_roots, sprintf('%s_left_ventral_branch_%d_Gen_%d.txt', root.name, j, k)), 'w');
                fprintf(fileID, '%.14f %.14f %.14f\n', Gen{k}');
                fclose(fileID);
            end
            
            % Guide curve.
            GC = root.get_GC('LV', j, 'without_spinerv');
            fileID = fopen(fullfile(dir_roots, sprintf('%s_left_ventral_branch_%d_GC.txt', root.name, j)), 'w');
            fprintf(fileID, '%.14f %.14f %.14f\n', GC');
            fclose(fileID);
            
        end
        
    end
    
end
