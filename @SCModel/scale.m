function scale(this, scale_factors)
    % Scale the geometric attributes of the SCModel.
    %
    % Usage:
    %   sc.scale(scale_factors)
        
    this.geom.zCS = this.geom.zCS * scale_factors(3);
    this.geom.roots_map(:, 1) = this.geom.roots_map(:, 1) * scale_factors(1);
    this.geom.roots_map(:, 2) = this.geom.roots_map(:, 2) * scale_factors(2);
    this.geom.roots_map(:, 3) = this.geom.roots_map(:, 3) * scale_factors(3);
    this.geom.roots_map(:, 4) = this.geom.roots_map(:, 4) * scale_factors(1);
    this.geom.roots_map(:, 5) = this.geom.roots_map(:, 5) * scale_factors(2);
    this.geom.roots_map(:, 6) = this.geom.roots_map(:, 6) * scale_factors(3);
    cellfun(@(x) x.scale(scale_factors), this.CS);
    this.set_dims();
    cellfun(@(x) x.scale(scale_factors), this.eCS);
    this.set_DC();
    this.set_OD();
    cellfun(@(x) x.scale(scale_factors), this.roots);
    this.roots_params.alphaD = [];
    this.roots_params.alphaV = [];
    this.roots_params.diam = [];
    this.roots_params.exc = [];
    this.roots_params.eps = [];
    this.roots_params.GC_pdens = [];
    this.roots_params.gap_GC =[];
    this.roots_params.CS_dens = [];
    this.roots_params.len_spinerv = [];
    
end
