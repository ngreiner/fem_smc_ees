function CS = get_CS_below(this, z)
    % Return the cross-section below a specific z-coordinate. 
    %
    % Parameters:
    %   z   : double    : z-coordinate.
    %
    % Return:
    %   CS  : CrossSection object   : cross-section directly below the
    %                                 input z-coordinate z.
    %
    % Description:
    %   Returns the cross-section at z=z_below (see method 'get_z_below').
    z_below = this.get_z_below(z);
    CS = this.CS{cellfun(@(x) isequal(x.z, z_below), this.CS)};
end
