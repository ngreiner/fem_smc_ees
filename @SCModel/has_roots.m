function bool = has_roots(this, seg_name)
    % Return a boolean indicating if the specified segment has roots.
    %
    % Usage:
    %   bool = sc.has_roots(seg_name)
    %
    % Parameters:
    %   seg_name : string : name of segment.
    %
    % Return:
    %   bool : logical : true if segment has roots, false otherwise.
    %
    % Description:
    %   A segment has roots if his name figures in the array
    %   morpho.segs_with_roots.
    bool = any(strcmp(seg_name, this.morpho.segs_with_roots));
end
