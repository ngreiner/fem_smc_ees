classdef SCModel < handle
    
    % Properties defined here:
    %
    %    dirs : struct :
    %       Each field is a directory (top, CS, resources, SW, Roots).
    %
    %    geom : struct :
    %       Geometric parameters descriptor (fields are zCS and roots_map).
    %
    %    morpho : struct :
    %       Morphologic parameters descriptor (fields are nseg, seg_names,
    %       nCS, CS_names, nroots, segs_with_roots, ratio_exts, l_exts,
    %       DC_repr, DC_slope, seg_length, WM_max_trans_width,
    %       WM_max_sag_width, CSF_max_trans_width, CSF_max_sag_width,
    %       fat_max_trans_width, fat_max_sag_width, dura_max_trans_width,
    %       dura_max_sag_width).
    %
    %    roots_params : struct :
    %       Roots' parameters descriptor (fields are alphaV, alphaD, diam,
    %       exc, excREZ, eps, GC_type, GC_pdens, GC_gap, CS_dens,
    %       with_rlet, nrlets, LD_rlets_nCS, LV_rlets_nCS, with_spinerv,
    %       spinerv_diam, spinerv_length, spinerv_exc).
    %
    %    CS : N*1 cell of CrossSections :
    %       Cross-sections of the SCModel. See class CrossSection.
    %
    %    eCS : K*1 cell of CrossSections :
    %       Cross-sections of the extended portions of the SCModel.
    %
    %    Gen : struct :
    %       Guide curves coordinates (fields are GM, WM, CSF, dura, fat).
    %
    %    DC : P*1 cell :
    %       Dorsal columns compartments descriptor. See method set_DC.
    %
    %    OD : M*1 cell :
    %       Coordinates of outer dorsal-columns compartments.
    %
    %    roots : R*1 cell of Roots :
    %       Roots of the SCModel. See class 'Root'.
    
    properties
        dirs            % struct
        geom            % struct
        morpho          % struct
        roots_params    % struct
        CS              % N*1 cell of CrossSections
        eCS             % K*1 cell of CrossSections
        Gen             % struct
        DC              % P*1 cell
        OD              % M*1 cell
        roots           % R*1 cell of Roots
    end
    
    methods
        
        function this = SCModel(varargin)
            % Class constructor method.
            %
            % Usage:
            %   sc = SCModel()
            %   sc = SCModel('topdir', topdir, ...
            %                      'config_file_name', config_file_name)
            %
            % Optional parameters:
            %   topdir              : str   : path to top-level directory.
            %   config_file_name    : str   : name of configuration file.
            %
            % Description:
            %   The SCModel class attempts to describe the geometrical
            %   features of a portion of spinal cord.
            %   
            %   In short, the elements which are represented are: the grey
            %   matter (GM), the white matter (WM), the dural sac (CSF),
            %   the dura matter (dura), the epidural tissue (fat) and the
            %   spinal roots.
            %
            %   To set-up a SCModel, it suffice to specify the path of a
            %   directory, called 'top-level' directory, and the name of a
            %   configuration file (which shall be found by the constructor
            %   in th tree below the top-level directory). What is more,
            %   the tree below the top-level directory should contain
            %   resources necessary to build the SCModel.
            %
            %   If the path to a top-level directory is not provided in
            %   argument, the constructor sets the working directory as 
            %   top-level directory.
            %
            %   If no configuration file is provided in argument, the
            %   constructor instantiate the SCModel object but do not build
            %   any of its attributes (except for dirs).
            
            idx = find(strcmp('topdir', varargin));
            if idx
                this.set_dirs(varargin{idx + 1});
            else
                this.set_dirs(pwd)
            end
            
            idx = find(strcmp('config_file_name', varargin));
            if idx
                file_path = fullfile(this.dirs.resources, 'sc_config_files', varargin{idx + 1});
                this.parse_config_file(file_path);
                this.build();
            end

        end
        
    end
    
    methods
        outbool = eq(obj1, obj2)
        from_struct(this, struc)
        MR_point = gen_MR_point(this, z, direction)
        VQ_points = gen_VQ_points(this, seg_name, np, z)
        contour = get_DC_comp_contour(this, DC_comp_name, CS_name)
        [idx1, idx2] = get_DC_comp_contour_idxs(this, DC_comp_name, CS_name)
        contours = get_DC_comp_contours(this, DC_comp_name)
        ymin_CSF = get_ymin_CSF(this, z)
        ymin_dura = get_ymin_dura(this, z)
        ymin_fat = get_ymin_fat(this, z)
        ymin_WM = get_ymin_WM(this, z)
        bool = has_roots(this, seg_name)
        plot_roots(this)
        plotCS(this, CSnames)
        plotDC(this, varargin)
        plotOD(this)
        scale(this, scale_factors)
        set_CS(this)
        set_DC(this)
        set_DC_ext(this, neCS)
        set_DC_parallel(this)
        set_dims(this)
        set_dirs(this, topdir)
        set_eCS(this)
        set_GC(this)
        set_OD(this)
        set_roots(this)
        struc = to_struct(this)
        bool = with_rlets(this)
        bool = with_spinerv(this)
    end

    methods (Static)
        [l_contour, l_area, m_contour, m_area] = get_new_DCCC(CS, prev_area)
    end
    
end
