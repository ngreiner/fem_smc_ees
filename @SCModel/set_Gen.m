function set_Gen(this)
    % Set the generatrices of the GM, WM, CSF, dura and fat.
    %
    % Usage:
    %   sc.set_Gen()
    %
    % Description:
    %   There are two generatrices for each structure (GM, WM, CSF, dura,
    %   fat). Those generatrices connect topologically coincident points of
    %   each CS and extended CS of the concerned structure.
    %   The 1st guide curve is made of the points satisfying to x=0 and
    %   y>0.
    %   The 2nd guide curve is made of the points satisfying to x=0 and
    %   y<0.
    
    % Unpack attributes in convenient variables.
    CS = this.CS;
    eCS = this.eCS;
    nCS = this.morpho.nCS;
    r_exts = this.morpho.ratio_exts;
    
    % Initialize guide curves.
    GM_Gen = cell(2, 1);
    GM_Gen{1} = zeros(nCS + 2 * r_exts, 3);
    GM_Gen{2} = zeros(nCS + 2 * r_exts, 3); 
    WM_Gen = cell(2, 1);
    WM_Gen{1} = zeros(nCS + 2 * r_exts, 3);
    WM_Gen{2} = zeros(nCS + 2 * r_exts, 3);
    CSF_Gen = cell(2, 1);
    CSF_Gen{1} = zeros(nCS + 2 * r_exts, 3);
    CSF_Gen{2} = zeros(nCS + 2 * r_exts, 3);
    fat_Gen = cell(2, 1);
    fat_Gen{1} = zeros(nCS + 2 * r_exts, 3);
    fat_Gen{2} = zeros(nCS + 2 * r_exts, 3);
    dura_Gen = cell(2, 1);
    dura_Gen{1} = zeros(nCS + 2 * r_exts, 3);
    dura_Gen{2} = zeros(nCS + 2 * r_exts, 3);
    
    % Form guide curves.
    for k = 1 : 2
        for i = 1 : nCS + 2 * (r_exts + 1)
            if i <= r_exts + 1
                GM_Gen{k}(i, :) = eCS{i}.GM(1 + (k - 1) * floor(size(eCS{i}.GM, 1) / 2), :);
                WM_Gen{k}(i, :) = eCS{i}.WM(1 + (k - 1) * floor(size(eCS{i}.WM, 1) / 2), :);
                CSF_Gen{k}(i, :) = eCS{i}.CSF(1 + (k - 1) * floor(size(eCS{i}.CSF, 1) / 2), :);
                fat_Gen{k}(i, :) = eCS{i}.fat(1 + (k - 1) * floor(size(eCS{i}.fat, 1) / 2), :);
                dura_Gen{k}(i, :) = eCS{i}.dura(1 + (k - 1) * floor(size(eCS{i}.dura, 1) / 2), :);
            elseif i <= r_exts + 1 + nCS
                GM_Gen{k}(i, :) = CS{i - (r_exts + 1)}.GM(1 + (k - 1) * floor(size(CS{i - (r_exts + 1)}.GM, 1) / 2), :);
                WM_Gen{k}(i, :) = CS{i - (r_exts + 1)}.WM(1 + (k - 1) * floor(size(CS{i - (r_exts + 1)}.WM, 1) / 2), :);
                CSF_Gen{k}(i, :) = CS{i - (r_exts + 1)}.CSF(1 + (k - 1) * floor(size(CS{i - (r_exts + 1)}.CSF, 1) / 2), :);
                fat_Gen{k}(i, :) = CS{i - (r_exts + 1)}.fat(1 + (k - 1) * floor(size(CS{i - (r_exts + 1)}.fat, 1) / 2), :);
                dura_Gen{k}(i, :) = CS{i - (r_exts + 1)}.dura(1 + (k - 1) * floor(size(CS{i - (r_exts + 1)}.fat, 1) / 2), :);
            else
                GM_Gen{k}(i, :) = eCS{i - nCS}.GM(1 + (k - 1) * floor(size(eCS{i - nCS}.GM, 1) / 2), :);
                WM_Gen{k}(i, :) = eCS{i - nCS}.WM(1 + (k - 1) * floor(size(eCS{i - nCS}.WM, 1) / 2), :);
                CSF_Gen{k}(i, :) = eCS{i - nCS}.CSF(1 + (k - 1) * floor(size(eCS{i - nCS}.CSF, 1) / 2), :);
                fat_Gen{k}(i, :) = eCS{i - nCS}.fat(1 + (k - 1) * floor(size(eCS{i - nCS}.fat, 1) / 2), :);
                dura_Gen{k}(i, :) = eCS{i - nCS}.dura(1 + (k - 1) * floor(size(eCS{i - nCS}.fat, 1) / 2), :);
            end
        end
    end
    
    % Assign to attributes.
    this.Gen.GM = GM_Gen;
    this.Gen.WM = WM_Gen;
    this.Gen.CSF = CSF_Gen;
    this.Gen.dura = dura_Gen;
    this.Gen.fat = fat_Gen;
    
end
