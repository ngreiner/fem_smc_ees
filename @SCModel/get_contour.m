function contour = get_contour(this, structure, z, position)
    % Return the specified cross-sectional contour.
    %
    % Usage:
    %   sc.get_contour(structure, z, position)
    %
    % Parameters:
    %   structure   : string    : name of the structure ('GM', 'WM', 'CSF',
    %                             'dura' or 'fat').
	%   z           : double    : z-coordinate of interest.
	%   position    : string    : position indicator ('above', 'below' or
	%                             'exact'). 
    %
    % Return:
    %   contour     : N*3 double    : contour coordinates.
    %
    % Description:
    %   This method first fetches the cross-section directly above or below
    %   the specified z-coordinate and then fetches the contour of the
    %   specified structure in this cross-section.
    %   If the specified position is 'exact' or is omitted, the specified
    %   z-coordinate should be one of an existing cross-section.

    % Parse inputs.
    if nargin == 3
        position = 'exact';
    end
    
    CS = this.get_CS(z, position);
    contour = CS.(structure);
    
end
