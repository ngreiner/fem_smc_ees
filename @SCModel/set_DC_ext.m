function set_DC_ext(this, neCS)
    % Update the attribute DC to take into account the extended CS.
    %
    % Usage:
    %   sc.set_DC_ext()
    %   sc.set_DC_ext(neCS)
    %
    % Optional parameters:
    %   neCS : int : number of extended CS. 
    %
    % Description:
    %   Computes the coordinates of the DC compartments in the rostrally
    %   extended CS of the SCModel, and adds them to the attribute DC.
    %   This method exploits the fact that the rostrally extended CS
    %   are identical to the rostral-most (unextended) CS with the sole
    %   difference of their z-coordinates.
    %   Therefore, the DC compartments in the extended CS are replicas,
    %   modified only in their z-coordinates, of the DC compartments found
    %   in the rostral-most unextended CS.
    %
    %   If neCS is specified, it gives the number of extended CS for which
    %   the DC compartments should be prolonged.
    
    % First, we arrange the rostrally extended cross-sections from most
    % caudal to most rostral for consistency with the structure of
    % attribute DC.
    eCS = flipud(this.eCS(1 : this.morpho.ratio_exts + 1));
    
    if nargin == 2
        eCS = eCS(1 : neCS);
    end
    
    % For all the DC compartments, build their contour in every rostrally
    % extended CS and add them in the attribute DC.
    for i = 1 : size(this.DC, 1)
        
        % Fetch contour and area of DC compartment in rostral-most
        % unextended CS.
        contour = this.DC{i, 2}{end, 2};
        area = this.DC{i, 2}{end, 3};
        
        % Build and add new contours in extended CS.
        for j = 1 : size(eCS, 1)
            this.DC{i, 2}{end + 1, 1} = eCS{j}.name;
            this.DC{i, 2}{end, 2} = [contour(:, 1 : 2), eCS{j}.z * ones(size(contour, 1), 1)];
            this.DC{i, 2}{end, 3} = area;
        end
        
    end
    
end
    