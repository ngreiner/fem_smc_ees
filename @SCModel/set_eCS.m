function set_eCS(this)
    % Set the attribute eCS.
    %
    % Usage:
    %   sc.set_eCS()
    %
    % Description:
    %   eCS is a cell-array of CrossSection objects, representing the
    %   cross-sections of the extended parts of the SCModel. 
    %   The eCS can be separated in two sets:
    %       1 composed of the rostrally-extended CS
    %       1 composed of the caudally-extended CS
    %   Each set is made of ratio_exts + 1 extended CS.
    %   Among these, #ratio_exts are regularly spaced by a multiple
    %   integer of morpho.seg_length. The (ratio_exts+1)-th is positioned
    %   at one 10-th of morpho.seg_length from the rostral-most or
    %   caudal-most CS of the SCModel.
    
    % Unpack attributes in convenient variables.
    r_exts = this.morpho.ratio_exts;
    l_exts = this.morpho.l_exts;
    CS = this.CS;
    
    % Initialize extended CS array.
    eCS = cell(2 * (r_exts + 1), 1);
    
    % Rostrally-extended CS.
    generic_name = CS{1}.name;
    dir = CS{1}.dir;
    
    % First rostrally-extended CS.
    zCS = CS{1}.z + l_exts / r_exts / 10;
    name_CS = sprintf('%s_Up', generic_name);
    cCS = CrossSection(name_CS, dir, zCS);
    eCS{r_exts + 1} = cCS;
    
    % Subsequent rostrally-extended CS.
    zCS = CS{1}.z;
    for i = 1 : r_exts
        zCS = zCS + l_exts / r_exts;
        name_CS = sprintf('%s_Up', name_CS);
        cCS = CrossSection(name_CS, dir, zCS);
        eCS{r_exts + 1 - i} = cCS;
    end
    
    % Caudally-extended cross-sections.
    generic_name = CS{end}.name;
    dir = CS{end}.dir;
    
    % First caudally-extended CS.
    zCS = CS{end}.z - l_exts / r_exts / 10;
    name_CS = sprintf('%s_Down', generic_name);
    cCS = CrossSection(name_CS, dir, zCS);
    eCS{r_exts + 2} = cCS;
    
    % Subsequent caudally-extended CS.
    zCS = CS{end}.z;
    for i = 1 : r_exts
        zCS = zCS - l_exts / r_exts;
        name_CS = sprintf('%s_Down', name_CS);
        cCS = CrossSection(name_CS, dir, zCS);
        eCS{r_exts+2+i} = cCS;
    end
    
    % Assign to attribute.
    this.eCS = eCS;
    
end
