function contours = get_DC_comp_contours(this, DC_comp_name)
    % Return the cross-sectional contours of the specified DC compartment.
    %
    % Usage:
    %   contours = sc.get_DC_comp_contours(DC_comp_name)
    %
    % Parameters:
    %   DC_comp_name    : string    : name of a DC compartment.
    %
    % Return:
    %   contours    : N*3 cell  : DC compartment contours.
    %
    % Description:
    %   Contours are retrieved by name matching.
    idx = strcmp(DC_comp_name, this.DC(:, 1));
    contours = this.DC{idx, 2};
end
