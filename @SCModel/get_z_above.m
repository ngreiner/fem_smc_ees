function z_above = get_z_above(this, z)
    % Return the z-coordinate of the cross-section above the specified z.
    %
    % Usage:
    %   z_above = sc.get_z_above(z)
    %
    % Parameters:
    %   z   : double.
    %
    % Return:
    %   z_above : double.
    
    if any(this.geom.zCS == z)
        z_above = z;
        
    elseif z < 0
        z_above = 0;
        
    elseif z > this.geom.zCS(1)
        z_above = this.geom.zCS(1);
        
    else
        stock = sort(this.geom.zCS(this.geom.zCS > z));
        z_above = stock(1);
        
    end
    
end
