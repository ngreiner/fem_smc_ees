function CS = get_CS_above(this, z)
    % Return the cross-section above a specific z-coordinate.
    %
    % Usage:
    %   CS = sc.get_CS_above(z)
    %
    % Parameters:
    %   z   : double    : z-coordinate.
    %
    % Return:
    %   CS  : CrossSection object.
    %
    % Description:
    %   Returns the cross-section at z=z_above (see method 'get_Z_above').
    z_above = this.get_z_above(z);
    CS = this.CS{cellfun(@(x) isequal(x.z, z_above), this.CS)};
end
