function bool = with_spinerv(this)
    % Return roots_params.with_spinerv.
    %
    % Usage:
    %   bool = sc.with_spinerv()
    %
    % Return:
    %   bool    : logical.
    bool = this.roots_params.with_spinerv;
end
