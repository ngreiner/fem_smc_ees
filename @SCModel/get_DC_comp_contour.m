function contour = get_DC_comp_contour(this, DC_comp_name, CS_name)
    % Return the specified contour of the specified DC compartment.
    %
    % Usage:
    %   contour = sc.get_DC_comp_contour(DC_comp_name, CS_name)
    %
    % Parameters:
    %   DC_comp_name    : string    : name of a DC compartment.
    %   CS_name         : string    : name of a cross-section.
    %
    % Return:
    %   contour     : N*3 double    : DC compartment contour.
    %
    % Description:
    %   contour is retrieved by name matching.
    
    % Fetch all DC compartment contours.
    contours = this.get_DC_comp_contours(DC_comp_name);

    % Fetch contour in CS named CSname.
    idx = cellfun(@(x) any(regexp(x, sprintf('%s$', CS_name))), contours(:, 1));
    if ~ any(idx)
        contour = [];
    else
        contour = contours{idx, 2};
    end
    
end    
    