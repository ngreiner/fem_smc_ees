function build(this)
    % Build the CS, GC, DC, OD and roots of the SCModel.
    %
    % Usage:
    %   sc.build()
    %
    % Description:
    %   The build requires that all the necessary parameters be already
    %   properly defined.
    this.set_CS();
    this.set_dims();
    this.set_eCS();
    this.set_Gen();
    this.set_DC();
    this.set_OD();
    this.set_roots();
end
