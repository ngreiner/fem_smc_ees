function set_OD(this)
    % Set the attribute OD.
    %
    % Usage:
    %   sc.set_OD()
    %
    % Description:
    %   OD is a cell-array containing the contours of the outer dorsal
    %   columns compartments.
    %   The outer dorsal-columns consist in 6 adjacent regular compartments
    %   spanning the outermost layer of the dorsal-columns. They are
    %   organized in two sublayers.
    %   The outermost, medial-most compartment is number #1. While staying
    %   in the outermost layer and moving laterally, we glide over
    %   compartments #2 and #3 sequentially.
    %   Again from medial to lateral within the second outermost layer, we
    %   glide over compartments #4, #5 and #6.
    
    nCS = this.morpho.nCS;
    OD = cell(6, 1);
    for i = 1 : 6
        OD{i} = cell(nCS, 1);
        for j = 1 : nCS
            OD{i}{j} = this.CS{j}.OD{i};
        end
    end
    this.OD = OD;
    
end
