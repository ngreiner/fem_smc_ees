function z_below = get_z_below(this, z)
    % Return the z-coordinate of the cross-section below the specified z.
    %
    % Usage:
    %   z_below = sc.get_z_below(z)
    %
    % Parameters:
    %   z   : double.
    %
    % Return:
    %   z_below     : double.
    
    if any(this.geom.zCS == z);
        z_below = z;
        
    elseif z < 0;
        z_below = 0;
        
    else
        stock = sort(this.geom.zCS(this.geom.zCS < z));
        z_below = stock(end);
        
    end
    
end
