function set_roots(this)
    % Set the attribute roots.
    %
    % Usage:
    %   sc.set_roots()
    %
    % Description:
    %   roots is a cell-array of Root objects.
    %   Each Root represents the spinal roots of a given segment.
    %   See class Root.
    this.roots = cell(this.morpho.nroots, 1);
    for i = 1 : this.morpho.nroots
        name = this.morpho.segs_with_roots{i};
        this.roots{i} = Root(name, this);
    end
end
