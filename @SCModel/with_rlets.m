function bool = with_rlets(this)
    % Return roots_params.with_rlets.
    %
    % Usage:
    %   bool = sc.with_rlets()
    %
    % Return:
    %   bool    : logical.
    bool = this.roots_params.with_rlets;
end
