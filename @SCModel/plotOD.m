function plotOD(this)
    % Produce a plot displaying the cross-sections along with their 
    % outer dorsal column compartments contours.
    %
    % Usage:
    %   sc.plotOD()

    % Initialize figure.
    figure()
    view(1, 28)
    hold on
    xmax =  1.1 * (this.dims.seg_length + 2 * this.dims.l_exts) / 2;
    xmin = -1.1 * (this.dims.seg_length + 2 * this.dims.l_exts) / 2;
    ymax =  xmax;
    ymin =  xmin;
    zmax =  1.1 * (this.dims.seg_length + this.dims.l_exts);
    zmin = -1.1 * this.dims.l_exts;
    axis([xmin xmax ymin ymax zmin zmax])
    
    % Plot cross-sections.
    for i = 1 : this.morpho.nCS
        CS = this.CS{i};
        plot3(CS.GM(:, 1), CS.GM(:, 2), CS.GM(:, 3), 'k');
        plot3(CS.WM(:, 1), CS.WM(:, 2), CS.WM(:, 3), 'k');
        for j = 1 : 6
            contour = this.OD{j}{i};
            plot3(contour(:, 1), contour(:, 2), contour(:, 3), 'cyan');
        end
    end
    
end