function parse_config_file(this, file_path)
	% Parse the configuration file at file_path.
    %
    % Usage:
    %   sc.parse_config_file(file_path)
    %
    % Parameters:
    %   file_path : string : path to configuration file.
    
    % Open file.
   if exist(file_path, 'file')
        fileID = fopen(file_path);
    else
        error('%s: file not found.', file_path);
    end
    
    % Read file lines.
    C = textscan(fileID, '%s', 'Delimiter', '\n');
    C = C{1};
    
    % Close file.
    fclose(fileID);
    
    % Parse config.
    
    while ~ isempty(C)
        
        if isempty(C{1}) || strcmp(C{1}(1), '#')
            C(1) = [];
            continue
        end
        
        words = strsplit(C{1});
        
        switch words{1}
            
            case 'nsegs'
                this.morpho.nsegs = str2num(words{3});
            
            case 'segments'
                this.morpho.seg_names = words(3 : end);

            case 'nCS'
                this.morpho.nCS = str2num(words{3});
            
            case 'cross_sections'
                this.morpho.CS_names = words(3 : end);
            
            case 'z_cross_sections'
                this.geom.zCS = cellfun(@str2num, words(3 : end));

            case 'nroots'
                this.morpho.nroots = str2num(words{3});
            
            case 'segs_with_roots'
                this.morpho.segs_with_roots = words(3 : end);
            
            case 'roots_map'
                this.geom.roots_map = cellfun(@str2num, words(3 : end));                
                for i = 2 : this.morpho.nroots
                    C(1) = [];
                    words = strsplit(C{1});
                    this.geom.roots_map(i, :) = cellfun(@str2num, words);
                end
                
            case 'roots_alphaD'
                this.roots_params.alphaD = str2num(words{3});
                
            case 'roots_alphaV'
                this.roots_params.alphaV = str2num(words{3});
                
            case 'roots_diam'
                this.roots_params.diam = str2num(words{3});
                
            case 'roots_npoints_ellipses'
                this.roots_params.npoints_ellipses = str2num(words{3});
                
            case 'roots_exc'
                this.roots_params.exc = str2num(words{3});
                
            case 'roots_excREZ'
                this.roots_params.excREZ = str2num(words{3});
                
            case 'roots_eps_p_entry'
                this.roots_params.eps_p_entry = str2num(words{3});
                
            case 'roots_eps_p_exit_CSF'
                this.roots_params.eps_p_exit_CSF = str2num(words{3});
                
            case 'roots_GC_type'
                this.roots_params.GC_type = words{3};
                
            case 'roots_GC_pdens'
                this.roots_params.GC_pdens = str2num(words{3});
                
            case 'roots_GC_gap'
                this.roots_params.GC_gap = str2num(words{3});
                
            case 'roots_LD_GC_ninter_points'
                this.roots_params.LD_GC_ninter_points = str2num(words{3});
                
            case 'roots_LV_GC_ninter_points'
                this.roots_params.LV_GC_ninter_points = str2num(words{3});
                                
            case 'roots_CS_dens'
                this.roots_params.CS_dens = str2num(words{3});
                
            case 'roots_with_rlets'
                this.roots_params.with_rlets = strcmp(words{3}, 'true');
                
            case 'roots_nrlets'
                this.roots_params.nrlets = str2num(words{3});
                
            case 'roots_LD_rlets_nCS'
                this.roots_params.LD_rlets_nCS = str2num(words{3});
                
            case 'roots_LV_rlets_nCS'
                this.roots_params.LV_rlets_nCS = str2num(words{3});
                
            case 'roots_rlet_diam'
                if strcmp(words{3}, 'auto')
                    this.roots_params.rlet_diam = this.roots_params.diam / sqrt(this.roots_params.nrlets);
                else
                    this.roots_params.rlet_diam = str2num(words{3});
                end
                
            case 'roots_rlet_exc'
                this.roots_params.rlet_exc = str2num(words{3});
                
            case 'roots_rlet_gap'
                if strcmp(words{3}, 'auto')
                    this.roots_params.rlet_gap = this.roots_params.rlet_diam * this.roots_params.rlet_exc / 4;
                else
                    this.roots_params.rlet_gap = str2num(words{3});
                end
                
            case 'roots_with_spinerv'
                this.roots_params.with_spinerv = strcmp(words{3}, 'true');
                
            case 'roots_spinerv_diam'
                this.roots_params.spinerv_diam = str2num(words{3});
                
            case 'roots_spinerv_length'
                this.roots_params.spinerv_length = str2num(words{3});
                
            case 'roots_spinerv_exc'
                this.roots_params.spinerv_exc = str2num(words{3});
                
            case 'ratio_exts'
                this.morpho.ratio_exts = str2num(words{3});
                
            case 'DC_repr'
                this.morpho.DC_repr = words{3};
                
            case 'DC_slope'
                this.morpho.DC_slope = str2num(words{3});

        end
        
        C(1) = [];
        
    end
        
end
