function struc = to_struct(this)
	% Return a struct equivalent to the SCModel object.
    %
    % Usage:
    %   struc = sc.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the SCModel object.
    struc.geom = this.geom;
    struc.morpho = this.morpho;
    struc.roots_params = this.roots_params;
    struc.CS = cellfun(@(x) x.to_struct(), this.CS, 'UniformOutput', false);
    struc.eCS = cellfun(@(x) x.to_struct(), this.eCS, 'UniformOutput', false);
    struc.Gen = this.Gen;
    struc.DC = this.DC;
    struc.OD = this.OD;
    struc.roots = cellfun(@(x) x.to_struct(), this.roots, 'UniformOutput', false);
end
