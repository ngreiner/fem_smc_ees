function ymin = get_ymin_WM(this,z)
    % Estimate the minimal WM y-coordinate at the specified elevation.
    %
    % Usage:
    %   ymin = sc.get_ymin_WM(z)
    %
    % Parameters:
    %   z   : double    : z-coordinate.
    %
    % Return:
    %   ymin    : double.
    %
    % Description:
    %   The estimate is obtained by linear interpolation from
    %   the cross-sections above and below the specified z-coordinate z.
    %   If z corresponds to the level of a CS, the estimate is exact.
    %   If z is < 0, the returned ymin is the one of the last CS
    %   (which lies at z=0).

    CS_above = this.get_CS_above(z);
    CS_below = this.get_CS_below(z);
    
    [w_above, w_below] = this.get_weights(z);
    
    ymin_above = min(CS_above.WM(:, 2));
    ymin_below = min(CS_below.WM(:, 2));
    
    ymin = ymin_above * w_above + ymin_below * w_below;
    
end
