function export_coords(this)
    % Write the coordinates textfiles of the structures of the SCModel.
    %
    % Usage:
    %   sc.export_coords()
    %
    % Description:
    %   See export_structs_coords abd export_roots_coords.
    this.export_structs_coords();
    this.export_roots_coords();
end
