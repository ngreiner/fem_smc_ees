function set_DC(this)
    % Set the attribute DC.
    %
    % Usage:
    %   sc.set_DC()
    %
    % Description:
    %   DC stores the coordinates of the dorsal columns compartments at
    %   each cross-section of the SCModel.
    %
    %   The dorsal column tract is segmented in compartments.
    %   We chose to represent as many DC compartments as there are CS's in
    %   the SCModel.
    %   The DC compartment corresponding to CS#i extends from CS#i to the
    %   uppermost CS of the model. One has to imagine that this compartment
    %   is the continuation of an hypothetical dorsal root entering below
    %   CS#i, upwards in the dorsal columns.
    %   To represent this DC compartment, we draw a contour in each of the
    %   CS from CS#i to the uppermost CS of the model.
    %   In the most caudal CS, below refered to as the first CS, only one
    %   DC compartment is present. It thus coincides with the total DC
    %   region in that CS.
    %   In the next CS, the second CS, which lies just above the first one,
    %   an additional DC compartment takes origin. The total DC region in
    %   that CS is thus split in 2. The lateral-most region is allocated to
    %   the new DC compartment, while the medial region is allocated to the
    %   DC compartment that took origin in the previous CS.
    %   This strategy is continued for the subsequent CS, where the
    %   lateral-most region of the total DC region is allocated to a new DC
    %   compartment while the medial region is allocated to the previous DC
    %   compartments.
    %   The employed algorithm to split the medial region among the
    %   different previous DC compartments depends upon the morpho.DC_repr
    %   attribute.
    %   At present, 'PARALLEL' is the only option available.
    %   The DC compartments are named after the CS in which they take
    %   origin.
    % 
    %   Structure of attribute DC: cell-array with 3 columns.
    %   Column 1: name of DC compartment;
    %   Column 2: cell-array describing the contours of individual DC
    %             compartments. They are organized as follows.
    %               - column 1: names of CS where the contours lie;
    %               - column 2: contours coordinates;
    %               - column 3: areas of contours.
    %   Column 3: areas of total DC regions at corresponding CS.
    
    if strcmp(this.morpho.DC_repr, 'parallel')
        this.set_DC_parallel();
    end
    
    % Prolong DC compartments to the first 2 rostrally extended CS of the
    % SCModel.
    this.set_DC_ext(2);    
    
end
