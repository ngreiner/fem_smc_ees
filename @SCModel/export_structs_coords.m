function export_structs_coords(this)
    % Write the spinal structures (GM, WM, CSF, dura and fat) coordinates
    % to textfiles.
    %
    % Usage:
    %   sc.export_structs_coords()
    %
    % Description:
    %   1 textfile for each generatrix and 1 textfile for each CS of the
    %   GM, WM, CSF, dura and fat are written.
    %   The textfiles are written in subfolder 'structs' of dirs.coords.
    %
    %   For the CS, the textfile names take the form 'X_CS_Y.txt' where X
    %   stands for 'GM', 'WM', 'CSF', 'dura' or 'fat', and Y is the index
    %   of the CS starting from the rostral-most extended CS and ending by
    %   the caudal-most extended CS.
    %
    %   For the generatrices, the textfile names take the form
    %   'X_Gen_Y.txt' where X stands for 'GM', 'WM', 'CSF', 'dura' or
    %   'fat', and Y is the index of the generatrix (=1 or 2).
    
    % Unpack attributes in convenient variables.
    GM_Gen = this.Gen.GM;
    WM_Gen = this.Gen.WM;
    CSF_Gen = this.Gen.CSF;
    dura_Gen = this.Gen.dura;
    fat_Gen = this.Gen.fat;
    CS = this.CS;
    nCS = this.morpho.nCS;
    eCS = this.eCS;
    r_exts = this.morpho.ratio_exts;
    
    % Make dirs.coords folder if needed.
    if ~ exist(this.dirs.coords, 'dir')
        mkdir(this.dirs.coords);
    end
        
    % Make 'structs' folder if needed.
    dir_structs = fullfile(this.dirs.coords, 'structs');
    if exist(dir_structs, 'dir')
        rmdir(dir_structs, 's');
    end
    mkdir(dir_structs);
    
    % Write guide curve textfiles.
    for i = 1 : 2
        
        GM = GM_Gen{i}';
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('GM_Gen_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', GM);
        fclose(fileID);
        
        WM = WM_Gen{i}';
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('WM_Gen_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', WM);
        fclose(fileID);
        
        CSF = CSF_Gen{i}';
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('CSF_Gen_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', CSF);
        fclose(fileID);
        
        fat = fat_Gen{i}';
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('fat_Gen_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', fat);
        fclose(fileID);
        
        dura = dura_Gen{i}';
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('dura_Gen_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', dura);
        fclose(fileID);
        
    end
    
    % Write CS textfiles.
    for i = 1 : nCS + 2 * (r_exts + 1)
        
        if i <= r_exts + 1;
            GM = eCS{i}.GM;
            WM = eCS{i}.WM;
            CSF = eCS{i}.CSF;
            fat = eCS{i}.fat;
            dura = eCS{i}.dura;
            
        elseif i <= r_exts + 1 + nCS;
            GM = CS{i - (r_exts + 1)}.GM;
            WM = CS{i - (r_exts + 1)}.WM;
            CSF = CS{i - (r_exts + 1)}.CSF;
            fat = CS{i - (r_exts + 1)}.fat;
            dura = CS{i - (r_exts + 1)}.dura;
            
        else
            GM = eCS{i - nCS}.GM;
            WM = eCS{i - nCS}.WM;
            CSF = eCS{i - nCS}.CSF;
            fat = eCS{i - nCS}.fat;
            dura = eCS{i - nCS}.dura;
            
        end
        
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('GM_CS_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', GM');
        fclose(fileID);
        
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('WM_CS_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', WM');
        fclose(fileID);
        
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('CSF_CS_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', CSF');
        fclose(fileID);
        
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('dura_CS_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', dura');
        fclose(fileID);
        
        fileID = fopen(fullfile(this.dirs.coords, 'structs', sprintf('fat_CS_%d.txt', i)), 'w');
        fprintf(fileID,'%.14f %.14f %.14f\n', fat');
        fclose(fileID);
        
    end
    
end
