function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if ~ isequal(obj1.dirs, obj2.dirs)
        fprintf('Attributes dirs are different.\n')
        return
        
    elseif ~ isequal(obj1.geom, obj2.geom)
        fprintf('Attributes geom are different.\n')
        return
        
    elseif ~ isequal(obj1.morpho, obj2.morpho)
        fprintf('Attributes morpho are different.\n')
        return
        
    elseif ~ isequal(obj1.roots_params, obj2.roots_params)
        fprintf('Attributes roots_params are different.\n')
        return
        
    elseif ~ isequal(obj1.CS, obj2.CS)
        fprintf('Attributes CS are different.\n')
        return
        
    elseif ~ isequal(obj1.eCS, obj2.eCS)
        fprintf('Attributes eCS are different.\n')
        return
        
    elseif ~ isequal(obj1.Gen, obj2.Gen)
        fprintf('Attributes Gen are different.\n')
        return
        
    elseif ~ isequal(obj1.DC, obj2.DC)
        fprintf('Attributes DC are different.\n')
        return
        
    elseif ~ isequal(obj1.OD, obj2.OD)
        fprintf('Attributes OD are different.\n')
        return
        
    elseif ~ all(cellfun(@(x,y) x == y, obj1.roots, obj2.roots))
        fprintf('Attributes roots are different.\n')
        return
        
    end
    
    outbool = true;
    
end
