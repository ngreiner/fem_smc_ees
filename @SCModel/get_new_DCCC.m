function [l_contour, l_area, m_contour, m_area] = get_new_DCCC(CS, prev_area)
    % Compute contour of DC compartment originating in the specified CS.
    %
    % Usage:
    %   [l_contour, l_area, m_contour, m_area] = get_new_DCCC(CS, prev_area)
    %
    % Parameters:
    %   CS          : CrossSection object.
    %   prev_area   : double    : area of previous DC compartment.
    %
    % Return:
    %   l_contour   : N*3 double    : lateral contour of DC region in
    %                                 specified CS.
    %   l_area      : double        : area of l_contour.
    %   m_contour   : N*3 double    : medial contour of DC region in
    %                                 specified CS.
    %   m_area      : double        : area of m_contour.
    %
    % Description:
    %   Computes the contour of the DC compartment originating in the
    %   specified CS.
    %   This is done by splitting the total DC region of the specified CS
    %   into a lateral and a medial zone: the lateral zone is allocated to
    %   the new DC compartment.
    %   It is bounded on its left by the Lissaeur tract and on its right
    %   by a vertical line.
    %
    %   The algorithm to obtain it is as follows:
    %   1. identify in the DC region contour the intersecting point between
    %      the medial Lissaeur tract segment and the WM
    %   2. starting from this point, sequentially and alternatively roam
    %      the DC region contour in the direct or reverse direction: each
    %      point visited is added to the lateral contour in formation
    %   3. at each step, compute the slope of the segment joining the 
    %      running points in the reverse and direct directions
    %   4. when positive, advance running point in the reverse direction,
    %      otherwise advance running point in the direct direction
    %   5. compute area of complementary contour: when it becomes lower 
    %      than the specified area (prev_area parameter) stop process
    %   6. sample points along the segment joining the two running points
    %      to close contour
    
    % DC tract contour in CS.
    DCTC = CS.DC.contour;
    
    % Find index of the point belonging to the DCTC intersecting the medial
    % LT on the WM.
    idxI = find(ismember(DCTC, CS.LT.m_seg(1, :), 'rows'), 1);
    
    % Initial running points in direct and reverse directions on contour.
    idxD = idxI + 1;
    A = DCTC(idxD, :);
    idxR = idxI - 1;
    B = DCTC(idxR, :);
    
    % Initial mContour and corresponding area.
    m_contour = [DCTC(1 : idxR, :); DCTC(idxD : end, :)];
    m_area = polyarea(m_contour(:, 1), m_contour(:, 2));

    % Roam the contour and add points to the lateral contour while area
    % of medial contour exceeds the target area.
    while m_area > prev_area
        
        % Compute xB - xA.
        dxAB = B(1) - A(1);
        
        % If dxAB is positive, point B is more on the right than point A:
        % we need to update point A: get next point in direct direction.
        if dxAB >= 0
            idxD = idxD + 1;
            A = DCTC(idxD, :);
            flag = 'D';
            
        % Otherwise, update point B: get next point in reverse direction.
        else
            idxR = idxR - 1;
            B = DCTC(idxR, :);
            flag = 'R';
            
        end
        
        % Compute the contour of the region within O, B, and A, where O is
        % the first point of the DCTC, ie the point with x=0 and y<0, ie
        % the lower-most, right-most point of DCTC.
        % Path connecting O to B.
        OB = DCTC(1 : idxR, :);
        
        % Segment connecting B to A.
        d = norm(A - B);
        x = linspace(B(1), A(1), floor(d * CS.mean_pdens))';
        y = linspace(B(2), A(2), floor(d * CS.mean_pdens))';
        BA = [x, y, CS.z * ones(size(x,1), 1)];
        
        % Path connecting A to I.
        AO = DCTC(idxD : end, :);
        
        % Whole contour.
        m_contour = [OB; BA(2 : end - 1, :); AO];
        
        % Compute area of the contour.
        m_area = polyarea(m_contour(:, 1), m_contour(:, 2));
        
    end

    % The current m_area just became <= prev_area. If it is < prev_are, we
    % want to go one point back in the previous loop so that m_arerea be
    % just > prev_area. 
    % If m_area = prev_are, we don't need to go one point back.
    if m_area < prev_area
        
        % The direction in which to go back is indicated by 'flag'.
        if strcmp(flag, 'D')
            idxD = idxD - 1;
            A = DCTC(idxD, :);
        else
            idxR = idxR + 1;
            B = DCTC(idxR, :);
        end
        
        % Recompute mContour and mArea.
        OB = DCTC(1 : idxR, :);
        d = norm(A - B);
        x = linspace(B(1), A(1), floor(d * CS.mean_pdens))';
        y = linspace(B(2), A(2), floor(d * CS.mean_pdens))';
        BA = [x, y, CS.z * ones(size(x, 1), 1)];
        AO = DCTC(idxD : end, :);
        m_contour = [OB; BA(2 : end - 1, :); AO];
        m_area = polyarea(m_contour(:, 1), m_contour(:, 2));
        
    end

    % Finally, compute lateral output contour and its area.
    
    % Segment connecting A to B.
    AB = flipud(BA);
    
    % Path connecting B to A on contour.
    BA = DCTC(idxR : idxD, :);
    
    % Whole lateral contour and its area.
    l_contour = [BA; AB(2 : end, :)];
    l_area = polyarea(l_contour(:, 1), l_contour(:, 2));

%     % Control plot.
%     figure()
%     plot3(CS.GM(:,1), CS.GM(:,2), CS.GM(:,3), 'k');
%     hold on
%     plot3(CS.WM(:,1), CS.WM(:,2), CS.WM(:,3), 'k');
%     plot3(DCTC(:,1), DCTC(:,2), DCTC(:,3))
%     plot3(mContour(:,1), mContour(:,2), mContour(:,3))
%     plot3(lContour(:,1), lContour(:,2), lContour(:,3))

end
