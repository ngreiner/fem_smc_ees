function CS = get_CS(this, z, pos)
    % Return the cross-section at, above, or below a specific z-coordinate.
    %
    % Usage:
    %   CS = sc.get_CS(z)
    %   CS = sc.get_CS(z, pos)
    %
    % Parameters:
    %   z   : double    : z-coordinate.
    %
    % Optional parameters:
	%   pos : string    : position indicator ('above', 'below' or 'exact').
    %
    % Return:
    %   CS  : CrossSection object.
    %
    % Description:
    %   If pos is omitted, it defaults to 'exact'. In this case, the
    %   specified z should be one of an existing  cross-section.
    %   Otherwise, position is either 'above' or 'below', in which case the
    %   z-coordinate of the cross-section just above or below the specified
    %   z is first retrieved, and the corresponding cross-section
    %   subsequently returned.
    
    if nargin == 2
        pos = 'exact';
    end
    
    if strcmp(pos, 'above')
        z = this.get_z_above(z);
    elseif strcmp(pos, 'below')
        z = this.get_z_below(z);
    end
    
    CS = this.CS{cellfun(@(x) isequal(x.z, z), this.CS)};
    
end
