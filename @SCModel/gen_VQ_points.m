function VQ_points = gen_VQ_points(this, seg_name, np, z)
    % Return points positioned in the VQ of the GM.
    %
    % Usage:
    %   VQ_point = sc.gen_VQ_points(seg_name, np)
    %   VQ_point = sc.gen_VQ_points(seg_name, np, z)
    %
    % Parameters:
    %   seg_name : string :
    %       Name of a segment.
    %
    %   np : integer :
    %       Number of points to generate (default=1).
    %
    % Optional parameters:
    %   z : double :
    %       Eventual prescribed z-coordinate for the generated points.
    %
    % Return:
    %   VQ_points : np*3 double :
    %       Coordinates of VQ points.
    %
    % Description:
    %   VQ = Ventro-lateral Quarter of the ventral horn of the GM.
    %   To randomly generate a VQ point, we effectively generate two random
    %   points: 1 in the upper and 1 in the lower cross-sectional contours
    %   of the VQ of the concerned segment (see method
    %   'Geom.gen_weighted_sum').
    %   We then average the coordinates of these two points to obtain a
    %   point lying at a z-coordinate generated uniformly at random in the
    %   interval [zDown, zUp], unless this z-coordinate is prescribed by
    %   the user.
    
    if nargin == 3
        prescribed_z = false;
    else
        prescribed_z = true;
    end
    
    % Recover upper and lower cross-sections of spinal segment.
    idx = find(cellfun( @(x) strcmp(x.name, seg_name), this.CS));
    CS_up = this.CS{idx};
    CS_down = this.CS{idx + 1};
    VQ_up = CS_up.VQ.contour;
    VQ_down = CS_down.VQ.contour;
    z_up = CS_up.z;
    z_down = CS_down.z;
    
    % Generate VQ points.
    VQ_points = zeros(np, 3);
    for i = 1 : np
        
        % Generate points in the upper and lower cross-sections.
        p_up = Geom.gen_weighted_sum(VQ_up, 4);
        p_down = Geom.gen_weighted_sum(VQ_down, 4);
        
        % Generate random z-coordinates in the interval [z_down, z_up] with
        % uniform probability and compute weights to apply to the upper and
        % lower cross-sections points.
        % This holds true unless z is specified in argument.
        if ~ prescribed_z
            z = z_down + rand(1) * (z_up - z_down);
        end
        w_up = (z - z_down) / (z_up - z_down);
        w_down = (z_up - z) / (z_up - z_down);
        
        % Average points.
        VQ_points(i, :) = w_up * p_up + w_down * p_down;
        
    end
    
end
