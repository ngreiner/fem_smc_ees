function from_struct(this, struc)
    % Set the SCModel from the input struct.
    %
    % Usage:
    %   sc.from_struct(struc)
    %
    % Parameters:
    %   struc   : struct    : with same fields as the SCModel class.
    this.geom = struc.geom;
    this.morpho = struc.morpho;
    this.roots_params = struc.roots_params;
    this.CS = cellfun(@(x) CrossSection(), struc.CS, 'UniformOutput', false);
    cellfun(@(x, y) x.from_struct(y), this.CS, struc.CS, 'UniformOutput', false);
    for i = 1 : struc.morpho.nCS
        this.CS{i}.dir = fullfile(this.dirs.CS, this.CS{i}.name);
    end
    this.eCS = cellfun(@(x) CrossSection(), struc.eCS, 'UniformOutput', false);
    cellfun(@(x, y) x.from_struct(y), this.eCS, struc.eCS, 'UniformOutput', false);
    for i = 1 : 2 * (struc.morpho.ratio_exts + 1)
        this.eCS{i}.dir = fullfile(this.dirs.CS, this.eCS{i}.name(1 : 2));
    end
    this.Gen = struc.Gen;
    this.DC = struc.DC;
    this.OD = struc.OD;
    this.roots = cellfun(@(x) Root(), struc.roots, 'UniformOutput', false);
    cellfun(@(x, y) x.from_struct(y), this.roots, struc.roots, 'UniformOutput', false);
    for i = 1 : struc.morpho.nroots
        this.roots{i}.sc = this;
    end
end
