function set_dirs(this, topdir)
    % Set the attribute dirs.
    %
    % Usage:
    %   sc.set_dirs(topdir)
    %
    % Parameters:
    %   topdir  : string    : path to directory.
    %
    % Description:
    %   dirs stores the paths to directories necessary to the construction
    %   of the SCModel. 
    this.dirs.top = topdir;
    this.dirs.resources = fullfile(this.dirs.top, 'resources');
    this.dirs.CS = fullfile(this.dirs.resources, 'geom', 'cross_sections');
    this.dirs.coords = fullfile(this.dirs.resources, 'CAD', 'coords');
end
