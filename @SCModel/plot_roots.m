function plot_roots(this)
    % Produce a plot displaying all the segments cross-sections, roots
    % cross-sections, guide curves, and roots entry and exit points.

    % Open figure. Define axes.
    figure
    hold on
    xmax =  1.1 * (this.morpho.seg_length + 2 * this.morpho.l_exts) / 2;
    xmin = -1.1 * (this.morpho.seg_length + 2 * this.morpho.l_exts) / 2;
    ymax =  xmax;
    ymin =  xmin;
    zmax =  1.1 * (this.morpho.seg_length + this.morpho.l_exts);
    zmin = -1.1 * this.morpho.l_exts;
    axis([xmin xmax ymin ymax zmin zmax])
    
    % Start ploting for every segment.
    for i = 1 : this.morpho.nsegs

        % Plot upper and lower cross-sections of segment.
        
        % Upper.
        plot3(this.CS{i}.GM(:, 1), this.CS{i}.GM(:, 2), this.CS{i}.GM(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i}.WM(:, 1), this.CS{i}.WM(:, 2), this.CS{i}.WM(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i}.CSF(:, 1), this.CS{i}.CSF(:, 2), this.CS{i}.CSF(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i}.dura(:, 1), this.CS{i}.dura(:, 2), this.CS{i}.dura(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i}.fat(:, 1), this.CS{i}.fat(:, 2), this.CS{i}.fat(:, 3), 'LineWidth', 2., 'Color', 'black')
        
        % Lower.
        plot3(this.CS{i + 1}.GM(:, 1), this.CS{i+1}.GM(:, 2), this.CS{i + 1}.GM(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i + 1}.WM(:, 1), this.CS{i+1}.WM(:, 2), this.CS{i + 1}.WM(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i + 1}.CSF(:, 1), this.CS{i+1}.CSF(:, 2), this.CS{i + 1}.CSF(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i + 1}.dura(:, 1), this.CS{i+1}.dura(:, 2), this.CS{i + 1}.dura(:, 3), 'LineWidth', 2., 'Color', 'black')
        plot3(this.CS{i + 1}.fat(:, 1), this.CS{i+1}.fat(:, 2), this.CS{i + 1}.fat(:, 3), 'LineWidth', 2., 'Color', 'black')
        
        % Plot root features if segment has roots.
        if this.has_roots(this.morpho.seg_names{i})
            
            % Recover root.
            root = this.roots{strcmp(this.morpho.seg_names{i}, this.morpho.segs_with_roots)};
            
            % Plot entry and exit points of root.
            scatter3(root.LV.p_entry(1), root.LV.p_entry(2), root.LV.p_entry(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red')
            scatter3(root.LD.p_entry(1), root.LD.p_entry(2), root.LD.p_entry(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue')
            scatter3(root.LV.p_exit_IF(1), root.LV.p_exit_IF(2), root.LV.p_exit_IF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red')
            scatter3(root.LD.p_exit_IF(1), root.LD.p_exit_IF(2), root.LD.p_exit_IF(3), 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue')
            
            % Plot main branches.
            
            % Guide curves.
            plot3(root.LV.GC(:, 1), root.LV.GC(:, 2), root.LV.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red')
            plot3(root.LD.GC(:, 1), root.LD.GC(:, 2), root.LD.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'blue')
            
            % Cross-sections.
            for k = 1 : root.LV.nCS
                plot3(root.LV.CS{k}(:, 1), root.LV.CS{k}(:, 2), root.LV.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'red')
            end
            for k = 1 : root.LD.nCS
                plot3(root.LD.CS{k}(:, 1), root.LD.CS{k}(:, 2), root.LD.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'blue')
            end
            
            % Generatrices.
            for k = 1 : 4
                plot3(root.LV.Gen{k}(:, 1), root.LV.Gen{k}(:, 2), root.LV.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'red')
                plot3(root.LD.Gen{k}(:, 1), root.LD.Gen{k}(:, 2), root.LD.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'blue')
            end
            
            % Plot rootlets.
            if this.with_rlets()
                
                % Loop over rootlets.
                for k = 1 : this.roots_params.nrlets
                    
                    % Guide curves.
                    plot3(root.LV.rlets{k}.GC(:, 1), root.LV.rlets{k}.GC(:, 2), root.LV.rlets{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red')
                    plot3(root.LD.rlets{k}.GC(:, 1), root.LD.rlets{k}.GC(:, 2), root.LD.rlets{k}.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'blue')
                    
                    % Cross-sections.
                    for j = 1 : root.LV.rlets_nCS
                        plot3(root.LV.rlets{k}.CS{j}(:, 1), root.LV.rlets{k}.CS{j}(:, 2), root.LV.rlets{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'red')
                    end
                    for j = 1 : root.nLDrletCS
                        plot3(root.LD.rlets{k}.CS{j}(:, 1), root.LD.rlets{k}.CS{j}(:, 2), root.LD.rlets{k}.CS{j}(:, 3), 'LineWidth', 2., 'Color', 'blue')
                    end
                    
                    % Generatrices.
                    for j = 1 : 4
                        plot3(root.LV.rlets{k}.Gen{j}(:, 1), root.LV.rlets{k}.Gen{j}(:, 2), root.LV.rlets{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'red')
                        plot3(root.LD.rlets{k}.Gen{j}(:, 1), root.LD.rlets{k}.Gen{j}(:, 2), root.LD.rlets{k}.Gen{j}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'blue')
                    end
                    
                end
                
            end
                
            % Plot spinal nerve.
            if this.with_spinerv()
                
                % Guide curve.
                plot3(root.Lspinerv.GC(:, 1), root.Lspinerv.GC(:, 2), root.Lspinerv.GC(:, 3), 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'magenta')
                
                % Cross-sections.
                for k = 1 : root.Lspinerv.nCS
                    plot3(root.Lspinerv.CS{k}(:, 1), root.Lspinerv.CS{k}(:, 2), root.Lspinerv.CS{k}(:, 3), 'LineWidth', 2., 'Color', 'magenta')
                end
                
                % Generatrices.
                for k = 1 : 4
                    plot3(root.Lspinerv.Gen{k}(:, 1), root.Lspinerv.Gen{k}(:, 2), root.Lspinerv.Gen{k}(:, 3), 'LineWidth', 1, 'LineStyle', '--', 'Color', 'magenta')
                end
                
            end
            
        end
        
    end

end
