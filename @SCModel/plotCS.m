function plotCS(this, varargin)
    % Produce a plot displaying the cross-sections along with the specified
    % inner compartments of the WM and the GM.
    %
    % Usage:
    %   sc.plotCS()
    %   sc.plotCS(['LT', 'DH', 'MGM', 'VH', 'CST', 'SCT', 'DC'])
    %   sc.plotCS('all')
    %
    % Parameters:
    %   varargin: can accept any of the followings: 'LT', 'DH',
    %             'MGM', 'VH', 'CT', 'ST', 'DC' or 'all'.
    
    
    % Initialize figure 
    figure()
    
    % Plot cross-sections WM and GM.
    for i = 1 : this.morpho.nCS
        CS = this.CS{i};
        plot3(CS.GM(:, 1), CS.GM(:, 2), CS.GM(:, 3),'k');
        hold on
        plot3(CS.WM(:, 1), CS.WM(:, 2), CS.WM(:, 3),'k');
    end
        
    % Deal with Lissaeur Tract.
    if any(strcmp(varargin, 'LT'))
        for i = 1 : this.nCS
            CS = this.CS{i};
            plot3(CS.LT.contour(:, 1), CS.LT.contour(:, 2), CS.LT.contour(:, 3), 'r');
        end
    end

    % Deal with Drosal Horn.
    if any(strcmp(varargin, 'DH'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.DH.contour(:, 1), CS.DH.contour(:, 2), CS.DH.contour(:, 3), 'r');
        end
    end

    % Deal with Middle-GM compartment.
    if any(strcmp(varargin, 'MGM'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.MGM.contour(:, 1), CS.MGM.contour(:, 2), CS.MGM.contour(:, 3), 'r');
        end
    end

    % Deal with Ventral Horn.
    if any(strcmp(varargin, 'VH'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.VH.contour(:, 1), CS.VH.contour(:, 2), CS.VH.contour(:, 3), 'r');
        end
    end

    % Deal with Cortico-spinal Tract.
    if any(strcmp(varargin, 'CT'))
        for i = 1:this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.CT.contour(:, 1), CS.CT.contour(:, 2), CS.CT.contour(:, 3), 'r');
        end
    end

    % Deal with Spino-cerebellar Tract.
    if any(strcmp(varargin, 'ST'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.ST.contour(:, 1), CS.ST.contour(:, 2), CS.ST.contour(:, 3), 'r');
        end
    end

    % Deal with Dorsal Columns Region.   
    if any(strcmp(varargin, 'DC'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.DC.contour(:, 1), CS.DC.contour(:, 2), CS.DC.contour(:, 3), 'r');
        end
    end

    % All inner compartments.   
    if any(strcmp(varargin, 'all'))
        for i = 1 : this.nCS
            CS = this.CS{this.nCS - i + 1};
            plot3(CS.LT.contour(:, 1), CS.LT.contour(:, 2), CS.LT.contour(:, 3), 'r');
            plot3(CS.DH.contour(:, 1), CS.DH.contour(:, 2), CS.DH.contour(:, 3), 'r');
            plot3(CS.MGM.contour(:, 1), CS.MGM.contour(:, 2), CS.MGM.contour(:, 3), 'r');
            plot3(CS.VH.contour(:, 1), CS.VH.contour(:, 2), CS.VH.contour(:, 3), 'r');
            plot3(CS.CT.contour(:, 1), CS.CT.contour(:, 2), CS.CT.contour(:, 3), 'r');
            plot3(CS.ST.contour(:, 1), CS.ST.contour(:, 2), CS.ST.contour(:, 3), 'r');
            plot3(CS.DC.contour(:, 1), CS.DC.contour(:, 2), CS.DC.contour(:, 3), 'r');
        end
    end
    
end
