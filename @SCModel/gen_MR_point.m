function MR_point = gen_MR_point(this, z, direction)
    % Return a point positioned in the middle region of the grey matter
    % at the z-coordinate z +/- dz.
    %
    % Usage:
    %   MR_point = sc.gen_MR_point(z)
    %   MR_point = sc.gen_MR_point(z, direction)
    %
    % Parameters:
    %   z   : double    : z-coordinate of a point in the VQ of the grey
    %                     matter (in mm).
    %
    % Optional parameters:
    %   direction   : str   : can be 'lower/below' or 'greater/above'.
    %
    % Return:
    %   MR_point    : 1*3 double.
    %
    % Description:
    %   The new z-coordinate is generated uniformly at random in the
    %   interval [z +- 0.1mm, z +- 0.3mm] (where direction prescribes the
    %   sign + or -).
    
    % Parse inputs.
    if nargin == 2
        sgn = -1;
    elseif any(strcmp(direction, {'above', 'greater'}))
        sgn = 1;
    elseif any(strcmp(direction, {'below', 'lower'}))
        sgn = -1;
    else
        error('Incorrect value for direction.')
    end
    
    % Generate point in medial region.
    dz = 0.1 + rand() * 0.2;
    z = z + sgn * dz;
    z_up = this.get_z_above(z);
    z_down = this.get_z_below(z);
    if z_up ~= z_down
        w = (z - z_down) / (z_up - z_down);
    else
        w = rand();
    end
    p_up = Geom.gen_weighted_sum(this.get_CS_above(z_up).MR.contour);
    p_down = Geom.gen_weighted_sum(this.get_CS_below(z_down).MR.contour);
    MR_point = w * p_up + (1 - w) * p_down;
    
end
