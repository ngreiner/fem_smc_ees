function set_dims(this)
    % Sets the attribute dims.
    %
    % Usage:
    %   sc.set_dims()
    %
    % Description:
    %   Derive useful dimensions of the SCModel from other attributes.
    
    this.morpho.seg_length = this.CS{1}.z - this.CS{end}.z;
    this.morpho.l_exts = this.morpho.ratio_exts * this.morpho.seg_length;

    CS = this.CS;
    this.morpho.WM_max_trans_width = max(cellfun(@(x) max(x.WM(:, 1)) - min(x.WM(:, 1)), CS));
    this.morpho.WM_max_sag_width = max(cellfun(@(x) max(x.WM(:, 2)) - min(x.WM(:, 2)), CS));
    this.morpho.CSF_max_trans_width = max(cellfun(@(x) max(x.CSF(:, 1)) - min(x.CSF(:, 1)), CS));
    this.morpho.CSF_max_sag_width = max(cellfun(@(x) max(x.CSF(:, 2)) - min(x.CSF(:, 2)), CS));
    this.morpho.fat_max_trans_width = max(cellfun(@(x) max(x.fat(:, 1)) - min(x.fat(:, 1)), CS));
    this.morpho.fat_max_sag_width = max(cellfun(@(x) max(x.fat(:, 2)) - min(x.fat(:, 2)), CS));
    this.morpho.dura_max_trans_width = max(cellfun(@(x) max(x.dura(:, 1)) - min(x.dura(:, 1)), CS));
    this.morpho.dura_max_sag_width = max(cellfun(@(x) max(x.dura(:, 2)) - min(x.dura(:, 2)), CS));

end
