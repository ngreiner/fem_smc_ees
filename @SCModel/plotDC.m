function plotDC(this, varargin)
    % Produce a plot displaying the cross-sections along with their 
    % segmented dorsal column compartments contours.
    %
    % Usage:
    %   sc.plotDC()
    %   sc.plotDC('DC_names', DC_names)
    %   sc.plotDC('with_exts')
    %
    % Parameters:
    %   DC_names    : N*1 cell  : names of DC compartments.

    % Parse varargin.
    with_exts = false;
    if any(strcmp(varargin, 'with_exts'))
        with_exts = true;
    end
    
    DC_names = this.DC(:, 1);
    idx = find(strcmp(varargin, 'DC_names'));
    if ~ isempty(idx)
        DC_names = varargin{idx + 1};
    end

    % Initialize figure.
    figure()
    view(1, 28)
    hold on
    xmax =  1.1 * (this.morpho.seg_length + 2 * this.morpho.l_exts) / 2;
    xmin = -1.1 * (this.morpho.seg_length + 2 * this.morpho.l_exts) / 2;
    ymax =  xmax;
    ymin =  xmin;
    zmax =  1.1 * (this.morpho.seg_length + this.morpho.l_exts);
    zmin = -1.1 * this.morpho.l_exts;
    axis([xmin xmax ymin ymax zmin zmax])
    
    % Plot cross-sections.
    for i = 1 : this.morpho.nCS
        CS = this.CS{i};
        plot3(CS.GM(:, 1), CS.GM(:, 2), CS.GM(:, 3), 'k');
        plot3(CS.WM(:, 1), CS.WM(:, 2), CS.WM(:, 3), 'k');
        for j = 1 : size(DC_names, 1)
            contour = this.get_DC_comp_contour(DC_names{j}, CS.name);
            if isempty(contour)
                continue
            end
            plot3(contour(:, 1), contour(:, 2), contour(:, 3), 'cyan');
        end
    end

    % Plot extended cross-sections.
    if with_exts
        for i = 1 : this.morpho.ratio_exts + 1
            eCS = this.eCS{i};
            plot3(eCS{i}.GM(:, 1), eCS{i}.GM(:, 2), eCS{i}.GM(:, 3), 'k')
            plot3(eCS{i}.WM(:, 1), eCS{i}.WM(:, 2), eCS{i}.WM(:, 3), 'k')
            for j = 1 : size(DC_names, 1)
                contour = this.get_DC_comp_contour(DC_names{j}, eCS.name);
                if isempty(contour)
                    continue
                end
                plot3(contour(:, 1), contour(:, 2), contour(:, 3), 'r');
            end
        end
    end
    
end
