function [w_above, w_below] = get_weights(this, z)
    % Return appropriate z-dependent weights for various averages.
    %
    % Usage:
    %   [w_above, w_below] = sc.get_weights(z)
    %
    % Parameters:
    %   z   : double    : z-coordinate.
    %
    % Return:
    %   w_above : double    : weight to apply to cross-section above z.
    %   w_below : double    : weight to apply to cross-section below z.
    %
    % Description:
    %   The computed weights should be applied to the contours of the
    %   cross-sections above and below the specified z-coordinate to
    %   estimate the coordinates of points in a virtual cross-sectional
    %   contour lying at the elevation z.
    %
    %   The computed weights are derived so that the specified z be the
    %   the barycenter of z_above and z_below.
    
    z_above = this.get_z_above(z);
    z_below = this.get_z_below(z);
    
    if z_above == z_below;
        w_above = 0.5;
        w_below = 0.5;
        
    else
        w_above = (z - z_below) / (z_above - z_below);
        w_below = (z_above - z) / (z_above - z_below);
        
    end
    
end
