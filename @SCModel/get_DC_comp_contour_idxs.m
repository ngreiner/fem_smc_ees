function [idx1, idx2] = get_DC_comp_contour_idxs(this, DC_comp_name, CS_name)
    % Return the indexes of the specified DC compartment contour.
    %
    % Parameters:
    %   DC_comp_name    : string    : name of a DC compartment.
    %   CS_name         : string    : name of a cross-section.
    %
    % Return:
    %   idx1    : double    : row index of DC compartment in attribute DC.
    %   idx2    : double    : row index of DC compartment contour in array
    %                         DC{idx1, 2}.
    %
    % Description:
    %   Indexes are retrieved by name matching.
    
    % Find indexes.
    idx1 = find(cellfun(@(x) any(regexp(x, sprintf('%s$', DC_comp_name))), this.DC(:, 1)));
    idx2 = find(cellfun(@(x) any(regexp(x, sprintf('%s$', CS_name))), this.DC{idx1, 2}(:, 1)));
    
    % Send warning if necessary.
    if isempty(idx2)
        warning(' Error: you are asking a DC compartment in a more caudal cross section!')
    end

end
