function set_CS(this)
    % Set the attribute CS.
    %
    % Usage:
    %   sc.set_CS()
    %
    % Description:
    %   CS is the cell-array of CrossSection objects representing the
    %   spinal segments of the SCModel.
    %   See CrossSection class.
    %   This method assumes that the morpho and geom attributes of the
    %   SCModel are already properly defined.
    
    this.CS = cell(this.morpho.nCS, 1);
    
    for i = 1 : this.morpho.nCS
        name = this.morpho.CS_names{i};
        dir = fullfile(this.dirs.CS, this.morpho.CS_names{i});
        z = this.geom.zCS(i);
        this.CS{i} = CrossSection(name, dir, z);
    end
    
end
