classdef DCcBFiber < DCFiber

	% This class is a near-perfect copy of the DCFiber class.
	% Its purpose is to represent DCFibers of the diameter class B.
	%
	% The DCcBFiber class defines the method set_cols, which extracts
	% diameter class B specific parameters for the fiber collaterals.
    
    methods
        function this = DCcBFiber()
        end
    end

    methods
    	set_cols(this, smcd)
    end
    
end