function export(this, dir_export, perm)
    % Export the Fiber information to textfile.
    %
    % Usage:
    %   fiber.export(dir_export, perm)
    %
    % Parameters:
    %   dir_export : str :
    %       Path to export directory.
    %
    %   perm : integer :
    %       Permission level.
    %
    % Description:
    %   See methods used below.
    this.make_folder(dir_export);
    this.write_morpho(dir_export, perm);
    this.write_geom(dir_export, perm);
    this.write_V(dir_export, perm);
end
