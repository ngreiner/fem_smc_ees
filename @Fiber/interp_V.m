function interp_V(this, smcd, perm)
    % Interpolate the electric potential at the Fiber compartments.
    %
    % Usage:
    %   fiber.interp_V(smcd, perm)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   perm : integer/string : permission level (0, 1, 2 or 'i').
    %
    % Description:
    %   Work is delegated to homonymous method of Branch class, which is
    %   called for each Branch composing the Fiber.
    
    fprintf('BEGIN: interp_V. %s #%d | musc: %s | seg: %s | AS_idx #%d ...\n', class(this), this.fidx, this.musc, this.seg, smcd.fem.AS.idx) 
    t0 = cputime;
        
    % Call Branch method for each Branch of the Fiber.
    cellfun(@(x) x.interp_V(smcd, perm), this.get_branches());
    
    fprintf('END. (Time spent: %d s.)\n', cputime - t0)

end
