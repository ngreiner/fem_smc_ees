function outbool = is_interp(this, AS_idx, varargin)
    % Return a logical indicating whether the Fiber is already well
    % interpolated for the input AS_idx.
    %
    % Usage:
    %   outbool = fiber.is_interp(AS_idx)
    %   outbool = fiber.is_interp(AS_idx, 'N')
    %   outbool = fiber.is_interp(AS_idx, 'P1')
    %   outbool = fiber.is_interp(AS_idx, 'P2')
    %   outbool = fiber.is_interp(AS_idx, 'I')
    %
    % Parameters:
    %   AS_idx : double.
    %
    % Return:
    %   outbool : logical.
    %
    % Description:
    %   If one of the fiber branches lacks the potential array for one of
    %   its nodes, paranodes1, paranodes2 or internodes, the Fiber is not
    %   well interpolated.
    %   When a branch doesn't possess any paranodes or internodes, the
    %   previous requirement is relaxed for the paranodes/internodes at
    %   concern.
    %
    %   If one of 'N', 'P1', 'P2' or 'I' is given in varargin, the test
    %   should be performed solely on the nodes/paranodes1/paranodes2/
    %   internodes respectively.
    %
    %   Otherwise it is the conjunction of all of them.
    outbool = all(cellfun(@(x) x.is_interp(AS_idx, varargin{:}), this.get_branches()));
end
