function write_V(this, dir_export, perm)
    % Export the Fiber electric potentials information to textfile.
    %
    % Parameters:
    %   dir_export : str : path to top-level export directory.
    %   perm : interger : writing permission level.
    %
    % Description:
    %   Separate files are written for each electrode active site.
    %   For each of them, the distributions along the different branches
    %   are written on different lines and the potentials at each
    %   compartment along a branch are separated by single whitespaces ' '.
    %   They are arranged from first to last node:
    %       VN(1) VP1(1) VP2(1) VI(1) VI(2) ... VP2(end) VP1(end) VN(end)
    %
    %   If it does not exist, the directory where all the potentials
    %   textfiles should be written is built first.
    %   
    %   Permission to write (perm=1) is granted by default.
    
    if nargin == 2
        perm = 1;
    end

    % Make potentials directory if necessary.
    folder = fullfile(dir_export, this.musc, this.seg, sprintf('%s%d', class(this), this.fidx));
    Vfolder = fullfile(folder, 'potentials');
    if ~ exist(Vfolder, 'dir')
        mkdir(Vfolder);
    end
    
    % Unpack branches of the Fiber.
    branches = this.get_branches();
    
    % Loop over the active sites.
    for i = 1 : size(branches{1}.VN, 1)

        % Check if AS file shall be built.
        AS_file_path = fullfile(Vfolder, sprintf('elec%d.txt', i));
        makeAS = this.is_interp(i) && (perm || ~ exist(AS_file_path, 'file'));
        if ~ makeAS
            continue
        end
        
        % Loop over the branches.
        AStext = '';
        for k = 1 : size(branches, 1)
            
            % Unpack potentials.
            VN = branches{k}.VN{i};
            VP1 = branches{k}.VP1{i};
            VP2 = branches{k}.VP2{i};
            VI = branches{k}.VI{i};
            
            % Write potentials.
            AStext = [AStext sprintf('%f ', VN(1))];
            for n = 1 : size(VN, 1) - 1
                AStext = [AStext sprintf('%f ', VP1(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VP2(2 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 1))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 3))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 4))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 5))];
                AStext = [AStext sprintf('%f ', VI(6 * (n - 1) + 6))];
                AStext = [AStext sprintf('%f ', VP2(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VP1(2 * (n - 1) + 2))];
                AStext = [AStext sprintf('%f ', VN(n + 1))];
            end
            AStext = [AStext '\n'];
            
        end

        % Write file.
        fileID = fopen(AS_file_path, 'w');
        fprintf(fileID, AStext);
        fclose(fileID);
        
        % Console message.
        fprintf('Writing ''elec%d.txt'' at `%s`.\n', i, Vfolder)

    end

end
