function make_folder(this, dir_export)
    % Make an export folder for the Fiber.
    %
    % Usage:
    %   fiber.make_folder(dir_export)
    %
    % Parameters:
    %   dir_export: str: path to top-level export directory.
    %
    % Description:
    %   A Fiber is rarely exported alone, but rather along with an entire
    %   sensorimotor circuit. Thus, fibers are regrouped by muscle-segment
    %   pairs.
    %   Within the top-level export directory of the sensorimotor circuit,
    %   we thus find muscle folders, and within the muscle folders, segment 
    %   folders: there we find Fiber folders.
    %   Fiber folders' names take the form 'ClassX', where Class is the
    %   name of the Fiber subclass, and X is the family index of the Fiber.
    %   This method creates all these folders if they do not yet exist.
    
    % Folder names.
    musc_folder = fullfile(dir_export, this.musc);
    seg_folder = fullfile(musc_folder, this.seg);
    folder = fullfile(seg_folder, sprintf('%s%d', class(this), this.fidx));

    % Make folders.
    if ~exist(folder, 'dir')
        if ~exist(seg_folder, 'dir')
            if ~exist(musc_folder, 'dir')
                if ~exist(dir_export, 'dir')
                    mkdir(dir_export)
                end
                mkdir(musc_folder)
            end
            mkdir(seg_folder)
        end
        mkdir(folder)
    end
    
end
