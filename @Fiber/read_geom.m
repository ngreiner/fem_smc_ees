function read_geom(this, folder)
	% Read the geometry information stored in folder.
    %
    % Usage:
    %   fiber.read_geom(folder)
    %
    % Parameters:
    %   folder : string : path to Fiber folder.
    %
    % Description:
    %   Inside the specified folder, the geometry information is written in
    %   a textfile called 'geom.txt'.
    %   It is composed of 3 columns of real values, coordinates expressed
    %   in mm.
    %   The geometry information of all the branches of the fiber is
    %   concatenated. It is always organized as:
    %       1. coordinates of the nodes (first nN rows)
    %       2. coordinates of the paranodes1 (next nP1 rows)
    %       3. coordinates of the paranodes2 (next nP2 rows)
    %       4. coordinates of the internodes (next nI rows)
    %       5. a single row: 'nP  NaN  NaN' where nP indicates the number
    %          of points (hence number of rows) composing the path
    %       6. coordinates of the path (next nP rows)
    %   and repeated for as many branches as the fiber possesses.
    
    % Read file
    file_path = fullfile(folder, 'geom.txt');
    fileID = fopen(file_path);
    geom = textscan(fileID, '%f %f %f');
    fclose(fileID);

    % Assign appropriate attributes.
    branches = this.get_branches();

    % Loop over the branches.
    for i = 1 : size(branches, 1)
        n = branches{i}.nN;
        branches{i}.Ncoords = [geom{1}(1 : n) geom{2}(1 : n) geom{3}(1 : n)];
        geom{1}(1 : n) = [];
        geom{2}(1 : n) = [];
        geom{3}(1 : n) = [];
        n = 2 * (n - 1);
        branches{i}.P1coords = [geom{1}(1 : n) geom{2}(1 : n) geom{3}(1 : n)];
        geom{1}(1 : n) = [];
        geom{2}(1 : n) = [];
        geom{3}(1 : n) = [];
        branches{i}.P2coords = [geom{1}(1 : n) geom{2}(1 : n) geom{3}(1 : n)];
        geom{1}(1 : n) = [];
        geom{2}(1 : n) = [];
        geom{3}(1 : n) = [];
        n = 3 * n;
        branches{i}.Icoords = [geom{1}(1 : n) geom{2}(1 : n) geom{3}(1 : n)];
        geom{1}(1 : n) = [];
        geom{2}(1 : n) = [];
        geom{3}(1 : n) = [];
        n = geom{1}(1);
        geom{1}(1) = [];
        geom{2}(1) = [];
        geom{3}(1) = [];
        branches{i}.path = [geom{1}(1 : n) geom{2}(1 : n) geom{3}(1 : n)];
        geom{1}(1 : n) = [];
        geom{2}(1 : n) = [];
        geom{3}(1 : n) = [];
    end
    assert(isempty(geom{1}) && isempty(geom{2}) && isempty(geom{3}))
    
end
