function from_struct(this, struc)
	% Set the Fiber from the input struct.
    %
    % Usage:
    %   fiber.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : fields are the same as the Fiber class properties.
    this.musc = struc.musc;
    this.seg = struc.seg;
    this.fidx = struc.fidx;
    this.gidx = struc.gidx;
    this.diam = struc.diam;
    this.model = struc.model;
end