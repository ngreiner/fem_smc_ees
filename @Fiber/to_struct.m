function struc = to_struct(this)
	% Return a matlab struct equivalent to the Fiber object.
    %
    % Usage:
    %   struc = fiber.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the Fiber object.
    struc.musc = this.musc;
    struc.seg = this.seg;
    struc.fidx = this.fidx;
    struc.gidx = this.gidx;
    struc.diam = this.diam;
    struc.model = this.model;
end