classdef Fiber < handle

    % Properties defined here:
    %
    %   musc : string :
    %       Name of muscle to which the Fiber belongs.
    %
    %   seg : string :
    %       Name of segment (ie spinal root) through which the Fiber runs.
    %
    %   fidx : integer :
    %       Index of Fiber in the family (muscle, seg).
    %
    %   gidx : integer :
    %       Global index of Fiber in a SMCD.
    %
    %   diam : double :
    %       Fiber diameter (in um).
    %
    %   model : string :
    %       Indicator of Fiber model.    
    
    properties
        musc    % string
        seg     % string
        fidx    % integer
        gidx    % integer
        diam    % double
        model   % string
    end
    
    methods
        function this = Fiber()
        end
    end
    
    methods
        export(this, dir_export, perm)
        from_struct(this, struc)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx, varargin)
        make_folder(this, dir_export)
        read_geom(this, folder)
        read_V(this, folder, nAS)
        struc = toStruct(this)
        write_geom(this, dir_export, perm)
        write_V(this, dir_export, AS_idx, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
