function write_geom(this, dir_export, perm)
    % Write the Fiber geometry information to textfile.
    %
    % Parameters:
    %   dir_export : str : path to top-level export directory.
    %   perm : integer : writing permission level.
    %
    % Description:
    %   A single textfile is written for the coordinates of each of the
    %   nodes, paranodes1, paranodes2, internodes and paths of all the
    %   branches of the Fiber.
    %   It is composed of 3 columns of real values, coordinates expressed
    %   in mm.
    %   The geometry information of all the branches of the fiber is
    %   concatenated and organized as:
    %       1. coordinates of the nodes (first nN rows)
    %       2. coordinates of the paranodes1 (next nP1 rows)
    %       3. coordinates of the paranodes2 (next nP2 rows)
    %       4. coordinates of the internodes (next nI rows)
    %       5. a single row: 'nP  NaN  NaN' where nP indicates the number
    %          of points (hence number of rows) composing the path
    %       6. coordinates of the path (next nP rows)
    %   and repeated for as many branches as the fiber possesses.

    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    % Derive geomFolder.
    folder = fullfile(dir_export, this.musc, this.seg, sprintf('%s%d', class(this), this.fidx));
    geom_file_path = fullfile(folder, 'geom.txt');
    branches = this.get_branches();
    
    if perm || ~ exist(geom_file_path, 'file')
        
        geom_text = '';

        % Loop over the branches.
        for i = 1 : size(branches, 1)
            % Write nodes' coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (branches{i}.Ncoords)')];
            % Write paranodes1' coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (branches{i}.P1coords)')];
            % Write paranodes2' coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (branches{i}.P2coords)')];
            % Write internodes' coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (branches{i}.Icoords)')];
            % Write line indicating the number of points in the path.
            geom_text = [geom_text sprintf('%d NaN NaN\n', size(branches{i}.path, 1))];
            % Write path coordinates.
            geom_text = [geom_text sprintf('%f %f %f\n', (branches{i}.path)')];
        end

        % Write info to file.        
        fileID = fopen(geom_file_path, 'w');
        fprintf(fileID, geom_text);
        fclose(fileID);
        
        % Console message.
        fprintf('Writing ''geom.txt'' at ''%s''.\n', folder)

    end

end
