function read_V(this, folder, nAS)
	% Read the Fiber electric potentials information from textfiles.
    %
    % Usage:
    %   fiber.read_V(folder, nAS)
    %
    % Parameters:
    %   folder : string :
    %       Path to Fiber folder.
    %
    %   nAS : integer :
    %       Total number of active sites to store the potentials from.
    %
    % Description:
    %   This method scans the specified folder for files of the form
    %   'elecX.txt' where X is a positive integer.
    %   Each of them stores the electric potential distribution along all
    %   the branches of the Fiber, obtained with the eponymous electrode.
    %   The distributions along the different branches are written on
    %   different lines. The potentials at each compartment along a branch
    %   are separated by single whitespaces ' '. They are arranged from
    %   first to last node:
    %       VN(1) VP1(1) VP2(1) VI(1) VI(2) ... VP2(end) VP1(end) VN(end)
    %
    %   This method initializes the cell-arrays VN, VP1, VP2 and VI of all
    %   the branches of the Fiber and fills the appropriate cells according
    %   to the files it finds in the specified folder.

    % Retrieve exported active sites files.
    elecs = Utils.scan_files(folder);
    elecs = elecs(cellfun(@(x) strcmp(x(1 : 4), 'elec'), elecs));
    ASindex = cellfun(@(x) str2double(x(5 : end - 4)), elecs);

    % Initialize potentials arrays in the branches of the Fiber.
    branches = this.get_branches();
    for i = 1 : size(branches, 1)
        branches{i}.VN = cell(nAS, 1);
        branches{i}.VP1 = cell(nAS, 1);
        branches{i}.VP2 = cell(nAS, 1);
        branches{i}.VI = cell(nAS, 1);
    end

    % Assign appropriate attributes.

    % Loop over the active sites.
    for i = 1 : size(ASindex, 1)

        % Read potentials.
        file_path = fullfile(folder, sprintf('elec%d.txt', ASindex(i)));
        fileID = fopen(file_path, 'r');
        Vs = textscan(fileID, '', 'delimiter', ' ', 'emptyvalue', NaN, 'collectoutput', 1);
        Vs = Vs{1};
        fclose(fileID);
        
        % Loop over the branches.
        for k = 1 : size(branches, 1)

            % Initialize active site's potentials in branch.
            VN = zeros(branches{k}.nN, 1);
            VP1 = zeros(branches{k}.nP1, 1);
            VP2 = zeros(branches{k}.nP2, 1);
            VI = zeros(branches{k}.nI, 1);

            % Extract branch's potentials from the imported data.
            V = Vs(k, :);
            V = V(~ isnan(V));

            % Distribute values to appropriate arrays.
            for m = 1 : branches{k}.nN - 1
                VN(m) = V(11 * (m - 1) + 1);
                VP1(2 * (m - 1) + 1) = V(11 * (m - 1) + 2);
                VP2(2 * (m - 1) + 1) = V(11 * (m - 1) + 3);
                VI(6 * (m - 1) + 1) = V(11 * (m - 1) + 4);
                VI(6 * (m - 1) + 2) = V(11 * (m - 1) + 5);
                VI(6 * (m - 1) + 3) = V(11 * (m - 1) + 6);
                VI(6 * (m - 1) + 4) = V(11 * (m - 1) + 7);
                VI(6 * (m - 1) + 5) = V(11 * (m - 1) + 8);
                VI(6 * (m - 1) + 6) = V(11 * (m - 1) + 9);
                VP2(2 * (m - 1) + 2) = V(11 * (m - 1) + 10);
                VP1(2 * (m - 1) + 2) = V(11 * (m - 1) + 11);
            end
            VN(end) = V(end);

            % Assign arrays branch's attributes.
            branches{k}.VN{ASindex(i)} = VN;
            branches{k}.VP1{ASindex(i)} = VP1;
            branches{k}.VP2{ASindex(i)} = VP2;
            branches{k}.VI{ASindex(i)} = VI;

        end
        
    end
    
end
