function verticalize(this, p0, direction)
    % Verticalize the DendSec and its downstream tree.
    %
    % Usage:
    %   dsec.verticalize()
    %   dsec.verticalize(p0)
    %   dsec.verticalize(direction)
    %   dsec.verticalize(p0, direction)
    %
    % Parameters:
    %   p0 : 1*3 double :
    %       The new initial point of the DendSec.
    %
    %   direction : str :
    %       'up' or 'down'. Indicates whether the verticalization should be
    %       performed upwards or downwards. 
    %
    % Description:
    %   This method should be applied to the stems of a dendritic tree.
    %   It verticalizes the dendritic tree, that is, it brings its paths
    %   closer to the z-axis, keeping their lengths unmodified.
    %   It operates recursively.
    %   If applied to the stems as intended, it first verticalizes the
    %   stems, then translates the downstream trees of each stem to match
    %   the new positions of the tips of the stems, and applies the
    %   verticalization to the direct children of the stems, and so on and
    %   so forth recursively. 
    
    % Parse inputs.
    if nargin == 1
        p0 = this.coords(1, :);
        direction = this.get_direction();
    elseif nargin == 2
        direction = this.get_direction();
    end
    
    if strcmp(direction, 'up')
        uz = [0, 0, 1];
        a = 1;
    else
        uz = [0, 0, -1];
        a = -1;
    end
    
    % First verticalize the DendSec itself.
    % This is done by verticalizing each segment of its path (this.coords).
    % The verticalization consists in 
    % (i) rectifying the direction of the DendSec: if the prescribed
    % direction is 'up' (resp. 'down') then each segment of the DendSec
    % sees its z-coordinate turned positive (resp. negative)
    % (ii) dividing by two the angle that each segment makes with the
    % z-axis. This is done by averaging the normalized segment vectors with
    % the uz vector (resp. -uz), and multiplying the resulting vectors by
    % the norms of the initial vectors.
    coords = zeros(size(this.coords));
    coords(1, :) = p0;
    for i = 2 : size(this.coords, 1)
        u = this.coords(i, :) - this.coords(i - 1, :);
        u(3) = a * sqrt(u(3)^2);
        l = norm(u);
        v = (u / l + uz) / 2 * l;
        p0 = p0 + v;
        coords(i, :) = p0;
    end
    this.coords = coords;
    
    % Second, verticalize the DendSec's offspring.
    if ~ isempty(this.siblings)
        cellfun(@(x) x.verticalize(), this.siblings);
    end
    if ~ isempty(this.lchild)
        this.lchild.verticalize(p0, direction);
    end
    if ~ isempty(this.rchild)
        this.rchild.verticalize(p0, direction);
    end
    
end
