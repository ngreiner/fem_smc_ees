function outbool = is_interp(this, AS_idx)
    % Return a logical indicating whether the DendSec is already well
    % interpolated for the input AS_idx.
    %
    % Usage:
    %   outbool = dsec.is_interp(AS_idx)
    %
    % Parameters:
    %   AS_idx : double.
    %
    % Return:
    %   outbool : logical.
    outbool = ~ isempty(this.V{AS_idx});          
end
