function direction = get_direction(this)
    % Return the direction of the tree downstream to the DendSec.
    %
    % Usage:
    %   direction = dsec.get_direction()
    %
    % Return:
    %   direction : string : 'up' or 'down'.
    %
    % Description:
    %   The tree originating from the DendSec is roamed to retrieve
    %   its most distant offspring.
    %   If that one is 'above' the DendSec (ie if z_offspring > z_DendSec)
    %   then the direction is 'up', otherwise it is 'down'.
    coords = cellfun(@(x) x.coords, this.get_offspring(), 'UniformOutput', false);
    coords = vertcat(coords{:});
    x2 = (coords(:, 1) - this.coords(1, 1)).^2;
    y2 = (coords(:, 2) - this.coords(1, 2)).^2;
    z2 = (coords(:, 3) - this.coords(1, 3)).^2;
    norms = sqrt(x2 + y2 + z2);
    [~, idx] = max(norms);
    dz = coords(idx, 3) - this.coords(1, 3);
    if dz < 0
        direction = 'down';
    else
        direction = 'up';
    end
end
