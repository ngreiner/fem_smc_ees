function struc = to_struct(this)
	% Return a matlab struct equivalent to the DendSec.
    %
    % Usage:
    %   struc = dsec.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the DendSec.
    struc.name = this.name;
    struc.coords = this.coords;
    struc.ds = this.ds;
    if ~ isempty(this.siblings)
        struc.siblings = cellfun(@(x) x.to_struct(), this.siblings, 'UniformOutput', false);
    end
    if ~ isempty(this.lchild)
        struc.lchild = this.lchild.to_struct();
    end
    if ~ isempty(this.rchild)
        struc.rchild = this.rchild.to_struct();
    end
    struc.V = this.V;
end
