classdef DendSec < handle

    % Properties defined here:
    %
    %   name : string :
    %       Name of the DendSec.
    %
    %   par : DendSec object :
    %       parent DendSec of the DendSec.
    %
    %   coords : N*3 double :
    %       Coordinates of DendSec's endpoints (in mm).
    %
    %   ds : N*1 double :
    %       Diameters of the DendSec at its endpoints (in mm).
    %
    %   siblings : K*1 cell :
    %       Sibling DendSecs of the DendSec.
    %
    %   lchild : DendSec object :
    %       Left child of the DendSec.
    %
    %   rchild : DendSec object :
    %       Right child of the DendSec.
    %
    %   V : nAS*1 cell :
    %       Electric potentials elicited by nAS active sites at the
    %       position of the DendSec.
    
    properties
        name        % string
        par         % DendSec object
        coords      % N*3 double
        ds          % N*1 double
        siblings    % K*1 cell
        lchild      % DendSec object
        rchild      % DendSec object
        V           % nAS*1 cell
    end
    
    methods
        
        function this = DendSec(varargin)
            % Class constructor method.
            %
            % Usage:
            %   dsec = DendSec(varargin)
            %   dsec = DendSec('name', name)
            %   dsec = DendSec('par', par)
            %   dsec = DendSec('name', name, 'par', par)
            %
            % Parameters:
            %   name : string : name of the DendSec.
            %   par : DendSec object : parent DendSec of the DendSec.
            
            idx = find(strcmp(varargin, 'name'));
            if idx
                this.name = varargin{idx + 1};
            end
            
            idx = find(strcmp(varargin, 'par'));
            if idx
                this.par = varargin{idx + 1};
            end
            
        end
        
    end
    
    methods
        from_struct(this, struc)
        direction = get_direction(this)
        dsecs = get_offspring(this)
        interp_V(this, smcd, perm)
        outbool = is_interp(this, AS_idx)
        rotate(this, ax, ay, az)
        struc = to_struct(this)
        translate(this, u)
        verticalize(this, p0, direction)
    end
    
end
            