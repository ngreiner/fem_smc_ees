function translate(this, u)
    % Translate the DendSec.
    %
    % Usage:
    %   dsec.translate(u)
    %
    % Parameters:
    %   u : 1*3 double : translation vector.
    for i = 1 : size(this.coords, 1)
        this.coords(i, :) = this.coords(i, :) + u;
    end
end
