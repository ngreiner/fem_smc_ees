function dsecs = get_offspring(this)
    % Return a cell-array of references to the offspring of the DendSec.
    %
    % Usage:
    %   dsecs = dsec.get_offspring()
    %
    % Return:
    %   dsecs : K*1 cell : DendSec objects.
    %
    % Description:
    %   Uses a recursive approach. The DendSec itself is included in the
    %   returned DendSecs.
    if isempty(this.lchild)
        dsecs = {this};
    elseif isempty(this.rchild)
        dsecs = [{this}; this.lchild.get_offspring()];
    else
        dsecs = [{this}; this.lchild.get_offspring(); this.rchild.get_offspring()];
    end
    
    if ~ isempty(this.siblings)
        offs = cellfun(@(x) x.get_offspring(), this.siblings, 'UniformOutput', false);
        dsecs = [dsecs; vertcat(offs{:})];
    end
end
