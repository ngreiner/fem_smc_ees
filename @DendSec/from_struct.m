function from_struct(this, struc)
	% Set the DendSec from the input struct.
    %
    % Usage:
    %   dsec.from_struct(struc)
    %
    % Parameters:
    %   struc : struct : with same fields as the DendSec.
    this.name = struc.name;
    this.coords = struc.coords;
    this.ds = struc.ds;
    if isfield(struc, 'siblings')
        this.siblings = cellfun(@(x) DendSec('par', this), struc.siblings, 'UniformOutput', false);
        cellfun(@(x, y) x.from_struct(y), this.siblings, struc.siblings, 'UniformOutput', false);
    end
    if isfield(struc, 'lchild')
        this.lchild = DendSec('par', this);
        this.lchild.from_struct(struc.lchild);
    end
    if isfield(struc, 'rchild')
        this.rchild = DendSec('par', this);
        this.rchild.from_struct(struc.rchild);
    end
    this.V = struc.V;
end
