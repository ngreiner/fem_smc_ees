function rotate(this, ax, ay, az)
    % Rotate the DendSec.
    %
    % Usage:
    %   dsec.rotate(ax, ay, az)
    %
    % Parameters:
    %   ax, ay, az : rotation angles around the x, y and z axes.
    %
    % Description:
    %   Composition of 3 rotations around x-, y- and z-axes.
    Rx = rotx(ax);
    Ry = roty(ay);
    Rz = rotz(az);
    for i = 1 : size(this.coords, 1)
        this.coords(i, :) = (Rx * Ry * Rz * this.coords(i, :)')';
    end
end
