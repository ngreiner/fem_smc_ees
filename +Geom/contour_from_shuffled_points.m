function contour = contour_from_shuffled_points(points)
    % Return a closed-contour from a shuffled collection of 2d points.
    %
    % Parameters:
    %   points : N*2 double : 2D-points collection.
    %
    % Return:
    %   contour : N*2 double : 2d closed-contour.
    %
    % Description:
    %   The returned closed-contour is formed by consecutively connecting
    %   the input points to their nearest neighbor. The first point is 
    %   finally added at the end of the list to close the contour.
    %   The resulting contour is not guaranteed to be intersection-free.
    contour = points(1, :);
    points(1, :) = [];
    while points
        [idx, x] = Geom.find_closest(contour(end, :), points);
        contour(end + 1, :) = x;
        points(idx, :) = [];
    end
    contour(end + 1, :) = contour(1, :);
end
