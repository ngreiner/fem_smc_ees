function C2 = symetrize(C1, mode)
    % Symetrizes the input 2d closed-contour C1 about the y-axis.
    %
    % Parameters:
    %   C1 : N*2 double : 2d closed-contour to symetrize.
    %   mode : string : symetrization mode.
    %
    % Return:
    %   C2 : K*2 double : symetrized 2d closed-contour.
    %
    % Description:
    %   Two modes are available to symetrize C1:
    %
    %   'minimal': C2 is obtained by folding the input contour C1 in the
    %   x>=0 plane, forming the convex hull of the resulting collection of
    %   points, and unfolding this convex hull (thus obtaining a contour
    %   symetric about the y-axis).
    %   In this case C2 is convex in each half plane x>0 and x<0 but not
    %   necessarily globally convex.
    %
    %   'convex': we form the convex hull of the contour obtained using the
    %   'minimal' algorithm.
    %
    %   Both algorithms start by reordering the points of the input contour
    %   such that its first point has x=0 and minimal y, that it possesses
    %   one other point with x=0, and that it be counterclockwise oriented.
    %
    %   Similarly, the returned symetrized contour possesses two points
    %   with x=0, the first of which is also the first point of the
    %   contour and has minimal y. The contour direction is clockwise.
    
    % Parse inputs.
    if (nargin == 1)
        mode = 'minimal';
    end
    
    % First, reorder points in input contour, eventually inserting points
    % on the y-axis, and such that the contour be counterclockwise oriented
    % with its first point having x=0 and minimal y.
    C2 = Geom.reorder_contour(C1, 'dir', 'counterclock', 'insert_yaxis_points', 'start_at', 'x=0,ymin');

    % Build minimal symetrized contour.
    C2 = [abs(C2(:, 1)), C2(:, 2)];
    K = convhull(C2(:, 1), C2(:, 2));
    x = C2(K(1 : end - 1), 1);
    y = C2(K(1 : end - 1), 2);
    C2 = [[-x, y]; flipud([x(1 : end - 1), y(1 : end - 1)])];
    
    % Form convex hull of minimal symetrized contour if mode is 'convex'.
    if strcmp(mode, 'convex')
        C2 = C2(convhull(C2(:, 1), C2(:, 2)), :);
        % Reorder points in C2.
        C2 = Geom.reorder_contour(C2, 'dir', 'clock', 'insert_yaxis_points', 'start_at', 'x=0,ymin');
    end

end
