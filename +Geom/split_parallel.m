function [contours, areas] = split_parallel(contour, target_areas, slope)
    % Split the input contour into parallel subregions of specified area.
    %
    % Usage:
    %   [contours, areas] = Geom.split_parallel(contour, target_areas, ...
    %                                           slope) 
    %
    % Parameters:
    %   contour: N * 3 double-array.
    %   target_areas: N * 1 double-array: target areas of split subregions.
    %   slope: double: slope of parallel splitting lines.
    %
    % Return:
    %   contours: N * 1 cell-array: parallel split contours.
    %   areas: N * 1 double-array: areas of the split contours.
    %
    % Description:
    %   The parameter target_areas, via its length, specifies the number of
    %   subregions into which the input contour should be split. 
    %   target_areas should be organized from left to right (so the 1st
    %   element corresponds to the area of the leftmost region of the
    %   contour).
    %   The split subregions are parallel in the sense that they are
    %   separated by straight lines which are parallel among them.
    %   The slope of these lines is specified by the parameter slope.
    %   Because of the employed algorithm, the output areas might differ
    %   from the target_areas.
    
    % Total area of contour.
    total_area = sum(target_areas);

    % Initialize array of output contours and areas.
    nc = size(target_areas, 1);
    contours = cell(nc, 1);
    areas = zeros(nc, 1);
    
    % If number of splitting regions is 1, return directly input contour
    % with input area.
    if nc == 1
        contours{1} = contour;
        areas = target_areas;
        return
    end

    % Otherwise, build contours iteratively by spliting the contour into
    % its leftmost region and remaining right region and reapplying the
    % function to the right region.
    Rcontour = contour;
    Rarea = total_area;
    for i = 1 : nc - 1
        Rarea = Rarea - target_areas(i);
        [Lcontour, Larea, Rcontour, Rarea] = Geom.split_contour(Rcontour, Rarea, slope);
        contours{i} = Lcontour;
        areas(i) = Larea;
    end
    
    % Add last remaing medial compartment and area to output arguments.
    contours{end} = Rcontour;
    areas(end) = Rarea;

end
