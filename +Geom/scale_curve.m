function C2 = scale_curve(C1, sf, varargin)
    % Translate a curve.
    %
    % Usage:
    %   C2 = Geom.scale_curve(C1)
    %   C2 = Geom.scale_curve(C1, 'inplace')
    %
    % Parameters:
    %   C1: N * 3 double-array: curve coordinates.
    %   sf: double: scaling factor.
    %
    % Return:
    %   C2: N * 3 double-array: scaled curve coordinates.
    if any(strcmp('in_place', varargin))
        x0 = repmat(mean(C1, 1), size(C1, 1), 1);
        C2 = (C1 - x0) * sf + x0;
    else
        C2 = C1 * sf;
    end
end
