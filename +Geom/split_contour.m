function [Lcontour, Larea, Rcontour, Rarea] = split_contour(contour, target_Rarea, slope)
    % Split the input contour into two along an oblique line.
    %
    % Parameters:
    %   contour: N * 3 double-array.
    %   target_Rarea: double: target area for the split right contour.
    %   slope: double: slope of splitting oblique line.
    %
    % Return:
    %   Lcontour: N * 3 double-array: split left contour.
    %   Larea: double: area of left contour.
    %   Rcontour: N * 3 double-array: split right contour.
    %   Rarea: double: area of right contour.
    %
    % Description: 
    %   Splits the input contour into a left and a right contour separated
    %   by an oblique straight line.
    %   The parameter target_Rarea specifies the target area for the split
    %   right contour, while slope specifies the slope of the splitting
    %   line.
    %
    %   IMPORTANT: this method assumes that the input contour is ordered
    %   in the reverse direction (clockwise) and that it presents a
    %   corner towards the x>0 and y<0 occupied by its 1st point.
    %
    %   The algorithm to split it thus goes as follows:
    %   1. add 1st point to the right contour in formation
    %   2. sequentially and alternatively roam the contour in the direct
    %      and reverse directions: each point visited is added to the right
    %      contour in formation
    %   3. at each step, compute the slope of the segment joining the
    %      running points in the reverse and the direct directions
    %   4. if this slope exceeds the specified slope, advance running point
    %      in the  direct direction, otherwise advance running point in
    %      the reverse direction: this ensures that the segment joining the
    %      two running points has approximately the requested slope
    %   5. at each step, compute the area of the right contour in formation
    %   6. when the area of the right contour in formation exceeds the
    %      requested area, stop the process
    %   7. sample points along the segment joining the two running points
    %      to close the thus formed right and left contours

    % Compute point density on contour.
    L = Geom.get_arc_length(contour);
    dens = size(contour, 1) / L;
    
    % Extract z-coordinate of contour.
    z = contour(1, 3);
    
    % First point of contour.
    I = contour(1, :);
    
    % Initial running points in reverse and direct directions on contour.
    idxR = 2;
    A = contour(idxR, :);
    idxD = size(contour, 1) - 1;
    B = contour(idxD, :);
    
    % Right contour in formation and corresponding area.
    Rcontour = [I; B; A; I];
    Rarea = polyarea(Rcontour(:, 1), Rcontour(:, 2));

    % Keep roaming the contour and adding points to the right contour in
    % formation while target area is not reached.
    while Rarea <= target_Rarea
        
        % Compute slope of current segment AB.
        slopeAB = (B(2) - A(2)) / (B(1) - A(1));
        
        % If slope of segment AB is smaller than requested slope, update
        % point B: get next point in direct direction.
        if slopeAB <= slope
            idxD = idxD - 1;
            B = contour(idxD, :);
            
        % Otherwise, update point A: get next point in reverse direction.
        else
            idxR = idxR + 1;
            A = contour(idxR, :);
            
        end
        
        % Compute the contour of the region within I, A, and B.
        % Path connecting I to A.
        IA = contour(1 : idxR, :);
        
        % Segment connecting A to B.
        L = norm(A - B);
        x = linspace(A(1), B(1), floor(L * dens))';
        y = linspace(A(2), B(2), floor(L * dens))';
        AB = [x, y, z * ones(size(x, 1), 1)];
        
        % Path connecting B to I.
        BI = contour(idxD : end, :);
        
        % Whole contour.
        Rcontour = [IA; AB(2 : end - 1, :); BI];
        
        % Compute area of the contour.
        Rarea = polyarea(Rcontour(:, 1), Rcontour(:, 2));
        
    end

    % Form left contour, and compute its area.
    pathAB = contour(idxR : idxD, :);
    Lcontour = [pathAB; flipud(AB(1 : end - 1, :))];
    Larea = polyarea(Lcontour(:, 1), Lcontour(:, 2));

end
