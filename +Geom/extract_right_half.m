function C2 = extract_contour_right_half(C1, varargin)
    % Extract right half of the input 2D-closed-contour.
    %
    % Usage:
    %   C2 = extract_right_half(C1, 'direction', direction)
    %   C2 = extract_right_half(C1, 'start_at', condition)
    %
    % Parameters:
    %   C1 : N*2 double : 2d closed-contour.
    %   direction : string : 'clock' or 'counterclock'.
    %   condition : string : 'x=0,ymin' or 'x=0,ymax'.
    %
    % Return:
    %   C2 : K*2 double : extracted right half of C1.
    %
    % Description:
    %   This function inserts two points with x=0, one between the two
    %   consecutive points of the input contour where the uppermost strict
    %   y-axis-crossing event (passage from x<0 and x>0, or vice-versa)
    %   occurs, one between the two consecutive points where the
    %   lowermost strict y-axis-crossing event occurs; discards the points
    %   with x<0; close the resulting curve and return the resulting closed
    %   contour.
    %   The inserted points are generated as weighted sums of the
    %   consecutive points involved in the crossing events.
    %   If no such event exist, the input contour is either completely
    %   enclosed in the x>=0 half-plane, either in the x<=0 plane. It is
    %   returned as is in the first case, and the empty contour is returned
    %   in the second case.
    %   If the input contour is subject to more than 2 y-axis-crossing
    %   events, an error is thrown. 
    %
    %   If 'direction' is specified in argument, the direction of the
    %   returned extracted right-half contour is set accordingly.
    %   Otherwise it remains the same as that of the input contour.
    %
    %   If 'start_at' is specified with a condition, the starting point of
    %   the contour (which is also the last point of the contour) is
    %   modified according to the specified condition.
    
    % Check whether contour is completely enclosed in half-plane x>=0 or
    % x<=0.
    idxn = C1(:, 1) < 0;
    if isempty(find(idxn, 1))
        C2 = Geom.reorder(C1, varargin{:});
        return
    end
    idxp = C1(:, 1) > 0;
    if isempty(find(idxp, 1))
        C2 = [];
        return
    end
    
    % Find direction of C1.
    % See https://math.stackexchange.com/questions/2152623/...
    %                          ...determine-the-order-of-a-3d-polygon?rq=1 
    vz = zeros(1, 3);
    for i = 1 : size(C1 , 1) - 1
        vz = vz + cross([C1(i, :), 0], [C1(i + 1, :), 0]);
    end
    dir_is_clock = (vz(3) < 0);
    
    % Impose clockwise direction for the time of the function execution.
    % We will change it again at the end if needed.
    if ~ dir_is_clock
        C2 = flipud(C1);
    else
        C2 = C1;
    end
    
    % Insert y-axis points (x=0) where x changes sign strictly.
    % First, find the points where the y-axis-crossing events occur.
    idxcs = find(C2(:, 1) .* circshift(C2(:, 1), -1) < 0);
    % If there are no strict crossing-events, we don't need to insert any
    % x=0 points (the contour already embeds them).
    % If there are more than two such crossing-events, throw error.
    if numel(idxcs) > 2
        error('Input contour crosses more than twice the y-axis.')
    % If there is a single such crossing-event, we need inserting a single
    % x=0 point.
    elseif numel(idxcs) == 1
        w = C2(idxcs, 1) / (C2(idxcs, 1) - C2(idxcs + 1, 1));
        x = [0, w * C2(idxcs + 1, 2) + (1 - w) * C2(idxcs, 2)];
        C2 = [C2(1 : idxcs, :); x; C2(idxcs + 1 : end, :)];
    % Otherwise we insert two x=0 points.
    elseif numel(idxcs) == 2
        idxcs1 = min(idxcs);
        idxcs2 = max(idxcs);
        w1 = C2(idxcs1, 1) / (C2(idxcs1, 1) - C2(idxcs1 + 1, 1));
        x1 = [0, w1 * C2(idxcs1 + 1, 2) + (1 - w1) * C2(idxcs1, 2)];
        w2 = C2(idxcs2, 1) / (C2(idxcs2, 1) - C2(idxcs2 + 1, 1));
        x2 = [0, w2 * C2(idxcs2 + 1, 2) + (1 - w2) * C2(idxcs2, 2)];
        C2 = [C2(1 : idxcs1, :); x1; C2(idxcs1 + 1 : idxcs2, :); x2; C2(idxcs2 + 1 : end, :)];
    end
    % Remove last point of input contour (which is equal to its first
    % point); remove points with x<0; close back contour.
    C2(end, :) = [];
    C2(C2(:, 1) < 0, :) = [];
    C2(end + 1, :) = C2(1, :);
    
    % Modify starting point (ie point of contour closure) if required.
    idx = find(strcmp(varargin, 'start_at'));
    if idx
        start_at = varargin{idx + 1};
    else
        start_at = false;
    end
    
    if strcmp(start_at, 'x=0,ymin')
        % Remove last point from C2 before going-on.
        C2(end, :) = [];
        idxs = find(C2(:, 1) == 0);
        ys = C2(idxs, 2);
        [~, n] = min(ys);
        C2 = circshift(C2, 1 - idxs(n), 1); 
        C2 = [C2; C2(1, :)];
    
    elseif strcmp(start_at, 'x=0,ymax')
        % Remove last point from C2 before going-on.
        C2(end, :) = [];
        ys = C2(C2(:, 1) == 0, 2);
        [~, idx] = max(ys);
        C2 = circshift(C2, 1 - idx, 1); 
        C2 = [C2; C2(1, :)];
        
    end
    
    % Switch contour direction if needed. This can be the case if specified
    % in varargin or if the original contour was anti-clockwise-directed
    % (since we switched to clockwise direction during the function
    % execution).
    idx = find(strcmp(varargin, 'dir'));
    if idx
        target_dir_is_clock = strcmp('clock', varargin{idx + 1});
        if ~ target_dir_is_clock
            C2 = flipud(C2);
        end
    else
        if ~ dir_is_clock
            C2 = flipud(C2);
        end
    end
    
end
