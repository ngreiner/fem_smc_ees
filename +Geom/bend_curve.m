function C2 = bend_curve(C1, u)
    % Bend the input curve.
    %
    % Usage:
    %   C2 = Geom.bend_curve(C1, u)
    %
    % Parameters:
    %   C1: N * 3 double-array: input curve coordinates.
    %   u: 1 * 3 double-array: bending direction vector.
    %
    % Return:
    %   C2: 3 * 1 double-array: the bent curve coordinates.
    %
    % Description:
    %   This method aims at bending the curve C1 such that the vector
    %   joining its two endpoints, e = C1(end) - C1(1), be colinear with
    %   the input vector u.
    %   It assumes that the input curve C1 lies in the xy-plane and is
    %   already such that e is colinear to the projection of u on the
    %   xy-plane (which is u(1:2)).

    % If the input direction vector has a null z-component, there is no
    % bending needed.
    if u(3) == 0
        C2 = C1;
        return
    end
    
    % Otherwise:
    % Find rotation axis direction.
    u1 = u;
    u1(3) = 0;
    u2 = u;
    v = cross(u1, u2) / norm(cross(u1, u2));
    v1 = v(1);
    v2 = v(2);
    
    % Find rotation angle.
    theta = asin(norm(cross(u1, u2)) / norm(u1) / norm(u2));
    c = cos(theta);
    s = sin(theta);
    
    % Form rotation matrix.
    rotMat = [c + v1^2 * (1 - c),    v1 * v2 * (1 - c),   v2 * s;
               v1 * v2 * (1 - c),   c + v2^2 * (1 - c),  -v1 * s;
                         -v2 * s,               v1 * s,        c];
                     
    % Perform rotation.
    C2 = C1 * rotMat;
    
end
