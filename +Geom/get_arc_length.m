function l = get_arc_length(path)
    % Return the cumulated arc-length of the input path.
    %
    % Parameters:
    %   path: N * 3 double-array: each row is a 3d-point.
    %
    % Return:
    %   l: double: the cumulated arc-length of the path.
    %
    % Description: Void.    
    l = 0;
    segs = diff(path, 1);
    for i = 1 : size(segs, 1)
        l = l + norm(segs(i, :));
    end
end
