function outbool = check_relative_position(E1, E2)
    % Check if ellipses are intersecting.
    %
    % Usage:
    %   outbool = Geom.check_relative_position(E1, E2)
    %
    % Parameters:
    %   E1: N * 3 double-array: 1st input ellipse coordinates.
    %   E2: N * 3 double-array: 2nd input ellipse coordinates.
    %
    % Return:
    %   outbool: double: true if E1's convex enveloppe intersects
    %                    E2's, false otherwise.
    %
    % Description:
    %   To assess wether E1 intersects E2, we look for the plane in which
    %   E2 is lying, and we check wether every point of E1 lie in the same
    %   half-space delimited by this plane. 
    %   If this is the case, E1 doesn't intersect E2, otherwise, it does
    %   intersect E2. 
    
    % Get 3 non-aligned points on E2.
    idx1 = 1;
    idx2 = floor(size(E2, 1) / 3);
    idx3 = 2 * floor(size(E2, 1) / 3);
    x21 = E2(idx1, :);
    x22 = E2(idx2, :);
    x23 = E2(idx3, :);
    
    % Normal vector to E2's plane.
    n = cross(x22 - x21, x23 - x21);
    
    % Initialize return value to 0: no intersection.
    outbool = 0;
    
    % Check if any point of E1 is on the opposite half-space compared to
    % the 1st point of E1.
    x11 = E1(1, :);
    i = 2;
    while i <= size(E1, 1)
        x1i = E1(i, :);
        if dot(x1i - x21, n) * dot(x11 - x21, n) <= 0
            outbool = 1;
            return
        end
        i = i+1;
    end
    
end
