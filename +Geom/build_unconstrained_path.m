function path = build_unconstrained_path(CS, dens, varargin)
    % Build an unconstrained path running through the input cross-sections
    % CS.
    %
    % Usage:
    %   path = Geom.build_unconstrained_path(CS, dens)
    %   path = Geom.build_unconstrained_path(CS, dens, 'starting_point', P0)
    %   path = Geom.build_unconstrained_path(CS, dens, 'refine_interp')    
    %
    % Parameters:
    %   CS: N*1 cell-array: each cell is a K*3 double array giving the
    %       coordinates of a single cross-section.
    %   dens: double: density of points on the final path (in points/mm).
    %   P0: 1*3 double-array: starting point of output path.
    %
    % Return:
    %   path: N*3 double-array: path running through the cross-sections CS.
    %
    % Description:
    % The path is made of endpoints. 
    % The path is made of endpoints. There is one endpoint in each CS.
    % It is obtained using a custom algorithm generating a random point in
    % the convex hull of the points of the CS.
    % If 'refine_interp' is given in argument, the amount of interpolation
    % points is doubled prior to cubic spline interpolation.
    
    % Parse inputs.
    if (nargin < 2)
        dens = 20;
    end
    
    % Recover number of cross-sections.
    nCS = size(CS, 1);

    % Generate interpolation points.
    intP = zeros(nCS, 3);
    for i = 1 : nCS
        nP = size(CS{i}, 1);
        % Generate weights for the weighted sums.
        w = Geom.gen_weights_unit_simplex(nP, 3);
        % Form weighted sum.
        intP(i, :) = w * CS{i};
    end
    
    % Add starting point if provided in varargin.
    idx = find(strcmp(varargin, 'starting_point'));
    if ~isempty(idx)
        P0 = varargin{idx + 1};
        intP = [P0; intP];
    end

    % Double number of interpolation points by linear interpolation applied
    % to the existing interpolation points if specified by varargin.
    if any(strcmp(varargin, 'refine_interp'))
        intP = Geom.interparc(2 * size(intP, 1), intP(:, 1), intP(:, 2), intP(:, 3), 'linear');
    end
    
    % Sample points along a cubic spline fitting the interpolation points.
    nP = round(Geom.get_arc_length(intP) * dens);
    path = Geom.interparc(nP, intP(:, 1), intP(:, 2), intP(:, 3), 'spline');
    
end
