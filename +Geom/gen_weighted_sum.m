function P = gen_weighted_sum(points, order)
    % Return a random convex combination of the input points.
    %
    % Usage:
    %   P = Geom.gen_weighted_sum(points)
    %   P = Geom.gen_weighted_sum(points, order)
    %
    % Parameters:
    %   Ps: N*3 double-array: each row is a point.
    %   order: int: exponent for the weights of the weighted sum.
    %          The larger it is, the more likely the resulting point falls
    %          close to the border of the convex hull of the input points.
    %
    % Return:
    %   P: 1*3 double-array: the convex combination.
    %
    % Description:
    % If np denotes the number of input points, we start by sampling a
    % vector ws uniformly at random in the unit simplex of R^np (which
    % yields np weights). 
    % P is simply the weighted average of the input points using the
    % previous weights.
    
    % Parse inputs.
    if nargin == 1
        order = 1;
    end
    
    % Generate weighted sum.
    np = size(points, 1);
    ws = Geom.gen_weights_unit_simplex(np, order);
    P = ws * points;
    
end
