function C2 = rotate_curve(C1, alpha)
    % Rotate curve in the xy-plane.
    %
    % Usage:
    %   C2 = Geom.rotate_curve(C1, alpha)
    %
    % Parameters:
    %   C1: N * 2/3 double-array: curve coordinates.
    %   alpha: double: rotation angle.
    %
    % Return:
    %   C2: N * 2/3 double-array: rotated curve coordinates.
    %
    % Description:
    %   The method can deal with both 2D- and 3D-curves.
    
    c = cos(alpha);
    s = sin(alpha);
    
    if size(C1, 2) == 2
        rotMat = [c, -s;
                  s,  c];
        
    else
        rotMat = [c, -s,  0;
                  s,  c,  0;
                  0,  0,  1];
        
    end
    
    C2 = (rotMat * C1')';

end
