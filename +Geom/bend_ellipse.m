function E2 = bend_ellipse(E1, u, w)
    % Bend the input ellipse.
    %
    % Usage:
    %   E2 = Geom.bend_ellipse(E1, u)
    %   E2 = Geom.bend_ellipse(E1, u, w)
    %
    % Parameters:
    %   E1: N * 3 double-array: input ellipse coordinates.
    %   u: 1 * 3 double-array: bending direction vector.
    %   w: double [optional]: weight applied to deviate from strict
    %                         orthogonality.
    %
    % Return:
    %   E2: N * 3 double-array: bent ellipse coordinates.
    %
    % Description:
    %   This method aims at bending the ellipse E1 such that the plane
    %   in which it lies be orthogonal to the vector u.
    %   It assumes that E1 is lying in the xy-plane.
    %   Let u2 be the projection of u in the xy-plane (u2 = [u(1) u(2) 0]).
    %   The rotation axis is lying in the xy-plane, orhtogonal to u2.
    %   We chose to orient thix axis (A) with the unitary vector v obtained
    %   by rotating u2 in the xy-plane by pi/2 in the direct direction.
    %   The rotation angle theta can be decomposed in theta = -pi/2 + phi
    %   where the term -pi/2 represents a bending of the ellipse to make
    %   its plane orthogonal to u2, and the term phi represents the bending
    %   necessary to bring u2 colinear to the original u.
    %   The unoriented angle between u and u2 is necessarily between 0 and
    %   pi/2 given the definition of u2.
    %   We can retrieve it from the dot product of the two vectors.
    %   Then, for its orientation, given the orientation of v for the axis
    %   A, if u was pointing upwards (u(3)>0), the orientation shall
    %   be indirect, otherwise, direct.
    %
    %   In the case where u is already lying in the xy-plane, we have
    %   directly u2=u and the definition of v is unchanged, the rotation
    %   angle -pi/2.
    %
    %   If the input weight w is provided, the term phi in the
    %   decomposition theta = -pi/2 + phi is weighted by w (which is
    %   between 0 and 1).
    %   If w is not provided, it is automatically assigned the value 1.
    
    % Parse inputs.
    if nargin == 2
        w = 1;
    end
    
    % Define u2, v and theta (cumulated rotation angle).
    u2 = [u(1) u(2) 0];
    v = [-u(2) u(1) 0] / norm([-u(2) u(1) 0]);
    
    % If u is lying in xy-plane, the rotation angle is -pi/2.
    if u(3) == 0
        theta = -pi/2;
        
    % Otherwise:
    else
        % Retrieve unoriented angle between u and u2.
        theta = acos(dot(u, u2) / norm(u) / norm(u2));
        % Orient it according to 3rd coordinate of u.
        if u(3) > 0
            theta = -theta;
        end
        % Cumulated rotation angle, taking weight into account.
        theta = w * theta - pi/2;
    end

    % Form rotation matrix.
    v1 = v(1);
    v2 = v(2);
    c = cos(theta);
    s = sin(theta);
    rotMat = [c + v1^2 * (1 - c),    v1 * v2 * (1 - c),  -v2 * s;
               v1 * v2 * (1 - c),   c + v2^2 * (1 - c),   v1 * s;
                          v2 * s,              -v1 * s,        c];
    
    % Perform rotation.
    E2 = E1 * rotMat;
    
end
