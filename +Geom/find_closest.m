function [idx, x2] = find_closest(x1, C)
    % Find and return the point of C closest to x1.
    %
    % Parameters:
    %   x1: 1*2 or 1*3 double-array: 2D-point.
    %   C: N*2 or N*3 double-array: 2D-points collection.
    %
    % Return:
    %   idx: double: index of closest point to x1 in C.
    %   x2: 1*2 double-array: closest point to x1 in C.
    idx = 1;
    x2 = C(1, :);
    for i = 2 : size(C, 1)
        if norm(x1 - C(i, :)) < norm(x1 - x2)
            idx = i;
            x2 = C(i, :);
        end
    end
end
