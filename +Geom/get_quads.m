function quads = get_quads(CS, k)
    % Return the quadrants #k of the input CS's.
    %
    % Usage:
    %   quads = Geom.get_quads(CS, k)
    %
    % Parameters:
    %   CS: N*1 cell-array of N*3 double-arrays: cross-sections' contours.
    %   k: integer: index of quadrant.
    %
    % Return:
    %   quad: N*1 cell-array of N*3 double-arrays: cross-sections'
    %         quadrants contours.
    %
    % Description: TO BE COMPLETED.
    
    % Convenient variable.
    nCS = size(CS, 1);
    
    % Build quadrants.
    quads = cell(nCS, 1);
    for i = 1 : nCS
        nP = size(CS{i}, 1); % number of points on contour.
        xc = mean(CS{i}, 1); % center of contour.
        quad = CS{i}(1 + floor((k - 1) / 4 * nP) : floor(k / 4 * nP), :); % quadrant's contour portion.
        quad = [xc; quad; xc]; % closing previous portion with center point to form contour.
        quad = Geom.interparc(nP, quad(:, 1), quad(:, 2), quad(:, 3), 'linear'); % uniform linear resampling of previous contour.
        quads{i} = quad;
    end
    
end
