function C2 = move_curve(C1, u)
    % Translate a curve.
    %
    % Usage:
    %   C2 = Geom.move_curve(C1, u)
    %
    % Parameters:
    %   C1: N * 3 double-array: curve coordinates.
    %   u: 1 * 3 double-array: translation vector.
    %
    % Return:
    %   C2: N * 3 double-array: translated curve coordinates.
    C2 = C1 + repmat(u, size(C1, 1), 1);
end
