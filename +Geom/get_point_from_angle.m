function point = get_point_from_angle(C, alpha)
    % Return a virtual point lying in the input curve C in the direction of
    % input angle alpha.
    %
    % Usage:
    %   point = Geom.get_point_from_angle(C, alpha)
    %
    % Parameters:
    %   C: N * 3 double-array: curve coordinates.
    %   alpha: double: direction angle.
    %
    % Return:
    %   point: 1 * 3 double-array: coordinates of desired virtual point.
    %
    % Description:
    %   The angle alpha is given in radian and specifies a direction in the
    %   xy-plane.
    %   The method seeks the two points of C closest to the direction
    %   alpha, and performs a weighted average of these 2 points to yield
    %   the desired virtual point.

	% Restrain curve to portion of interest (in the appropriate quadrant).
    if cos(alpha) >= 0
        if sin(alpha) >= 0
            idx = (C(:, 1) >= 0) & (C(:, 2) >= 0);
        else
            idx = (C(:, 1) >= 0) & (C(:, 2) < 0);
        end
    else
        if sin(alpha) >= 0
            idx = (C(:, 1) < 0) & (C(:, 2) >= 0);
        else
            idx = (C(:, 1) < 0) & (C(:, 2) < 0);
        end
    end
    C = C(idx, :);
    x = C(:, 1);
    y = C(:, 2);

	% Find index of closest points to direction alpha by determinant
    % minimization.
    det1 = abs(x(1) * sin(alpha) - y(1) * cos(alpha)) / sqrt(x(1)^2 + y(1)^2);
    idx1 = 1;
    for j = 2 : size(x, 1)
        if abs(x(j) * sin(alpha) - y(j) * cos(alpha)) / sqrt(x(j)^2 + y(j)^2) <= det1
            det1 = abs(x(j) * sin(alpha) - y(j) * cos(alpha)) / sqrt(x(j)^2 + y(j)^2);
            idx1 = j;
        end
    end
    det2 = abs(x(1) * sin(alpha) - y(1) * cos(alpha)) / sqrt(x(1)^2 + y(1)^2);
    idx2 = 1;
    for j = 2 : size(x, 1)
        if (abs(x(j) * sin(alpha) - y(j) * cos(alpha)) / sqrt(x(j)^2 + y(j)^2) <= det2) && (j ~= idx1)
            det2 = abs(x(j) * sin(alpha) - y(j) * cos(alpha)) / sqrt(x(j)^2 + y(j)^2);
            idx2 = j;
        end
    end

    % Weighted sum of the previous points.
    w1 = det2 / (det1 + det2);
    w2 = det1 / (det1 + det2);
    point = w1 * C(idx1, :) + w2 * C(idx2, :);
    
end
