function point = get_farthest_point_from_zaxis(C)
    % Return the point of C farthest from the z-axis.
    %
    % Usage:
    %   point = Geom.get_farthest_point_from_zaxis(C)
    %
    % Parameters:
    %   C : N*2 or N*3 double : curve coordinates.
    %
    % Return:
    %   point : 1*3 double : coordinates of farthest point of C.
    [~, idx] = max(sum(C(:, 1 : 2) .^ 2, 2));
    point = C(idx, :);
end
