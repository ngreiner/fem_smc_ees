function C2 = dilate_curve(C1, lx, ly)
    % Anisotropic dilatation of a 2D-curve.
    %
    % Usage:
    %   C2 = Geom.dilate_curve(C1, lx, ly)
    %
    % Parameters:
    %   C1: N * 2 double-array: input curve coordinates.
    %   lx: double: dilatation coefficient in x-direction.
    %   ly: double: dilatation coefficient in y-direction.
    %
    % Return:
    %   C2: 2 * 1 cell-array: dilated 2D-curve coordinates.
    C2 = C1;
    C2(:, 1) = lx * C2(:, 1);
    C2(:, 2) = ly * C2(:, 2);
end
