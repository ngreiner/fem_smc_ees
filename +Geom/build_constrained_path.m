function path = build_constrained_path(CS, dens, varargin)
    % Build a constrained path running through the input cross-sections CS.
    %
    % Usage:
    %   path = Geom.build_constrained_path(CS, dens)
    %   path = Geom.build_constrained_path(CS, dens, 'starting_point', P0)
    %   path = Geom.build_constrained_path(CS, dens, 'refine_interp')    
    %   path = Geom.build_constrained_path(CS, dens, 'prolong')    
    %
    % Parameters:
    %   CS: N*1 cell array: each cell is a K*3 double array giving the
    %       coordinates of a single cross-section.
    %   dens: double: density of points on the final path (in points/mm).
    %   P0: 1*3 double-array: starting point of output path.
    %
    % Return:
    %   path: N*3 double-array: path running through the cross-sections CS.
    %
    % Description: 
    % The path is made of endpoints. There is one endpoint in each CS.
    % It is obtained using a custom algorithm generating a random point in
    % the convex hull of the points of the CS.
    % It is assumed that all CS in input are composed of the same number of
    % points. This is needed to build a 'constrained' path. Indeed, the
    % path is constrained because the same collection of weights is used
    % to build the point in the convex hull of each CS.
    
    % Parse inputs.
    if (nargin < 2)
        dens = 20;
    end
    
    % Recover number of points on each cross-section.
    nP = size(CS{1}, 1);
    
    % Generate weights for the weighted sums.
    w = Geom.gen_weights_unit_simplex(nP, 3);
    
    % Generate a weighted sum in each cross-section using the previous
    % weights.
    intP = cell2mat(cellfun(@(x) w * x, CS, 'UniformOutput', false));

    % Add starting point if provided in varargin.
    idx = find(strcmp(varargin, 'starting_point'));
    if ~isempty(idx)
        P0 = varargin{idx + 1};
        intP = [P0; intP];
    end

    % Double number of interpolation points by linear interpolation applied
    % to the existing interpolation points if specified by varargin.
    if any(strcmp(varargin, 'refine_interp'))
        intP = Geom.interparc(2 * size(intP, 1), intP(:, 1), intP(:, 2), intP(:, 3), 'linear');
    end
    
    % Prolong path if specified by varargin.
    if any(strcmp(varargin, 'prolong'))
        l = Geom.get_arc_length(intP);
        u = intP(end, :) - intP(end - 1, :);
        intP(end + 1, :) = intP(end, :) + l / norm(u) * u;
    end
    
    % Sample points along a cubic spline interpolating the previous points.
    nP = round(Geom.get_arc_length(intP) * dens);
    path = Geom.interparc(nP, intP(:, 1), intP(:, 2), intP(:, 3), 'spline');
    
end
