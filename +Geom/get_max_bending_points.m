function [p1, p2] = get_max_bending_points(C)
    % Find and return points of maximal curvature on 2d-curve. 
    %
    % Usage:
    %   [p1, p2] = Geom.get_max_bending_points(C)
    %
    % Parameters:
    %   C: N * 2 double-array: curve coordinates.
    %
    % Return:
    %   p1: 1 * 2 double-array: 1st maximal curvature point.
    %   p2: 1 * 2 double-array: 2nd maximal curvature point.
    %
    % Description:
    %   Curvature is evaluated by the amount of change of direction
    %   between two consecutive edges of the curve.   
    
    x = C(:, 1);
    y = C(:, 2);

    angles = acos(diff(x) ./ sqrt(sum(diff([x, y]).^2, 2)));
    sinAngles = diff(y) ./ sqrt(sum(diff([x, y])).^2, 2);
    for i = 1 : size(sinAngles, 1)
        if sinAngles(i) < 0;
            angles(i) = -angles(i);
        end
    end

    diffAngles = diff(angles);
    i1 = 1;
    i2 = 1;
    max1 = 0;
    max2 = 0;
    for i = 1 : size(diffAngles)
        if abs(diffAngles(i)) > max1;
            max2 = max1;
            i2 = i1;
            max1 = abs(diffAngles(i));
            i1 = i;
        elseif abs(diffAngles(i)) > max2;
            max2 = abs(diffAngles(i));
            i2 = i;
        end
    end

    p1 = [x(i1 + 1), y(i1 + 1)];
    p2 = [x(i2 + 1), y(i2 + 1)];
    
end
