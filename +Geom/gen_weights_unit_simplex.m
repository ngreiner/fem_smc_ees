function w = gen_weights_unit_simplex(d, order)
    % Return a vector w sampled uniformly at random in the unit simplex of
    % R^d (thus yielding d weights summing up to 1).
    %
    % Usage:
    %   w = Geom.gen_weights_unit_simplex(d, order)
    %
    % Parameters:
    %   d: int: desired number of weights.
    %   order: int: exponent for the weights. The larger it is, the more
    %          likely the resulting vector falls close to the border of the
    %          unit simplex. 
    %
    % Return:
    %   w: 1*d double-array: the vector of weights.
    %
    % Description: Void.
    
    % Generate weights from the unit simplex.
	w = rand(1, d + 1);
    w(1) = 0;
    w(end) = 1;
	w = sort(w);
	w = diff(w);
    
    % Exponentiate if necessary.
    if (nargin == 2) && (order ~= 1)
        w = w.^order;
        w = w / sum(w);
    end
        
end
