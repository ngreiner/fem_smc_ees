function rect_areas = rectify_areas(areas, available_area)
    % Rectify the given areas to fit in the specified total area.
    %
    % Usage:
    %   rect_areas = rectify_areas(areas, available_area)
    %
    % Parameters:
    %   areas: N * 1 double-array: areas to rectify.
    %   available_area: double: total available area.
    %
    % Return:
    %   rect_areas: N * 1 double-array: rectified areas.
    %
    % Description:
    %   The aim of this function is to determine whether the specified
    %   available_area is sufficient to host the specified areas and still
    %   offer a bit of additional area, and to rectify (by scaling down)
    %   the specified areas to meet this condition if it is not case.
    %
    %   This can be expressed:
    %       if available_area > (1 + eps) * sum(areas)
    %           CONDITION IS MET
    %       else
    %           RECTIFY AREAS
    %
    %   where eps is a small >0 parameter hardcoded in the function corpus.
    %
    %   If rectification is needed, it consists in replacing the areas to
    %   fit by N copies of 1/(N+1)-th of the available_area, where N is the
    %   number of areas to fit. This thus equalizes the areas to fit and
    %   manage a space for an (N+1)-th identical area to be fit in the
    %   available_area.
    
    eps = 0.25;
    
    if available_area < (1 + eps) * sum(areas)
        N = size(areas, 1);
        rect_areas = available_area / (N + 1) * ones(N, 1);
        
    else
        rect_areas = areas;
        
    end
    
end
