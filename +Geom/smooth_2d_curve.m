function C2 = smooth_curve(C1, win_size)
	% 
    %
    % Parameters:
    %   C1 : N*2 double: curve to smooth
    %   win_size : double : moving window size.
    %
    % Return:
    %   C2 : N*2 double : smoothed curve.
    %
    % Description:
    %   Apply a moving average filter to remove high frequency components
    %   from C1.
    P = complex(C1(:, 1), C1(:, 2));
    b = (1 / win_size) * ones(1, win_size);
    a = 1;
    Z = filtfilt(b, a, P);
    C2 = [real(Z), imag(Z)];
end
