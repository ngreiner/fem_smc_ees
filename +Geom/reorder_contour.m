function C2 = reorder_contour(C1, varargin)
    % Reorder the points of the input 2D-closed-contour.
    %
    % Usage:
    %   C2 = reorder_contour(C1, 'dir', dir)
    %   C2 = reorder_contour(C1, 'insert_yaxis_points')
    %   C2 = reorder_contour(C1, 'start_at', condition)
    %
    % Parameters:
    %   C1 : N*2 double : 2d closed-contour.
    %   dir : string : 'clock' or 'counterclock'.
    %   condition : string : 'x=0,ymin' or 'x=0,ymax'.
    %
    % Return:
    %   C2 : N(+1/2)*2 double : reordered 2d closed-contour.
    %
    % Description:
    %   If dir is specified, the direction of the input contour C1 is
    %   changed accordingly. Otherwise it remains the same.
    %   If 'insert_yaxis_points' is specified, points with x=0 are inserted
    %   between two consecutive points where a strict y-axis-crossing event
    %   happens (the first point has x<0 and the second has x>0, or
    %   vice-versa). In fact, we only insert points for the two extremal
    %   such y-axis-crossing events (extremal in the sense of their
    %   location along the y-axis). If no such event exists, no points are
    %   inserted. If such events exist, the inserted points are generated
    %   as weighted sums of the consecutive points involved in the
    %   crossing-events.
    %   If 'start_at' is specified with a condition, the starting point of
    %   the contour (which is also the last point of the contour) is
    %   modified according to the specified condition.
    
    % Find direction of C1.
    % See https://math.stackexchange.com/questions/2152623/...
    %                          ...determine-the-order-of-a-3d-polygon?rq=1 
    vz = zeros(1, 3);
    for i = 1 : size(C1 , 1) - 1
        vz = vz + cross([C1(i, :), 0], [C1(i + 1, :), 0]);
    end
    dir_is_clock = (vz(3) < 0);
    
    % Impose clockwise direction for the time of the function execution.
    % We will change it again at the end if needed.
    if ~ dir_is_clock
        C2 = flipud(C1);
    else
        C2 = C1;
    end
    
    % Insert y-axis points (x=0) where x changes sign strictly if specified
    % in varargin.
    if any(strcmp('insert_yaxis_points', varargin))
        % First, find the points where the y-axis-crossing events occur.
        idxcs = find(C2(:, 1) .* circshift(C2(:, 1), -1) < 0);
        % If there are no strict crossing-events, we don't need to insert
        % any x=0 point.
        % If there is a single such crossing-event, we need inserting a
        % single x=0 point.
        if numel(idxcs) == 1
            w = C2(idxcs, 1) / (C2(idxcs, 1) - C2(idxcs + 1, 1));
            x = [0, w * C2(idxcs + 1, 2) + (1 - w) * C2(idxcs, 2)];
            C2 = [C2(1 : idxcs, :); x; C2(idxcs + 1 : end, :)];
        % If there are more than two such crossing-events, insert y-axis
        % points only for the two extremal events (extremal in the sense of
        % their location along the y-axis).
        elseif numel(idxcs) >= 2
            crossings = C2(idxcs, :);
            [~, idx] = max(crossings(:, 2));
            idxcs1 = idxcs(idx);
            [~, idx] = min(crossings(:, 2));
            idxcs2 = idxcs(idx);
            if idxcs2 < idxcs1
                tmp = idxcs1;
                idxcs1 = idxcs2;
                idxcs2 = tmp;
            end
            w1 = C2(idxcs1, 1) / (C2(idxcs1, 1) - C2(idxcs1 + 1, 1));
            x1 = [0, w1 * C2(idxcs1 + 1, 2) + (1 - w1) * C2(idxcs1, 2)];
            w2 = C2(idxcs2, 1) / (C2(idxcs2, 1) - C2(idxcs2 + 1, 1));
            x2 = [0, w2 * C2(idxcs2 + 1, 2) + (1 - w2) * C2(idxcs2, 2)];
            C2 = [C2(1 : idxcs1, :); x1; C2(idxcs1 + 1 : idxcs2, :); x2; C2(idxcs2 + 1 : end, :)];
        end
    end
    
    % Modify starting point (ie point of contour closure) if required.
    idx = find(strcmp(varargin, 'start_at'));
    if idx
        start_at = varargin{idx + 1};
    else
        start_at = false;
    end
    
    if strcmp(start_at, 'x=0,ymin')
        % Remove last point from C2 before going-on.
        C2(end, :) = [];
        idxs = find(C2(:, 1) == 0);
        ys = C2(idxs, 2);
        [~, n] = min(ys);
        C2 = circshift(C2, 1 - idxs(n), 1); 
        C2 = [C2; C2(1, :)];
    
    elseif strcmp(start_at, 'x=0,ymax')
        % Remove last point from C2 before going-on.
        C2(end, :) = [];
        ys = C2(C2(:, 1) == 0, 2);
        [~, idx] = max(ys);
        C2 = circshift(C2, 1 - idx, 1); 
        C2 = [C2; C2(1, :)];
        
    end
    
    % Switch contour direction if needed. This is the case if varargin
    % specifies something else than 'clock' for the direction of the
    % returned contour, or if varargin don't specify this direction and the
    % original contour was anti-clockwise-directed (for we modified its
    % direction to clockwise during the function execution, and we actually
    % want this direction to be left unchanged).
    idx = find(strcmp(varargin, 'dir'));
    if idx
        target_dir_is_clock = strcmp('clock', varargin{idx + 1});
        if ~ target_dir_is_clock
            C2 = flipud(C2);
        end
    else
        if ~ dir_is_clock
            C2 = flipud(C2);
        end
    end
    
end
