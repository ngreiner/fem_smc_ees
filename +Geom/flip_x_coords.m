function new_coords = flip_x_coords(coords)
    % Flip the x-coordinates of the given coordinates.
    %
    % Usage:
    %   new_coords = Geom.flip_x_coords(coords)
    %
    % Parameters:
    %   coords: N * 3 double-array / cell-array of N * 3 double-arrays.
    %
    % Return:
    %   new_coords: the x-flipped coords.
    %
    % Description: the input coords can be a double-array with its first
    % column being the x-coordinates or a cell-array of the previous-like.
    
    % Recursive call on each cell if coords is a cell-array.
    if iscell(coords)
        new_coords = cellfun(@Geom.flip_x_coords, coords, 'UniformOutput', false);
        return
    end
    
    % Flip first column otherwise.
    new_coords = coords;
    new_coords(:, 1) = - coords(:, 1);

end