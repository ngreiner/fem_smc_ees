function E = build_ellipse(diam, exc, np)
    % Build an ellipse in the xy-plane.
    %
    % Usage:
    %   E = Geom.build_ellipse(diam, exc)
    %   E = Geom.build_ellipse(diam, exc, np)
    %
    % Parameters:
    %   diam: double: diameter of ellipse to build.
    %   exc: double: excentricity of ellipse to build.
    %   np: double [optional]: number of points composing the ellipse.
    %
    % Return:
    %   E: N * 3 double-array: ellipse coordinates.
    %
    % Description:
    %   The excentricity is applied multiplicatively along the x-axis, and
    %   in a divise manner along the y-axis.
    %   Default number of points composing the ellipse is np=20.
    
    if nargin == 2
        np = 40;
    end

    t = linspace(0, 2 * pi, np)';
    E = diam / 2 * [cos(t) * exc, sin(t) / exc, zeros(size(t))];
    
end
