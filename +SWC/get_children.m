function cids = get_children(sid, pids)
    % Return the ids of the children samples of the input sid.
    %
    % Parameters:
    %   sid : integer : sample id.
    %   pids : N*1 integer :list of parent ids of sample ids. 
    %
    % Return:
    %   cids : K*1 integer :
    %       List of ids of the children of the input sid. 
    %
    % Description:
    %   The children of sid are those samples whose parent is sid.
    cids = find(pids == sid);
end
