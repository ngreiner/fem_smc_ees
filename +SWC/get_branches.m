function brs = get_branches(sid, pids)
    % Return the dendritic branches originating from the sample #sid. The
    % branches are in effect lists of other sids, each belonging to a
    % single dendritic branch. 
    %
    % Parameters:
    %   sid : integer :
    %       Sample identifier.
    %
    %   pids : N*1 integer :
    %       Parent sample identifiers. pids(i) is the identifier of the
    %       parent sample of sample #i.
    %
    % Return:
    %   brs : K*1 cell :
    %       Branches (M*1 integer) originating from sample #sid. 
    %
    % Description:
    %   Uses a recursive approach.

    cids = swc.get_children(sid, pids);  % direct children of node id
    if size(cids, 1) == 0
        brs = {};
    elseif size(cids, 1) == 1
        ccids = swc.get_children(cids(1), pids);
        if size(ccids, 1) == 0
            brs = {[sid; cids(1)]};
        elseif size(ccids, 1) == 1
            cbr0 = swc.get_branches(cids(1), pids);
            brs = [{[sid; cbr0{1}]}; cbr0(2 : end)];
        else  % len(ccids)=2
            brs = [{[sid; cids(1)]}; swc.get_branches(cids(1), pids)];
        end
    else  % len(cids)=2
        c0cids = swc.get_children(cids(1), pids);
        if size(c0cids, 1) == 0
            br0 = {[sid; cids(1)]};
        elseif size(c0cids, 1) == 1
            cbr0 = swc.get_branches(cids(1), pids);
            br0 = [{[sid; cbr0{1}]}; cbr0(2 : end)];
        else  % len(c0cids)=2
            br0 = [{[sid; cids(1)]}; swc.get_branches(cids(1), pids)];
        end
        c1cids = swc.get_children(cids(2), pids);
        if size(c1cids, 1) == 0
            br1 = {[sid; cids(2)]};
        elseif size(c1cids, 1) == 1
            cbr1 = swc.get_branches(cids(2), pids);
            br1 = [{[sid; cbr1{1}]}; cbr1(2 : end)];
        else  % len(c1cids)=2
            br1 = [{[sid; cids(2)]}; swc.get_branches(cids(2), pids)];
        end
        brs = [br0; br1];
    end
    
end
