function [sids, stypes, xs, ys, zs, ds, pids] = read_file(file_path, target_diam)
    % Read the SWC file at file_path and return its extracted information.
    %
    % Parameters:
    %   file_path : string :
    %       Path to .swc file.
    %
    %   target_diam : double :
    %       The diameter of the soma of the motoneuron which the user
    %       intends to build using the swc file at file_path.
    %
    % Return:
    %   sids : N*1 integer :
    %       Sample identifiers. A sample is a 3D point belonging to one
    %       section of the dendritic tree described by the swc file.
    % 
    %   stypes : N*1 integer :
    %       Sample type identifiers.
    %
    %   xs : N*1 double :
    %       Sample x-coordinates.
    %
    %   ys : N*1 double :
    %       Sample y-coordinates.
    %
    %   zs : N*1 double :
    %       Sample z-coordinates.
    %
    %   ds : N*1 double :
    %       Sample diameters.
    %
    %   pids : N*1 integer :
    %       Sample parent identifiers.
    %
    % Description:
    %   See http://www.neuronland.org/NLMorphologyConverter/...
    %                                   ...MorphologyFormats/SWC/Spec.html 
    %   for a description of the swc format.
    %   If target_diam is specified, target_diam refers to the diameter of
    %   the soma of a cell. Thus, all the 3D-values contained in the swc
    %   file at file_path are scaled such that the soma diameter originally
    %   given in the swc file equals the target diameter.
    
    if nargin == 1
        target_diam = false;
    end

    if exist(file_path, 'file')
        fileID = fopen(file_path);
    else
        error('%s: file not found.', file_path);
    end
    
    % Read file lines.
    C = textscan(fileID, '%s', 'Delimiter', '\n');
    C = C{1};
    fclose(fileID);

    % split lines around blank spaces
    lines = cellfun(@strsplit, C, 'UniformOutput', false);
    
    % remove first lines (which are comments)
    while strcmp(lines{1}{1}, '#')
        lines(1) = [];
    end
    
    % convert lines elements from strings to int/floats
    sids = cellfun(@(x) str2double(x{1}), lines);
    stypes = cellfun(@(x) str2double(x{2}), lines);
    xs = cellfun(@(x) str2double(x{3}), lines) * 1e-3;
    ys = cellfun(@(x) str2double(x{4}), lines) * 1e-3;
    zs = cellfun(@(x) str2double(x{5}), lines) * 1e-3;
    ds = cellfun(@(x) str2double(x{6}), lines) * 2 * 1e-3;
    pids = cellfun(@(x) str2double(x{7}), lines);

    % scale 3d info if target_diam provided.
    if target_diam
        f = target_diam / ds(1);
        xs = f * xs;
        ys = f * ys;
        zs = f * zs;
        ds = f * ds;
    end

end
