function read_morpho(this, folder)
    % Read the IaFiber morphology information from textfile.
    %
    % Usage:
    %   ia.read_morpho(folder)
    %
    % Parameters:
    %   folder : str : path to IaFiber folder.
    %
    % Description:
    %   The textfile from which to read is found in the input folder at
    %   'morpho.txt'. It is organized as follows:
    %       1. diam
    %       2. ncols
    %       3. Abranch.diam
    %       4. Abranch.nN
    %       5. Dbranch.diam
    %       6. Dbranch.nN
    %       7. DRbranch.diam
    %       8. DRbranch.nN
    %       9. Cols{1}.diam
    %      10. Cols{1}.nN
    %      11. Cols{1}.pAttach
    %      12. Cols{2}.diam
    %      13. Cols{2}.nN
    %      14. Cols{2}.pAttach
    %            .
    %            .
    %            .
    %       n. Cols{m}.diam
    %   (n+1). Cols{m}.nN
    %   (n+2). Cols{m}.pAttach
    
    % Read file.
    file_path = fullfile(folder, 'morpho.txt');
    fileID = fopen(file_path);
    morpho = textscan(fileID, '%s');
    fclose(fileID);

    % Assign appropriate attributes.
    this.diam = str2double(morpho{1}{1});
    this.cols = cell(str2double(morpho{1}{2}), 1);
    this.Abranch = Branch();
    this.Abranch.diam = str2double(morpho{1}{3});
    this.Abranch.nN = str2double(morpho{1}{4});
    this.Abranch.nP1 = 2 * (this.Abranch.nN - 1);
    this.Abranch.nP2 = 2 * (this.Abranch.nN - 1);
    this.Abranch.nI = 6 * (this.Abranch.nN - 1);
    this.Dbranch = Branch();
    this.Dbranch.diam = str2double(morpho{1}{5});
    this.Dbranch.nN = str2double(morpho{1}{6});
    this.Dbranch.nP1 = 2 * (this.Dbranch.nN - 1);
    this.Dbranch.nP2 = 2 * (this.Dbranch.nN - 1);
    this.Dbranch.nI = 6 * (this.Dbranch.nN - 1);
    this.DRbranch = Branch();
    this.DRbranch.diam = str2double(morpho{1}{7});
    this.DRbranch.nN = str2double(morpho{1}{8});
    this.DRbranch.nP1 = 2 * (this.DRbranch.nN - 1);
    this.DRbranch.nP2 = 2 * (this.DRbranch.nN - 1);
    this.DRbranch.nI = 6 * (this.DRbranch.nN - 1);
    for n = 1 : size(this.Cols, 1)
        this.Cols{n} = Branch();
        this.Cols{n}.diam = str2double(morpho{1}{9 + 3 * (n - 1)});
        this.Cols{n}.nN = str2double(morpho{1}{10 + 3 * (n - 1)});
        this.Cols{n}.nP1 = 2 * (this.Cols{n}.nN - 1);
        this.Cols{n}.nP2 = 2 * (this.Cols{n}.nN - 1);
        this.Cols{n}.nI = 6 * (this.Cols{n}.nN - 1);
        this.Cols{n}.p_attach = morpho{1}{11 + 3 * (n - 1)};
    end
    
end
