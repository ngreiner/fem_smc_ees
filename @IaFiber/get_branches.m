function branches = get_branches(this)
    % Return the cell-array of all the branches of the IaFiber.
    %
    % Usage: 
    %   branches = ia.get_branches()
    %
    % Outputs:
    %   branches : K*1 cell : the IaFiber's branches.
    %
    % Description:
    %   The branches are stored as follows in the output array:
    %       1. Abranch
    %       2. Dbranch
    %       3. DRbranch
    %       4. cols{1}
    %       5. cols{2}
    %       6. cols{3}
    %       7. ...
    branches = [{this.Abranch; this.Dbranch; this.DRbranch}; this.cols];
end
