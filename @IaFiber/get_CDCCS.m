function CS = get_CDCCS(this, smcd)
    % Get and return the CDCCS of the IaFiber.
    %
    % Usage:
    %   CS = ia.get_CDCCS(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Return:
    %   CS : K*1 cell : each cell is a N*3 double.
    %
    % Description:
    %   'CDCCS' = 'Caudal Dorsal Columns Cross-Sections'.
    %   The CDCCS are the cross-sectional contours of the dorsal columns
    %   compartment through which the descending branch of the IaFiber
    %   shall run.
    %   They are restricted to those caudal contours which delimit
    %   segments possessing MNs innervated by the IaFiber.
    %   They are retrieved from the spinal cord model attached to the
    %   SMCDModel passed to the method.
    
    % Retrieve minimal z-level reached by the IaFiber's innervated MNs.
    zmin = min(cellfun(@(x) x.soma.coords(3), smcd.entities.MNs(this.idx_MNs)));
    
    % Fetch appropriate dorsal columns cross-sectional contours.
    DC = smcd.fem.sc.DC;
    idx = find(strcmp(DC(:, 1), this.seg));
    CCS = cell(idx - 1, 1);
    for i = 1 : idx - 1
        CCS{i} = DC{idx - i, 2}{1, 2};
    end
    idx = sum(cellfun(@(x) x(1, 3) > zmin, CCS));
    CS = CCS(1 : idx + 1);
    
    % Flip x-coordinates if muscle is right-sided.
    if strcmp(this.musc(end - 1 : end), '_r')
        CS = Geom.flip_x_coords(CS);
    end
    
end
