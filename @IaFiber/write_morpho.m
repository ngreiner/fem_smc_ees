function write_morpho(this, dir_export, perm)
    % Write the IaFiber morphology information to textfile.
    %
    % Usage:
    %   iaf.write_morpho(dir_export)
    %   iaf.write_morpho(dir_export, perm)
    %
    % Parameters:
    %   dir_export : string :
    %       Path to top-level export directory.
    %
    %   perm : integer :
    %       Writing permission level.
    %
    % Description:
    %   A single textfile is written, the lines of which are organized as
    %   follows:
    %       1. this.diam
    %       2. size(this.cols, 1)
    %       3. this.Abranch.diam
    %       4. this.Abranch.nN
    %       5. this.Dbranch.diam
    %       6. this.Dbranch.nN
    %       7. this.DRbranch.diam
    %       8. this.DRbranch.nN
    %       9. this.cols{1}.diam
    %      10. this.cols{1}.nN
    %      11. this.cols{1}.pAttach
    %      12. this.cols{2}.diam
    %      13. this.cols{2}.nN
    %      14. this.cols{2}.pAttach
    %            .
    %            .
    %       n. this.cols{m}.diam
    %   (n+1). this.cols{m}.nN
    %   (n+2). this.cols{m}.pAttach
    %
    %   Permission to write (perm=1) is granted by default.

    % Parse inputs.
    if nargin == 2
        perm = 1;
    end
    
    folder = fullfile(dir_export, this.musc, this.seg, sprintf('%s%d', class(this), this.fidx));
    morpho_file_path = fullfile(folder, 'morpho.txt');

    if perm || ~ exist(morpho_file_path, 'file')

        % Write fiber general morphology info.
        morpho_text = sprintf('%f\n', this.diam);
        morpho_text = [morpho_text sprintf('%d\n', size(this.cols, 1))];

        % Write Abranch morphology info.
        morpho_text = [morpho_text sprintf('%f\n', this.Abranch.diam)];
        morpho_text = [morpho_text sprintf('%d\n', this.Abranch.nN)];

        % Write Dbranch morphology info.
        morpho_text = [morpho_text sprintf('%f\n', this.Dbranch.diam)];
        morpho_text = [morpho_text sprintf('%d\n', this.Dbranch.nN)];

        % Write DRbranch morphology info.
        morpho_text = [morpho_text sprintf('%f\n', this.DRbranch.diam)];
        morpho_text = [morpho_text sprintf('%d\n', this.DRbranch.nN)];

        % Write Collaterals morphology info.
        for n = 1 : size(this.cols, 1)
            morpho_text = [morpho_text sprintf('%f\n', this.cols{n}.diam)];
            morpho_text = [morpho_text sprintf('%d\n', this.cols{n}.nN)];
            morpho_text = [morpho_text sprintf('%s\n', this.cols{n}.p_attach)];
        end

        % Write info to file.
        fileID = fopen(morpho_file_path, 'w');
        fprintf(fileID, morpho_text);
        fclose(fileID);
        
        % Console message.
        fprintf('Writing ''morpho.txt'' at ''%s''.\n', folder)
        
    end
    
end
