function nMNs = get_nMNs(this)
    % Get and return the number of MNs innervated by the IaFiber.
    %
    % Usage:
    %   nMNs = ia.get_nMNs()
    %
    % Return:
    %   nMNs : integer : size of the field 'idx_MNs'.
    nMNs = size(this.idx_MNs, 1);    
end
