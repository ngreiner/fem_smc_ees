function CS = get_RDCCS(this, smcd)
    % Get and return the RDCCS of the IaFiber.
    %
    % Usage:
    %   CS = ia.get_RDCCS(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Return:
    %   CS : K*1 cell : each cell is a N*3 double.
    %
    % Description:
    %   'RDCCS' = 'Rostral Dorsal Columns Cross-Sections'.
    %   The RDCCS are the cross-sectional contours of the dorsal columns
    %   compartment through which the ascending branch of the IaFiber
    %   shall run.
    %   They are restricted to those rostral contours which delimit
    %   segments possessing MNs innervated by the IaFiber.
    %   They are retrieved from the spinal cord model attached to the
    %   SMCDModel passed to the method.

    % Retrieve maximal z-level reached by the IaFiber's innervated MNs.
    zmax = max(cellfun(@(x) x.soma.coords(3), smcd.entities.MNs(this.idx_MNs)));
    
    % Fetch appropriate dorsal columns cross-sectional contours.
    DC = smcd.fem.sc.DC;
    idx = find(strcmp(DC(:, 1), this.seg));
    CS = DC{idx, 2}(:, 2);
    for i = 3 : size(CS, 1)
        try
            CS{i} = DC{idx + i - 2, 2}{2, 2};
        catch
        end
    end
    CStmp = CS(cellfun(@(x) x(1, 3) < zmax, CS));
    CS = CS(1 : size(CStmp, 1) + 1);
    
    % Flip x-coordinates if muscle is right-sided.
    if strcmp(this.musc(end - 1 : end), '_r')
        CS = Geom.flip_x_coords(CS);
    end
    
end
