function from_struct(this, struc)
	% Set the IaFiber from the input struct.
    %
    % Usage:
    %   ia.from_struct(struc)
    %
    % Parameters: 
    %   struc : struct : fields are the same as the IaFiber class properties.
    from_struct@Fiber(this, struc);
    this.idx_MNs = struc.idx_MNs;
    this.MNfamilies = struc.MNfamilies;
    this.DRbranch = Branch();
    this.Abranch = Branch();
    this.Dbranch = Branch();
    this.cols = cellfun(@(x) Branch(), struc.cols, 'UniformOutput', false);
    this.DRbranch.from_struct(struc.DRbranch);
    this.Abranch.from_struct(struc.Abranch);
    this.Dbranch.from_struct(struc.Dbranch);
    cellfun(@(x,y) x.from_struct(y), this.cols, struc.cols, 'UniformOutput', false);
end
