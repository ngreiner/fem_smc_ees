function set_Abranch(this, smcd)
    % Set the ascending branch 'Abranch' of the IaFiber.
    %
    % Usage:
    %   ia.set_Abranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The ascending branch of the IaFiber runs in the dorsal columns,
    %   more precisely in the portion of the DC rostral to the spinal root
    %   in which the IaFiber is running.
    %   The contours materializing this compartment are contained in the
    %   cross-sections of the spinal cord model attached to the input
    %   SMCDModel.
    %   The trajectory of the ascending branch is simply a cubic spline
    %   running through these contours.
    %   Its diameter is obtained from that of the IaFiber (which coincides
    %   with the diameter of the dorsal root branch) according to the two
    %   following assumptions: 
    %       1. the ascending branch is twice as thick as the descending
    %          branch (Burke and Glenn, 1996)
    %       2. the combined cross-sectional areas of the ascending and
    %          descending branches equals that of the dorsal root branch
    %   The coordinates of the ascending branch's compartments are finally
    %   distributed along its trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting A branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.Abranch = Branch();
    this.Abranch.label = 'Abranch';
    this.Abranch.model = this.model;
    
    % The diameter of the DRbranch is the diameter of the Ia fiber.
    this.Abranch.diam = this.diam * sqrt(4 / 5);
    
    % Recover root's dorsal columns compartment rostral CS, and build path
    % of Abranch.
    CS = this.get_RDCCS(smcd);
    this.Abranch.path = Geom.build_unconstrained_path(CS, smcd.params.pdens, 'starting_point', this.DRbranch.path(1, :), 'refine_interp');
    
    % Derive branch's compartments' coordinates.
    this.Abranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.Abranch.VN = cell(smcd.get_nAS(), 1);
    this.Abranch.VP1 = cell(smcd.get_nAS(), 1);
    this.Abranch.VP2 = cell(smcd.get_nAS(), 1);
    this.Abranch.VI = cell(smcd.get_nAS(), 1);
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting A branch. (Time spent: %f s.)  ', cputime-t0), 'both', '-', 70))
    end

end
