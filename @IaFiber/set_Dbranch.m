function set_Dbranch(this, smcd)
    % Set the descending branch 'Dbranch' of the IaFiber.
    %
    % Usage:
    %   ia.set_Dbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The descending branch of the IaFiber runs in the dorsal columns,
    %   more precisely in the portion of the DC caudal to the spinal root
    %   in which the IaFiber is running.
    %   The contours materializing this compartment are contained in the
    %   cross-sections of the spinal cord model attached to the input
    %   SMCDModel.
    %   The trajectory of the descending branch is simply a cubic spline
    %   running through these contours.
    %   Its diameter is obtained from that of the IaFiber (which coincides
    %   with the diameter of the dorsal root branch) according to the two
    %   following assumptions: 
    %       1. the ascending branch is twice as thick as the descending
    %          branch (Burke and Glenn, 1996)
    %       2. the combined cross-sectional areas of the ascending and
    %          descending branches equals that of the dorsal root branch
    %   The coordinates of the descending branch's compartments are finally
    %   distributed along its trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting D branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.Dbranch = Branch();
    this.Dbranch.label = 'Dbranch';
    this.Dbranch.model = this.model;
    
    % The diameter of the Dbranch is half the diameter of the Abranch.
    this.Dbranch.diam = this.diam * sqrt(1 / 5);
    
    % Recover root's dorsal columns compartment rostral CS, and build path
    % of Abranch.
    CS = this.get_CDCCS(smcd);
    this.Dbranch.path = Geom.build_unconstrained_path(CS, smcd.params.pdens, 'starting_point', this.DRbranch.path(1, :), 'refine_interp');
    
    % Derive and build branch's compartments' coordinates.
    this.Dbranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.Dbranch.VN = cell(smcd.get_nAS(), 1);
    this.Dbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.Dbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.Dbranch.VI = cell(smcd.get_nAS(), 1);
    
    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting D branch. (Time spent: %f s.)  ', cputime-t0), 'both', '-', 70))
    end

end
