function [MNfamilies, ncols] = gen_MN_families(this, smcd, ncols)
    % Generate and return ncols families of innervated MN indexes.
    %
    % Usage:
    %   MNfamilies = fiber.genMNfamilies(smcd, ncols)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %   ncols : integer : number of collaterals/families of MNs.
    %
    % Return:
    %   MNfamilies : ncols*1 cell : each cell is a family of indexes.
    %
    % Description:
    %   The MNs are grouped based on their z-coordinates. Each group is
    %   made of MNs whose z-coordinates are greater than those of the next
    %   group.
    %   The cardinalities of the groups are obtained at random to target a
    %   coefficient of variation of 0.1 around the mean defined by nMNs /
    %   ncols.

    if smcd.params.verbose
        fprintf('BEGIN: genMNfamilies ... ')
        t0 = cputime;
    end
    
    % Unpack attributes in convenient variables.
    nMNs = this.get_nMNs();

    % Targeted coefficient of variation for the families' cardinalities.
    cv = 0.1;

    % Determine cardinalities of the families.
    while true
        w = Geom.gen_weights_unit_simplex(ncols) - 1 / ncols;
        l = sqrt(nMNs * cv / dot(w, w));
        cards = round(nMNs / ncols + l * w);
        cards(end) = nMNs - sum(cards(1 : end - 1));
        if all(cards > 0)
            break
        else
            ncols = ncols - 1;
        end
    end

    % Sort MNs in decreasing order of their z-coordinates.
    MNs = smcd.entities.MNs(this.idx_MNs);
    [~, order] = sort(cellfun(@(x) x.soma.coords(3), MNs), 'descend');
    idx_MNs = this.idx_MNs(order);

    % Form families.
    MNfamilies = cell(ncols, 1);
    cnt = 1;
    for i = 1 : ncols
        MNfamilies{i} = idx_MNs(cnt : cnt + cards(i) - 1);
        cnt = cnt + cards(i);
    end
    this.MNfamilies = MNfamilies;

    if smcd.params.verbose
        fprintf('END. (Time spent: %f s.)\n', cputime - t0)
    end

end
