function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if ~ eq@Fiber(obj1, obj2)
        return
    end
            
    if ~ (obj1.DRbranch == obj2.DRbranch)
        fprintf('Attributes `DRbranch` are different.\n')
        return
    end
    
    if ~ (obj1.Abranch == obj2.Abranch)
        fprintf('Attributes `Abranch` are different.\n')
        return
    end
    
    if ~ (obj1.Dbranch == obj2.Dbranch)
        fprintf('Attributes `Dbranch` are different.\n')
        return
    end
    
    if ~ all(cellfun(@(x, y) x == y, obj1.cols, obj2.cols))
        fprintf('Attributes `Cols` are different.\n')
        return
    end
    
    outbool = true;
    
end