classdef IaFiber < Fiber

    % Properties defined here:
    %
    %   idx_MNs : N*1 double :
    %       Indexes of the MNs innervated by the IaFiber in the MN table.
    %
    %   MNfamilies : ncols*1 cell :
    %       Families of MNs innervated by the IaFiber's ncols collaterals.
    %
    %   DRbranch : Branch object :
    %       Dorsal roots branch of the IaFiber.
    %
    %   Abranch : Branch object :
    %       Ascending branch of the IaFiber.
    %
    %   Dbranch : Branch object :
    %       Descending branch of the IaFiber.
    %
    %   cols : ncols*1 cell :
    %       Collaterals (Branch objects) of the IaFiber.
    
    properties
        idx_MNs      % N*1 double
        MNfamilies   % K*1 cell
        DRbranch     % Branch object
        Abranch      % Branch object
        Dbranch      % Branch object
        cols         % N*1 cell
    end
    
    methods
        function this = IaFiber()
        end
    end
    
    methods
        assign_MNs(this, smcd)
        [P, loc] = find_node(this, z)
        from_struct(this, struc)
        [MNfamilies, ncols] = gen_MN_families(this, smcd, ncols)
        ncols = gen_ncols(this, varargin)
        branches = get_branches(this)
        CS = get_CDCCS(this, smcd)
        CS = get_DRCS(this, smcd)
        nMNs = get_nMNs(this)
        CS = get_RDCCS(this, smcd)
        read_connec(this, IaFolder)
        read_morpho(this, IaFolder)
        set_Abranch(this, smcd)
        set_cols(this, smcd)
        set_Dbranch(this, smcd)
        set_DRbranch(this, smcd)
        struc = to_struct(this)
        writeIaGeomNodePath(this, folder)
        write_morpho(this, dir_export, perm)
    end
    
    methods
        outbool = eq(obj1, obj2)
    end
    
end
