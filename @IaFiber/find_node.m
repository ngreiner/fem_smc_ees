function [P, loc] = find_node(this, z)
    % Return the coordinates of the node of the rostro-caudal branch which
    % has closest z-coordinate to the input z.
    %
    % Usage:
    %   [P, loc] = this.find_node(z)
    %
    % Parameters:
    %   z : double : target z-coordinate.
    %
    % Return:
    %   P : 1*3 double :
    %       Desired node's coordinates.
    %
    %   loc : string :
    %       Indicator of node identity. The first character is either
    %       'A' for ascending branch or 'D' for descending branch.The
    %       subsequent characters indicate the node index on the ascending
    %       or descending branch. Nodes are indexed starting from 0.
    %
    % Description:
    %   Looks for the node of the ascending or descending branch that is
    %   just below the input z-coordinate (more precisely, for a node that
    %   would be 100um to 300um below the input z-coordinate).
    %   The first node of the ascending branch (the rostralmost) is
    %   discarded from the selection: in practice, no z-coordinate above
    %   this node will be passed to this method.
    %   The method returns the coordinates of the selected node and a
    %   string identifying its position on either the ascending either the
    %   descending branch.
    
    dz = 0.1 + rand() * 0.2;
    
    % Abranch.
    Ncoords = this.Abranch.Ncoords(2 : end - 1, :);
    zs = Ncoords(:, 3) - (z - dz);
    [minA, idxA] = min(abs(zs));
    pA = Ncoords(idxA, :);
    
    % Dbranch.
    Ncoords = this.Dbranch.Ncoords(2 : end, :);
    zs = Ncoords(:, 3) - (z - dz);
    [minD, idxD] = min(abs(zs));
    pD = Ncoords(idxD, :);
    
    % End.
    if isempty(minD)
        P = pA;
        loc = sprintf('A%d', idxA);
    elseif isempty(minA)
        P = pD;
        loc = sprintf('D%d', idxD);
    elseif minA < minD
        P = pA;
        loc = sprintf('A%d', idxA);
    else
        P = pD;
        loc = sprintf('D%d', idxD);
    end
    
end
