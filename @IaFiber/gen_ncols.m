function ncols = gen_ncols(this, varargin)
    % Generate the number of collaterals of the IaFiber.
    %
    % Usage:
    %   ncols = ia.gen_ncols()
    %
    % Return:
    %   ncols : integer : number of collaterals of the IaFiber.
    %
    % Description:
    %   The number of collaterals is generated from a statistical
    %   model initially elaborated for group-Ia fibers from the results of
    %   (Brown and Fyffe, 1978).
    %
    %   This model uses the mathematical formulation:
    %       ncols = round(lambda * L) +- eta
    %   where lambda is the lineic density of collaterals along the branch
    %   (number of collaterals per mm) and eta is a Poisson-distributed
    %   deviation from the ideal case, which can be either positive or
    %   negative.
    
    L = this.Abranch.path(end, 3) - this.Dbranch.path(end, 3);
    eta = random('poiss', 0.7);
    sgn = 2 * (randi(2, 1) - 1) - 1;
    ncols = round(0.75 * L) + eta * sgn;

end
