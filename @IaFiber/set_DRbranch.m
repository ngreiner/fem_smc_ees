function set_DRbranch(this, smcd)
    % Set the dorsal root branch 'DRbranch' of the IaFiber.
    %
    % Usage:
    %   ia.set_Dbranch(smcd)
    %
    % Parameters:
    %   smcd : SMCDModel object.
    %
    % Description:
    %   The dorsal root branch of the IaFiber runs in the dorsal root
    %   hosting the IaFiber.
    %   The contours materializing the dorsal root are contained in the
    %   spinal cord model attached to the input SMCDModel.
    %   The trajectory of the dorsal root branch is simply a cubic spline
    %   running through these contours.
    %   Its diameter is that of the IaFiber.
    %   Its compartments' coordinates are finally distributed along its
    %   trajectory.

    if smcd.params.verbose
        fprintf(Utils.padn('  Setting DR branch  ', 'both', '-', 70))
        t0 = cputime;
    end

    % Initialize branch.
    this.DRbranch = Branch();
    this.DRbranch.label = 'DRbranch';
    this.DRbranch.model = this.model;

    % The diameter of the DRbranch is the diameter of the Ia fiber.
    this.DRbranch.diam = this.diam;
    
    % Recover root's CS, and build path of DRbranch.
    CS = this.get_DRCS(smcd);
    this.DRbranch.path = Geom.build_constrained_path(CS, smcd.params.pdens);
    
    % Derive and build branch's compartments' coordinates.
    this.DRbranch.build_comps_coords(0);
    
    % Initialize cell-arrays for branch's compartments potentials.
    this.DRbranch.VN = cell(smcd.get_nAS(), 1);
    this.DRbranch.VP1 = cell(smcd.get_nAS(), 1);
    this.DRbranch.VP2 = cell(smcd.get_nAS(), 1);
    this.DRbranch.VI = cell(smcd.get_nAS(), 1);

    if smcd.params.verbose
        fprintf(Utils.padnn(sprintf('  Finished setting DR branch. (Time spent: %f s.)  ', cputime - t0), 'both', '-', 70))
    end

end
