function writeIaGeomNodePath(this, folder)
    % This method writes the geometry information of the Ia in the input
    % folder.
    %
    % Inputs: - folder: str: path to folder.
    %
    % Outputs: None.
    %
    % Actions: writes the textfiles describing the geometry of the Ia in
    % the input folder.
    %
    % Description: void.
    
    if ~exist(folder, 'dir')
        mkdir(folder)
    end

    % Abranch.
    % Write nodes' coordinates.
    geomText = sprintf('%f %f %f\n', (this.Abranch.NCoords)');
    filePath = fullfile(folder, 'Abranch_NCoords.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);
    % Write path's coordinates.
    geomText = sprintf('%f %f %f\n', (this.Abranch.path)');
    filePath = fullfile(folder, 'Abranch_path.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);

    % Dbranch.
    % Write nodes' coordinates.
    geomText = sprintf('%f %f %f\n', (this.Dbranch.NCoords)');
    filePath = fullfile(folder, 'Dbranch_NCoords.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);
    % Write path's coordinates.
    geomText = sprintf('%f %f %f\n', (this.Dbranch.path)');
    filePath = fullfile(folder, 'Dbranch_path.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);

    % DRbranch.
    % Write nodes' coordinates.
    geomText = sprintf('%f %f %f\n', (this.DRbranch.NCoords)');
    filePath = fullfile(folder, 'DRbranch_NCoords.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);
    % Write path's coordinates.
    geomText = sprintf('%f %f %f\n', (this.DRbranch.path)');
    filePath = fullfile(folder, 'DRbranch_path.dat');
    fileID = fopen(filePath,'w');
    fprintf(fileID, geomText);
    fclose(fileID);

    % Cols.
    for i = 1:size(this.Cols,1)
        % Write nodes' coordinates.
        geomText = sprintf('%f %f %f\n', (this.Cols{i}.NCoords)');
        filePath = fullfile(folder, sprintf('Col%d_NCoords.dat', i));
        fileID = fopen(filePath,'w');
        fprintf(fileID, geomText);
        fclose(fileID);
        % Write path's coordinates.
        geomText = sprintf('%f %f %f\n', (this.Cols{i}.path)');
        filePath = fullfile(folder, sprintf('Col%d_path.dat', i));
        fileID = fopen(filePath,'w');
        fprintf(fileID, geomText);
        fclose(fileID);
    end
    
end