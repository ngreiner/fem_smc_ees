function struc = to_struct(this)
	% Return a matlab struct equivalent to the IaFiber.
    %
    % Usage:
    %   struc = ia.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the IaFiber.
    struc = to_struct@Fiber(this);
    struc.idx_MNs = this.idx_MNs;
    struc.MNfamilies = this.MNfamilies;
    struc.DRbranch = this.DRbranch.to_struct();
    struc.Abranch = this.Abranch.to_struct();
    struc.Dbranch = this.Dbranch.to_struct();
    struc.cols = cellfun(@(x) x.to_struct(), this.cols, 'UniformOutput', false);
end
