function set_AS_coords(this)
	% Set the attribute AS.coords.
    %
    % Usage:
    %   elec.set_AS_coords()
    %
    % Description:
    %   AS.coords is a cell-array, each cell of which describes one active
    %   site.
    %   The numbering is done starting with the leftmost active site of the
    %   lowermost row, left to right first, bottom to top next.
	%   One active site is described by the coordinates of its center, and
    %   the rotation angle to apply to the block representing it so that it
    %   faces the spinal cord.
    %   Therefore, it is represented by a cell-array {x, y, z, phi}.
    % 
    %   This method assumes that the attributes sc, AS.rows and AS.nrows
    %   are already properly defined.

	% Unpack attributes in convenient variables.
	rows = this.AS.rows;
	nrows = this.AS.nrows;
	sc = this.sc;

    % Determine number of active sites.
    nAS = 0;
    for i = 1 : nrows
        nAS = nAS + sum(rows{i}.mask);
    end

    % Initialize active site array.
    coords = cell(nAS, 1);


    % Define active sites.
    % If the paddle is not strictly enclosed in the fat.
    if ~ strcmp(this.paddle.flag, 'in_fat')

        % Radius of active sites rows.
        d = cell(nrows, 1);
        for i = 1 : nrows
            ymin_dura = sc.get_ymin_dura(rows{i}.z);
            d{i} = abs(ymin_dura);
        end
        
        % Within-rows inter-active sites angular deviations.
        psi = cell(nrows, 1);
        for i = 1 : nrows
            psi{i} = rows{i}.hspacing / d{i};
        end

        % Angular direction of each active site.
        phi = cell(nrows, 1);
        for i = 1 : nrows
            phi{i} = cell(sum(rows{i}.mask), 1);
            cnt_row = 1;
            for j = 1 : rows{i}.n
                if rows{i}.mask(j)
                    phi{i}{cnt_row} = - pi / 2 - psi{i} * (rows{i}.n - 1) / 2 + psi{i} * (j - 1);
                    cnt_row = cnt_row + 1;
                end
            end
        end
        
        % Active site parameters.
        cnt_AS = 1;
        for i = 1 : nrows
            cnt_row = 1;
            [w_up, w_down] = sc.get_weights(rows{i}.z);
            a = this.paddle.CS_coeffs{i + 1}{1};
            nASi = rows{i}.n;
            hsi = rows{i}.hspacing;
            for j = 1 : rows{i}.n
                if rows{i}.mask(j)
                    p_up = Geom.get_point_from_angle(sc.get_contour('dura', rows{i}.z, 'above'), phi{i}{cnt_row});
                    p_down = Geom.get_point_from_angle(sc.get_contour('dura', rows{i}.z, 'below'), phi{i}{cnt_row});
                    p = p_up * w_up + p_down * w_down;
                    d0 = norm(p(1 : 2));
                    % Derive rotation angle for AS.
                    lij = (2 * j - 1 - nASi) / 2 * hsi;
                    eqroot = fzero(@(x) x * sqrt(1 + x^2) + asinh(x) - 2 * a * 2 * lij, 1);
                    xij = eqroot / (2 * a);
                    phiij = acos(1 / sqrt(1 + (2 * a * xij) ^ 2)) * 180 / pi * sign(xij);
                    coords{cnt_AS} = {d0 * cos(phi{i}{cnt_row}); d0 * sin(phi{i}{cnt_row}); rows{i}.z; phiij};
                    cnt_row = cnt_row + 1;
                    cnt_AS = cnt_AS + 1;
                end
            end
        end
        
        
    % Otherwise, paddle.flag is 'in_fat', and we derive AS parameters from
    % paddle parameters.
    else
        cnt = 1;
        for i = 1 : nrows
            a = this.paddle.CS_coeffs{i + 1}{1};
            b = this.paddle.CS_coeffs{i + 1}{2};
            nASi = rows{i}.n;
            hsi = rows{i}.hspacing;
            zi = rows{i}.z;
            mask = rows{i}.mask;
            for j = 1 : nASi
                if mask(j)
                    xij = (2 * j - 1 - nASi) / 2 * hsi;
                    yij = a * xij ^ 2 + b;
                    phiij = acos(1 / sqrt(1 + (2 * a * xij) ^ 2)) * 180 / pi * sign(xij);
                    coords{cnt} = {xij; yij; zi; phiij};
                    cnt = cnt + 1;
                end
            end
        end
        
    end
    
	% Assign attributes.
	this.AS.coords = coords;
    this.AS.count = nAS;
    
end
