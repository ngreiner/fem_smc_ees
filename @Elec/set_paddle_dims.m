function set_paddle_dims(this)
    % Set the attributes paddle.length, paddle.width and paddle.thick.
    %
    % Usage: 
    %   elec.set_paddle_dims()
    %
    % Description:
    %   The paddle.dx attributes describe the margins bordering the extreme
    %   active sites in the paddle and are specified by a configuration
    %   file. 
    %   The sketch below illustrates their meaning:
    %
    %
    %                Front view                       Lateral view
    %             _________________                     _______
    %             |   ^           |                     |      |
    %             |   | dl        |                     |      |
    %             |   v           |                     |    __|
    %             |   _     _  dw |                     | dt | |
    %             |  | |   | |<-->|                     |<-->| |
    %             |  |_|   |_|    |                     |    |_|
    %             |               |                     |      |
    %             |               |                     |      |
    %             |               |                     |      |
    %             |       .       |                     |   .  |
    %             |       .       |                     |   .  |
    %                     .                                 .
    %
    %   The dimensions of the paddle are subsequently derived from them
    %   and from the specifications of the AS rows.
        
    % Length.
    this.paddle.length = (this.AS.rows{end}.z - this.AS.rows{1}.z) + this.AS.length + 2 * this.paddle.dl;
    
    % Width.
    this.paddle.width = max(cellfun( @(x) ((x.n - 1) * x.hspacing), this.AS.rows)) + this.AS.width + 2 * this.paddle.dw;
    
    % Thickness.
    this.paddle.thick = this.AS.thick + this.paddle.dt;
        
end
