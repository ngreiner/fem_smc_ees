function from_struct(this, struc)
    % Set the Elec object from the input struct.
    %
    % Usage:
    %   elec.from_struct(struc)
    %
    % Parameters: 
    %   struc : struct : fields are the same as the Elec class properties.
    this.AS = struc.AS;
    this.paddle = struc.paddle;
end
