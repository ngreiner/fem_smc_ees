function set_paddle_coords(this)
    % Set the coordinates attributes of the paddle.
    %
    % Usage: 
    %   elec.set_paddle_coords()
    %
    % Description:
    %   This method sets the attributes paddle.CS, paddle.CS_coeffs and
    %   paddle.Gen.
    %   
    %   paddle.CS is a cell-array, each cell of which is a cross-section
    %   contour of the paddle.
    %   These are obtained at several z-coordinates: 1 per row of active
    %   sites plus 2 for the uppermost and lowermost cross-sections of the
    %   paddle.
    %   At each z-level, the cross-section contour is obtained as two
    %   portions of parabola connected pair-wisely by their edges, one of
    %   which is a scaled version of the other such that an uniform
    %   gap=paddle.thick separates the two.
    %   These portions of parabola are symmetrical with respect to the
    %   yz-plane, representing a symetrical left-right placement of the
    %   paddle with respect to the spinal cord.
    %   Their coordinates are obtained by considering the dorsal contour
    %   of the dura locally at the appropriate z-level. See code below.
    %
    %   paddle.CS_coeffs store the coefficients describing the previous
    %   parabola.
    %
    %   paddle.Gen are the generatrices connecting the paddle.CS. Here the
    %   points of the CS which are connected together are the coincident
    %   corners of the CS.
    
    % Unpack attributes in convenient variables.
    sc = this.sc;
    rows = this.AS.rows;
    nrows = this.AS.nrows;
    dl = this.paddle.dl;
    width = this.paddle.width;
    dt = this.paddle.dt;
    
    % Determine z-coordinates of paddle contours. They are the
    % z-coordinates of the rows of active sites plus two z-coordinates, one
    % below the lowermost row, and one above the uppermost row.
    z0 = rows{1}.z - this.AS.length / 2 - dl;
    zf = rows{end}.z + this.AS.length / 2 + dl;
    zrows = cellfun(@(x) x.z, rows);
    zs = [z0 ; zrows; zf];
    
    % Number of points composing the contours.
    np = floor(width * sc.CS{1}.mean_pdens);
    
    % Number of interpolation points for parabola.
    ninterp = 11;

    % Initialize array of cross-sections and array of generatrices.
    CS = cell(nrows + 2, 1);
    CS_coeffs = cell(nrows + 2, 1);
    Gen = cell(4, 1);
    for i = 1 : 4
        Gen{i} = zeros(nrows + 2, 3);
    end
    
    % Compute contours.
    for i = 1 : nrows + 2
        
        % Derive distance of electrode array to z-axis at height zs(i).
        ymin_dura = sc.get_ymin_dura(zs(i));
        d_elec = abs(ymin_dura);
        
        % Derive angles of interpolation points from paddle width and
        % previous distance as angle = arc_length / radius.
        dtheta = width / d_elec;
        thetas = linspace(- pi / 2 - dtheta / 2, - pi / 2 + dtheta / 2, ninterp);
        
        % Recover Dura and Fat contours just above and below zs(i) to
        % define a series of points lying in between the two (using
        % attribute pos_ratio) at the previous angles.
        dura_up = sc.get_contour('dura', zs(i), 'above');
        points_up = zeros(ninterp, 3);
        for j = 1 : ninterp
            points_up(j, :) = Geom.get_point_from_angle(dura_up, thetas(j));
        end
        dura_down = sc.get_contour('dura', zs(i), 'below');
        points_down = zeros(ninterp, 3);
        for j = 1 : ninterp
            points_down(j, :) = Geom.get_point_from_angle(dura_down, thetas(j));
        end
        [w_up, w_down] = sc.get_weights(zs(i));
        points = w_up * points_up + w_down * points_down;
        
        % Regress previous points into a parabola y=ax^2+b.
        x = points(:, 1);
        y = points(:, 2);
        u = ones(ninterp, 1);
        A = [dot(x.^2, x.^2) dot(x.^2, u); dot(x.^2, u) dot(u, u)];
        Y = [dot(y, x.^2); dot(y, u)];
        X = A \ Y;
        CS_coeffs{i}{1} = X(1);
        CS_coeffs{i}{2} = X(2);
        
        % Find abscissa x0 such that the arc length of the parabola portion
        % obtained for x in [-x0,x0] be the paddle width. We find the zero
        % of the function f(x)-paddle.width where f(x) = int for u=-x to
        % u=x of sqrt(1+P'(u)^2) du, where P is the previous parabola.
        % The formula inside the fzero function below is the primitive of
        % the previous integral.
        eqroot = fzero(@(x) x * sqrt(1 + x^2) + asinh(x) - 2 * X(1) * width, 1);
        x0 = eqroot / (2 * X(1));
        
        % Define inner curve of paddle contour. 
        Xs = linspace(- x0, x0, ninterp)';
        Ys = X(1) * Xs.^2 + X(2);
        Zs = zs(i) * ones(ninterp, 1);
        curve0 = Geom.interparc(np, Xs, Ys, Zs, 'spline');
        inner_curve = curve0;
        for j = 1 : np
            inner_curve(j, 1 : 2) = curve0(j, 1 : 2) * (1 - dt / norm(curve0(j, 1 : 2)));
        end
        
        % Define outer curve of paddle contour by simple scaling of the
        % inner curve.
        outer_curve = curve0;
        for j = 1 : np
            outer_curve(j, 1 : 2) = outer_curve(j, 1 : 2) * (1 + dt / norm(outer_curve(j, 1 : 2)));
        end
        
        % Paddle cross-section at zs(i).
        contour = [inner_curve; flipud(outer_curve); inner_curve(1, :)];        
        CS{i} = contour;
        
        % Paddle generatrix points at zs(i).
        Gen{1}(i, :) = inner_curve(1, :);
        Gen{2}(i, :) = inner_curve(end, :);
        Gen{3}(i, :) = outer_curve(end, :);
        Gen{4}(i, :) = outer_curve(1, :);
        
%         % Control Plots.
%         figure
%         hold on
%         title('Contour Down')
%         plot(Dura_down(:, 1), Dura_down(:, 2), 'Color', 'black')
%         plot(contour(:, 1), contour(:, 2), 'Color', 'green')
%         figure
%         hold on
%         plot(Dura_up(:, 1), Dura_up(:, 2), 'Color', 'black')
%         plot(contour(:, 1), contour(:, 2), 'Color', 'red')
%         
%         % Control Paddle width.
%         paddle.width
%         real_width = sum(sqrt(sum(diff(inner_curve).^2, 2)), 1)
        
    end
    
    % Assign attributes.
    this.paddle.CS = CS;
    this.paddle.CS_coeffs = CS_coeffs;
    this.paddle.Gen = Gen;
    
end
