function build(this)
	% Build the Elec object.
    %
    % Usage:
    %   elec.build()
    %
    % Description:
    %   This method should be called after a call to parse_config_file with
    %   an appropriate configuration file.
    this.set_paddle_coords();
    this.set_AS_coords();
end
