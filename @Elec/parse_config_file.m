function parse_config_file(this, file_path)
    % Parse the configuration file at file_path.
    %
    % Usage:
    %   elec.parse_config_file(file_path)
    %
    % Parameters:
    %   file_path : string : path to configuration file.
    %
    % Description:
    %   A configuration file for an Elec object specifies:
    %       1. the relative position of the active sites centers between
    %          the epidural fat and the dura, as described by a spinal cord
    %          model 
    %       2. the dimensions of the active sites
    %       3. the number of rows of active sites
    %       4. the specifications of each row, that is: the number of
    %          active site loci in the row, the mask specifying which of
    %          these loci should be effectively occupied by an active site,
    %          the center-to-center horizontal spacing between two
    %          neighbouring loci, and the elevation (z-coordinate) of the
    %          row 
    %       5. the paddle specifications, that is: its relative position in
    %          the fat/dura, and the extent of its borders around the
    %          active sites
    %   This and the set_AS_rows methods extract this information.
    
    % Open file.
   if exist(file_path, 'file')
        fileID = fopen(file_path);
    else
        error('%s: file not found.', file_path);
    end
    
    % Read file content as single strings separated by spaces.
    C = textscan(fileID, '%s ');
    C = C{1};
    
    % Close file.
    fclose(fileID);
    
    % Parse config.
    for i = 1 : size(C, 1)
        
        switch C{i}
            
            case 'pos_ratio'
                this.AS.pos_ratio = str2double(C{i + 2});
            
            case 'AS_length'
                this.AS.length = str2double(C{i + 2});
                
            case 'AS_width'
                this.AS.width = str2double(C{i + 2});
                
            case 'AS_thickness'
                this.AS.thick = str2double(C{i + 2});
                
            case 'nrows'
                this.AS.nrows = str2double(C{i + 2});
                
            case 'paddle_flag'
                this.paddle.flag = C{i + 2};
                
            case 'paddle_dl'
                this.paddle.dl = str2double(C{i + 2});
                
            case 'paddle_dw'
                this.paddle.dw = str2double(C{i + 2});
                
            case 'paddle_dt'
                this.paddle.dt = str2double(C{i + 2});

        end
        
    end
    
    this.set_AS_rows(C);
    this.set_paddle_dims();

end
