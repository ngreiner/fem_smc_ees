classdef Elec < handle
    
    % Properties defined here:
    %
    %   sc : SCModel object :
    %       Describing the spinal cord onto which the electrode array is
    %       applied.
    %
    %   AS : struct :
    %       Describing the active sites of the electrode array. Fields are:
    %       pos_ratio, length, width, thick, nrows, rows, coords, count.
    %
    %   paddle : struct :
    %       Describing the paddle of the electrode array. Fields are:
    %       flag, dl, dw, dt, length, width, thick, CS, CS_coeffs, Gen.

	properties
		sc      % SCModel object
        AS      % struct
        paddle  % struct
	end

	methods
        
		function this = Elec(sc, config_file_name)
            % Class constructor method.
            %
            % Usage:
            %   elec = Elec()
            %   elec = Elec(sc, config_file_name)
            %
            % Parameters:
            %   sc : SCModel object : a spinal cord model.
            %   config_file_name : str : name of Elec configuration file.
            
            if nargin == 0
				return
            
            else
				this.sc = sc;
                file_path = fullfile(sc.dirs.resources, 'elec_config_files', config_file_name);
                this.parse_config_file(file_path);
                this.build();
            
            end
            
        end
        
	end

	methods
        build(this)
        outbool = eq(obj1, obj2)
        from_struct(this, struc)
        parse_config_file(this, file_path)
		set_AS_coords(this)
		set_AS_rows(this, C)
        set_paddle_coords(this)
        set_paddle_dims(this)
		struc = to_struct(this)
        export_paddle_coords(this)
    end
    
end
