function set_AS_rows(this, C)
	% Set the attribute AS.rows.
    %
    % Usage:
    %   elec.set_AS_rows(C)
    %
    % Parameters:
    %   C : cell-array of string : split text from a configuration file.
    %
    % Description:
    %   Each cell of AS.rows describes one row of active sites of the
    %   electrode array. One row is described by the number of active
    %   sites (n) it contains, the center-to-center horizontal spacing
    %   between two neighbours of them, the elevation (z-coordinate) of
    %   their centers, and a mask. 
	%   The mask is a vector of size n made of 0s and 1s. mask(i)=1
	%   indicates that the active site i should effectively be built, and
	%   conversly mask(i)=0 indicates that it shouldn't.

    rows = cell(this.AS.nrows, 1);
    
    % 1st row.
    idx = find(strcmp('n1', C));
    rows{1}.n = str2double(C{idx + 2});
    
    idx = find(strcmp('hspacing1', C));
    rows{1}.hspacing = str2double(C{idx + 2});
    
    idx = find(strcmp('z1', C));
    rows{1}.z = str2double(C{idx + 2});
    
    idx = find(strcmp('mask1', C));
    rows{1}.mask = zeros(rows{1}.n, 1);
    for i = 1 : rows{1}.n
        rows{1}.mask(i) = str2double(C{idx + 1 + i});
    end
    
    % Subsequent rows.
    for i = 2 : this.AS.nrows
        idx = find(strcmp(sprintf('n%d', i), C));
        rows{i}.n = str2double(C{idx + 2});
        
        idx = find(strcmp(sprintf('hspacing%d', i), C));
        rows{i}.hspacing = str2double(C{idx + 2});
        
        idx = find(strcmp(sprintf('vspacing%d%d', i - 1, i), C));
        rows{i}.z = rows{i - 1}.z + str2double(C{idx + 2});
        
        idx = find(strcmp(sprintf('mask%d', i), C));
        rows{i}.mask = zeros(rows{i}.n, 1);
        for j = 1 : rows{i}.n
            rows{i}.mask(j) = str2double(C{idx + 1 + j});
        end
    end
    
	% Set attribute ASrows.
	this.AS.rows = rows;
    
end
