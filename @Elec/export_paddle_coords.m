function export_paddle_coords(this)
    % Writes the paddle contour coordinates to textfiles.
    %
    % Usage: 
    %   elec.export_paddle_coords()
    %
    % Description:
    %   Builds the directory where to write the textfiles (erasing any
    %   previous version) and writes the textfiles.
    %   There is one textfile per cross-section of the paddle, and one
    %   textfile per generatrix.
    
    % Make 'Paddle' directory if needed.
    pdir = fullfile(this.sc.dirs.coords, 'paddle');
    if exist(pdir, 'dir')
        rmdir(pdir, 's');
    end
    mkdir(pdir);
    
    % Write paddle cross-sections textfiles.
    for i = 1 : this.AS.nrows + 2
        CS = this.paddle.CS{i};
        fileID = fopen(fullfile(pdir, sprintf('paddle_CS_%d.txt', i)), 'w');
        fprintf(fileID, '%f %f %f\n', CS');
        fclose(fileID);
    end
    
    % Write paddle generatrix textfiles.
    for i = 1 : 4
        Gen = this.paddle.Gen{i};
        fileID = fopen(fullfile(pdir, sprintf('paddle_Gen_%d.txt', i)), 'w');
        fprintf(fileID, '%f %f %f\n', Gen');
        fclose(fileID);
    end
    
end
