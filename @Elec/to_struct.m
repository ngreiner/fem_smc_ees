function struc = to_struct(this)
	% Return a matlab struct equivalent to the Elec object.
    %
    % Usage:
    %   struc = elec.to_struct()
    %
    % Return:
    %   struc : struct : with same fields as the Elec object.
    struc.AS = this.AS;
    struc.paddle = this.paddle;
end
