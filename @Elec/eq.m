function outbool = eq(obj1, obj2)
    % Define the output of the statement "obj1 == obj2".
    
    outbool = false;
    
    if ~ isequal(obj1.AS, obj2.AS)
        fprintf('Attributes `AS` are different.\n')
        return
    end
    
    if ~ isequal(obj1.paddle, obj2.paddle)
        fprintf('Attributes `paddle` are different.\n')
        return
    end
    
    outbool = true;
    
end
