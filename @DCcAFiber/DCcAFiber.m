classdef DCcAFiber < DCFiber

	% This class is a perfect copy of the DCFiber class.
	% They only differ by their name, which influences the reading
	% and writing of files in the file system.
    
    methods
        function this = DCcAFiber()
        end
    end
    
end